package grand.app.moon.viewmodels.trip;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.triphistory.TripHistoryResponse;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;


public class ItemTripHistoryViewModel extends ParentViewModel {
    public TripHistoryResponse.Datum trips = null;
    public String image;
    public int position = 0;
    public String price;

    public ItemTripHistoryViewModel(TripHistoryResponse.Datum trips, int position) {
        this.trips = trips;
        this.position = position;
        this.price = trips.price+" " +UserHelper.retrieveCurrency();
        notifyChange();
    }

    @Bindable
    public Float getRating(){
        return trips.rate;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public static void setRating(RatingBar rating , Float rate){
        rating.setRating(rate);
    }
}
