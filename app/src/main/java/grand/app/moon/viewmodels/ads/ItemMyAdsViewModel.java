package grand.app.moon.viewmodels.ads;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.ads.MyAdsModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemMyAdsViewModel extends ParentViewModel {
    public MyAdsModel myAdsModel ;
    public int position = 0;
    public int width = 0;
    private String priceTotal;

    public ItemMyAdsViewModel(MyAdsModel myAdsModel, int position) {
        this.myAdsModel = myAdsModel;
        this.priceTotal = myAdsModel.price + UserHelper.retrieveCurrency();
        this.position = position;
        notifyChange();
    }

    public String getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(String priceTotal) {
        this.priceTotal = priceTotal;
    }

    public void imageSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }

    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void editSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.EDIT,position));
    }

    public String getImageUrl(){
        return myAdsModel.image;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
