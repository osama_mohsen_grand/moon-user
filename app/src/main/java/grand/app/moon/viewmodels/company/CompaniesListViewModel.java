package grand.app.moon.viewmodels.company;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.adapter.CompanyAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.moon.MoonHelper;
import grand.app.moon.repository.CompanyRepository;
import grand.app.moon.utils.Constants;
import timber.log.Timber;

public class CompaniesListViewModel extends ParentViewModel {
    private static final String TAG = "CompaniesListViewModel";
    private CompanyRepository companyRepository;
    public String type = "";
    public int tab = 1;
    public int flag = 0;
    public int filter = 1,category_id,sub_category_id;
    public CompanyAdapter adapter = new CompanyAdapter(new ArrayList<>());
    public ArrayList<AdsCompanyModel> ads;
    public ArrayList<AdsCompanyModel> adsNearest;

    public CompaniesListViewModel(String type) {
        this.type = type;
        companyRepository = new CompanyRepository(mMutableLiveData);
        if(type.equals(Constants.NEWEST)) tab = 1;
        else if(type.equals(Constants.NEAREST)) tab = 2;
        notifyChange();
    }
    //NEW - CLOSE - MAP


    public void updateView(List<AdsCompanyModel> adsList){
        this.ads = new ArrayList<>(adsList);
        if(type.equals(Constants.NEAREST)){
            Log.d(TAG,type);
            MoonHelper.sortAds(ads);
        }
        setData();
    }

    public void setData() {
        if(ads.size() > 0) {
            haveData();
            adapter.update(ads);
        }else{
            noData();
        }
        showPage(true);
//        notifyChange();
    }


    public CompanyRepository getCompanyRepository() {
        return companyRepository;
    }
}
