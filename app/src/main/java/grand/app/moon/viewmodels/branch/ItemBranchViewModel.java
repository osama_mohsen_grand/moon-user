package grand.app.moon.viewmodels.branch;

import android.util.Log;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.shop.Branch;
import grand.app.moon.utils.resources.ResourceManager;

public class ItemBranchViewModel extends ParentViewModel {
    public Branch branch ;
    private String price;
    public int position = 0;
    public int width = 0;

    private static final String TAG = "ItemBranchViewModel";
    public ItemBranchViewModel(Branch branch, int position) {
        this.branch = branch;
        Log.d(TAG,branch.name);
        this.position = position;
        notifyChange();
    }

    public ItemBranchViewModel() {
        this.branch = branch;
        Log.d(TAG,branch.name);
        this.position = position;
        notifyChange();
    }

    public String getImageUrl(){
        return branch.image;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
