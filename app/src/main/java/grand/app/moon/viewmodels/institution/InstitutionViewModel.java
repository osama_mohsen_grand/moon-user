package grand.app.moon.viewmodels.institution;


import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.URLS;
import timber.log.Timber;

public class InstitutionViewModel extends ParentViewModel {
    private HomeRepository homeRepository;
    public int type = 0,flag = 0;
    public String service_id = "";
    public ArrayList<Integer> tags;
    public int tab = 1;
    public int filter;
    Service service;
    String url = "";
    public InstitutionViewModel(String type, Service service, String service_id, ArrayList<Integer> tags, int filter) {
        Timber.e("type_name:"+type);
        homeRepository = new HomeRepository(mMutableLiveData);
        if(type.equals(Constants.NEWEST)) tab = 1;
        else if(type.equals(Constants.OFFER)) tab = 2;
        this.service = service;
        this.type = service.mType;
        this.flag = service.mFlag;
        this.service_id = service_id;
        this.tags = tags;
        this.filter = filter;
        notifyChange();
    }


    public void callService(){
        showPage(false);
        url =  URLS.INSTITUTION_DETAILS;
        if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC))
            url = URLS.CLINIC_LIST;
        homeRepository.serviceDetails(type,flag,tab,service_id,filter,tags,-1,-1, URLS.INSTITUTION_DETAILS);
    }

    public HomeRepository getHomeRepository() {
        return homeRepository;
    }
}
