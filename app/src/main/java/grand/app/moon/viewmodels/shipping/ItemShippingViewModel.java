package grand.app.moon.viewmodels.shipping;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.shipping.Shipping;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemShippingViewModel extends ParentViewModel {
    public boolean selected = false;
    public Shipping shipping = null;
    public String price = "";
    public int position = 0;

    public ItemShippingViewModel(Shipping shipping, int position, boolean selected) {
        this.shipping = shipping;
        this.position = position;
        this.selected =  selected;
        price = shipping.price+" "+ UserHelper.retrieveCurrency();
        notifyChange();
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
