package grand.app.moon.viewmodels.review;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.review.Review;

public class ItemReviewViewModel extends ParentViewModel {
    public Review review = null;
    public String image;
    public int position = 0;

    public ItemReviewViewModel(Review review, int position) {
        this.review = review;
        this.position = position;
        notifyChange();
    }

    public void reviewSubmit(){
        mMutableLiveData.setValue(position);
    }
}
