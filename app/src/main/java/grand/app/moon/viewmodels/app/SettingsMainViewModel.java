
package grand.app.moon.viewmodels.app;

import java.util.ArrayList;

import grand.app.moon.adapter.settings.SettingsAdapter;
import grand.app.moon.adapter.settings.SettingsMainAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.SettingsRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class SettingsMainViewModel extends ParentViewModel {

    private SettingsRepository settingsRepository;
    public SettingsMainAdapter adapter;

    /* if type = 1 link terms and  if type = 2 link about us */

    public SettingsMainViewModel(int type) {
        settingsRepository = new SettingsRepository(mMutableLiveData);
        adapter = new SettingsMainAdapter(new ArrayList<>());
        settingsRepository.getSettings(type);
    }


    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
