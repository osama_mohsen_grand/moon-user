package grand.app.moon.viewmodels.map;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.repository.CompanyRepository;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.vollyutils.URLS;


public class MapViewModel extends ParentViewModel {

    public MapConfig mapConfig;
    HomeRepository homeRepository;
    CompanyRepository companyRepository;
    public int type = 0, flag = 0;
    public String service_id;
    public ArrayList<Integer> tags;
    public int tab = 1;
    public int filter;
    public List<ShopDetails> shops = null;
    public int category_id, sub_category_id, city_id;


    public MapViewModel(Service service,int filter) {
        init();
        this.type = service.mType;
        this.flag = service.mFlag;
        this.filter = filter;
        notifyChange();
    }


    public void init() {
        homeRepository = new HomeRepository(mMutableLiveData);
        companyRepository = new CompanyRepository(mMutableLiveData);
    }

    public MapViewModel() {
        init();

    }

    public void callService() {
        if (type == Integer.parseInt(Constants.TYPE_INSTITUTIONS))
            homeRepository.serviceMapDetails(type, flag, 3, service_id, filter, tags, URLS.INSTITUTION_DETAILS);
//        else if (type == Integer.parseInt(Constants.TYPE_COMPANIES)) {
//            if (service_id == null || service_id.equals("")) service_id = "-1";
//            companyRepository.getCompanies(Integer.parseInt(Constants.TYPE_COMPANIES), Integer.parseInt(service_id), filter, category_id, sub_category_id, city_id, 3);
//        }
        else if (type == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) || type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
            if (service_id == null || service_id.equals("")) service_id = "";
            homeRepository.serviceMapDetails(type, flag, 3, service_id, filter, tags, URLS.CLINIC_LIST);
        } else{
//            homeRepository.serviceMapDetails(type, flag, 4, service_id, filter, tags, URLS.SERVICE_DETAILS);
        }
    }


    public void updateView(List<ShopDetails> mShops) {
        this.shops = new ArrayList<>(mShops);
        getMutableLiveData().setValue(Constants.SUCCESS);
    }

    private static final String TAG = "MapViewModel";

    public void updateViewCompanies(List<AdsCompanyModel> adsList) {
        this.shops = new ArrayList<>();
        for(AdsCompanyModel ads : adsList){
            ShopDetails shopDetails = new ShopDetails();
            shopDetails.lat = ads.lat;
            shopDetails.lng = ads.lng;
            shopDetails.mId = ads.id;
            shopDetails.mName = ads.name;
            shopDetails.mRate = ads.rate;
            shopDetails.mImage = ads.image;
            this.shops.add(shopDetails);
            //TODO price ads
        }
        Log.d(TAG,this.shops.size()+"");
        getMutableLiveData().setValue(Constants.SUCCESS);
    }

    public HomeRepository getHomeRepository() {
        return homeRepository;
    }

    public CompanyRepository getCompanyRepository() {
        return companyRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}