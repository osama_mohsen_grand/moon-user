
package grand.app.moon.viewmodels.location;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class OpenLocationViewModel extends ParentViewModel {


    public OpenLocationViewModel() {

        notifyChange();
    }

    public void submit() {
        mMutableLiveData.setValue(Constants.LOCATION_ENABLE);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
