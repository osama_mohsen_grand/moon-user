
package grand.app.moon.viewmodels.cartype;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.adapter.CarTypeAdapter;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CarTypeViewModel extends ParentViewModel {

    public ArrayList<String> sections = new ArrayList<>();

    public CarTypeAdapter carTypeAdapter = new CarTypeAdapter(sections);

    public CarTypeViewModel(){
        sections.add("we");
        sections.add("we");
        sections.add("we");
    }

    @BindingAdapter({"adapter","data"})
    public static void bind(RecyclerView recyclerView, CarTypeAdapter adapter, List<String> data
    ) {
        AppUtils.initVerticalRV(recyclerView, recyclerView.getContext(), 1);
        recyclerView.setAdapter(adapter);
    }
    @Bindable
    public  List<String> getSectionsData() { return this.sections; }
    @Bindable
    public  CarTypeAdapter getSectionsAdapter() { return this.carTypeAdapter; }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
