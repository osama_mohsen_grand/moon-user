
package grand.app.moon.viewmodels.adsCompanyFilter;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.repository.AdvertisementCompanyRepository;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AdsCompanyFilterViewModel extends ParentViewModel {

    private AdsRepository advertisementCompanyRepository;

    public AdsCompanyFilterViewModel(int service_id) {
        advertisementCompanyRepository = new AdsRepository(mMutableLiveData);
            advertisementCompanyRepository.getAdsCompanyFilter(service_id);
    }

    public AdsCompanyFilterViewModel() {
        advertisementCompanyRepository = new AdsRepository(mMutableLiveData);
    }

    public void submit() {
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public AdsRepository getAdvertisementCompanyRepository() {
        return advertisementCompanyRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
