package grand.app.moon.viewmodels.moon;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemCheckPriceViewModel extends ParentViewModel {
    public boolean selected = false;
    public IdNamePrice check = null;
    public String price = "";
    public int position = 0;

    public ItemCheckPriceViewModel(IdNamePrice check, int position, boolean selected) {
        this.check = check;
        this.position = position;
        this.selected =  selected;
        price = check.price+" "+ UserHelper.retrieveCurrency();
        notifyChange();
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
