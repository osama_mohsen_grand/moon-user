package grand.app.moon.viewmodels.order;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentBaseObservableViewModel;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.order.Order;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import timber.log.Timber;


public class ItemOrderViewModel extends ParentViewModel {
    public Order order = null;
    private int position = 0;
    public String orderNo;
    public ObservableBoolean allowDelete = new ObservableBoolean(false);

    public ItemOrderViewModel(Order order, int position) {
        this.order = order;
        orderNo =  ResourceManager.getString(R.string.order_number_quote)+" "+order.orderNumber;
        this.position = position;
        if(order.status_id == 1) {
            allowDelete.set(true);
        }
    }

    public void delete(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }


    public void submit(){

        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }


}
