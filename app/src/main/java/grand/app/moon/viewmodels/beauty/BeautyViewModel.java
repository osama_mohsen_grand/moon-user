package grand.app.moon.viewmodels.beauty;


import java.util.ArrayList;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.ClinicRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

public class BeautyViewModel extends ParentViewModel {

    public String price = "0"+ UserHelper.retrieveCurrency();
    public ArrayList<Integer> ids = new ArrayList<>();
    List<IdNamePrice> idNamePrices;

    public BeautyViewModel(){
    }

    public void setIdNamePrices(List<IdNamePrice> idNamePrices) {
        this.idNamePrices = idNamePrices;
    }

    public void calculatePrice(){
        int priceTotal = 0;
        for(IdNamePrice idNamePrice : idNamePrices){
            if(ids.contains(Integer.parseInt(idNamePrice.id))){
                priceTotal += Integer.parseInt(idNamePrice.price);
            }
        }
        price = priceTotal+UserHelper.retrieveCurrency();
        notifyChange();
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }
}
