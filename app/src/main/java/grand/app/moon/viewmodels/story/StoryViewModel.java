
package grand.app.moon.viewmodels.story;

import android.widget.RadioGroup;

import java.util.ArrayList;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.repository.MoonMapRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class StoryViewModel extends ParentViewModel {
    public Story story;
    MoonMapRepository moonMapRepository;

    public StoryViewModel() {
        moonMapRepository = new MoonMapRepository(mMutableLiveData);
    }

    public void viewStory(ArrayList<String> storiesId){
        if(UserHelper.isLogin()) moonMapRepository.viewStory(storiesId);
    }

    public void submit(){
        getMutableLiveData().setValue(Constants.SUBMIT);
    }
}
