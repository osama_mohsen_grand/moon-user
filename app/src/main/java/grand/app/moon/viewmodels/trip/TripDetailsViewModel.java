package grand.app.moon.viewmodels.trip;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.triphistory.TripHistoryResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;


public class TripDetailsViewModel extends ParentViewModel {
    public TripHistoryResponse.Datum trip = null;
    public String price,dateTime,carColorModel = "",status ="";
    //1=>accept , 2=>reject  , 3=>arrive  , 4=>start  , 5=> cancel after accept ,  6=>finished trip

    public TripDetailsViewModel(TripHistoryResponse.Datum trip) {
        this.trip = trip;
        price = trip.price +" "+ UserHelper.retrieveCurrency();
        if(trip.dateSchedule != null && trip.timeSchedule != null){
            dateTime = trip.dateSchedule+" , "+trip.timeSchedule;
        }else
            dateTime = trip.tripDate;

        carColorModel = trip.car_brand +" ,"+trip.car_number;
        String status_text = "";
        if(trip.status == 0){
            status_text = ResourceManager.getString(R.string.waiting_response_from_driver);
        }else if(trip.status == 1){
            status_text = ResourceManager.getString(R.string.trip_had_been_accepted);
        }else if(trip.status == 2){
            status_text = ResourceManager.getString(R.string.trip_had_been_rejected);
        }else if(trip.status == 3){
            status_text = ResourceManager.getString(R.string.driver_had_been_arrived);
        }else if(trip.status == 4){
            status_text = ResourceManager.getString(R.string.trip_had_been_started);
        }else if(trip.status == 5){
            status_text = ResourceManager.getString(R.string.trip_had_been_cancel_after_accept);
        }else if(trip.status == 6){
            status_text = ResourceManager.getString(R.string.trip_had_been_finished);
        }
        status = ResourceManager.getString(R.string.status_quote)+" "+status_text;
    }

    public void trackOrder(){
        getMutableLiveData().setValue(Constants.TRIP_DETAILS_MAP);
    }

    @Bindable
    public Float getRating(){
        return trip.rate;
    }


    public static void setRating(RatingBar rating , Float rate){
        rating.setRating(rate);
    }
}
