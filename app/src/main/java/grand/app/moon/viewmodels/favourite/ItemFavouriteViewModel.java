package grand.app.moon.viewmodels.favourite;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.favourite.FavouriteModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemFavouriteViewModel extends ParentViewModel {
    public FavouriteModel favourite = null;
    public int position = 0;
    public String price;

    public ItemFavouriteViewModel(FavouriteModel favourite, int position) {
        this.favourite = favourite;
        this.position = position;
        this.price = favourite.price + " " + UserHelper.retrieveCurrency();
    }

    public void submit() {
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT, position));
    }

    public void remove() {
        mMutableLiveData.setValue(new Mutable(Constants.DELETE, position));
    }
}
