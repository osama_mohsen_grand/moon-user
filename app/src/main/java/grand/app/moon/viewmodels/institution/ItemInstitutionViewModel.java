package grand.app.moon.viewmodels.institution;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.service.ShopDetails;

public class ItemInstitutionViewModel extends ParentViewModel {
    public ShopDetails shop ;
    public int position = 0;

    public int width = 0;

    public ItemInstitutionViewModel(ShopDetails shop, int position) {
        this.shop = shop;
        this.position = position;
        notifyChange();
    }

    public String getImageUrl(){
        return shop.mImage;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
