package grand.app.moon.viewmodels.city;

import android.util.Log;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.country.Datum;

public class ItemCityRegionViewModel extends ParentViewModel {
    public IdName model = null;
    public int position = 0;
    public ObservableBoolean selected  = new ObservableBoolean(false);
    private static final String TAG = "ItemCityRegionViewModel";
    public ItemCityRegionViewModel(IdName model, int position, boolean selected) {
        this.model = model;
        this.position = position;
        this.selected.set(selected);
    }

    public void submit(){
        this.selected.set(true);
        mMutableLiveData.setValue(position);
    }

}
