
package grand.app.moon.viewmodels.ads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.AddAdsRequest;
import grand.app.moon.models.adsDetails.AdsDetailsModel;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import grand.app.moon.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddAdvertisementViewModel extends ParentViewModel {
    public int position = -1;
    public int category_delete_position = -1;
    public int new_image_position = -1;
    public ArrayList<IdNameImage> productImages = new ArrayList<>(Arrays.asList(null, null, null, null, null, null));
    public List<VolleyFileObject> volleyFileObject = new ArrayList<>();

    private AdsRepository adsRepository;
    public AddAdsRequest addAdsRequest;
    public boolean isEdit = false;


    public AddAdvertisementViewModel(int service_id, int category_id, int subCategory_id) {
        addAdsRequest = new AddAdsRequest(service_id, category_id, subCategory_id);
        adsRepository = new AdsRepository(mMutableLiveData);
        showPage(true);
    }


    public AddAdvertisementViewModel(int id, int type) {
        addAdsRequest = new AddAdsRequest(id);
        adsRepository = new AdsRepository(mMutableLiveData);
        isEdit = true;
        adsRepository.getAdsDetails(id, type);
    }

    public void submit() {
        if (validImages()) {
            if (isEdit) {
                if (addAdsRequest.isValid()) {
                    adsRepository.updateAds(addAdsRequest, volleyFileObject);
                } else if (addAdsRequest.isValid() && volleyFileObject.size() == 0) {
                    baseError = ResourceManager.getString(R.string.please_upload_advertisement_images);
                    mMutableLiveData.setValue(Constants.ERROR);
                }
            } else {
                if (addAdsRequest.isValid() && volleyFileObject.size() > 0) {
                    adsRepository.addAds(addAdsRequest, volleyFileObject);
                } else if (addAdsRequest.isValid() && volleyFileObject.size() == 0) {
                    baseError = ResourceManager.getString(R.string.please_upload_advertisement_images);
                    mMutableLiveData.setValue(Constants.ERROR);
                }
            }
        } else {
            baseError = ResourceManager.getString(R.string.please_upload_advertisement_images);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public boolean validImages() {
        volleyFileObject.clear();
        boolean valid = false;
        for (IdNameImage image : productImages) {
            if (image != null && (image.volleyFileObject != null || !image.image.equals(""))) {
                valid = true;
                if(image.volleyFileObject != null) volleyFileObject.add(image.volleyFileObject);
            }
        }
        return valid;
    }

    public void selectImages() {
        mMutableLiveData.setValue(Constants.IMAGE);
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATION);
    }


    public void setAddress(double lat, double lng) {
        addAdsRequest.setLat(lat);
        addAdsRequest.setLng(lng);

        addAdsRequest.setAddress(new MapConfig(MyApplication.getInstance(), null).getAddress(lat, lng));
        notifyChange();
    }


    public AdsRepository getAdsRepository() {
        return adsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void updateView(AdsDetailsModel data) {
        addAdsRequest.setName(data.name);
        addAdsRequest.setPhone(data.phone);
        addAdsRequest.setCity(data.city);
        addAdsRequest.setCity_id(data.city_id);
        addAdsRequest.setAddress(data.address);
        addAdsRequest.setLat(data.lat);
        addAdsRequest.setLng(data.lng);
        addAdsRequest.setEmail(data.email);
        addAdsRequest.setDescription(data.description);
        addAdsRequest.setPrice(data.price);
        int position = 0;
        for(IdNameImage idNameImage : data.productImages){
            productImages.set(position,idNameImage);
            position++;
        }
        notifyChange();
    }

    public void delete(int id) {
        adsRepository.deleteImage(id);
    }

}
