package grand.app.moon.viewmodels.tags;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.service.TagsRepository;
import grand.app.moon.utils.Constants;

public class TagsViewModel extends ParentViewModel {
    private TagsRepository tagsRepository;
    private Service service;
    public TagsViewModel(String service_id, Service service) {
        tagsRepository = new TagsRepository(mMutableLiveData);
        this.service = service;
        tagsRepository.getTags(service_id,service.mType);
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public TagsRepository getTagsRepository() {
        return tagsRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
