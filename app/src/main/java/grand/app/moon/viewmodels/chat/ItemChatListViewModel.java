package grand.app.moon.viewmodels.chat;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.chat.list.ChatList;
import grand.app.moon.utils.Constants;


public class ItemChatListViewModel extends ParentViewModel {

    public ChatList chat;
    public int position;
//    public ObservableBoolean imageVisible = new ObservableBoolean(true);
//    private int layout;
//
//    public Drawable backgroundColor;
//
    public ItemChatListViewModel(ChatList chat , int position) {
        this.chat = chat;
        this.position = position;
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.CHAT_DETAILS,position));
    }

//    @Bindable
//    public Drawable getBackgroundColor() {
//        return backgroundColor;
//    }
//
//    @BindingAdapter("bgColor")
//    public static void setBackgroundColor(LinearLayout linearLayout , Drawable backgroundColor) {
//        linearLayout.setBackground(backgroundColor);
//    }
//
//
//    public int getLayout(){
//        return layout;
//    }
//
//    @BindingAdapter("layout")
//    public static void setLayout(LinearLayout linearLayout,int layout){
//        linearLayout.setLayoutDirection(layout);
//    }
//
//    @Bindable
//    public String getImageUrl(){
//        return chat.userImage;
//    }
//
//    @BindingAdapter("imageUrl")
//    public static void loadImage(ImageView imageView, String image) {
//        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
//    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
