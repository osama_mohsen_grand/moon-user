package grand.app.moon.viewmodels.reservation;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.service.clinic.ClinicModel;
import grand.app.moon.utils.Constants;
import timber.log.Timber;

public class ItemClinicListViewModel extends ParentViewModel {
    public ClinicModel clinicModel = null;
    public int position = 0;
    public String type;

    public ItemClinicListViewModel(ClinicModel clinicModel,String type, int position) {
        this.clinicModel = clinicModel;
        this.type = type;
        this.position = position;
        Timber.e("clinic_type:"+type);
        notifyChange();
    }

    public void details(){
        mMutableLiveData.setValue(new Mutable(Constants.DETAILS,position));
    }
    public void follow(){
        mMutableLiveData.setValue(new Mutable(Constants.FOLLOW,position));
    }
}
