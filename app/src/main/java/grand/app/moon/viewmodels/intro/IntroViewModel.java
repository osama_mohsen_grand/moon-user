
package grand.app.moon.viewmodels.intro;

import android.view.Gravity;

import java.util.ArrayList;

import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.views.fragments.intro.Slide1Fragment;
import grand.app.moon.views.fragments.intro.Slide2Fragment;
import grand.app.moon.views.fragments.intro.Slide3Fragment;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class IntroViewModel extends ParentViewModel {

    public FragmentManager fragmentManager;
    public IntroViewModel(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public FragmentManager getAdapter(){
        return fragmentManager;
    }

    @BindingAdapter("adapter")
    public static void adapter(ViewPager wPager, FragmentManager fragmentManager) {

        ArrayList<TabModel> tabModels = new ArrayList<>();
        tabModels.add(new TabModel("", new Slide1Fragment()));
        tabModels.add(new TabModel("", new Slide2Fragment()));
        tabModels.add(new TabModel("", new Slide3Fragment()));
        SwapAdapter adapter = new SwapAdapter(fragmentManager,tabModels);
        wPager.setAdapter(adapter);
        wPager.setEnabled(false);
        final ViewPager.LayoutParams layoutParams = new ViewPager.LayoutParams();
        layoutParams.width = ViewPager.LayoutParams.MATCH_PARENT;
        layoutParams.height = ViewPager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.BOTTOM;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
