package grand.app.moon.viewmodels.shop;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.personalnfo.Profile;
import grand.app.moon.models.shop.ShopDepartment;


public class ItemShopDepartmentViewModel extends ParentViewModel {
    public ShopDepartment model = null;
    private int position = 0;

    public ItemShopDepartmentViewModel(ShopDepartment model, int position) {
        this.model = model;
        this.position = position;
    }

    public void submit(){
        getMutableLiveData().setValue(model.constant);
    }

}
