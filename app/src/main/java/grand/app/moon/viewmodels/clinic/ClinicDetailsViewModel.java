
package grand.app.moon.viewmodels.clinic;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.adapter.BeautyServiceAdapter;
import grand.app.moon.adapter.DoctorsAdapter;
import grand.app.moon.adapter.ShopDepartmentAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.clinic.Data;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.ShopDepartment;
import grand.app.moon.repository.ClinicRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ClinicDetailsViewModel extends ParentViewModel {
    public Data data;
    ClinicRepository clinicRepository;
    public int specialist_id = -1;
    public int id, type;
    public String comments = "";
    public ObservableField<String> price= new ObservableField<>("");
    public ObservableBoolean isClinic = new ObservableBoolean(true);
    public ShopDepartmentAdapter shopDepartmentAdapter = new ShopDepartmentAdapter(new ArrayList<>());
    public DoctorsAdapter doctorsAdapter = new DoctorsAdapter(new ArrayList<>());
    public BeautyServiceAdapter beautyServiceAdapter = new BeautyServiceAdapter(new ArrayList<>());

    public ArrayList<Integer> ids = new ArrayList<>();
    List<IdNamePrice> idNamePrices;


    public ClinicDetailsViewModel(int id, int type) {
        clinicRepository = new ClinicRepository(mMutableLiveData);
        this.id = id;
        this.type = type;
        if(type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)){
            isClinic.set(false);
        }
        callService();
    }

    public void callService() {
        clinicRepository.clinicDetails(id, type, specialist_id);
    }

    public ClinicRepository getClinicRepository() {
        return clinicRepository;
    }

    public void reviews() {
        mMutableLiveData.setValue(Constants.REVIEW);
    }

//    public String getComments() {
//        if (clinicRepository.getClinicDetailsResponse() != null) {
//            clinicRepository.getClinicDetailsResponse().data.details.setCommentCount(clinicRepository.getClinicDetailsResponse().data.details.getCommentCount());
//            comments = clinicRepository.getClinicDetailsResponse().data.details.getCommentCount();
//        }
//        return comments;
//    }

    public void filterSubmit(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void setData() {
        ArrayList<ShopDepartment> shopDepartments = AppMoon.shopDetailsDepartment(clinicRepository.getClinicDetailsResponse().data.details.isFollow ? R.drawable.ic_followed : R.drawable.ic_follow);
        shopDepartmentAdapter.update(shopDepartments);
        if(isClinic.get()) {
            if(getClinicRepository().getClinicDetailsResponse().data.doctorsList.size() > 0) {
                haveData();
                doctorsAdapter.update(getClinicRepository().getClinicDetailsResponse().data.doctorsList);
            }else
                noData();
        }else {
            if(getClinicRepository().getClinicDetailsResponse().data.services.size() > 0){
                haveData();
                beautyServiceAdapter.update(getClinicRepository().getClinicDetailsResponse().data.services);
                setIdNamePrices(getClinicRepository().getClinicDetailsResponse().data.services);
                calculatePrice();
            }else noData();
        }

    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void filter(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void chat() {
        if(UserHelper.getUserId() != -1) mMutableLiveData.setValue(Constants.CHAT);
        else {
            baseError =  ResourceManager.getString(R.string.please_login_first);
            getMutableLiveData().setValue(Constants.ERROR);
        }
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.BEAUTY_SUBMIT);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setIdNamePrices(List<IdNamePrice> idNamePrices) {
        this.idNamePrices = idNamePrices;
    }

    private static final String TAG = "ClinicDetailsViewModel";

    public void calculatePrice() {
        ids = beautyServiceAdapter.ids;
        int priceTotal = 0;
        for(IdNamePrice idNamePrice : idNamePrices){
            if(ids.contains(Integer.parseInt(idNamePrice.id))){
                priceTotal += Integer.parseInt(idNamePrice.price);
            }
        }
        price.set(priceTotal +" "+ UserHelper.retrieveCurrency());
        beautyServiceAdapter.setIds(ids);
//        Log.d(TAG,price);
//        notifyChange();
    }
}
