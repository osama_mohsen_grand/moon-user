package grand.app.moon.viewmodels.suggest;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdName;

public class ItemSuggestViewModel extends ParentViewModel {
    public IdName idName ;
    public int position = 0;


    public ItemSuggestViewModel(IdName idName, int position) {
        this.idName = idName;
        this.position = position;
        notifyChange();
    }


    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
