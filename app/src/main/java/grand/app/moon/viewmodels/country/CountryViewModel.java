
package grand.app.moon.viewmodels.country;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CountryViewModel extends ParentViewModel {

    private CountryRepository countryRepository;

    public CountryViewModel() {
        countryRepository = new CountryRepository(mMutableLiveData);
        countryRepository.getCountries(true);
    }


    public void submit() {
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
