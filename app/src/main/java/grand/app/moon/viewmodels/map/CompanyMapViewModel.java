package grand.app.moon.viewmodels.map;

import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.vollyutils.URLS;


public class CompanyMapViewModel extends ParentViewModel {

    public MapConfig mapConfig;
    HomeRepository homeRepository;
    public int type = 0,flag = 0;
    public String service_id;
    public ArrayList<Integer> tags;
    public int tab = 1;
    public int filter;
    public CompanyMapViewModel(Service service, String service_id, ArrayList<Integer> tags, int filter) {
        homeRepository = new HomeRepository(mMutableLiveData);
        this.type = service.mType;
        this.flag = service.mFlag;
        this.service_id = service_id;
        this.tags = tags;
        this.filter = filter;
        notifyChange();
    }

    public void callService() {
        if(type == Integer.parseInt(Constants.TYPE_INSTITUTIONS))
            homeRepository.serviceMapDetails(type,flag,3,service_id,filter,tags, URLS.INSTITUTION_DETAILS);
        else
            homeRepository.serviceMapDetails(type,flag,4,service_id,filter,tags, URLS.SERVICE_DETAILS);
    }

    public HomeRepository getHomeRepository() {
        return homeRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}