package grand.app.moon.viewmodels.order.details;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.OrderRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class OrderDetailsViewModel extends ParentViewModel {
    private OrderRepository orderRepository;
    public boolean haveDelegate = false,haveShop = false,allowChat = false;;
    private String subTotal = "", delivery = "", total = "",imageUrl = "",name="";


    public OrderDetailsViewModel(int order_id) {
        orderRepository = new OrderRepository(mMutableLiveData);
        orderRepository.getOrderDetails(order_id);
    }

    public String getSubTotal() {
        if (orderRepository.getOrderDetailsResponse() != null)
            subTotal = orderRepository.getOrderDetailsResponse().data.subTotal + " " + UserHelper.retrieveCurrency();
        return subTotal;
    }

    public String getImageUrl(){
        if (orderRepository.getOrderDetailsResponse() != null && haveDelegate)
            imageUrl = orderRepository.getOrderDetailsResponse().data.delegate.image;
        return imageUrl;
    }
    public String getName(){
        if (orderRepository.getOrderDetailsResponse() != null && haveDelegate)
            name = orderRepository.getOrderDetailsResponse().data.delegate.name;
        return name;
    }

    public String getDelivery() {
        if (orderRepository.getOrderDetailsResponse() != null)
            delivery = orderRepository.getOrderDetailsResponse().data.delivery + " " + UserHelper.retrieveCurrency();
        return delivery;
    }

    public String getTotal() {
        if (orderRepository.getOrderDetailsResponse() != null)
            total = orderRepository.getOrderDetailsResponse().data.totalPrice + " " + UserHelper.retrieveCurrency();
        return total;
    }

    public void chatDelegateSubmit(){
        mMutableLiveData.setValue(Constants.CHAT);
    }

    public void chatShopSubmit(){
        mMutableLiveData.setValue(Constants.CHAT_SHOP);
    }

    public void delegateLocation(){
        mMutableLiveData.setValue(Constants.DELEGATE_LOCATION);
    }
    public void shopLocation(){
        mMutableLiveData.setValue(Constants.SHOP_LOCATION);
    }


    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
    public void setStatusChat() {
        if(orderRepository.getOrderDetailsResponse().data.statusId < 2){
            allowChat = false;
        }else{
            allowChat = true;
        }
    }
}
