package grand.app.moon.viewmodels.shop;

import android.util.Log;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemShopProductViewModel extends ParentViewModel {
    public ProductList product ;
    public String price = "",priceAfter="",priceAfterConst="0",currency=UserHelper.retrieveCurrency();
    public int position = 0;
    public int width = 0;

    public ItemShopProductViewModel(ProductList product, int position) {
        this.product = product;
        this.position = position;
        price = product.price+" "+currency;
        priceAfterConst = product.priceAfter;
        priceAfter = product.priceAfter+ " "+currency;
        if(priceAfterConst.equals("0")) {
            priceAfter = price;
        }
    }
    
    public String getImageUrl(){
        return product.image;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
