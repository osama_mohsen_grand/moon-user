
package grand.app.moon.viewmodels.shop;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import de.hdodenhof.circleimageview.CircleImageView;
import grand.app.moon.R;
import grand.app.moon.adapter.ShopDepartmentAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.models.shop.ShopDepartment;
import grand.app.moon.models.shop.ShopDetailsResponse;
import grand.app.moon.repository.ShopRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.BaseActivity;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ShopDetailsViewModel extends ParentViewModel {

    public ShopRepository shopRepository;
    public int type = -1;
    private String reviews = "", image = "", backgroundImage = "";
    private String average = "";
    private String delivery = "";
    private String min = "";
    private String follow = "";
    //    public ObservableBoolean showAverageReviews = new ObservableBoolean(false);
    public ObservableBoolean isFamous = new ObservableBoolean(false);
    public ObservableField famousType = new ObservableField("");
    public ShopDepartmentAdapter shopDepartmentAdapter = new ShopDepartmentAdapter(new ArrayList<>());

    public ShopDetailsViewModel(int id, int type, int flag) {
        shopRepository = new ShopRepository(mMutableLiveData);
        shopRepository.getShopDetails(id, type, flag);
        this.type = type;
    }


    public String getReviews() {
        if (shopRepository.getData() != null)
            reviews = "(" + shopRepository.getData().shopData.shop_details.commentCount + ")";
        return reviews;
    }

    public void details() {
        mMutableLiveData.setValue(Constants.DETAILS);

    }

    public String getAverage() {
        if (shopRepository.getData() != null)
            average = ResourceManager.getString(R.string.avg_quote) + " " + shopRepository.getData().shopData.shop_details.avg + " " + ResourceManager.getString(R.string.min);
        return average;
    }

    public String getImage() {
        if (shopRepository.getData() != null)
            image = shopRepository.getData().shopData.shop_details.image;
        return image;
    }

    public void famousSubmit() {
        mMutableLiveData.setValue(Constants.FAMOUS_DETAILS);
    }

    public void back() {
        mMutableLiveData.setValue(Constants.BACK);
    }

    public String getImageBackground() {
        if (shopRepository.getData() != null)
            backgroundImage = shopRepository.getData().shopData.shop_details.backgroundImage;
        return backgroundImage;
    }

    public String getDelivery() {
        delivery = ResourceManager.getString(R.string.delivery_quote) + " ";
        if (shopRepository.getData() != null)
            if (!shopRepository.getData().shopData.shop_details.delivery.equals("0"))
                delivery += shopRepository.getData().shopData.shop_details.delivery;
            else
                delivery += ResourceManager.getString(R.string.free);
        return delivery;
    }

    public String getMin() {
        if (shopRepository.getData() != null)
            min = ResourceManager.getString(R.string.min_quote) + " " + shopRepository.getData().shopData.shop_details.miniCharge + " " + UserHelper.retrieveCurrency();
        return min;
    }

    public void rateSubmit() {
        mMutableLiveData.setValue(Constants.REVIEW);
    }

    public ShopRepository getShopRepository() {
        return shopRepository;
    }

    public void follow() {
        shopRepository.follow(new FollowRequest(shopRepository.getData().shopData.shop_details.id, type));
    }

    public void share() {
        mMutableLiveData.setValue(Constants.SHARE);
    }

    public String getFollow() {
        if (shopRepository.getData() != null)
            follow = "(" + shopRepository.getData().shopData.shop_details.followersCount + ")";
        return follow;
    }

    public void chat() {
        mMutableLiveData.setValue(Constants.CHAT);
    }

    public ShopDetailsResponse getStory() {
        return shopRepository.getData();
    }

    @BindingAdapter("story")
    public static void setStory(CircleImageView circleImageView, ShopDetailsResponse response) {
        if (response != null) {
            ImageLoader.getInstance().displayImage(
                    response.shopData.shop_details.image, circleImageView,
                    new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            circleImageView.setImageDrawable(ResourceManager.getDrawable(R.mipmap.ic_launcher));
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            if (response.shopData.shop_details.isStory == 1) { //have stroy
                                if (response.shopData.shop_details.storyView == 0) {
                                    circleImageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_gradient)); // not seen
                                } else
                                    circleImageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_silver)); // seen before
                                circleImageView.setOnClickListener(view1 -> {//submit story
                                    Intent intent = new Intent(circleImageView.getContext(), BaseActivity.class);
                                    intent.putExtra(Constants.PAGE, Constants.STORY);
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable(Constants.STORY, response.shopData.shop_details.story);
                                    intent.putExtra(Constants.BUNDLE, bundle);
                                    intent.putExtra(Constants.NAME_BAR, response.shopData.shop_details.story.mName);
                                    ((Activity) circleImageView.getContext()).startActivityForResult(intent, Constants.VIEW_REQUEST);
                                });
                            }// end found story
                        }
                    }
            );
        }
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setTabBar() {
        ArrayList<ShopDepartment> shopDepartments = AppMoon.shopDetailsDepartment(shopRepository.getData().shopData.shop_details.isFollow ? R.drawable.ic_followed : R.drawable.ic_follow);
        shopDepartmentAdapter.update(shopDepartments);
    }

    private static final String TAG = "ShopDetailsViewModel";

    public void setData() {
        isFamous.set(!shopRepository.getData().shopData.shop_details.isFamous.equals("0"));
        if (shopRepository.getData().shopData.shop_details.isFamous.equals("1"))
            famousType.set(ResourceManager.getString(R.string.famous));
        else if (shopRepository.getData().shopData.shop_details.isFamous.equals("2"))
            famousType.set(ResourceManager.getString(R.string.photographer));
        else if (shopRepository.getData().shopData.shop_details.isFamous.equals("3"))
            famousType.set(ResourceManager.getString(R.string.designer));
    }
}
