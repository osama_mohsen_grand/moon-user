package grand.app.moon.viewmodels.company;


import java.util.ArrayList;
import java.util.List;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.company.CompanyDetails;
import grand.app.moon.models.company.CompanyMainResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.CompanyRepository;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.repository.service.DiscoverRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.URLS;
import timber.log.Timber;

public class CompaniesDetailsViewModel extends ParentViewModel {
    public int city_id = -1;
    public int category_id,sub_category_id;
    public CompanyMainResponse response;
    private DiscoverRepository discoverRepository;
    CompanyRepository companyRepository;
    public Service service;
    public ArrayList<Integer> tags;
    public String service_id="";
    public int filter = 1;

    public CompaniesDetailsViewModel(Service service) {
        discoverRepository = new DiscoverRepository(mMutableLiveData);
        companyRepository = new CompanyRepository(mMutableLiveData);
        this.service = service;
        callService();
    }

    public void callService(){
        showPage(false);
        Timber.e("list_category_id"+category_id);
        Timber.e("list_sub_category_id"+sub_category_id);
        companyRepository.getCompanies(Integer.parseInt(Constants.TYPE_COMPANIES),service_id,
                filter,category_id,sub_category_id,city_id,2);
    }


    public CompanyRepository getCompanyRepository() {
        return companyRepository;
    }

    public void filterSubmit(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void citySubmit(){
        mMutableLiveData.setValue(Constants.CITY);
    }

    public void DiscoverSubmit(){
        mMutableLiveData.setValue(Constants.DISCOVER);
    }

    public DiscoverRepository getDiscoverRepository() {
        return discoverRepository;
    }
}
