
package grand.app.moon.viewmodels.user;

import androidx.databinding.ObservableField;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.UpdateInfoRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class UpdateInfoViewModel extends ParentViewModel {

    public String type = "";
    UpdateInfoRepository updateInfoRepository;
    public UserHelperMessage userHelperMessage = null;
    public ObservableField text = new ObservableField("");
    public ObservableField textError = new ObservableField();

    public UpdateInfoViewModel(String type) {
        this.type = type;
        userHelperMessage = new UserHelperMessage(type);
    }

    private boolean validateInput() {
        boolean valid = true;
        if (text.get().equals("")) {
            textError.set(userHelperMessage.hint);
            valid = false;
        }
        return valid;
    }

    public void submit() {
        if(validateInput()) {
            if(!type.equals(Constants.FORGET_PASSWORD)) {
                int id = UserHelper.getUserId();
//                ProfileRequest profileRequest = new ProfileRequest(id);
//                profileRequest.updateRequest(type, text.get().toString());
//                updateInfoRepository = new UpdateInfoRepository(mMutableLiveData, profileRequest);
//                updateInfoRepository.submit();
            }else if(type.equals(Constants.FORGET_PASSWORD)){
//                ForgetPasswordRequest forgetPasswordRequest = new ForgetPasswordRequest(text.get().toString());
//                updateInfoRepository = new UpdateInfoRepository(mMutableLiveData, forgetPasswordRequest);
//                updateInfoRepository.checkPhone();
            }
        }
    }

    public UpdateInfoRepository getUpdateInfoRepository() {
        return updateInfoRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
