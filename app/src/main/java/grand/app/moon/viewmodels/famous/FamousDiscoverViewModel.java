
package grand.app.moon.viewmodels.famous;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.FamousRepository;
import grand.app.moon.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousDiscoverViewModel extends ParentViewModel {

    FamousRepository famousRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.gallery));
    private ObservableBoolean noData = new ObservableBoolean(false);
    public int type;


    public FamousDiscoverViewModel(int type) {
        famousRepository = new FamousRepository(mMutableLiveData);
        this.type = type;
        getFamousDiscover();
    }

    private void getFamousDiscover() {
        famousRepository.getFamousDiscover(type);
    }

    public void noData(){
        noData.set(true);
    }

    public ObservableBoolean getNoData() {
        return noData;
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
