package grand.app.moon.viewmodels.service;


import android.util.Log;

import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.DataDetails;
import grand.app.moon.models.service.ServiceListResponse;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.repository.service.DiscoverRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.URLS;

public class ServiceDetailsViewModel extends ParentViewModel {
    public int service_id;
    public ArrayList<Integer> tags = new ArrayList<>();
    public int filter;
    public HomeRepository homeRepository;
    public boolean isOneService = false;
    public DataDetails response = null;
    public int city_id = -1,region_id=-1;

    private DiscoverRepository discoverRepository;
    public Service service;
    public ServiceListResponse serviceListResponse = null;
    public ServiceDetailsViewModel(Service service) {
        discoverRepository = new DiscoverRepository(mMutableLiveData);
        homeRepository = new HomeRepository(mMutableLiveData);
        this.service = service;
        Log.d(TAG,service.mType+"");
        Log.d(TAG,service.mFlag+"");
    }

    public void filterSubmit(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void citySubmit(){
        mMutableLiveData.setValue(Constants.CITY);
    }

    public void tagsSubmit(){
        mMutableLiveData.setValue(Constants.TAGS);
    }

    public void DiscoverSubmit(){
        mMutableLiveData.setValue(Constants.DISCOVER);
    }

    public DiscoverRepository getDiscoverRepository() {
        return discoverRepository;
    }

    private static final String TAG = "ServiceDetailsViewModel";

    public void callService() {
        String end = URLS.SERVICE_DETAILS;
        if(service.mType == Integer.parseInt(Constants.TYPE_COMPANIES) || service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING))
            end = URLS.COMPANIES_LIST;
        else if(service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) || service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)){
            end = URLS.CLINIC_LIST;
        }else if(service.mType == Integer.parseInt(Constants.TYPE_INSTITUTIONS)){
            end = URLS.INSTITUTION_DETAILS;
        }
//        if(service.mType <= Integer.parseInt(Constants.TYPE_INDUSTRIES)) {
            isOneService = true;
            homeRepository.serviceDetails(service.mType, service.mFlag, 1, service_id+"", filter, tags,city_id,region_id, end);
//        }
    }

    public void reset(){
        filter = 0;
        tags.clear();
        city_id = -1;
        region_id = -1;
    }
}
