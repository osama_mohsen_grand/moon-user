package grand.app.moon.viewmodels.search;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.clinic.DoctorsList;
import grand.app.moon.models.search.SearchModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;


public class ItemSearchViewModel extends ParentViewModel {

    public SearchModel searchModel;
    public int position;
    public ObservableBoolean priceVisible = new ObservableBoolean(false);
    public ObservableBoolean rateVisible = new ObservableBoolean(true);
    public ObservableBoolean descVisible = new ObservableBoolean(true);
//    private int layout;
//
//    public Drawable backgroundColor;
//
    public ItemSearchViewModel(SearchModel searchModel , int position) {
        this.searchModel = searchModel;
        if(searchModel.description == null && searchModel.address != null) {
            searchModel.description = searchModel.address;
        }else if(searchModel.seller != null){
            rateVisible.set(false);
            priceVisible.set(true);
            searchModel.description = ResourceManager.getString(R.string.seller)+" "+searchModel.seller.name;
            searchModel.tags =ResourceManager.getString(R.string.price)+" "+searchModel.price;
        }
        if(searchModel.description == null)
            descVisible.set(false);
        if(searchModel.rate == -1)
            rateVisible.set(false);
        notifyChange();
        this.position = position;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
