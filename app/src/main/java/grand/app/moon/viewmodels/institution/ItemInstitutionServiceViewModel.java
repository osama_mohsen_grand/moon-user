package grand.app.moon.viewmodels.institution;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.service.ShopDetails;


public class ItemInstitutionServiceViewModel extends ParentViewModel {
    public IdNameImage idNameImage ;
    public int position = 0;

    public int width = 0;

    public ItemInstitutionServiceViewModel(IdNameImage idNameImage, int position) {
        this.idNameImage = idNameImage;
        this.position = position;
        notifyChange();
    }

    public String getImageUrl(){
        return idNameImage.image;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
