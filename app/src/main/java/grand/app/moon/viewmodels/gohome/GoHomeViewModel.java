package grand.app.moon.viewmodels.gohome;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.libraries.places.api.model.Place;
import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;


public class GoHomeViewModel extends ParentViewModel {
    //legs : 5.3 km , time 12 minutes
    private static final String TAG = "GoHomeViewModel";
    public MapConfig mapConfig = null;
    Marker marker = null;
    public String dest = "";

    public ObservableBoolean destError = new ObservableBoolean(false);

    public GoHomeViewModel() {}

    public void destSubmit() {
        mMutableLiveData.setValue(Constants.DEST_SUBMIT);
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.GO_HOME);
    }

    public void setDest(Place place) {
        if ( place != null) {
            //first marker is pickup
            LatLng latLng = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
            if (marker != null) marker.remove();
            setDest(place.getName());
            moveCamera(place.getLatLng());
            notifyChange();
        }
    }

    public void moveCamera(LatLng latLng) {
        mapConfig.getGoogleMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
    }

    public void setDest(String dest) {
        this.dest = dest;
        destError.set(false);//remove error if exist
        notifyChange();
    }
    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
