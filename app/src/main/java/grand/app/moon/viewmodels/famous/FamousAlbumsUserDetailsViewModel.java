
package grand.app.moon.viewmodels.famous;

import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.repository.FamousRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousAlbumsUserDetailsViewModel extends ParentViewModel {
    private FamousRepository famousRepository;
    private AdsRepository adsRepository;
    private String type;
    private int id;
    private int tab;

    public FamousAlbumsUserDetailsViewModel() {
        famousRepository = new FamousRepository(mMutableLiveData);
        adsRepository = new AdsRepository(mMutableLiveData);
    }

    public void setData(int id, int tab,String type){
        this.id = id;
        this.type = type;
        this.tab = tab;
    }

    public void getFamousDetails(){
        famousRepository.getFamousDetails(id,tab,Integer.parseInt(type));
    }

    public void filter(){
//        if(getFamousRepository().getFamousDetailsResponse() != null && getFamousRepository().getFamousDetailsResponse().data != null )
//            mMutableLiveData.setValue(Constants.FILTER);
//        else
            adsRepository.getShopAds("?id="+id+"&type=1");
    }

    public void submitFilter(ArrayList<Integer> ids){
        adsRepository.getFamousShopAds(ids,id,Integer.parseInt(type));
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public AdsRepository getAdsRepository() {
        return adsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
