
package grand.app.moon.viewmodels.branch;

import java.util.ArrayList;

import grand.app.moon.adapter.BranchAccountInfoAdapter;
import grand.app.moon.base.ParentViewModel;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BranchesAccountInfoViewModel extends ParentViewModel {

    public BranchAccountInfoAdapter adapter = new BranchAccountInfoAdapter(new ArrayList<>());

    public BranchesAccountInfoViewModel() {
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
