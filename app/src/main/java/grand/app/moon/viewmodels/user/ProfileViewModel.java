
package grand.app.moon.viewmodels.user;

import android.util.Log;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.country.City;
import grand.app.moon.models.user.profile.ProfileRequest;
import grand.app.moon.repository.LoginRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.MyApplication;
import grand.app.moon.vollyutils.VolleyFileObject;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ProfileViewModel extends ParentViewModel {

    LoginRepository loginRepository;
    public ProfileRequest profileRequest;
    public StatusMsg statusMsg = null;
    public ObservableBoolean isLogin = new ObservableBoolean(false);
    public ObservableField<String> phoneHelper =
            new ObservableField<>();
    public ArrayList<String> cities = new ArrayList<>();
    public int country_position = -1;

    public ProfileViewModel() {

        if (UserHelper.getUserId() != -1) {
            isLogin.set(true);
            profileRequest = new ProfileRequest();
        }
        loginRepository = new LoginRepository(mMutableLiveData);
        loginRepository.getCountries(true);
        setHelper(String.valueOf(UserHelper.getUserDetails().country_id));
    }


    public void setHelper(String countryId) {
        phoneHelper.set(ResourceManager.getString(R.string.please_enter_phone_with_country_code) + "(" +
                AppMoon.getCountry(countryId).code + ")");
    }

    public void submit() {
        if (profileRequest.isValid()) {
            if (profileRequest.volleyFileObject == null)
                loginRepository.updateProfile(profileRequest);
            else
                loginRepository.updateProfile(profileRequest, profileRequest.volleyFileObject);
        }
    }

    public String getImageUrl() {
        return UserHelper.getUserDetails().image;
    }

    public void selectImage() {
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }

    private static final String TAG = "ProfileViewModel";

    public void citySubmit() {
        Log.d(TAG, "city");
        mMutableLiveData.setValue(Constants.CITY);
    }

    public void regionSubmit() {
        Log.d(TAG, "REGION");
        mMutableLiveData.setValue(Constants.REGION);
    }

    public void countrySubmit() {
        mMutableLiveData.setValue(Constants.CHOOSE);
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(VolleyFileObject volleyFileObject) {
        profileRequest.volleyFileObject = volleyFileObject;
    }

    public void changePassword() {
        mMutableLiveData.setValue(Constants.CHANGE_PASSWORD);
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public void setAddress(double lat, double lng) {
        profileRequest.setLat(lat);
        profileRequest.setLng(lng);
        profileRequest.setAddress(new MapConfig(MyApplication.getInstance(), null).getAddress(lat, lng));
        notifyChange();
    }


    public LoginRepository getLoginRepository() {
        return loginRepository;
    }

    public void setData() {
        if (UserHelper.isLogin()) {
            Log.d(TAG,"isLogin");
            if (!profileRequest.country_name.equals("") && profileRequest.city_name.equals("") && profileRequest.region_name.equals("")) {
                String country_id = UserHelper.retrieveKey(Constants.COUNTRY_ID);
                country_position = AppMoon.getCountryPosition(country_id);
                for(City city : UserHelper.getCountries().data.get(country_position).cities)
                    cities.add(city.cityName);
            }
        }
    }
}
