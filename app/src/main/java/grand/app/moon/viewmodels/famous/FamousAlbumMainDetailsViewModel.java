
package grand.app.moon.viewmodels.famous;

import android.view.View;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.famous.album.FamousAddImageInsideAlbumRequest;
import grand.app.moon.repository.FamousRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.VolleyFileObject;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousAlbumMainDetailsViewModel extends ParentViewModel {
    private FamousRepository famousRepository;
    public FamousAddImageInsideAlbumRequest famousAddImageInsideAlbumRequest;
    public int category_delete_position = -1;
    public ObservableBoolean visible;

    public FamousAlbumMainDetailsViewModel(int id,int shop_id, String type, String tab,boolean ads) {
        type = (type.equals(Constants.IMAGE) ? "1" : "2");
        famousAddImageInsideAlbumRequest = new FamousAddImageInsideAlbumRequest(id,type,tab);
        famousRepository = new FamousRepository(mMutableLiveData);
        if(ads)
            famousRepository.getShopAds(id,shop_id);//get ads
        else
            famousRepository.getAlbumImages(id,shop_id);
        visible = new ObservableBoolean(false);
    }

    //have list that mean make page visible
    public FamousAlbumMainDetailsViewModel() {
        visible = new ObservableBoolean(true);
    }

    public void add(){
        mMutableLiveData.setValue(Constants.ADD);
    }

    //add image im album
    public void addImage(VolleyFileObject volleyFileObject) {
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>(); volleyFileObjects.add(volleyFileObject);
        famousRepository.addImageToAlbum(famousAddImageInsideAlbumRequest,volleyFileObjects);
    }

    //delete image from album
    public void delete(int id) {
        famousRepository.removeAlbumImage(id);
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
