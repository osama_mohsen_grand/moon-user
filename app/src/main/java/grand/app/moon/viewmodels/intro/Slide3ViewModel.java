
package grand.app.moon.viewmodels.intro;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class Slide3ViewModel extends ParentViewModel {

    public Slide3ViewModel() {

    }
    public void submit(){
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
