
package grand.app.moon.viewmodels.beauty;


import android.util.Log;
import android.widget.RadioGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.PaymentMethodAdapter;
import grand.app.moon.adapter.ShippingAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.reservation.ReservationRequest;
import grand.app.moon.models.shipping.Shipping;
import grand.app.moon.repository.ClinicRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BookBeautyViewModel extends ParentViewModel {

    public String text = "";
    ClinicRepository clinicRepository;
    public ReservationRequest reservationRequest;
    public ClinicDetailsResponse clinicDetailsResponse;
    public String comments = "";
    public PaymentMethodAdapter adapter = new PaymentMethodAdapter(new ArrayList<>());

    public BookBeautyViewModel(ClinicDetailsResponse clinicDetailsResponse) {
        reservationRequest = new ReservationRequest(Constants.TYPE_RESERVATION_BEAUTY, clinicDetailsResponse.data.details.id);
        reservationRequest.period = 1;
        this.clinicDetailsResponse = clinicDetailsResponse;
        adapter.update(this.clinicDetailsResponse.data.paymentList);
        if(clinicDetailsResponse.data.paymentList.size() > 0){
            reservationRequest.payment_method = Integer.parseInt(clinicDetailsResponse.data.paymentList.get(0).id);
            adapter.position = 0;
        }
//        setEventAdapter();
        clinicRepository = new ClinicRepository(mMutableLiveData);
    }

    public void onSplitTypeChanged(RadioGroup radioGroup, int id) {
        if (id == R.id.rb_morning)
            reservationRequest.period = 1;
        else if (id == R.id.rb_evening)
            reservationRequest.period = 2;
    }

    public void submit() {
        if(adapter.position >= 0) {
            reservationRequest.payment_method = Integer.parseInt(clinicDetailsResponse.data.paymentList.get(adapter.position).id);
            clinicRepository.makeReservation(reservationRequest);
        }
    }
    public void reviews() {
        mMutableLiveData.setValue(Constants.REVIEW);
    }


    public void chat() {
        mMutableLiveData.setValue(Constants.CHAT);
    }

    public ClinicRepository getClinicRepository() {
        return clinicRepository;
    }

    public boolean allowCash = false, allowOnline = false, checkCash = false, checkOnline = false;
    private static final String TAG = "BookBeautyViewModel";
    public void updateUI(int payment_type){
        Log.e(TAG, "updateUI: "+payment_type );
        if (payment_type == 1) {
            allowCash = true;
            checkCash = true;
        } else if (payment_type == 2) {
            allowOnline = true;
            checkOnline = true;
            reservationRequest.payment_method = 2;
        } else {
            checkCash = true;
            allowCash = true;
            allowOnline = true;
        }
    }



    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
