package grand.app.moon.viewmodels.branch;

import android.util.Log;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.shop.Branch;

public class ItemBranchAccountInfoViewModel extends ParentViewModel {
    public Branch branch ;
    private String price;
    public int position = 0;
    public int width = 0;

    private static final String TAG = "ItemBranchViewModel";
    public ItemBranchAccountInfoViewModel(Branch branch, int position) {
        this.branch = branch;
        Log.d(TAG,branch.address);
        this.position = position;
        notifyChange();
    }

    public ItemBranchAccountInfoViewModel() {
        this.branch = branch;
//        Log.d(TAG,branch.name);
        notifyChange();
    }

    public String getImageUrl(){
        return branch.image;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
