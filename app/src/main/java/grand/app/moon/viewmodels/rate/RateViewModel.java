
package grand.app.moon.viewmodels.rate;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.rate.RateRequest;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.repository.TripActionRepository;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class RateViewModel extends ParentViewModel {
    public RateRequest rateRequest ;
    public ObservableInt rating = new ObservableInt(0);
    public TripDetailsResponse tripDetailsResponse;
    TripActionRepository tripActionRepository;
    public String total = "";
    public String tripId = "";
    public String name = "";
    public String image = "";
    //tripDetailsResponse.data.user.name
    public static ObservableField<String> text = new ObservableField<>("");


    public RateViewModel(String tripId,String name,String image,String total) {
        tripActionRepository = new TripActionRepository(mMutableLiveData);
        text.set("");
        this.tripId = tripId;
        this.name = name;
        this.image = image;
        this.total = ResourceManager.getString(R.string.price_is)+" "+total +" "+ UserHelper.retrieveCurrency();
        notifyChange();
    }

    public void sendSubmit() {
        rateRequest = new RateRequest(tripId,rating.get(),text.get());
//        tripActionRepository.rate(rateRequest);
    }


    @Bindable
    public String getImageUrl(){
        return image;
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, Object image) {
        if (image instanceof String) {
            Glide
                    .with(imageView.getContext())
                    .load((String) image)
                    .centerCrop()
//                .placeholder(R.drawable.progress_animation)
                    .into(imageView);
        } else if (image instanceof Drawable) {
            imageView.setImageDrawable((Drawable) image);
        }
    }



    
    public TripActionRepository getTripActionRepository() {
        return tripActionRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
