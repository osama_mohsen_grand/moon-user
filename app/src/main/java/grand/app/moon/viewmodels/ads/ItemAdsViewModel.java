package grand.app.moon.viewmodels.ads;


import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.vollyutils.AppHelper;

public class ItemAdsViewModel extends ParentViewModel {
    public ImageVideo imageVideo;
    public ObservableBoolean play;
    public int width = 0;
    public int position = 0;
    public ObservableBoolean visible;
    public ObservableBoolean isAds;


    public ItemAdsViewModel(ImageVideo imageVideo, int position) {
        this.position = position;
        this.imageVideo = imageVideo;
        if (imageVideo.type.equals(2))
            play = new ObservableBoolean(true);
        else
            play = new ObservableBoolean(false);

        if (imageVideo.count == null)//meaning adds
            isAds = new ObservableBoolean(true);
        else
            isAds = new ObservableBoolean(false);
    }

    public int getWidth() {
        return 0;
    }

    @BindingAdapter("width")
    public static void setWidth(RelativeLayout relativeLayout, int w) {
        int width = AppHelper.getScreenWidth(relativeLayout.getContext()) / 3;
        relativeLayout.getLayoutParams().width = width;
    }


    public ImageVideo getImageVideo(){
        return imageVideo;
    }

    @BindingAdapter("imageVideo")
    public static void setImageVideo(ImageView imageView, ImageVideo imageVideo) {
        if(!imageVideo.image.equals("")) {
            if(imageVideo.type == 2){
                Glide.with(imageView.getContext())
                        .load(imageVideo.image)
                        .into(imageView);
            }else {
                ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(), imageVideo.image, imageView);
            }
        }else{
            imageView.setImageResource(R.drawable.ic_logo_original);
        }
    }

    public void video() {
        mMutableLiveData.setValue(new Mutable(Constants.VIDEO, position));
    }

    public void submit() {
//        if (imageVideo.capture != null && !imageVideo.capture.equals(""))
        if (imageVideo.type.equals(2))
            mMutableLiveData.setValue(new Mutable(Constants.VIDEO, position));
        else
            mMutableLiveData.setValue(new Mutable(Constants.IMAGE, position));
    }

    public String getImageUrl() {
        return imageVideo.image;
    }
}
