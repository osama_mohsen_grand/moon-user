package grand.app.moon.viewmodels.cartype;


import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;

public class ItemCarTypeViewModel extends ParentViewModel {
    public String notification = null;
    public String image;
    public int position = 0;


    public ItemCarTypeViewModel(String notification, int position) {
        this.notification = notification;
        this.position = position;
        notifyChange();
    }

    public String getImageUrl(){
        return Constants.image_dummy;
    }

//    @BindingAdapter("imageUrl")
//    public static void loadImage(ImageView imageView, String image) {
//        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
//    }



    public void notificationSubmit(){
        mMutableLiveData.setValue(position);
    }
}
