package grand.app.moon.viewmodels.product;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.product.Addition;

public class ItemAdditionViewModel  extends ParentViewModel {
    public boolean selected = false;
    public Addition addition = null;
    public int position = 0;

    public ItemAdditionViewModel(Addition addition, int position, boolean selected) {
        this.addition = addition;
        this.position = position;
        this.selected =  selected;
        notifyChange();
    }

    public void additionSubmit(){
        mMutableLiveData.setValue(position);
    }
    
}
