package grand.app.moon.viewmodels.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Transfer  implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("bank_name")
    @Expose
    public String bankName = "";
    @SerializedName("account_number")
    @Expose
    public String accountNumber = "";
    @SerializedName("country")
    @Expose
    public String country = "";
    @SerializedName("city")
    @Expose
    public String city = "";
    @SerializedName("region")
    @Expose
    public String region = "";
    @SerializedName("notes")
    @Expose
    public String notes = "";
}
