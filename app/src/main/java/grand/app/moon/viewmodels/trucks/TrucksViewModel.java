package grand.app.moon.viewmodels.trucks;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.service.TagsRepository;
import grand.app.moon.repository.taxi.TrucksRepository;
import grand.app.moon.utils.Constants;

public class TrucksViewModel extends ParentViewModel {
    private TrucksRepository trucksRepository;
    public TrucksViewModel() {
        trucksRepository = new TrucksRepository(mMutableLiveData);
        trucksRepository.getTrucks();
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public TrucksRepository getTrucksRepository() {
        return trucksRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
