package grand.app.moon.viewmodels.ads;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.companyAdsCategory.Datum;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;

public class ItemShopAlbumViewModel extends ParentViewModel {
    public Datum model ;
    public int position = 0;
    public boolean selected = false;
    public int width = 0;
    public String imagesCount,videosCount;

    public ItemShopAlbumViewModel(Datum model, int position) {
        this.model = model;
        this.position = position;
        imagesCount = model.imagesCount + " "+ResourceManager.getString(R.string.photos);
        videosCount = model.videoCount + " "+ResourceManager.getString(R.string.video);
    }

    public void shopSubmit(){
        getMutableLiveData().setValue(new Mutable(Constants.SHOP,position));
    }

    public void submit(){
        getMutableLiveData().setValue(new Mutable(Constants.ALBUM,position));
    }
}
