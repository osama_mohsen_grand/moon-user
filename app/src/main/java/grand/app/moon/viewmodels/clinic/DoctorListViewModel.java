
package grand.app.moon.viewmodels.clinic;


import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class DoctorListViewModel extends ParentViewModel {

    public int category_book_pos = -1;
    public String text = "";

    public DoctorListViewModel() {

    }

    public void filterSubmit(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
