
package grand.app.moon.viewmodels.checkout;

import android.util.Log;
import android.widget.RadioGroup;

import java.util.ArrayList;

import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.PaymentMethodAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.cart.CartData;
import grand.app.moon.models.order.OrderRequest;
import grand.app.moon.repository.OrderRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.cart.CartViewModel;
import grand.app.moon.vollyutils.MyApplication;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CheckoutViewModel extends ParentViewModel {

    public OrderRequest orderRequest;
    OrderRepository orderRepository;
    public String delivery = "", total = "", subTotal = "";
    public boolean hasDelegate;
    //payment_type: (1)cash , (2)online , (3)cash&online
    public PaymentMethodAdapter paymentMethodAdapter = new PaymentMethodAdapter(new ArrayList<>());
    public CartData cartData;


    public CheckoutViewModel(boolean hasDelegate, String total, String subTotal, String delivery) {
        orderRepository = new OrderRepository(mMutableLiveData);
        orderRequest = new OrderRequest();
        this.hasDelegate = hasDelegate;
        this.total = total;
        this.subTotal = subTotal;
        this.delivery = "0";
        if (!hasDelegate) {
            orderRepository.getShipping();
        }
        setEventAdapter();
    }


    public void init() {
        paymentMethodAdapter.update(cartData.paymentList);
    }

    private void setEventAdapter() {
        paymentMethodAdapter.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                int position = (int) o;
                paymentMethodAdapter.setPosition(position);
                orderRequest.payment_method = cartData.paymentList.get(paymentMethodAdapter.getPosition()).id;
                paymentMethodAdapter.notifyDataSetChanged();
            }
        });
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public void setAddress(double lat, double lng) {
        orderRequest.setLat(lat);
        orderRequest.setLng(lng);
        orderRequest.setAddress(new MapConfig(MyApplication.getInstance(), null).getAddress(lat, lng));
        notifyChange();
    }

    public void submit() {
        if (orderRequest.isValid()) {
            orderRepository.confirm(orderRequest);
        } else
            mMutableLiveData.setValue(Constants.SCROLL_TOP);
    }

    private static final String TAG = "CheckoutViewModel";
    public void updateDelivery(String newDelivery) {
        double previousDelivery = Double.parseDouble(delivery);
        Log.d(TAG,"previous:"+previousDelivery);
        Log.d(TAG,"previous_total:"+total);
        Log.d(TAG,"previous_delivery+:"+previousDelivery);
        double totalCost = Double.parseDouble(total);
        totalCost -= previousDelivery;
        Log.d(TAG,"previous_totalCost:"+totalCost);

        delivery = newDelivery;
        totalCost += Double.parseDouble(delivery);
        Log.d(TAG,"previous_totalCost:"+totalCost);

        total = totalCost + "";
        notifyChange();
    }

}
