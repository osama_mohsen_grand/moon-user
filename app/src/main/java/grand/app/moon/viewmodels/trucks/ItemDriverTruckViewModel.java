package grand.app.moon.viewmodels.trucks;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.triphistory.TripHistoryResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;


public class ItemDriverTruckViewModel extends ParentViewModel {
    public IdNamePrice driver;
    public int position;
    public String price;

    public ItemDriverTruckViewModel(IdNamePrice driver , int position) {
        this.driver = driver;
        this.position = position;
        price = driver.price+" "+ UserHelper.retrieveCurrency()+" "+ResourceManager.getString(R.string.per_kilo);
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
