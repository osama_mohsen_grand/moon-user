
package grand.app.moon.viewmodels.user;

import android.os.Bundle;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;

import org.json.JSONException;

import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.customviews.facebook.FacebookModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.user.login.FacebookLoginRequest;
import grand.app.moon.repository.LoginRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.models.user.login.LoginRequest;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.URLS;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class LoginViewModel extends ParentViewModel {
    public LoginRequest loginRequest;
    public FacebookModel facebookModel;
    LoginRepository loginRepository;
    public CallbackManager callbackManager;
    public LoginButton btnFacebook;


    public static ObservableField<String> type = new ObservableField<>("");


    public LoginViewModel() {
        loginRequest = new LoginRequest();
        loginRepository = new LoginRepository(mMutableLiveData);
    }

    public void submitGoogle() {
        mMutableLiveData.setValue(Constants.GOOGLE);
    }

    public void submitFacebook() {
        // Callback registration
        mMutableLiveData.setValue(Constants.FACEBOOK);
    }


    public void call() {
        loginRepository.loginUser(loginRequest);
    }

    public void loginSubmit() {
        if (loginRequest.isValid()) {
            call();
        }
    }

    public void signUp() {
        mMutableLiveData.setValue(Constants.REGISTRATION);

    }

    public void loginFacebookSubmit() {
        mMutableLiveData.setValue(Constants.FACEBOOK);

    }

    public void forgetPassword() {
        mMutableLiveData.setValue(Constants.FORGET_PASSWORD);
    }


    public void socialLogin(FacebookModel facebookModel) {
        this.facebookModel = facebookModel;
        loginRepository.checkSocialIdExist(new FacebookLoginRequest(facebookModel.social_id));
    }


    public LoginRepository getLoginRepository() {
        return loginRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    private static final String TAG = "LoginViewModel";
    public void setupFacebookSignIn() {
        // Callback registration
        btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("start", "start here success login");
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            Log.e("facebook", "completed");
                            try {
                                Log.d(TAG,object.toString());
                                Log.d(TAG,response.toString());
                                loginRequest.socialId = object.getString("id");
                                Log.d(TAG,""+loginRequest.socialId);
                                if(object.has("email"))
                                    loginRequest.setEmail(object.getString("email"));
                                else
                                    loginRequest.setEmail(null);
                                loginRequest.socialName = object.getString("name");
                                loginRequest.countryId = UserHelper.retrieveKey(Constants.COUNTRY_ID);
                                loginRequest.socialType = "1";
                                loginRequest.socialImage = "https://graph.facebook.com/" + object.getString("id") + "/picture?width=250&height=250";
                                loginRepository.loginSocial(loginRequest);
                                LoginManager.getInstance().logOut();
                            } catch (JSONException e) {
                                Log.e("start", "error:"+e.getMessage());
                                e.printStackTrace();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                mMutableLiveData.setValue(new Mutable(Constants.ERROR, ResourceManager.getString(R.string.please_try_again)));
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                mMutableLiveData.setValue(new Mutable(Constants.ERROR, ResourceManager.getString(R.string.please_try_again)));
            }
        });
    }

    public void googleSignInAccount(GoogleSignInAccount account) {
        if (account.getPhotoUrl() != null)
            loginRequest.socialImage = account.getPhotoUrl().toString();
        else
            loginRequest.socialImage = URLS.DEFAULT_IMAGE;
        loginRequest.socialId = account.getId();
        Log.d(TAG,""+loginRequest.socialId);
        loginRequest.socialName = account.getDisplayName();
        loginRequest.countryId = UserHelper.retrieveKey(Constants.COUNTRY_ID);
        loginRequest.setEmail(account.getEmail());
        loginRequest.socialType = "2";
        loginRepository.loginSocial(loginRequest);
    }
}
