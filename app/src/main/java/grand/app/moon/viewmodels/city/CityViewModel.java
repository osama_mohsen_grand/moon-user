
package grand.app.moon.viewmodels.city;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CityViewModel extends ParentViewModel {

    public void submit() {
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
