
package grand.app.moon.viewmodels.app;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.SettingsRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class PrivacyListViewModel extends ParentViewModel {

    private SettingsRepository settingsRepository;

    /* if type = 1 link terms and  if type = 2 link about us */

    public PrivacyListViewModel(int type) {
        settingsRepository = new SettingsRepository(mMutableLiveData);
        settingsRepository.getSettings(type);
    }


    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
