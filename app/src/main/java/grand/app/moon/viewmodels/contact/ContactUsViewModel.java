
package grand.app.moon.viewmodels.contact;

import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.customviews.facebook.FacebookModel;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.contact.ContactUsRequest;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.repository.RegisterRepository;
import grand.app.moon.repository.SettingsRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import grand.app.moon.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ContactUsViewModel extends ParentViewModel {
    public static ObservableField<String> type = new ObservableField<>("");
    public ContactUsRequest request ;
    private SettingsRepository repository;
    public StatusMsg statusMsg = null;


    public ContactUsViewModel() {
        request = new ContactUsRequest();
        repository = new SettingsRepository(mMutableLiveData);
        notifyChange();
    }
    public void send() {
        if(request.isValid()) {
            repository.contactUs(request); // send request
        }
    }


    public void sendSupport() {
        if(request.isValidSupport()) {
            repository.contactUs(request); // send request
        }
    }

    public SettingsRepository getRepository() {
        return repository;
    }

}
