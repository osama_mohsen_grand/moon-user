
package grand.app.moon.viewmodels.chat;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.chat.ChatRequest;
import grand.app.moon.repository.ChatRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.VolleyFileObject;
import okhttp3.MultipartBody;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ChatViewModel extends ParentViewModel {

    ChatRepository chatRepository;
    public String text = "";
    public int id,type,receiver_id,id_chat;
    public boolean allowChat,firstChat;

    public ChatViewModel(int id, int type, boolean allowChat, boolean firstChat, int id_chat) {
        chatRepository = new ChatRepository(mMutableLiveData);
        this.id = id;
        this.id_chat = id_chat;
        this.type = type;
        this.allowChat = allowChat;
        this.firstChat = firstChat;
        if(id_chat != -1)
            chatRepository.getChatDetailsFromChatList(id_chat,type);
        else
            chatRepository.getChatDetails(id,type);
    }


    public void sendMessage(){
        Timber.e("actionText:"+text);
        if(!text.trim().equals("")) {
            Timber.e("action:sendMessage");
            chatRepository.send(new ChatRequest(id,receiver_id,text,type,firstChat),null,id_chat);
        }
    }


    public ChatRepository getChatRepository() {
        return chatRepository;
    }

    public void camera(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }

    public void setFile(VolleyFileObject volleyFileObject){
        text = "";
        chatRepository.send(new ChatRequest(id,receiver_id,text,type,firstChat),volleyFileObject,id_chat);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


    MultipartBody.Builder builder = null;
}
