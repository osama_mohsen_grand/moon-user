package grand.app.moon.viewmodels.order.details;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.order.Order;
import grand.app.moon.models.order.details.OrderProductDetails;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;


public class ItemOrderDetailsViewModel extends ParentViewModel {
    public OrderProductDetails order = null;
    private int position = 0;
    public String price,price_addition,count;
    public boolean haveAddition = false,haveColor = false,haveSize=false,haveSpecialRequest=false;

    public ItemOrderDetailsViewModel(OrderProductDetails order, int position) {
        this.order = order;
        this.position = position;
        this.price = order.price+" "+UserHelper.retrieveCurrency();
        this.count = order.qty+"x";
        if( order.additions != null && !order.additions.equals("")) {
            haveAddition = true;
            price_addition = order.price_addition +" "+ UserHelper.retrieveCurrency();
        }
        if(order.color != null && !order.color.equals("")){
            haveColor = true;
        }
        if(order.size != null && !order.size.equals("")){
            haveSize = true;
        }
        if(order.specialRequest!=null && !order.specialRequest.trim().equals("")){
            haveSpecialRequest = true;
        }
    }


    public void submit(){
        mMutableLiveDataBaseObservable.setValue(position);
    }


}
