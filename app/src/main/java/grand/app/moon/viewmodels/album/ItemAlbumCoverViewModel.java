package grand.app.moon.viewmodels.album;


import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.AppHelper;

public class ItemAlbumCoverViewModel extends ParentViewModel {
    public ImageVideo imageVideo;
    public int position = 0;
    public String photos,videos;

    private static final String TAG = "ItemAlbumCoverViewModel";

    public ItemAlbumCoverViewModel(ImageVideo imageVideo, int position) {
        this.position = position;
        this.imageVideo = imageVideo;
        photos = ResourceManager.getString(R.string.photos)+" "+imageVideo.count;
        videos = ResourceManager.getString(R.string.videos)+" "+imageVideo.videoCount;
    }

    public void submit() {
        mMutableLiveData.setValue(position);
    }
}
