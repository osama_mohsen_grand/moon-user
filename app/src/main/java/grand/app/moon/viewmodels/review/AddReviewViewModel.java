package grand.app.moon.viewmodels.review;

import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.rate.RateRequest;
import grand.app.moon.models.review.AddReviewRequest;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.repository.ReviewRepository;
import grand.app.moon.repository.TripActionRepository;
import grand.app.moon.utils.images.ImageLoaderHelper;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddReviewViewModel extends ParentViewModel {
    public AddReviewRequest addReviewRequest;
    ReviewRepository reviewRepository;
    //tripDetailsResponse.data.user.name

    public AddReviewViewModel(int shopId, int type_id) {
        reviewRepository = new ReviewRepository(mMutableLiveData);
        addReviewRequest = new AddReviewRequest(shopId, type_id, 0);
    }

    public void submit() {
        reviewRepository.addReview(addReviewRequest);
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        addReviewRequest.setRate(rating);
    }

    public ReviewRepository getReviewRepository() {
        return reviewRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}