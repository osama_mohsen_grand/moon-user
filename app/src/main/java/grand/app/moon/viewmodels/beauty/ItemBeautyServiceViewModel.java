package grand.app.moon.viewmodels.beauty;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;


public class ItemBeautyServiceViewModel extends ParentViewModel {

    public IdNamePrice service;
    public int position;
    public String price;
    public ObservableBoolean isChecked;

    public ItemBeautyServiceViewModel(IdNamePrice service , int position,boolean isChecked) {
        this.service = service;
        this.position = position;
        this.isChecked = new ObservableBoolean(isChecked);
        price = service.price+" "+ UserHelper.retrieveCurrency();
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
