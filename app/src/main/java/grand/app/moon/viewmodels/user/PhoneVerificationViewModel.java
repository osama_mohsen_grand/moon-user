
package grand.app.moon.viewmodels.user;

import android.util.Log;

import com.hbb20.CountryCodePicker;

import androidx.databinding.ObservableField;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moon.repository.LoginRepository;
import grand.app.moon.repository.VerificationFirebaseSMSRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class PhoneVerificationViewModel extends ParentViewModel {
    VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    public MutableLiveData<Object> mMutableLiveDataLogin = new MutableLiveData<>();
    LoginRepository loginRepository;

    public String cpp = "";
    private String phone = "";
    private String phoneCpp = "";
    public ObservableField phoneError;
    public ForgetPasswordRequest request = new ForgetPasswordRequest(cpp, phoneCpp);
    public PhoneVerificationViewModel() {
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        loginRepository = new LoginRepository(mMutableLiveDataLogin);
        phoneError = new ObservableField();
    }

    public static void setCpp(CountryCodePicker countryCodePicker, String text) {
        countryCodePicker.setCountryForNameCode(text);
    }

    private static final String TAG = "PhoneVerificationViewMo";

    private boolean isValid() {
        boolean valid = true;
        Log.d(TAG, cpp);
        Log.d(TAG, phoneCpp);
        if (!Validate.isValid(cpp + phone, Constants.PHONE)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("phone:error");
        }
        return valid;
    }

    public void verificationSubmit() {
        if (isValid()) {
            phoneCpp = cpp + phone;
            request.phone = phoneCpp;
            Timber.e("phone:" + phoneCpp);
            loginRepository.checkPhone(request);
        }
    }

    public void sendCode(FragmentActivity fragmentActivity) {
        verificationFirebaseSMSRepository.sendVerificationCode(phoneCpp, fragmentActivity);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
