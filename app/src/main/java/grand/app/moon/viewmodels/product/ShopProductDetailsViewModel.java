
package grand.app.moon.viewmodels.product;

import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.R;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.favourite.FavouriteRequest;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.repository.ProductRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.cart.CartViewModel;
import grand.app.moon.vollyutils.AppHelper;
import grand.app.moon.vollyutils.MyApplication;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ShopProductDetailsViewModel extends CartViewModel {
    public MutableLiveData<Object> shopMutableLiveData;
    ProductRepository productRepository;
    public ProductList product;
    public Service service;
    public String price = "",priceAfter="",priceAfterConst="0",currency=UserHelper.retrieveCurrency();
    public String[] images = new String[0];


    public ShopProductDetailsViewModel(ProductList product, Service service) {
        super(product,service);
        shopMutableLiveData = new MutableLiveData<>();
        productRepository = new ProductRepository(shopMutableLiveData);
        this.service = service;
        productRepository.getProductDetails(product.id,service.mFlag);
        this.product = product;
        setPrice(product.price);
    }

    public String[] getImages(){
        Log.d(TAG,"get images");
        if(productRepository.getProductResponse() != null) {
            Log.d(TAG,"get getProductResponse");
            if(productRepository.getProductResponse().data.images.size() == 0)
                productRepository.getProductResponse().data.images.add(new IdNameImage("-1",productRepository.getProductResponse().data.productImage));
            images = new String[productRepository.getProductResponse().data.images.size()];
            Log.d(TAG,"get images"+images.length);

            for (int i = 0; i < productRepository.getProductResponse().data.images.size(); i++)
                images[i] = productRepository.getProductResponse().data.images.get(i).image;
        }
        return images;
    }

    public void setPrice(String price){
        this.price = price + " "+ UserHelper.retrieveCurrency();
    }


    public void fav(){
        if (UserHelper.getUserId() != -1)
            productRepository.addFavourite(new FavouriteRequest(Integer.parseInt(product.id),service.mType));
        else
            Toast.makeText(MyApplication.getInstance(), ResourceManager.getString(R.string.please_login_to_complete_this_action), Toast.LENGTH_SHORT).show();
    }


    public void share(){
        shopMutableLiveData.setValue(Constants.SHARE);
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    private static final String TAG = "ShopProductDetailsViewM";
    
    public void setData() {
        setPrice(getProductRepository().getProductResponse().data.price);
        priceAfterConst = getProductRepository().getProductResponse().data.priceAfter;
        priceAfter = getProductRepository().getProductResponse().data.priceAfter+ " "+UserHelper.retrieveCurrency();
        if(priceAfterConst.equals("0")) {
            Log.d(TAG,"equal 0");
            priceAfter = price;
        }
        Log.d(TAG,"AfterConst"+priceAfterConst);
        Log.d(TAG,"price"+price);
        Log.d(TAG,"priceAfter"+priceAfter);
        notifyChange();
    }
}
