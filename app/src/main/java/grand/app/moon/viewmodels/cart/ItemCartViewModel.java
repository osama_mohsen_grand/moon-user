package grand.app.moon.viewmodels.cart;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.cart.Item;
import grand.app.moon.models.product.Color;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemCartViewModel extends ParentViewModel {
    public Item cart = null;
    public int position = 0;
    public String price = "",price_addition = "";

    public boolean haveAddition = false,haveColor = false,haveSize=false,haveSpecialRequest = false;

    public ItemCartViewModel(Item cart, int position) {
        this.cart = cart;
        this.position = position;
        price = cart.price+" "+ UserHelper.retrieveCurrency();
        if( cart.additions != null && !cart.additions.equals("")) {
            haveAddition = true;
            price_addition = cart.price_addition +" "+UserHelper.retrieveCurrency();
        }
        if(cart.color != null && !cart.color.equals("")){
            haveColor = true;
        }
        if(cart.size != null && !cart.size.equals("")){
            haveSize = true;
        }
        if(cart.specialRequest!=null && !cart.specialRequest.trim().equals("")){
            haveSpecialRequest = true;
        }
    }


    public void add(){
        int qty_tmp = Integer.parseInt(cart.qty);
        qty_tmp++;
        cart.qty_tmp = String.valueOf(qty_tmp);
        notifyChange();
        mMutableLiveData.setValue(new Mutable(Constants.ADD,position));
    }

    public void minus(){
        int qty_tmp = Integer.parseInt(cart.qty);
        if((qty_tmp-1) > 0) {
            qty_tmp--;
            cart.qty_tmp = String.valueOf(qty_tmp);
            notifyChange();
            mMutableLiveData.setValue(new Mutable(Constants.MINUS, position));
        }
    }

    public void delete(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }
}
