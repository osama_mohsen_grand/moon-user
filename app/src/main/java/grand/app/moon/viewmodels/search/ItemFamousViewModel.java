package grand.app.moon.viewmodels.search;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.famous.list.Famous;

public class ItemFamousViewModel extends ParentViewModel {
    public Famous famous;
    public String image;
    public int position;

    public ItemFamousViewModel(Famous famous, int position) {
        this.famous = famous;
        this.position = position;
        notifyChange();
    }

    public String getImageUrl(){
        return famous.getImage();
    }

    public void famousSubmit(){
        mMutableLiveData.setValue(position);
    }
}
