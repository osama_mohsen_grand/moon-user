package grand.app.moon.viewmodels.company;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.AdsCompanyModel;

public class ItemCompanyListViewModel extends ParentViewModel {
    public AdsCompanyModel adsCompanyModel ;
    public int position = 0;

    public int width = 0;

    public ItemCompanyListViewModel(AdsCompanyModel adsCompanyModel, int position) {
        this.adsCompanyModel = adsCompanyModel;
        this.position = position;
        notifyChange();
    }

    public String getImageUrl(){
        return adsCompanyModel.image;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
