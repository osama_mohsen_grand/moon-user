package grand.app.moon.viewmodels.clinic;

import android.util.Log;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.clinic.DoctorsList;
import grand.app.moon.utils.Constants;


public class ItemDoctorListViewModel extends ParentViewModel {

    public DoctorsList doctor;
    public int position;
//    public ObservableBoolean imageVisible = new ObservableBoolean(true);
//    private int layout;
//
//    public Drawable backgroundColor;
//
    public ItemDoctorListViewModel(DoctorsList doctor , int position) {
        this.doctor = doctor;
        this.position = position;
    }

    public void book(){
        Log.d("here change","here change");
        mMutableLiveData.setValue(new Mutable(Constants.BOOK_CLINIC,position));
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
