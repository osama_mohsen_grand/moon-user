package grand.app.moon.viewmodels.trucks;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.truck.TruckRequest;
import grand.app.moon.repository.taxi.TrucksRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

public class TrucksPickupFormViewModel extends ParentViewModel {
    private TrucksRepository trucksRepository;
    public TruckRequest truckRequest;
    public String price = "";
    public TrucksPickupFormViewModel(TruckRequest truckRequest) {
        this.truckRequest = truckRequest;
        price = truckRequest.total_price+ UserHelper.retrieveCurrency();
        trucksRepository = new TrucksRepository(mMutableLiveData);
    }

    public void onTypeChecked(){
        if(truckRequest.helper == 1)
            truckRequest.helper = 0;
        else
            truckRequest.helper = 1;
        Timber.e("helper:"+truckRequest.helper);
    }

    public void submit(){
        if(truckRequest.isValid()){
            trucksRepository.makeOrder(truckRequest);
        }
    }

    public TrucksRepository getTrucksRepository() {
        return trucksRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
