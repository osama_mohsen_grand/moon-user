
package grand.app.moon.viewmodels.ads;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.service.DiscoverRepository;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AdsViewModel extends ParentViewModel {

    public ObservableBoolean isFilter = new ObservableBoolean(false);
    private DiscoverRepository discoverRepository;
    public void init(){
        discoverRepository = new DiscoverRepository(mMutableLiveData);
    }
    public AdsViewModel(Service service, String service_id) {
        init();
        discoverRepository.getDiscover(service_id,service.mType,service.mFlag);
    }

    public AdsViewModel(){
        init();
    }

    public void getDiscover(){
        discoverRepository.getAllDiscoverWithServices();
    }

    public void filter(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public DiscoverRepository getDiscoverRepository() {
        return discoverRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setType(String type){
        Log.d("TYPEADS",type);
        if(type.equals(Constants.ADS)){
            Log.d("TYPEADS","DONE");
            isFilter.set(true);
            notifyChange();
        }
    }

}
