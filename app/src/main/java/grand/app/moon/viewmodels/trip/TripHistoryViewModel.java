
package grand.app.moon.viewmodels.trip;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.R;
import grand.app.moon.repository.TripHistoryRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class TripHistoryViewModel extends ParentViewModel {

    TripHistoryRepository tripHistoryRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.ride_history));
    public ObservableBoolean noData = new ObservableBoolean(false);
    public TripHistoryViewModel(String type) {
        tripHistoryRepository = new TripHistoryRepository(mMutableLiveData);
        tripHistoryRepository.getTrips(type);
    }

    public TripHistoryRepository getTripHistoryRepository() {
        return tripHistoryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
