
package grand.app.moon.viewmodels.review;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.ReviewRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ReviewViewModel extends ParentViewModel {

    ReviewRepository reviewRepository;
    private int shop_id,type;
    public ObservableBoolean allowAdd = new ObservableBoolean(true);

    public ReviewViewModel(int shop_id,int type){
        reviewRepository = new ReviewRepository(mMutableLiveData);
        this.shop_id = shop_id;
        this.type = type;
        if(type == 0) allowAdd.set(false);
        callService();
    }

    public void callService(){
        reviewRepository.getReviews(shop_id,type);
    }

    public ReviewRepository getReviewRepository() {
        return reviewRepository;
    }

    public void add(){
        mMutableLiveData.setValue(Constants.ADD_REVIEW);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
