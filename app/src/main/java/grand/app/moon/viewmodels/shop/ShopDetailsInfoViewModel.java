
package grand.app.moon.viewmodels.shop;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.BindingAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.famous.details.FamousDetailsResponse;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.repository.FamousRepository;
import grand.app.moon.repository.SettingsRepository;
import grand.app.moon.repository.ShopRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.BaseActivity;


public class ShopDetailsInfoViewModel extends ParentViewModel {

    public AccountInfoResponse response = new AccountInfoResponse();
    private SettingsRepository repository;
    public int id; public String followers= "";

    public ShopDetailsInfoViewModel(int id) {
        Log.d("account_info","ShopDetailsInfoViewModel:"+id);
        repository = new SettingsRepository(mMutableLiveData);
        this.id = id;
        repository.accountInfo(id);
    }

    public String getFollowerCount(){
        return followers = response.followers + " " + ResourceManager.getString(R.string.followers);
    }

    public void setFollowerCount(String followers) {
        this.followers = followers;
    }

    public SettingsRepository getRepository() {
        return repository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
