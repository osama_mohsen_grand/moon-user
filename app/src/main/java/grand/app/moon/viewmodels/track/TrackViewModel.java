
package grand.app.moon.viewmodels.track;

import android.widget.ImageView;
import android.widget.RatingBar;

import com.google.maps.model.LatLng;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.trip.TripActionRequest;
import grand.app.moon.repository.TripActionRepository;
import grand.app.moon.repository.TripRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class TrackViewModel extends ParentViewModel {

    int trip_id = -1;
    private static final String TAG = "TrackViewModel";
    public ObservableField<LatLng>  latLng = new ObservableField<>();
    public ObservableBoolean showDriverInfoDialog = new ObservableBoolean(false);
    public ObservableBoolean showDistanceDialog = new ObservableBoolean(false);
    public ObservableBoolean allowCallDriver = new ObservableBoolean(false);
    public ObservableBoolean cancelTripDialog = new ObservableBoolean(false);

    public String type;
    public String price =  "";

    public TripActionRequest tripActionRequest;
    public MapConfig mapConfig;
    TripRepository tripRepository;
    TripActionRepository tripActionRepository;

    public TrackViewModel(int trip_id) {
        this.trip_id = trip_id;
        tripActionRequest = new TripActionRequest(trip_id);
        tripRepository = new TripRepository(mMutableLiveData);
        tripActionRepository = new TripActionRepository(mMutableLiveData);
        tripDetails();
    }


    public void showDialogDriver(){
        showDriverInfoDialog.set(true);
        notifyChange();
    }

    public void showDistanceDialog(){
        showDriverInfoDialog.set(false);
        showDistanceDialog.set(true);
        notifyChange();
    }



    @Bindable
    public String getUserImageUrl(){
        if(getTripRepository() != null && getTripRepository().getTripDetailsResponse() != null && getTripRepository().getTripDetailsResponse().data != null)
            return getTripRepository().getTripDetailsResponse().data.image;
        return "";
    }

    @BindingAdapter("userImageUrl")
    public static void loadImageUser(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
    }



    @Bindable
    public String getCarImageUrl(){
        if(getTripRepository() != null && getTripRepository().getTripDetailsResponse() != null && getTripRepository().getTripDetailsResponse().data != null){
            return getTripRepository().getTripDetailsResponse().data.image;
        }
        return "";
    }

    @BindingAdapter("carImageUrl")
    public static void loadImageCar(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
    }


    @Bindable
    public String getType(){
//        if(getTripRepository() != null && getTripRepository().getTripDetailsResponse() != null && getTripRepository().getTripDetailsResponse().data != null){
//            if(getTripRepository().getTripDetailsResponse().data.type.equals("1"))
//                return ResourceManager.getString(R.string.cash);
//            else
//                return ResourceManager.getString(R.string.online);
//        }
        return "";
    }

    public void callCaption(){
        getMutableLiveData().setValue(Constants.CALL);
    }

    @Bindable
    public Float getRating(){
        if(getTripRepository() != null && getTripRepository().getTripDetailsResponse() != null && getTripRepository().getTripDetailsResponse().data != null)
            return getTripRepository().getTripDetailsResponse().data.rate;
        return 0f;
    }

    public void updateUi(){
        price = getTripRepository().getTripDetailsResponse().data.price + " "+ UserHelper.retrieveCurrency();
        Timber.e("status:"+getTripRepository().getTripDetailsResponse().data.status);
        if(getTripRepository().getTripDetailsResponse().data.status != 2 &&
                getTripRepository().getTripDetailsResponse().data.status != 5 &&
                getTripRepository().getTripDetailsResponse().data.status != 6){
            allowCallDriver.set(true);
        }else
            allowCallDriver.set(false);
        notifyChange();
    }


    @BindingAdapter("rating")
    public static void setRating(RatingBar rating , Float rate){
        rating.setRating(rate);
    }

    public void KeepTripSubmit(){
        showDriverInfoDialog.set(true);
        cancelTripDialog.set(false);
    }

    public void showCancelDialog(){
        showDriverInfoDialog.set(false);
        cancelTripDialog.set(true);
    }

    public void cancelSubmit(){
        tripActionRequest.status = 5;
        tripRepository.tripAction(tripActionRequest);
    }

    public void tripDetails(){
        tripRepository.getDetails(trip_id);
    }

    public TripRepository getTripRepository() {
        return tripRepository;
    }

    public TripActionRepository getTripActionRepository() {
        return tripActionRepository;
    }

    public void chatSubmit(){
        mMutableLiveData.setValue(Constants.CHAT);
    }

}
