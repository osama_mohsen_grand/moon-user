
package grand.app.moon.viewmodels.discover;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.adapter.AlbumAdapter;
import grand.app.moon.adapter.discover.DiscoverCategoryAdapter;
import grand.app.moon.adapter.discover.DiscoverServiceAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.discover.DiscoverService;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ServiceListResponse;
import grand.app.moon.repository.service.DiscoverRepository;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class DiscoverMainViewModel extends ParentViewModel {

    public DiscoverServiceAdapter discoverServiceAdapter;
    public DiscoverCategoryAdapter discoverCategoryAdapter;
    public AlbumAdapter albumAdapter = new AlbumAdapter(new ArrayList<>(), false);
    public DiscoverRepository discoverRepository;
    public List<ImageVideo> originalList = new ArrayList<>();
    public ObservableBoolean noData = new ObservableBoolean(false);
    public ObservableBoolean allServices = new ObservableBoolean(true);

    public ServiceListResponse serviceListResponse = null;
    public Service service = null;

    public void init() {
        discoverRepository = new DiscoverRepository(mMutableLiveData);
    }

    public DiscoverMainViewModel(Bundle arguments) {
        discoverServiceAdapter = new DiscoverServiceAdapter(new ArrayList<>());
        discoverCategoryAdapter = new DiscoverCategoryAdapter(new ArrayList<>());
        discoverRepository = new DiscoverRepository(mMutableLiveData);
        if(arguments != null && arguments.containsKey(Constants.SERVICES)){
            //from service details , have list of services , with no categories
            allServices.set(false);
            serviceListResponse = (ServiceListResponse) arguments.getSerializable(Constants.SERVICES);
            service = (Service) arguments.getSerializable(Constants.SERVICE);
            DiscoverService discoverService = new DiscoverService();
            discoverService.id = service.mId;
            discoverService.type = service.mType;
            discoverService.flag = service.mFlag;
            discoverServiceAdapter.selected = 0;
            for(Service service : serviceListResponse.mServices){
                discoverService.categories.add(new IdName(service.mId+"",service.mName));
            }
            discoverRepository.mainDiscoverResponse.data.add(discoverService);
            discoverServiceAdapter.update(discoverRepository.mainDiscoverResponse.data);
            discoverCategoryAdapter.update(discoverService.categories);
            if(discoverService.categories.size() > 0)
                getDiscoverAll(Integer.parseInt(discoverService.categories.get(0).id),service.mType,service.mFlag);
        }else
            discoverRepository.getAllDiscoverWithServices();
    }

    public DiscoverRepository getDiscoverRepository() {
        return discoverRepository;
    }

    public void setData() {
        discoverServiceAdapter.update(discoverRepository.mainDiscoverResponse.data);
        if (discoverRepository.mainDiscoverResponse.data.size() > 0)
            discoverCategoryAdapter.update(discoverRepository.mainDiscoverResponse.data.get(0).categories);
//        getDiscoverRepository().getAlbumResponse().data = new ArrayList<>(discoverRepository.mainDiscoverResponse.discover);
//        discoverRepository.mainDiscoverResponse.discover
        setOriginalList(discoverRepository.mainDiscoverResponse.discover);

    }

    private static final String TAG = "DiscoverMainViewModel";
    public void setAlbum() {
        Log.d(TAG, "setAlbum: ");
        if (getDiscoverRepository().getAlbumResponse().data.size() > 0) {
            Log.d(TAG, "size: "+getDiscoverRepository().getAlbumResponse().data.size());
            originalList = getDiscoverRepository().getAlbumResponse().data;
            setOriginalList(getDiscoverRepository().getAlbumResponse().data);
            Log.d(TAG,"originalList data: "+originalList.size());
        }
        noData.set(getDiscoverRepository().getAlbumResponse().data.size() == 0);
//        notifyChange();
    }

    private void setOriginalList(List<ImageVideo> list){
        albumAdapter.clear();
        albumAdapter.update(list);
    }

    public void getDiscoverAll(int id, int type, int flag) {
        discoverRepository.getAllDiscover(id, type, flag);
    }
}
