package grand.app.moon.viewmodels.ads;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.famous.details.AlbumModel;
import grand.app.moon.utils.resources.ResourceManager;

public class ItemCompanyAdsViewModel extends ParentViewModel {
    public AlbumModel model ;
    public int position = 0;
    public boolean selected = false;
    public int width = 0;
    public String count;                               

    public ItemCompanyAdsViewModel(AlbumModel model, int position) {
        this.model = model;
        this.position = position;
        count = model.shopsCount + " "+ResourceManager.getString(R.string.shop);
    }

    public void submit(){
        getMutableLiveData().setValue(position);
    }
}
