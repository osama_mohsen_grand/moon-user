package grand.app.moon.viewmodels.discover;

import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNameImage;

public class ItemDiscoverCategoryViewModel extends ParentViewModel {
    public IdName model;
    public int position = 0;
    public boolean checked;

    public ItemDiscoverCategoryViewModel(IdName model, int position, boolean checked) {
        this.model = model;
        this.position = position;
        this.checked = checked;
    }


    public void submit() {
        mMutableLiveData.setValue(position);
    }
}
