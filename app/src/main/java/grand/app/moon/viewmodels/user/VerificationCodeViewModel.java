package grand.app.moon.viewmodels.user;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.user.activation.VerificationResponse;
import grand.app.moon.repository.RegisterRepository;
import grand.app.moon.repository.VerificationCodeRepository;
import grand.app.moon.repository.VerificationFirebaseSMSRepository;
import grand.app.moon.utils.Constants;

import grand.app.moon.models.user.activation.VerificationRequest;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class VerificationCodeViewModel extends ParentViewModel {

    public VerificationRequest activeRequest = new VerificationRequest();
    String verify_id;
    VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    RegisterRepository registerRepository;
    public VerificationCodeViewModel(String verify_id) {
        this.verify_id = verify_id;
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        registerRepository = new RegisterRepository(mMutableLiveData);
    }

    public void verificationSubmit(){
        if(activeRequest.isValid()) {
            verificationFirebaseSMSRepository.verifyCode(verify_id,activeRequest.code);
        }
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }

    public RegisterRepository getRegisterRepository() {
        return registerRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}