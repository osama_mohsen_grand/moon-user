
package grand.app.moon.viewmodels.app;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.PrivacyRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class PrivacyViewModel extends ParentViewModel {

    public ObservableBoolean visible = new ObservableBoolean(false);
    PrivacyRepository privacyRepository;
    String text = "";

    public PrivacyViewModel() {
        privacyRepository = new PrivacyRepository(mMutableLiveData);
    }


    @Bindable
    public String getText() {
        return text;
    }

    public void setText(String text){
        this.text = text;
    }

    public void setText() {
        this.text = getPrivacyRepository().getPrivacyTextResponse().data.translation.name;
        setText(this.text);
        visible.set(true);
        notifyChange();
    }

    public PrivacyRepository getPrivacyRepository() {
        return privacyRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
