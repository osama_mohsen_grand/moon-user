
package grand.app.moon.viewmodels.explore;

import android.util.Log;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.adapter.AlbumAdapter;
import grand.app.moon.adapter.ExploreAdapter;
import grand.app.moon.adapter.discover.DiscoverCategoryAdapter;
import grand.app.moon.adapter.discover.DiscoverServiceAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.explore.Explore;
import grand.app.moon.repository.CommonRepository;
import grand.app.moon.repository.service.DiscoverRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ExploreViewModel extends ParentViewModel {

    private static final String TAG = "ExploreViewModel";
    public ExploreAdapter adapter;
    private DiscoverRepository discoverRepository;
    public MutableLiveData<Object> mMutableLiveDataCommon  = new MutableLiveData<>();;
    public CommonRepository commonRepository;

    public ExploreViewModel(int shop_id , int category_id , int type , int flag) {
        adapter = new ExploreAdapter(new ArrayList<>());
        discoverRepository = new DiscoverRepository(mMutableLiveData);
        commonRepository = new CommonRepository(mMutableLiveDataCommon);
        discoverRepository.getShopsDiscover(shop_id,category_id,type,flag);
        adapter.setCommonRepository(commonRepository);
    }

    public DiscoverRepository getDiscoverRepository() {
        return discoverRepository;
    }

    public void setData(int shop_id) {
        int counter = -1;
        for(Explore explore : discoverRepository.exploreResponse.data){
            counter++;
            if(explore.shopId == shop_id){
                break;
            }
        }
        Log.d(TAG,"position: "+counter);
        Log.d(TAG,"position: "+discoverRepository.exploreResponse.data.get(counter).shopId );
        if(counter > -1 && counter < discoverRepository.exploreResponse.data.size() && discoverRepository.exploreResponse.data.get(counter).shopId == shop_id){
            discoverRepository.exploreResponse.data.add(0,discoverRepository.exploreResponse.data.get(counter));
            discoverRepository.exploreResponse.data.remove(counter+1);
        }
        adapter.update(discoverRepository.exploreResponse.data);
    }

    public void getDiscoverAll(int id,int type,int flag) {
        discoverRepository.getAllDiscover(id,type,flag);
    }
}
