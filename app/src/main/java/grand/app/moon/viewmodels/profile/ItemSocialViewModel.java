package grand.app.moon.viewmodels.profile;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.personalnfo.Profile;


public class ItemSocialViewModel extends ParentViewModel {
    public Profile model = null;
    private int position = 0;

    public ItemSocialViewModel(Profile model, int position) {
        this.model = model;
        this.position = position;
    }

    public void submit(){
        getMutableLiveData().setValue(position);
    }

}
