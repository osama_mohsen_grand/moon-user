package grand.app.moon.viewmodels.explore;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.BindingAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.explore.Explore;
import grand.app.moon.models.famous.details.FamousDetailsResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.moon.MoonHelper;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;
import grand.app.moon.views.fragments.story.StoryFragment;
import grand.app.moon.vollyutils.AppHelper;


public class ItemExploreViewModel extends ParentViewModel {
    public Explore model = null;
    public int position = 0;
    public String[] images = new String[0];

    public ItemExploreViewModel(Explore model, int position) {
        this.model = model;
        this.position = position;
    }

    public void follow(){
        if (UserHelper.getUserId() == -1) {
            baseError = ResourceManager.getString(R.string.please_login_first);
            mMutableLiveData.setValue(Constants.ERROR);
        } else
            mMutableLiveData.setValue(Constants.FOLLOW);
    }

    ArrayList<ImageVideo> imageVideos = new ArrayList<>();

    public List<ImageVideo> getImages() {
        return model.discover;
    }

    public Explore getStory() {
        return model;
    }

    public Explore getShopSubmit(){
        return model;
    }


    @BindingAdapter("story")
    public static void setStory(CircleImageView circleImageView, Explore response) {
        if (response.isStory == 1) {
            circleImageView.setBorderColor(ResourceManager.getColor(R.color.multiple_image_select_primary));
            circleImageView.setBorderWidth(8);
            circleImageView.setOnClickListener(view -> {
                Intent intent = new Intent(circleImageView.getContext(), BaseActivity.class);
                intent.putExtra(Constants.PAGE, StoryFragment.class.getName());
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.STORY, response.story);
                intent.putExtra(Constants.BUNDLE, bundle);
                intent.putExtra(Constants.NAME_BAR, response.story.mName);
                circleImageView.getContext().startActivity(intent);
            });
        }
    }

    @BindingAdapter("txt_shop_submit")
    public static void setShopSubmit(AppCompatTextView textView, Explore model) {
//        if (model.isStory == 1) {
//
//        }
        textView.setOnClickListener(view -> {
            if(model.workDays != 0) MoonHelper.shopDetails(textView.getContext(),model.type,model.flag,model.shopId,model.shopName);
            else
                Toasty.info(textView.getContext(), ResourceManager.getString(R.string.this_shop_is_close_now),Toasty.LENGTH_SHORT).show();
        });

    }



}
