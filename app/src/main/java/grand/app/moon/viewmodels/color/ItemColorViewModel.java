package grand.app.moon.viewmodels.color;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.product.Color;

public class ItemColorViewModel extends ParentViewModel {
    public Color color = null;
    public int position = 0;
    public boolean selected = false;

    public ItemColorViewModel(Color color, int position , boolean selected) {
        this.color = color;
        this.position = position;
        this.selected = selected;
    }

    public void colorSubmit(){
        mMutableLiveData.setValue(position);
    }
}
