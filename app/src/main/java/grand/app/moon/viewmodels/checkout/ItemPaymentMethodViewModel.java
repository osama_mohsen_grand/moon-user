package grand.app.moon.viewmodels.checkout;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNameImage;


public class ItemPaymentMethodViewModel extends ParentViewModel {
    public IdNameImage model ;
    public int position = 0;
    public ObservableBoolean selected = new ObservableBoolean(false);
    public int width = 0;

    public ItemPaymentMethodViewModel(IdNameImage model, int position, boolean selected) {
        this.model = model;
        this.position = position;
        this.selected.set(selected);
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
