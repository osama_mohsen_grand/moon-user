package grand.app.moon.viewmodels.ads;


import android.graphics.drawable.Drawable;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemAdsCompanyViewModel extends ParentViewModel {
    public AdsCompanyModel adsCompanyModel ;
    public int position = 0;
    public int width = 0;
    private String priceTotal;

    public ItemAdsCompanyViewModel(AdsCompanyModel adsCompanyModel, int position) {
        this.adsCompanyModel = adsCompanyModel;
        this.priceTotal = adsCompanyModel.price + UserHelper.retrieveCurrency();
        this.position = position;
        notifyChange();
    }

    public String getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(String priceTotal) {
        this.priceTotal = priceTotal;
    }


    public String getImageUrl(){
        return adsCompanyModel.image;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
