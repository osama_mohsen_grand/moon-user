package grand.app.moon.viewmodels.notification;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.notifications.Notification;

public class ItemNotificationViewModel extends ParentViewModel {
    public Notification notification = null;
    public String image;
    public int position = 0;

    public ItemNotificationViewModel(Notification notification, int position) {
        this.notification = notification;
        this.position = position;
        notifyChange();
    }

    public void delete(){
        mMutableLiveData.setValue(position);
    }
}
