package grand.app.moon.viewmodels.country;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.country.Datum;
public class ItemCountryViewModel extends ParentViewModel {
    private Datum country = null;
    private int position = 0;
    private boolean selected;
    public ItemCountryViewModel(Datum country, int position,boolean selected) {
        this.country = country;
        this.position = position;
        this.selected = selected;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public String getImageUrl(){
        return country.countryImage;
    }

    public Datum getCountry() {
        return country;
    }

    public boolean isSelected() {
        return selected;
    }
}
