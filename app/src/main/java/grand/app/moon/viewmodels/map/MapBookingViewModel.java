
package grand.app.moon.viewmodels.map;


import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.Place;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.truck.TruckRequest;
import grand.app.moon.repository.taxi.TrucksRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import timber.log.Timber;


public class MapBookingViewModel extends ParentViewModel {
    //legs : 5.3 km , time 12 minutes
    private static final String TAG = "HomeViewModel";
    private TrucksRepository trucksRepository;

    public int value = -1;
    public MapConfig mapConfig = null;
    public ObservableBoolean priceVisible = new ObservableBoolean(false);
    public ObservableBoolean destError = new ObservableBoolean(false);
    private String pickup = "";
    //init 0=>pickup , 1=>first-dest , 2=>second-dest , 3=>third-dest
    ArrayList<Marker> markers = new ArrayList<>(Arrays.asList(null,null,null,null));
    public String dest_text_1 = "", dest_text_2 = "", dest_text_3 = "";
    public String tripPrice = "";

    public ArrayList<LatLng> markers_locations = new ArrayList<>(Arrays.asList(null,null,null,null));
    public String type = "";
    //server request
    public int priceKM_per_kilo = 0,start_counter = 0;

    public TruckRequest truckRequest;

    public String submitText = ResourceManager.getString(R.string.confirm_pickup);

    public ObservableBoolean isSchedule = new ObservableBoolean(false);

    public ObservableBoolean pickupSelected = new ObservableBoolean(false);
    public ObservableBoolean plus_new_dest = new ObservableBoolean(true);
    public ObservableBoolean destination_2_visible = new ObservableBoolean(false);
    public ObservableBoolean destination_3_visible = new ObservableBoolean(false);

    public MapBookingViewModel() {
        trucksRepository = new TrucksRepository(mMutableLiveData);
        truckRequest = new TruckRequest();
    }


//    @BindingAdapter("imageUrl")
//    public static void loadImage(ImageView imageView , String imageUrl){
//        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),imageUrl,imageView);
//    }


    public void pickupSubmit() {
        mMutableLiveData.setValue(Constants.PICKUP_SUBMIT);
    }

    public void destSubmit(int dest) {
        value = dest;
        mMutableLiveData.setValue(Constants.DEST_SUBMIT);
    }


    public void setDest(Place place) {
        if (value > -1 && place != null) {
            //first marker is pickup
            pickupSelected.set(true);//remove marker center screen
            LatLng latLng = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
            if (markers.get(value) != null) markers.get(value).remove();
            markers_locations.set(value,place.getLatLng());
            addMarkerOnMap(markers_locations.get(0),R.drawable.ic_map_pin,pickup,0);//draw marker pickup
            addMarkerOnMap(latLng,R.drawable.ic_marker_dest,place.getName(),value);//draw marker destination

            String location = new MapConfig(MyApplication.getInstance(), null).getAddress(place.getLatLng().latitude, place.getLatLng().longitude);
            if(location.equals(ResourceManager.getString(R.string.your_location))) {
                setDest(place.getName());
            }else{
                setDest(location);
            }

            moveCamera(place.getLatLng());
            notifyChange();
        }
    }

    public void moveCamera(LatLng latLng) {
        mapConfig.getGoogleMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
    }

    public void setDest(String dest) {
        Log.e(TAG, "setDest: "+dest );
        Log.e(TAG, "setDest_value: "+value );
        if (value == 1){
            dest_text_1 = dest;
            destError.set(false);//remove error if exist
        }
        else if (value == 2) dest_text_2 = dest;
        else if (value == 3) dest_text_3 = dest;

        notifyChange();
    }

    public void scheduleSubmit() {
        mMutableLiveData.setValue(Constants.SCHEDULE);
    }


    public void addNewDest() {
        if (!destination_2_visible.get()) destination_2_visible.set(true);
        else if (!destination_3_visible.get()) {
            destination_3_visible.set(true);
            plus_new_dest.set(false);
        }
        notifyChange();

    }



    public void removeDest(int dest) {
        if (dest == 2){
            destination_2_visible.set(false);
            dest_text_2 = "";
        }
        else if (dest == 3){
            destination_3_visible.set(false);
            dest_text_3 = "";
        }
        plus_new_dest.set(true);// show plus
        if(markers.get(dest) != null) markers.get(dest).remove(); // remove marker on map
        markers.set(dest,null);//remove marker
        markers_locations.set(dest,null);//remove lat-lng
        resetDirection();
        notifyChange();
    }

    public void removeSchedule() {
        truckRequest.type = Constants.URGENT;
        truckRequest.date = null;
        truckRequest.time = null;
        isSchedule.set(false);
        notifyChange();
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        notifyChange();
    }

    public void setPickupLocation(LatLng latLng) {
        String address = mapConfig.getAddress(latLng.latitude, latLng.longitude);
        Log.e(TAG, "setPickupLocation: "+latLng.latitude+","+latLng.longitude );
        markers_locations.set(0,latLng);
        setPickup(address);//fetch address
        if(pickupSelected.get()) addMarkerOnMap(markers_locations.get(0),R.drawable.ic_map_pin,address,0);//draw before
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
        notifyChange();
    }

    public void setPickUp(Place place) {
        pickupSelected.set(false);
        if(markers.get(0) != null){
            markers.get(0).remove();
            markers.set(0,null);
            resetDirection();
        }
        markers_locations.set(0,place.getLatLng());
        String location = new MapConfig(MyApplication.getInstance(), null).getAddress(place.getLatLng().latitude, place.getLatLng().longitude);
        if(location.equals(ResourceManager.getString(R.string.your_location))) {
            setPickup(place.getName());
        }else{
            setPickup(location);
        }
        mapConfig.moveCamera(place.getLatLng());
    }


    public void resetDirection(){
        submitText = ResourceManager.getString(R.string.confirm_pickup);//make button text to order new
        mapConfig.removeRoute();//remove route
        priceVisible.set(false);
    }

    public void addMarkerOnMap(LatLng latLng , int drawable , String title , int value ){
        Log.e(TAG, "addMarkerOnMap: "+title );
        if (markers.get(value) != null) markers.get(value).remove();
        Marker marker = mapConfig.getGoogleMap().addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(drawable))
                .title(title));
        markers.set(value,marker);
        Log.e(TAG, "addMarkerOnMap: "+marker.getPosition().latitude +" , "+ marker.getPosition().longitude +" , " + marker.getTitle()+" " );
        resetDirection();
        notifyChange();
    }

    public void clearMap(){
        resetDirection();
        for(Marker marker: markers) {
            if (marker != null) marker.remove();
        }
        dest_text_1 = "";
        dest_text_2 = "";
        dest_text_3 = "";
        truckRequest.date = "";
        truckRequest.time = "";
        removeDest(2);
        removeDest(3);
        priceVisible.set(false);
        removeSchedule();
        mapConfig.checkLastLocation();
        notifyChange();
    }


    //order_new
//    OrderRequest orderRequest ;
    public void orderSubmit() {
        if(dest_text_1.equals("")){
            destError.set(true);
        }else {
            destError.set(false);//remove error in destination
            pickupSelected.set(true); //remove center pin
            if(submitText.equals(ResourceManager.getString(R.string.confirm_pickup))){//draw route
                submitText = ResourceManager.getString(R.string.confirm);
                if(markers.get(0) == null){
                    addMarkerOnMap(markers_locations.get(0),R.drawable.ic_map_pin,getPickup(),0);
                }
                priceVisible.set(true);
                mMutableLiveData.setValue(Constants.ORDER);
            }else{//send order
                Timber.e("welcome here");
                String type = "urgent";
                if(truckRequest.trip_type.equals(Constants.TYPE_TRUCKS)) {
                    mMutableLiveData.setValue(Constants.CONFIRM);
                }else{
                    prepareLocationRequest();
                    trucksRepository.makeOrder(truckRequest);
                }
            }
        }
        notifyChange();
    }


    public void setPrice(double price){
        tripPrice = String.valueOf(price);
        truckRequest.total_price = tripPrice;
        priceVisible.set(true);
        notifyChange();
    }


    @Bindable
    public float getWeight(){
        if(priceVisible.get()) return 5f;
        return 10.05f;
    }

    @BindingAdapter("weight")
    public static void setWeight(LinearLayout linearLayout , float weight){
        Log.e(TAG, "setWeight: "+weight );
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT,
                weight
        );
        linearLayout.setLayoutParams(param);
    }

    public TrucksRepository getTrucksRepository() {
        return trucksRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void prepareLocationRequest() {
        truckRequest.lat.clear();
        truckRequest.lng.clear();
        truckRequest.address.clear();

        truckRequest.lat.add(markers_locations.get(0).latitude);
        truckRequest.lat.add(markers_locations.get(1).latitude);

        truckRequest.lng.add(markers_locations.get(0).longitude);
        truckRequest.lng.add(markers_locations.get(1).longitude);

        truckRequest.address.add(getPickup());
        truckRequest.address.add(dest_text_1);
    }
}
