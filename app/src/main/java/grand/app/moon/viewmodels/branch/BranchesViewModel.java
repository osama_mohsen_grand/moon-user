
package grand.app.moon.viewmodels.branch;

import java.util.ArrayList;

import grand.app.moon.R;
import grand.app.moon.adapter.BranchAccountInfoAdapter;
import grand.app.moon.adapter.ShopDepartmentAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.repository.ShopRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BranchesViewModel extends ParentViewModel {


    public BranchesViewModel() {
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
