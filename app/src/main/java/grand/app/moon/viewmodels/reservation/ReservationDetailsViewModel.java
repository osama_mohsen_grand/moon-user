package grand.app.moon.viewmodels.reservation;


import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.reservation.ReservationOrderActionRequest;
import grand.app.moon.models.reservation.detatils.ReservationDetailsModel;
import grand.app.moon.repository.ClinicRepository;
import grand.app.moon.repository.OrderRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ReservationDetailsViewModel extends ParentViewModel {
    public ReservationDetailsModel model;
    private ClinicRepository clinicRepository;
    private String specialist = "";
    private String price="";
    public ObservableBoolean isClinic = new ObservableBoolean(true);
    public ReservationOrderActionRequest reservationOrderActionRequest = new ReservationOrderActionRequest();
    public ReservationDetailsViewModel(int order_id,int reserved_type) {
        clinicRepository = new ClinicRepository(mMutableLiveData);
        clinicRepository.getReservationDetails(order_id,reserved_type);
        if(reserved_type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)){
            isClinic.set(false);
        }
    }

    public ClinicRepository getClinicRepository() {
        return clinicRepository;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price+" "+ UserHelper.retrieveCurrency();
    }

    public void confirm(){
        reservationOrderActionRequest.status = 1;
        reservationOrderActionRequest.order_id = model.orderId;
        clinicRepository.reservationOrderAction(reservationOrderActionRequest);
    }


    public void reject(){
        reservationOrderActionRequest.status = 0;
        reservationOrderActionRequest.order_id = model.orderId;
        clinicRepository.reservationOrderAction(reservationOrderActionRequest);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
