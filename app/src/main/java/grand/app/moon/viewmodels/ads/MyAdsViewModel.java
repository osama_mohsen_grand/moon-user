
package grand.app.moon.viewmodels.ads;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.adsCompanyFilter.AdsCompanyFilterResponse;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.repository.NotificationRepository;
import grand.app.moon.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class MyAdsViewModel extends ParentViewModel {

    AdsRepository adsRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.label_notification));
    private ObservableBoolean noData = new ObservableBoolean(false);

    public MyAdsViewModel() {
        adsRepository = new AdsRepository(mMutableLiveData);
        adsRepository.myAds();
    }

    public void noData(){
        noData.set(true);
    }

    public ObservableBoolean getNoData() {
        return noData;
    }

    public AdsRepository getAdsRepository() {
        return adsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void deleteAds(Integer id) {
        adsRepository.deleteAdvertisement(id);
    }
}
