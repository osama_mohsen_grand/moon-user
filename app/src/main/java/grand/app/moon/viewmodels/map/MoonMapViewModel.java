package grand.app.moon.viewmodels.map;

import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.repository.MoonMapRepository;
import grand.app.moon.utils.maputils.base.MapConfig;


public class MoonMapViewModel extends ParentViewModel {

    public MapConfig mapConfig;
    MoonMapRepository moonMapRepository;
    public int type = 0,flag = 0;
    public String service_id;
    public ArrayList<Integer> tags;
    public int tab = 1;
    public int filter;
    public MoonMapViewModel() {
        moonMapRepository = new MoonMapRepository(mMutableLiveData);
    }

    public void callService() {
        moonMapRepository.getStories();
    }

    public MoonMapRepository getMoonMapRepository() {
        return moonMapRepository;
    }

    public void reset() {
        moonMapRepository = null;
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}