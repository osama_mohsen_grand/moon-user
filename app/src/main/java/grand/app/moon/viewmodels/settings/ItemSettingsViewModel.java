package grand.app.moon.viewmodels.settings;


import android.view.View;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.settings.SettingsModel;
import grand.app.moon.models.settings.SettingsResponse;

public class ItemSettingsViewModel extends ParentViewModel {
    public SettingsModel model = null;
    public String image;
    public int position = 0;
    public ObservableBoolean messageVisible = new ObservableBoolean(false);

    public ItemSettingsViewModel(SettingsModel model, int position) {
        this.model = model;
        this.position = position;
    }

    public int rotation = 0;

    public void submit(View view) {
        rotation += 180;
        view.animate().rotation(rotation).setDuration(200).start();
        messageVisible.set(!messageVisible.get());
        notifyChange();
    }
}
