package grand.app.moon.viewmodels.followers;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.followers.Follower;
import grand.app.moon.models.product.Addition;
import grand.app.moon.utils.Constants;

public class ItemFollowerViewModel extends ParentViewModel {
    public Follower follower = null;
    public int position = 0;

    public ItemFollowerViewModel(Follower follower, int position) {
        this.follower = follower;
        this.position = position;
        notifyChange();
    }

    public void details(){
        mMutableLiveData.setValue(new Mutable(Constants.DETAILS,position));
    }
    public void unFollowSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.FOLLOW,position));
    }
    
}
