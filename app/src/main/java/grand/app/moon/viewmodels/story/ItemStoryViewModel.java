package grand.app.moon.viewmodels.story;


import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

//import com.devlomi.circularstatusview.CircularStatusView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.resources.ResourceManager;

public class ItemStoryViewModel extends ParentViewModel {
    public Story story ;
    public int position = 0;


    public ItemStoryViewModel(Story story, int position) {
        this.story = story;
        this.position = position;
        notifyChange();
    }

    public Story getImageUrl(){
        return story;
    }


    public void storySubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.STORY,position));
    }

    public void shopSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.SHOP,position));
    }

    private static final String TAG = "ItemStoryViewModel";
    @BindingAdapter("imageUrlSrcCustom")
    public static void loadImage(ImageView imageView, Story story) {
        if(story.mImage != null) {
            if (story.mImage.startsWith("http")) {

                ImageLoader.getInstance().displayImage(
                        story.mImage, imageView,
                        new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                imageView.setImageDrawable(ResourceManager.getDrawable(R.mipmap.ic_launcher));
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                Log.d(TAG,"loadData");
                                if (story.storyView == 0)
                                    imageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_gradient));
                                else
                                    imageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_silver));
                            }
                        }
                );


            } else if (!story.mImage.equals("")) {
                imageView.setImageResource(Integer.parseInt(story.mImage));
            }
        }
    }


//    @BindingAdapter("storyStatus")
//    public static void storyCircleStatus(CircularStatusView circularStatusView, Story story) {
//        if (story != null && story.mStories != null) {
//            circularStatusView.setPortionsColor(ResourceManager.getColor(R.color.colorPrimary));
//            circularStatusView.setPortionsCount(story.mStories.size());
//            if (story.storyViewCount < story.mStories.size()) {
//                int count = 0;
//                while (count < story.storyViewCount) {
//                    circularStatusView.setPortionColorForIndex(count, ResourceManager.getColor(R.color.colorSilver));
//                    count++;
//                }
//            }
//        }
//    }


}
