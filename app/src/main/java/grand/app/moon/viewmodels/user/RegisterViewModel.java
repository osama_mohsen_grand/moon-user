
package grand.app.moon.viewmodels.user;

import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.customviews.facebook.FacebookModel;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.models.user.register.RegisterUserRequest;
import grand.app.moon.repository.RegisterRepository;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import grand.app.moon.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class RegisterViewModel extends ParentViewModel {
    public static ObservableField<String> type = new ObservableField<>("");
    public RegisterUserRequest registerUserRequest ;
    RegisterRepository registerRepository;
    CountryRepository countryRepository;
    public StatusMsg statusMsg = null;
    public VolleyFileObject volleyFileObject;


    public RegisterViewModel(String phone) {
        registerUserRequest = new RegisterUserRequest();
        registerRepository = new RegisterRepository(mMutableLiveData);
        countryRepository = new CountryRepository(mMutableLiveData);
        countryRepository.getCountries(true);
        registerUserRequest.setPhone(phone);
        notifyChange();
    }

    public void selectImage() {
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }

    public void signUp() {
        if(registerUserRequest.isValid()) {
            if( volleyFileObject == null && !registerUserRequest.facebook_id.equals("")) {
                baseError = ResourceManager.getString(R.string.select_image_profile);
                mMutableLiveData.setValue(Constants.ERROR);
            }else {
                registerRepository.registerUser(registerUserRequest, volleyFileObject); // send request
            }
        }
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }



    public RegisterRepository getRegisterRepository() {
        return registerRepository;
    }

    public void login() {
        mMutableLiveData.setValue(Constants.LOGIN);

    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
    

    public void setImage(VolleyFileObject volleyFileObject) {
        this.volleyFileObject = volleyFileObject;
    }

    public void setAddress(double lat, double lng) {
        registerUserRequest.setLat(lat);
        registerUserRequest.setLng(lng);
        registerUserRequest.setAddress(new MapConfig(MyApplication.getInstance(),null).getAddress(lat,lng));
        notifyChange();
    }

    public void setSocial(FacebookModel facebookModel) {
        registerUserRequest.setName(facebookModel.name);
        registerUserRequest.setEmail(facebookModel.email);
        registerUserRequest.facebook_id = facebookModel.social_id;
        registerUserRequest.facebook_image = facebookModel.image;
        notifyChange();
    }
}
