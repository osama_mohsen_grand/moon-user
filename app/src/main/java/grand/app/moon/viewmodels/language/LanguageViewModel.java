
package grand.app.moon.viewmodels.language;

import android.content.Intent;

import grand.app.moon.R;
import grand.app.moon.base.IAnimationSubmit;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.LanguagesHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class LanguageViewModel extends ParentViewModel implements IAnimationSubmit {

    public int select = -1;
    private CountryRepository countryRepository;

    public String error = "";

    public LanguageViewModel(){
        if(LanguagesHelper.getCurrentLanguage().equals(Constants.LANGUAGE_EN))
            select = 1;
        else if(!UserHelper.retrieveKey(Constants.LANGUAGE_KEY).equals("")){
            select = Integer.parseInt(UserHelper.retrieveKey(Constants.LANGUAGE_KEY));
        }
    }

    public void onTypeChecked(int i) {
        // if it is a check. set the type
        select = i;
        countryRepository = new CountryRepository(mMutableLiveData);
        notifyChange();
    }

    public void countryIsCalled(){
        countryRepository.getCountries(true);
    }

    public LanguageViewModel getAnimationSubmit(){
        return this;
    }

    public String getLanguage() {
        String lng = "";
        if (select == -1) {
            lng = "";
        } else {
            if (select == 1) {
                lng = Constants.LANGUAGE_EN;
            } else if (select == 2) {
                lng = Constants.LANGUAGE_AR;
            } else if (select == 3) {
                lng = Constants.LANGUAGE_FR;
            } else if (select == 4) {
                lng = Constants.LANGUAGE_GR;
            } else if (select == 5) {
                lng = Constants.LANGUAGE_IT;
            } else if (select == 6) {
                lng = Constants.LANGUAGE_HY;

            } else if (select == 7) {
                lng = Constants.LANGUAGE_CS;

            } else if (select == 8) {
                lng = Constants.LANGUAGE_HI;

            } else if (select == 9) {
                lng = Constants.LANGUAGE_KO;

            } else if (select == 10) {
                lng = Constants.LANGUAGE_PL;

            } else if (select == 11) {
                lng = Constants.LANGUAGE_TR;

            } else if (select == 12) {
                lng = Constants.LANGUAGE_RU;

            } else if (select == 13) {
                lng = Constants.LANGUAGE_IND;

            } else if (select == 14) {
                lng = Constants.LANGUAGE_BN;

            } else if (select == 15) {
                lng = Constants.LANGUAGE_TH;

            } else if (select == 16) {
                lng = Constants.LANGUAGE_FIL;

            }
            UserHelper.saveKey(Constants.LANGUAGE_KEY,select+"");
        }
        return lng;
    }


    @Override
    public void animationSubmit() {
        if(!getLanguage().equals("")){
            mMutableLiveData.setValue(Constants.LANGUAGE);
        }else {
            error = ResourceManager.getString(R.string.please_choose_your_language);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
