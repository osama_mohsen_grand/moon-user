package grand.app.moon.viewmodels.settings;


import android.view.View;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.settings.Settings;
import grand.app.moon.models.settings.SettingsModel;
import grand.app.moon.utils.Constants;

public class ItemSettingsMainViewModel extends ParentViewModel {
    public Settings model = null;
    public String image;
    public int position = 0;
    public ObservableBoolean messageVisible = new ObservableBoolean(false);

    public ItemSettingsMainViewModel(Settings model, int position) {
        this.model = model;
        this.position = position;
    }

    public int rotation = 0;

    public void submit() {
        getMutableLiveData().setValue(new Mutable(Constants.SUBMIT,position));
    }
}
