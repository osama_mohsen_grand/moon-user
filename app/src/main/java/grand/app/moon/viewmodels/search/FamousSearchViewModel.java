
package grand.app.moon.viewmodels.search;

import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.search.SearchRepository;
import grand.app.moon.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousSearchViewModel extends ParentViewModel {

    int type;
    public SearchRepository searchRepository;
    public String text = "";

    public FamousSearchViewModel(int type) {
        searchRepository = new SearchRepository(mMutableLiveData);
        noDataText = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.result));
        this.type = type;
    }

    public void submitSearch(){
        searchRepository.famous(type,text);
    }

    public SearchRepository getSearchRepository() {
        return searchRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
