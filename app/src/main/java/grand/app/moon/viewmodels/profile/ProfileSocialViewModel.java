
package grand.app.moon.viewmodels.profile;

import android.util.Log;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.adapter.ServiceInfoAdapter;
import grand.app.moon.adapter.SocialAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.repository.NotificationRepository;
import grand.app.moon.repository.SettingsRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ProfileSocialViewModel extends ParentViewModel {

    int id = -1;
    public SettingsRepository repository;
    private ObservableBoolean noData = new ObservableBoolean(false);
    public AccountInfoResponse response = new AccountInfoResponse();
    public ServiceInfoAdapter adapter = new ServiceInfoAdapter(new ArrayList<>());;
    public SocialAdapter socialAdapter = new SocialAdapter(new ArrayList<>());;
    public ProfileSocialViewModel() {
        repository = new SettingsRepository(mMutableLiveData);
    }
    public ObservableBoolean getNoData() {
        return noData;
    }

    public void email(){
        mMutableLiveData.setValue(Constants.EMAIL);
    }

    public void number(){
        mMutableLiveData.setValue(Constants.PHONE);
    }

    public void website(){
        mMutableLiveData.setValue(Constants.WEB);
    }

    public void getData(){
        Log.d("account_info","ProfileSocialViewModel:"+id);
        repository.accountInfo(id);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setId(int id) {
        this.id = id;
    }

    private static final String TAG = "ProfileSocialViewModel";
    public void setResponse(AccountInfoResponse response) {
        Log.d(TAG,"response:");
        this.response = response;
        adapter.update(response.infoServices);
        socialAdapter.update(response.profile);
        Log.d(TAG,"profile:"+response.profile.size());
        notifyChange();
    }
}
