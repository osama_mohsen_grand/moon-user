
package grand.app.moon.viewmodels.user;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.user.changepassword.ChangePasswordRequest;
import grand.app.moon.repository.LoginRepository;
import grand.app.moon.utils.storage.user.UserHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ChangePasswordViewModel extends ParentViewModel {
    public ChangePasswordRequest changePasswordRequest;
    LoginRepository loginRepository;

    public ChangePasswordViewModel(String phone) {
        changePasswordRequest = new ChangePasswordRequest();
        if(!phone.equals("") && UserHelper.getUserId() != -1)
            phone = UserHelper.getUserDetails().phone;
        changePasswordRequest.setPhone(phone);
        loginRepository = new LoginRepository(mMutableLiveData);
    }

    public void submit(){
        if(changePasswordRequest.validateInput()){
            loginRepository.changePassword(changePasswordRequest);
        }
    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
