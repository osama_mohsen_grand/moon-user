package grand.app.moon.viewmodels.discover;

import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.discover.DiscoverService;

public class ItemDiscoverServiceViewModel extends ParentViewModel {
    public DiscoverService model;
    public int position = 0;
    public final boolean checked;

    public ItemDiscoverServiceViewModel(DiscoverService model, int position, boolean checked) {
        this.model = model;
        this.position = position;
        this.checked = checked;
    }


    public void submit() {
        mMutableLiveData.setValue(position);
    }
}
