package grand.app.moon.viewmodels.reservation;


import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.ClinicRepository;
import grand.app.moon.utils.Constants;
import timber.log.Timber;

public class ClinicListViewModel extends ParentViewModel {
    private ClinicRepository clinicRepository;
    public int type = 0,flag = 0;
    public String service_id;
    public ArrayList<Integer> tags;
    public int tab = 1;
    public int filter;
    public int followPosition = -1;
    
    public ClinicListViewModel(String type, Service service, String service_id, ArrayList<Integer> tags, int filter) {
        Timber.e("type_name:"+type);
        clinicRepository = new ClinicRepository(mMutableLiveData);
        if(type.equals(Constants.NEWEST)) tab = 1;
        else if(type.equals(Constants.OFFER)) tab = 2;
        this.type = service.mType;
        this.flag = service.mFlag;
        this.service_id = service_id;
        this.tags = tags;
        this.filter = filter;
        notifyChange();
    }


    public void callService(){
        showPage(false);
        clinicRepository.serviceDetails(type,flag,tab,service_id,filter,tags);
    }

    public ClinicRepository getClinicRepository() {
        return clinicRepository;
    }
}
