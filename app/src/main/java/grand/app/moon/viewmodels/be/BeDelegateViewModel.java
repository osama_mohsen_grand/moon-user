package grand.app.moon.viewmodels.be;


import java.util.ArrayList;
import java.util.Iterator;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.be.BeDelegateRequest;
import grand.app.moon.models.be.VolleyFileObjectSerializable;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.repository.BeDelegateRepository;
import grand.app.moon.repository.VerificationFirebaseSMSRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BeDelegateViewModel extends ParentViewModel {
    private VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    public BeDelegateRequest beDelegateRequest = new BeDelegateRequest();
    public MutableLiveData<Object> mMutableLiveDataCountry  = new MutableLiveData<>();;
    BeDelegateRepository BeDelegateRepository;
    CountryRepository countryRepository;
    public StatusMsg statusMsg = null;
    public String image_select = "";
    boolean[] images = {false, false, false, false, false};
    public VolleyFileObjectSerializable volleyFileObjectSerializable;
    public ArrayList<String> imagesPath = new ArrayList<>();
    public ArrayList<String> imagesKey = new ArrayList<>();
    public ObservableBoolean isDriver = new ObservableBoolean(false);


    public BeDelegateViewModel() {
        volleyFileObjectSerializable = new VolleyFileObjectSerializable();
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        BeDelegateRepository = new BeDelegateRepository(mMutableLiveData);
        countryRepository = new CountryRepository(mMutableLiveDataCountry);
        countryRepository.getCountries(true);
    }

    public void color(){
        mMutableLiveData.setValue(Constants.COLOR);
    }



    public BeDelegateRepository getBeDelegateRepository() {
        return BeDelegateRepository;
    }


    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    public void signUp() {
        if (beDelegateRequest.isValid() && images[0] && images[1] && images[2] && images[3] && images[4]) {
            Iterator myVeryOwnIterator = volleyFileObjectSerializable.volleyFileObjectMap.keySet().iterator();
            while (myVeryOwnIterator.hasNext()) {
                String key = (String) myVeryOwnIterator.next();
                imagesKey.add(key);
                imagesPath.add(volleyFileObjectSerializable.volleyFileObjectMap.get(key).getFilePath());
            }
            beDelegateRequest.setPhone("+"+beDelegateRequest.cpp+beDelegateRequest.getPhone());
            BeDelegateRepository.registerUser(beDelegateRequest, imagesKey, imagesPath);
        } else if (!images[0]) {
            baseError = ResourceManager.getString(R.string.select_image_profile);
            mMutableLiveData.setValue(Constants.ERROR);
        } else {
            baseError = ResourceManager.getString(R.string.please_complete_form);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void selectImage(String image) {
        image_select = image;
        if (image_select.equals("image")) {
            mMutableLiveData.setValue(Constants.SELECT_IMAGE);
        } else {
            mMutableLiveData.setValue(Constants.SELECT_IMAGE_MULTIPLE);
        }
    }


    public void terms() {
        mMutableLiveData.setValue(Constants.TERMS);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(VolleyFileObject volleyFileObject) {
        volleyFileObjectSerializable.volleyFileObjectMap.put(image_select, volleyFileObject);
        if (image_select.equals("image")) {
            //Done Select ImageVideo
            images[0] = true;
        } else if (image_select.equals(Constants.ID_IMAGE)) {
            //Done Select image
            images[1] = true;
            images[2] = true;
        } else if (image_select.equals("license_image")) {
            //Done select licence
            images[3] = true;
            images[4] = true;
            notifyChange();
        }else if(image_select.equals("car_image")){
            images[5] = true;
            notifyChange();
        }
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }


    public void setAddress(double lat, double lng) {
        beDelegateRequest.setLat(lat);
        beDelegateRequest.setLng(lng);
        beDelegateRequest.setAddress(new MapConfig(MyApplication.getInstance(),null).getAddress(lat,lng));
        notifyChange();
    }

    public void updateType() {
        if(beDelegateRequest.getType().equals(Constants.DEFAULT_DELEGATE)){
            isDriver.set(false);
        }else
            isDriver.set(true);
        notifyChange();
    }
}
