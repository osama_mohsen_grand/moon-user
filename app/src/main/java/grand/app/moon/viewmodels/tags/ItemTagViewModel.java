package grand.app.moon.viewmodels.tags;

import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.utils.resources.ResourceManager;
import timber.log.Timber;

public class ItemTagViewModel extends ParentViewModel {
    public IdNameImage tag;
    public int position = 0;

    public ItemTagViewModel(IdNameImage tag, int position, boolean contains) {
        this.tag = tag;
        this.position = position;
        if (contains)
            tag.image = "true";
        else tag.image = "false";
    }

    public IdNameImage getTextCheck() {
        return tag;
    }

    @BindingAdapter("textCheck")
    public static void setTextCheck(TextView textView, IdNameImage tag) {
        textView.setText(tag.name);
        if (tag.image.equals("true"))
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle, 0, 0, 0);
        else
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    public void submit() {
        mMutableLiveData.setValue(position);
    }
}
