
package grand.app.moon.viewmodels.be;

import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.be.BeShopRequest;
import grand.app.moon.models.be.VolleyFileObjectSerializable;
import grand.app.moon.repository.BeShopRepository;
import grand.app.moon.repository.CountryRepository;
import grand.app.moon.repository.ServiceRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BeShopViewModel extends ParentViewModel {
    public BeShopRequest beShopRequest = new BeShopRequest();

    public ObservableBoolean isPhotographer = new ObservableBoolean(false);
    public ObservableBoolean isFamous = new ObservableBoolean(false);


    ServiceRepository serviceRepository;
    BeShopRepository beShopRepository;
    CountryRepository countryRepository;
    public StatusMsg statusMsg = null;
    public String image_select = "";
    boolean[] images = {false, false, false};
    public VolleyFileObjectSerializable volleyFileObjectSerializable;
    public static ObservableField<String> type = new ObservableField<>("");
    public ObservableBoolean showPage = new ObservableBoolean(false);
    public ArrayList<String> imagesPath = new ArrayList<>();
    public ArrayList<String> imagesKey = new ArrayList<>();
    public ObservableBoolean isPhotographerOrFamous = new ObservableBoolean(false);
    public ObservableBoolean isCommercial = new ObservableBoolean(false);
    public ObservableBoolean isReservation = new ObservableBoolean(false);


    public BeShopViewModel() {
        volleyFileObjectSerializable = new VolleyFileObjectSerializable();
        beShopRepository = new BeShopRepository(mMutableLiveData);
        serviceRepository = new ServiceRepository(mMutableLiveData);
        countryRepository = new CountryRepository(mMutableLiveData);
        countryRepository.getCountries(true);
    }


    public void getServices(int type_id) {
        serviceRepository.getServices(type_id);
    }

    public BeShopRepository getBeShopRepository() {
        return beShopRepository;
    }

    public ServiceRepository getServiceRepository() {
        return serviceRepository;
    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    public void onSplitTypeChanged(RadioGroup radioGroup, int id) {
        if (id == R.id.male) {
            beShopRequest.setGender("1");
        } else if (id == R.id.female) {
            beShopRequest.setGender("2");
        }
    }


    public void signUp() {
        Timber.e("valid_input"+beShopRequest.isValid());
        Timber.e("type"+beShopRequest.getType());
        Timber.e("image[0]"+images[0]);
        Timber.e("image[1]"+images[1]);
        Timber.e("image[2]"+images[2]);

        if (beShopRequest.isValid() && ((images[0] == true && images[1] == true && images[2] == true) ||
                (images[0] == true && (beShopRequest.getType().equals(Constants.TYPE_PHOTOGRAPHER) ||
                        beShopRequest.getType().equals(Constants.TYPE_FAMOUS_PEOPLE) ) )))  {
            Iterator myVeryOwnIterator = volleyFileObjectSerializable.volleyFileObjectMap.keySet().iterator();
            while (myVeryOwnIterator.hasNext()) {
                String key = (String) myVeryOwnIterator.next();
                imagesKey.add(key);
                imagesPath.add(volleyFileObjectSerializable.volleyFileObjectMap.get(key).getFilePath());
            }
            beShopRequest.setPhone("+"+beShopRequest.cpp+beShopRequest.getPhone());
            beShopRepository.registerUser(beShopRequest,imagesKey,imagesPath);


        } else if (images[0] == false) {
            baseError = ResourceManager.getString(R.string.select_image_profile);
            mMutableLiveData.setValue(Constants.ERROR);
        }else{
            baseError = ResourceManager.getString(R.string.please_complete_form);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void selectImage(String image) {
        image_select = image;
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public void terms() {
        mMutableLiveData.setValue(Constants.TERMS);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(String paramName , VolleyFileObject volleyFileObject) {
        volleyFileObjectSerializable.volleyFileObjectMap.put(paramName, volleyFileObject);
        if (image_select.equals("shop_image")) {
            //Done Select ImageVideo
            images[0] = true;
        } else if (image_select.equals(Constants.COMMERCIAL_IMAGE)) {
            images[1] = true;
//            beShopRequest.setCommercial(ResourceManager.getString(R.string.done_selected_image));
//            notifyChange();
        } else if (image_select.equals(Constants.LICENCE_IMAGE)) {
            images[2] = true;
//            beShopRequest.setLicense(ResourceManager.getString(R.string.done_selected_image));
//            notifyChange();
        }
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public void changeVisibility() {
//        switch (beShopRequest.getType()) {
//            case Constants.TYPE_PHOTOGRAPHER:
//                isPhotographer.set(true);
//                break;
//            case Constants.TYPE_FAMOUS_PEOPLE:
//                isFamous.set(true);
//                break;
//            default:
//                isFamous.set(false);
//                isPhotographer.set(false);
//
//        }

        if(beShopRequest.getType().equals(Constants.TYPE_PHOTOGRAPHER) || beShopRequest.getType().equals(Constants.TYPE_FAMOUS_PEOPLE) )
            isPhotographerOrFamous.set(true);
        else
            isPhotographerOrFamous.set(false);

        if(beShopRequest.getType().equals(Constants.TYPE_COMPANIES)){
            isCommercial.set(true);
        }else
            isCommercial.set(false);

        if(beShopRequest.getType().equals(Constants.TYPE_RESERVATION_CLINIC) || beShopRequest.getType().equals(Constants.TYPE_RESERVATION_BEAUTY)){
            isReservation.set(true);
        }else
            isReservation.set(false);
        
        notifyChange();
    }

    public void setAddress(double lat, double lng) {
        beShopRequest.setLat(lat);
        beShopRequest.setLng(lng);
        beShopRequest.setAddress(new MapConfig(MyApplication.getInstance(),null).getAddress(lat,lng));
        notifyChange();
    }

    public void setShowPage(boolean showPage) {
        this.showPage.set(showPage);
//        notifyChange();
    }
}
