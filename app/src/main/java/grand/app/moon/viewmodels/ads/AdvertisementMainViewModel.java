package grand.app.moon.viewmodels.ads;

import grand.app.moon.repository.AdsRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.common.BaseViewModel;
import grand.app.moon.vollyutils.URLS;

public class AdvertisementMainViewModel extends BaseViewModel {
    AdsRepository advertisementCompanyRepository;
    public int flag = 0;
    public String type;
    public int city_id = -1,service_id=-1;
    public int filter = 1,category_id,sub_category_id;

    public AdvertisementMainViewModel(String type) {
        advertisementCompanyRepository = new AdsRepository(mMutableLiveData);
        this.type = type;
        callService();
    }


    //filter userApp 1
    public void callService(){
        if(type.equals(Constants.ADS)){
            advertisementCompanyRepository.getAds(Integer.parseInt(Constants.TYPE_ADVERTISING),service_id,filter,category_id,sub_category_id,city_id, URLS.ADS_LIST);
        }else{
            advertisementCompanyRepository.getAds(Integer.parseInt(Constants.TYPE_COMPANIES),service_id,filter,category_id,sub_category_id,city_id,URLS.ADS_LIST);
        }
    }


    public void filterSubmit(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void citySubmit(){
        mMutableLiveData.setValue(Constants.CITY);
    }

    public void add(){mMutableLiveData.setValue(Constants.ADD);}

    public AdsRepository getAdvertisementCompanyRepository() {
        return advertisementCompanyRepository;
    }
}
