
package grand.app.moon.viewmodels.settings;

import android.widget.ImageView;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.rate.RateRequest;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.repository.TripActionRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FilterViewModel extends ParentViewModel {

    public int filter;

    public boolean isMinOrder = false;

    public FilterViewModel(int filter, Service service) {
        this.filter = filter;
        if(service != null && service.mType == 1 && service.mFlag == 1)
            isMinOrder = true;
        notifyChange();
    }


    public void onSplitTypeChanged(RadioGroup radioGroup, int id) {
        if(id == R.id.rating){
            filter = 1;
        }else if(id == R.id.newest_first){
            filter = 2;
        }else if(id == R.id.a_to_z){
            filter = 3;
        }else if(id == R.id.min_order_amount){
            filter = 4;
        }
    }



    public void submit(){
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
