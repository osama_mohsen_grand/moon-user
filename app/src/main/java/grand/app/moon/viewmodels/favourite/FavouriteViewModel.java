
package grand.app.moon.viewmodels.favourite;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.favourite.FavouriteModel;
import grand.app.moon.models.favourite.FavouriteRequest;
import grand.app.moon.repository.NotificationRepository;
import grand.app.moon.repository.ProductRepository;
import grand.app.moon.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FavouriteViewModel extends ParentViewModel {

    public int remove_pos = -1;
    ProductRepository productRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.favourite));
    public boolean productSelected = true;
    public boolean adsSelected = false;
    private ObservableBoolean noData = new ObservableBoolean(false);
    int type = 2;

    public FavouriteViewModel() {
        productRepository = new ProductRepository(mMutableLiveData);
        productRepository.getFavourite(type);
    }

    public void noData(){
        noDataTextDisplay.set(true);
        notifyChange();
    }

    public ObservableBoolean getNoData() {
        return noData;
    }
    
    public void ads(){
        if(type != 1) {
            type = 1;
            productSelected = !productSelected;
            adsSelected = !adsSelected;
            productRepository.getFavourite(type);
            notifyChange();
        }
    }

    public void products(){
        if(type != 2) {
            type = 2;
            productSelected = !productSelected;
            adsSelected = !adsSelected;
            productRepository.getFavourite(type);
            notifyChange();
        }
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void remove() {
        FavouriteModel model = productRepository.getFavouriteResponse().data.get(remove_pos);
        productRepository.addFavourite(new FavouriteRequest(model.id,model.type));
    }
}
