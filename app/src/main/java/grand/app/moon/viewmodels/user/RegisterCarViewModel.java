
package grand.app.moon.viewmodels.user;

import androidx.databinding.ObservableField;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.user.register.SecondRegisterRequest;
import grand.app.moon.repository.RegisterRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class RegisterCarViewModel extends ParentViewModel {
    public SecondRegisterRequest secondRegisterRequest ;
    RegisterRepository registerRepository;
    public boolean isUpdate = false;
    public static ObservableField<String> type = new ObservableField<>("");


    public RegisterCarViewModel(boolean isUpdate) {
        registerRepository = new RegisterRepository(mMutableLiveData);
        secondRegisterRequest = new SecondRegisterRequest();
        this.isUpdate = isUpdate;
    }

    public void signUp() {
        if(secondRegisterRequest.isValid()){
//            if(isUpdate)
//                registerRepository.updateCar(secondRegisterRequest);
//            else
//                registerRepository.registerCar(secondRegisterRequest);
        }
    }


    public RegisterRepository getRegisterRepository() {
        return registerRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
