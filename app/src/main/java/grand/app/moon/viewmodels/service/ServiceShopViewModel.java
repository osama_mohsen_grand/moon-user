package grand.app.moon.viewmodels.service;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.adapter.ShopAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.URLS;

public class ServiceShopViewModel extends ParentViewModel {
    public String type = "";
    public HomeRepository homeRepository;
    public String service_id;
    public ArrayList<Integer> tags = new ArrayList<>();
    public int tab = 1;
    public int filter;

    public ShopAdapter adapter;
    public List<ShopDetails> shops;

    private static final String TAG = "ServiceShopViewModel";

    public ServiceShopViewModel(String type) {
        homeRepository = new HomeRepository(mMutableLiveData);
        this.type = type;
        Log.d(TAG,"type:"+type);
    }


    public void updateView(List<ShopDetails> mShops){
        this.shops = new ArrayList<>();
        if(type.equals(Constants.NEWEST)){
            Log.d(TAG,type);
            this.shops = new ArrayList<>(mShops);
        }else if(type.equals(Constants.OFFER)){
            Log.d(TAG,type);
            for(ShopDetails shop : mShops){
                if(shop.mOffer == 1) this.shops.add(shop);
            }
        }else if(type.equals(Constants.VOUCHER)){
            Log.d(TAG,type);
            for(ShopDetails shop : mShops){
                if(shop.mVoucher == 1) this.shops.add(shop);
            }
        }
        setData();
    }

    public void setData() {
        showPage(true);
        if(shops.size() > 0) {
            haveData();
            adapter.update(shops);
        }else{
            noData();
        }
    }

    public HomeRepository getHomeRepository() {
        return homeRepository;
    }
}
