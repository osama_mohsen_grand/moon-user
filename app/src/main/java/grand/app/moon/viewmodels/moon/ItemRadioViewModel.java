package grand.app.moon.viewmodels.moon;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNamePrice;

public class ItemRadioViewModel extends ParentViewModel {
    public boolean selected = false;
    public IdNamePrice size = null;
    public int position = 0;

    public ItemRadioViewModel(IdNamePrice size, int position, boolean selected) {
        this.size = size;
        this.position = position;
        this.selected =  selected;
        notifyChange();
    }
}
