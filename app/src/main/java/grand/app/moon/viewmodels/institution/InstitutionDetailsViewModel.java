package grand.app.moon.viewmodels.institution;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.ShopDetails;
import grand.app.moon.repository.ProductRepository;
import grand.app.moon.utils.Constants;

public class InstitutionDetailsViewModel extends ParentViewModel {
    private ProductRepository productRepository;
    public ShopDetails shopDetails;
    public InstitutionDetailsViewModel(int id, Service service, ShopDetails shopDetails) {
        productRepository = new ProductRepository(mMutableLiveData);
        this.shopDetails = shopDetails;
    }


    public void locationSubmit(){
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public void chatSubmit(){
        mMutableLiveData.setValue(Constants.CHAT);
    }

    public void callService(){
        showPage(false);
    }

}
