
package grand.app.moon.viewmodels.city;

import android.util.Log;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.Observer;
import grand.app.moon.adapter.CityRegionAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.country.Datum;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CityRegionViewModel extends ParentViewModel {

    public enum TYPE {
        CITY,
        REGION
    }

    public ObservableBoolean cityListVisible = new ObservableBoolean(true);
    public ObservableBoolean regionListVisible = new ObservableBoolean(false);

    public CityRegionAdapter cityAdapter = new CityRegionAdapter(new ArrayList<>());
    public CityRegionAdapter regionAdapter = new CityRegionAdapter(new ArrayList<>());

    public Datum country = AppMoon.getCities(UserHelper.retrieveKey(Constants.COUNTRY_ID), UserHelper.getCountries().data);

    public CityRegionViewModel() {
        cityAdapter.updateCities(country.cities);
        setEventAdapter();
    }

    private void setEventAdapter() {
        cityAdapter.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                if(cityAdapter.getPosition() != -1)
                    cityAdapter.notifyItemChanged(cityAdapter.getPosition());
                int position = (int) o;
                cityAdapter.setPosition(position);
                updateRegions(position);
                regionAdapter.setPosition(-1);
                cityAdapter.notifyItemChanged(position);
                regionAdapter.notifyDataSetChanged();
                submit(TYPE.REGION);
            }
        });
        regionAdapter.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                int position = (int) o;
                regionAdapter.setPosition(position);
                regionAdapter.notifyItemChanged(position);
            }
        });
        updateRegions(cityAdapter.getPosition());
    }

    private void updateRegions(int position) {
        if (country.cities.size() > 0) {
            if (position == -1) {
                regionAdapter.updateRegions(country.cities.get(0).regions);
            } else {
                regionAdapter.updateRegions(country.cities.get(position).regions);
            }
        }
    }

    public void submit(Object type) {
        if (type == TYPE.CITY) {
            cityListVisible.set(true);
            regionListVisible.set(false);
        } else if (type == TYPE.REGION) {
            regionListVisible.set(true);
            cityListVisible.set(false);
        }
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
