
package grand.app.moon.viewmodels.famous;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.ads.companyAdsCategory.Datum;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.repository.FamousRepository;
import grand.app.moon.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ShopAlbumViewModel extends ParentViewModel {
    public List<Datum> result;
    private FamousRepository famousRepository;

    public ShopAlbumViewModel() {
        famousRepository = new FamousRepository(mMutableLiveData);
    }


    public FamousRepository getFamousRepository() {
        return famousRepository;
    }


    public void filter() {
        mMutableLiveData.setValue(Constants.FILTER);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void getCategoryDetails(int famous_id, int id) {
        famousRepository.getCompanyAdsCategories(famous_id, id);
    }
}
