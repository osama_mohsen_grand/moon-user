package grand.app.moon.viewmodels.ads;


import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.base.IdNameDescription;
import grand.app.moon.utils.Constants;

public class ItemFilterAdsViewModel extends ParentViewModel {
    public ObservableBoolean selected;
    public int position = 0;
    public IdNameDescription data;

    public ItemFilterAdsViewModel(IdNameDescription data, int position, boolean isSelected) {
        this.position = position;
        this.data = data;
        selected = new ObservableBoolean(isSelected);
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }
}
