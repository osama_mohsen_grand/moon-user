
package grand.app.moon.viewmodels.reservation;

import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.rate.RateRequest;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.repository.TripActionRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ReservationViewModel extends ParentViewModel {
    public ReservationViewModel() {

    }

    public void health(){
        mMutableLiveData.setValue(Constants.HEALTH);
    }

    public void beauty(){
        mMutableLiveData.setValue(Constants.BEAUTY);
    }
}
