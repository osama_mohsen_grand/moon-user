package grand.app.moon.viewmodels.ads;

import android.widget.Toast;

import java.util.ArrayList;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.favourite.FavouriteRequest;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.MyApplication;

public class AdvertisementDetailsViewModel extends ParentViewModel {
    AdsRepository advertisementCompanyRepository;
    public String price = "",priceAfter="",priceAfterConst="0",currency=UserHelper.retrieveCurrency();
    public String[] images = new String[0];
    public int id,type;


    public AdvertisementDetailsViewModel(int id,int type) {
        advertisementCompanyRepository = new AdsRepository(mMutableLiveData);
        advertisementCompanyRepository.getAdsDetails(id,type);
        this.id = id;
        this.type = type;
    }

    public AdvertisementDetailsViewModel() {
        advertisementCompanyRepository = new AdsRepository(mMutableLiveData);
    }

    public void getFamousShopAds(int famous_id , int shop_id){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(shop_id);
        advertisementCompanyRepository.getFamousShopAds(arrayList,famous_id,1);
    }

    public void setPrice(String price){
        this.price = price + " "+ UserHelper.retrieveCurrency();
    }


    public String[] getImages(){
        if(advertisementCompanyRepository.getAdsDetailsResponse() != null) {
            images = new String[advertisementCompanyRepository.getAdsDetailsResponse().data.productImages.size()];
            for (int i = 0; i < advertisementCompanyRepository.getAdsDetailsResponse().data.productImages.size(); i++)
                images[i] = advertisementCompanyRepository.getAdsDetailsResponse().data.productImages.get(i).image;
        }
        return images;
    }


    public void fav(){
        if (UserHelper.getUserId() != -1)
            advertisementCompanyRepository.addFavourite(new FavouriteRequest(id,type));
        else
            Toast.makeText(MyApplication.getInstance(), ResourceManager.getString(R.string.please_login_to_complete_this_action), Toast.LENGTH_SHORT).show();
    }

    private static final String TAG = "AdvertisementDetailsVie";
    public void setData(){
        setPrice(advertisementCompanyRepository.getAdsDetailsResponse().data.price);
//        priceAfterConst = advertisementCompanyRepository.getAdsDetailsResponse().data.priceAfter;
//        priceAfter = advertisementCompanyRepository.getAdsDetailsResponse().data.priceAfter+ " "+UserHelper.retrieveCurrency();
//        if(priceAfterConst.equals("0")) {
//            priceAfter = price;
//        }
    }


    public void locationSubmit(){
        mMutableLiveData.setValue(Constants.LOCATION);
    }

    public AdsRepository getAdvertisementCompanyRepository() {
        return advertisementCompanyRepository;
    }
}
