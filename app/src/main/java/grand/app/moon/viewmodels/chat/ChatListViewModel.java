
package grand.app.moon.viewmodels.chat;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.ChatRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ChatListViewModel extends ParentViewModel {

    ChatRepository chatRepository;
    public String text = "";

    public ChatListViewModel() {
        chatRepository = new ChatRepository(mMutableLiveData);
        chatRepository.getChatList();

    }

    public ChatRepository getChatRepository() {
        return chatRepository;
    }



    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
