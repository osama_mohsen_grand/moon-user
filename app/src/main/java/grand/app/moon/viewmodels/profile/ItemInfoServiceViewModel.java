package grand.app.moon.viewmodels.profile;

import android.graphics.drawable.Drawable;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import grand.app.moon.base.ParentBaseObservableViewModel;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.customviews.menu.MenuModel;
import grand.app.moon.models.base.IdNameImage;


public class ItemInfoServiceViewModel extends ParentViewModel {
    public IdNameImage model = null;
    private int position = 0;

    public ItemInfoServiceViewModel(IdNameImage model, int position) {
        this.model = model;
        this.position = position;
    }

}
