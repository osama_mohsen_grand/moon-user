
package grand.app.moon.viewmodels.followers;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.repository.MoonMapRepository;
import grand.app.moon.utils.maputils.base.MapConfig;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FollowersViewModel extends ParentViewModel {
    private static final String TAG = "HomeViewModel";
    MoonMapRepository moonMapRepository;
    public MapConfig mapConfig;
    public String[] images = new String[0];

    public FollowersViewModel() {
        moonMapRepository = new MoonMapRepository(mMutableLiveData);
        moonMapRepository.getStories();
    }

    public MoonMapRepository getMoonMapRepository() {
        return moonMapRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
