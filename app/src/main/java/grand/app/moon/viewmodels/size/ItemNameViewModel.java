package grand.app.moon.viewmodels.size;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdName;

public class ItemNameViewModel extends ParentViewModel {
    public boolean selected = false;
    public IdName text = null;
    public int position = 0;

    public ItemNameViewModel(IdName text, int position, boolean selected) {
        this.text = text;
        this.position = position;
        this.selected =  selected;
        notifyChange();
    }

    public void textSubmit(){
        mMutableLiveData.setValue(position);
    }
}
