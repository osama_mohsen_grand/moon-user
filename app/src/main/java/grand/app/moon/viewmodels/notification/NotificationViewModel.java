
package grand.app.moon.viewmodels.notification;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.adapter.NotificationAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.NotificationRepository;
import grand.app.moon.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class NotificationViewModel extends ParentViewModel {

    NotificationRepository notificationRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.label_notification));
    private ObservableBoolean noData = new ObservableBoolean(false);
    public NotificationAdapter notificationAdapter = new NotificationAdapter(new ArrayList<>());

    public NotificationViewModel() {
        notificationRepository = new NotificationRepository(mMutableLiveData);
        notificationRepository.getNotification();
        notificationAdapter.repository = notificationRepository;
    }

    public void noData(){
        noDataTextDisplay.set(true);
        notifyChange();
    }

    public ObservableBoolean getNoData() {
        return noData;
    }

    public NotificationRepository getNotificationRepository() {
        return notificationRepository;
    }



    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
