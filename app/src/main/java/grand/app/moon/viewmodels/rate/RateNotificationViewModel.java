
package grand.app.moon.viewmodels.rate;

import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseBindingMethod;
import androidx.databinding.InverseBindingMethods;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableFloat;
import androidx.databinding.ObservableInt;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.customviews.dialog.rate.DialogRateModel;
import grand.app.moon.models.rate.RateRequest;
import grand.app.moon.models.review.AddReviewRequest;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.repository.CommonRepository;
import grand.app.moon.repository.TripActionRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */

public class RateNotificationViewModel extends ParentViewModel {
    public AddReviewRequest addReviewRequest ;
    public ObservableFloat rating = new ObservableFloat(0);
    public CommonRepository commonRepository;
    public DialogRateModel dialogRateModel;
    //tripDetailsResponse.data.user.name
    public static ObservableField<String> text = new ObservableField<>("");


    public RateNotificationViewModel(DialogRateModel dialogRateModel) {
        commonRepository = new CommonRepository(mMutableLiveData);
        addReviewRequest = new AddReviewRequest();
        this.dialogRateModel = dialogRateModel;
        if(dialogRateModel.type.equals(Constants.TYPE_RESERVATION_BEAUTY) || dialogRateModel.type.equals(Constants.TYPE_RESERVATION_CLINIC)
        || dialogRateModel.type.equals(Constants.TYPE_TAXI) || dialogRateModel.type.equals(Constants.TYPE_TRUCKS))
            addReviewRequest.setFlag("1");
        notifyChange();
    }

    public void sendSubmit() {
        addReviewRequest.setOrder_id(String.valueOf(dialogRateModel.order_id));
        addReviewRequest.setShop_id((dialogRateModel.id));
        commonRepository.addReview(addReviewRequest);
    }

    private static final String TAG = "RateNotificationViewMod";

    public void onRateChange(RatingBar ratingBar, float rating,boolean fromUser) {
        Log.d(TAG,"rating:"+rating);
        Log.d(TAG,"rating:"+ratingBar.getRating());
        addReviewRequest.setRate(rating);
    }

    public void cancel(){
        mMutableLiveData.setValue(Constants.CANCELED);
    }

    public CommonRepository getCommonRepository() {
        return commonRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
