
package grand.app.moon.viewmodels.common;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.CountryRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class SplashViewModel extends ParentViewModel {

    CountryRepository countryRepository;
    public SplashViewModel() {
        countryRepository = new CountryRepository(mMutableLiveData);
        countryRepository.getCountries(false);

    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }
}
