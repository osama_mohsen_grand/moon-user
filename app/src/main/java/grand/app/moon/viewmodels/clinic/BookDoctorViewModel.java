
package grand.app.moon.viewmodels.clinic;


import android.util.Log;
import android.widget.RadioGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.PaymentMethodAdapter;
import grand.app.moon.adapter.ShippingAdapter;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.clinic.Data;
import grand.app.moon.models.doctor.DoctorDetailsResponse;
import grand.app.moon.models.reservation.ReservationRequest;
import grand.app.moon.models.shipping.Shipping;
import grand.app.moon.repository.ClinicRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BookDoctorViewModel extends ParentViewModel {

    public String text = "";
    ClinicRepository clinicRepository;
    public ReservationRequest reservationRequest;
    private String price = "";
    public boolean allowCash = false, allowOnline = false, checkCash = false, checkOnline = false;
    public PaymentMethodAdapter adapter = new PaymentMethodAdapter(new ArrayList<>());
    public Data clinicDetailsData;

    public BookDoctorViewModel(int id) {
        reservationRequest = new ReservationRequest(Constants.TYPE_RESERVATION_CLINIC, id);
        clinicRepository = new ClinicRepository(mMutableLiveData);
        callService();
    }

    public void setAdapter(Data clinicDetailsData) {
        this.clinicDetailsData = clinicDetailsData;
        if(clinicDetailsData.paymentList.size() > 0){
            reservationRequest.payment_method = Integer.parseInt(clinicDetailsData.paymentList.get(0).id);
        }
        adapter.update(clinicDetailsData.paymentList);
        setEventAdapter();
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price + " " + UserHelper.retrieveCurrency();
    }

    public void setData(DoctorDetailsResponse doctorDetailsResponse) {
        setPrice(doctorDetailsResponse.data.price);
    }

    private void setEventAdapter() {
        adapter.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                IdNameImage idNameImage = clinicDetailsData.paymentList.get(pos);
                reservationRequest.payment_method = Integer.parseInt(idNameImage.id);
            }
        });
    }

    public void reviews() {
        mMutableLiveData.setValue(Constants.REVIEW);
    }


    public void submit() {
        if (reservationRequest.service != null && !reservationRequest.service.equals("")) {
            clinicRepository.makeReservation(reservationRequest);
        } else {
            baseError = ResourceManager.getString(R.string.please_select_time_reservation);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void callService() {
        clinicRepository.doctorDetails(reservationRequest.doctor_id, reservationRequest.date);
    }

    private static final String TAG = "BookDoctorViewModel";

    public void updateUI(int payment_type) {
        Log.e(TAG, "updateUI: " + payment_type);
        if (payment_type == 1) {
            allowCash = true;
            checkCash = true;
        } else if (payment_type == 2) {
            allowOnline = true;
            checkOnline = true;
            reservationRequest.payment_method = 2;
        } else {
            checkCash = true;
            allowCash = true;
            allowOnline = true;
        }
    }

    public ClinicRepository getClinicRepository() {
        return clinicRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
