
package grand.app.moon.viewmodels.discover;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.service.DiscoverRepository;
import grand.app.moon.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class DiscoverViewModel extends ParentViewModel {

    private DiscoverRepository discoverRepository;
    public ObservableBoolean isFilter = new ObservableBoolean(false);
    int shop_id = -1;
    public void init(){
        discoverRepository = new DiscoverRepository(mMutableLiveData);
    }
    public DiscoverViewModel(Service service,String service_id) {
        init();
        discoverRepository.getDiscover(service_id,service.mType,service.mFlag);
    }

    public DiscoverViewModel(){
        init();
    }

    public void getDiscover(){
            discoverRepository.getAllDiscoverWithServices();
    }

    public void submit(){
        if(getDiscoverRepository().getFilterShopAdsResponse() == null)
            getFilterAds(shop_id);
        else
            mMutableLiveData.setValue(Constants.FILTER);
    }

    public DiscoverRepository getDiscoverRepository() {
        return discoverRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
    public void getFilterAds(int id){
        discoverRepository.getShopFilterAds(id);
    }

    public void setType(int shop_id , String type){
        this.shop_id = shop_id;
        if(type.equals(Constants.ADS)){
            isFilter.set(true);
        }
    }

}
