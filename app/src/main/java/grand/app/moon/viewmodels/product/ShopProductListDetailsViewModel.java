
package grand.app.moon.viewmodels.product;

import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.ProductRepository;
import grand.app.moon.repository.ShopRepository;
import grand.app.moon.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ShopProductListDetailsViewModel extends ParentViewModel {

    ProductRepository productRepository;

    public ShopProductListDetailsViewModel(String category_id, String shop_id , int type , int flag) {
        productRepository = new ProductRepository(mMutableLiveData);
        productRepository.getShopProducts(category_id,shop_id,type,flag);
    }


    public ProductRepository getProductRepository() {
        return productRepository;
    }
}
