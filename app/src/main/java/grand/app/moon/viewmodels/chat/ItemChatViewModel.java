package grand.app.moon.viewmodels.chat;

import android.util.LayoutDirection;
import android.util.Log;
import android.widget.RelativeLayout;

import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.chat.ChatDetailsModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;


public class ItemChatViewModel extends ParentViewModel {

    public ChatDetailsModel chat;
    public int position;
    public ObservableBoolean isMessage = new ObservableBoolean(false);
    public ObservableBoolean isImage = new ObservableBoolean(false);
    public ObservableBoolean isAudio = new ObservableBoolean(false);
    private int layout;
    public String userName = "";
    private static final String TAG = "ItemChatViewModel";
//
//    public Drawable backgroundColor;
//
    public ItemChatViewModel(ChatDetailsModel chat , int position) {
        this.chat = chat;
        this.position = position;
        Log.e(TAG, "chatType: "+chat.type );
        Log.e(TAG, "AppMoon.sender(): "+chat.sender);
        Log.e(TAG, "AppMoon.getUserId(): "+UserHelper.getUserId() );
        if(chat.type.equals(AppMoon.getUserType()) && chat.sender == UserHelper.getUserId()) {
            Log.e(TAG, "ItemChatViewModel: right" );
            layout = LayoutDirection.RTL;
            userName = UserHelper.getUserDetails().name;
        }else {
            Log.e(TAG, "ItemChatViewModel: left" );
            layout = LayoutDirection.LTR;
            userName = chat.senderName;
        }


        if(!chat.message.equals(""))
            isMessage.set(true);
        if(!chat.image.equals(""))
            isImage.set(true);
        if(!chat.audio.equals(""))
            isAudio.set(true);

//        Timber.e("==");
//        Timber.e("message:"+chat.message);
//        Timber.e("image:"+chat.image);
//        Timber.e("audio:"+chat.audio);
//        Timber.e("==");

        notifyChange();
    }

    public String imageUrl(){
        Log.d(TAG,"imageUrlChatType:"+chat.type);
        Log.d(TAG,"imageUrlChatUserType:"+AppMoon.getUserType());
        Log.d(TAG,"imageUrlChatImage:"+UserHelper.getUserDetails().image);
        Log.d(TAG,"imageUrlAppMoon:"+AppMoon.getUserType());
        if(chat.type.equals(AppMoon.getUserType()))
            return UserHelper.getUserDetails().image;
        return chat.senderImage;
    }

    public int getLayout(){
        return layout;
    }

    @BindingAdapter("layout")
    public static void setLayout(ConstraintLayout linearLayout, int layout){
        linearLayout.setLayoutDirection(layout);
    }

    public String getAudio(){
        return chat.audio;
    }

    @BindingAdapter("audio")
    public static void setAudio(AudioSenseiPlayerView audio, String url){
        audio.setAudioTarget(url);
    }

    public void imageSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.IMAGE,position));
    }

//    @Bindable
//    public Drawable getBackgroundColor() {
//        return backgroundColor;
//    }
//
//    @BindingAdapter("bgColor")
//    public static void setBackgroundColor(LinearLayout linearLayout , Drawable backgroundColor) {
//        linearLayout.setBackground(backgroundColor);
//    }
//
//
//    public int getLayout(){
//        return layout;
//    }
//
//    @BindingAdapter("layout")
//    public static void setLayout(LinearLayout linearLayout,int layout){
//        linearLayout.setLayoutDirection(layout);
//    }
//
//    @Bindable
//    public String getImageUrl(){
//        return chat.userImage;
//    }
//
//    @BindingAdapter("imageUrl")
//    public static void loadImage(ImageView imageView, String image) {
//        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
//    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
