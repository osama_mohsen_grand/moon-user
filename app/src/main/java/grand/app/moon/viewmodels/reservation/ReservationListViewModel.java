
package grand.app.moon.viewmodels.reservation;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.reservation.ReservationOrderActionRequest;
import grand.app.moon.repository.ClinicRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ReservationListViewModel extends ParentViewModel {

    public int category_delete_position = -1;
    ClinicRepository reservationRepository;
    public ReservationOrderActionRequest reservationOrderActionRequest = new ReservationOrderActionRequest();
    public String text = "";

    public ReservationListViewModel(String status,int reserved_type) {
        reservationRepository = new ClinicRepository(mMutableLiveData);
        reservationRepository.getReservations(status,reserved_type);

    }

    public ClinicRepository getReservationRepository() {
        return reservationRepository;
    }

    public void submit(){

    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
