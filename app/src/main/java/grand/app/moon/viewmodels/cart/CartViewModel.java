
package grand.app.moon.viewmodels.cart;


import android.view.View;
import android.view.animation.AnimationUtils;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.cart.AddCartRequest;
import grand.app.moon.models.cart.DeleteCartRequest;
import grand.app.moon.models.cart.Item;
import grand.app.moon.models.chat.ChatRequest;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.product.Product;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.repository.CartRepository;
import grand.app.moon.repository.ChatRepository;
import grand.app.moon.repository.ProductRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.MyApplication;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CartViewModel extends ParentViewModel {
    public MutableLiveData<Object> cartMutableLiveData;
    public AddCartRequest addCartRequest;
    private String subTotal = "", delivery = "", total = "";
    CartRepository cartRepository;
    Service service;

    public void init() {
        cartMutableLiveData = new MutableLiveData<>();
        cartRepository = new CartRepository(cartMutableLiveData);
        addCartRequest = new AddCartRequest();
    }

    public CartViewModel(ProductList product, Service service) {
        init();
        this.service = service;
        addCartRequest.type = service.mType;
        addCartRequest.flag = service.mFlag;
        addCartRequest.product_id = product.id;
    }

    public CartViewModel() {
        init();
        cartRepository.getCart();
    }

    public void submitAddToCart() {
        cartMutableLiveData.setValue(Constants.SUBMIT);
    }

    public void add(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(MyApplication.getInstance(), R.anim.image_click));
        int x = Integer.parseInt(addCartRequest.qty);
        x++;
        addCartRequest.qty = x + "";
        notifyChange();
    }

    public void minus(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(MyApplication.getInstance(), R.anim.image_click));
        int x = Integer.parseInt(addCartRequest.qty);
        if (x - 1 != 0) {
            x--;
            addCartRequest.qty = "" + x;
            notifyChange();
        }
    }

    public String getSubTotal() {
        if (cartRepository.getCartResponse() != null)
            subTotal = cartRepository.getCartResponse().data.subTotal + " " + UserHelper.retrieveCurrency();
        return subTotal;
    }

    public String getDelivery() {
        if (cartRepository.getCartResponse() != null)
            delivery = cartRepository.getCartResponse().data.delivery + " " + UserHelper.retrieveCurrency();
        return delivery;
    }

    public String getTotal() {
        if (cartRepository.getCartResponse() != null)
            total = cartRepository.getCartResponse().data.totalPrice + " " + UserHelper.retrieveCurrency();
        return total;
    }

    public void checkOutSubmit() {
        cartMutableLiveData.setValue(Constants.SUBMIT);
    }

    public CartRepository getCartRepository() {
        return cartRepository;
    }

    public void callService() {
        cartRepository.addOrUpdate(addCartRequest);
    }

    public void deleteCart() {
        cartRepository.deleteCart(new DeleteCartRequest(-1, 1));
    }

    public void update(Item item) {
        addCartRequest.cart_item_id = item.id;
        addCartRequest.product_id = item.productId;
        addCartRequest.qty = item.qty_tmp;
        cartRepository.addOrUpdate(addCartRequest);
    }

    public void updateTotal(Item item) {
        double qty = Double.parseDouble(item.qty);
        double qty_update = Double.parseDouble(item.qty_tmp);
        double unit_price = Double.parseDouble(item.price);
        double price_addition = 0;
        if(item.price_addition != null && !item.price_addition.equals("")){
            price_addition = Double.parseDouble(item.price_addition);
        }
        if (qty_update > qty) {
            cartRepository.getCartResponse().data.totalPrice += unit_price + price_addition;
            cartRepository.getCartResponse().data.subTotal += unit_price + price_addition;
        } else {
            cartRepository.getCartResponse().data.totalPrice -= (unit_price + price_addition);
            cartRepository.getCartResponse().data.subTotal -= (unit_price + price_addition);
        }
        notifyChange();
    }

    public void updateDeleteTotal(Item item) {
        double qty = Double.parseDouble(item.qty);
        double unit_price = Double.parseDouble(item.price);
        cartRepository.getCartResponse().data.totalPrice -= unit_price * qty;
        cartRepository.getCartResponse().data.subTotal -= unit_price * qty;
        notifyChange();
    }
}
