
package grand.app.moon.viewmodels.famous;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import de.hdodenhof.circleimageview.CircleImageView;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.famous.details.FamousDetailsResponse;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.repository.FamousRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousDetailsViewModel extends ParentViewModel {

    public FamousDetailsResponse famousDetailsResponse = new FamousDetailsResponse();
    public AccountInfoResponse accountInfoResponse = null;

    private FamousRepository famousRepository;
    public int id = -1, type = -1;
    public String name = "", nickname = "", followerCount;
    public String followers = "";
    public boolean isFollow = false, allowChat = false, hasShop = false;


    public FamousDetailsViewModel(int type) {
        famousRepository = new FamousRepository(mMutableLiveData);
        this.type = type;
//        if (type == Integer.parseInt(Constants.TYPE_PHOTOGRAPHER)) {
        allowChat = true;
//            hasShop = true;
//        }
    }


    public void setData(int id, String name, String image, String followerCount, int isFollow, boolean hasShop) {
        this.id = id;
        this.name = name;
        this.followerCount = followerCount;
        if (isFollow == 1) this.isFollow = true;
        this.hasShop = hasShop;
        setFollowerCount();
        notifyChange();
    }

    public void setFollowerCount() {
        followers = followerCount + " " + ResourceManager.getString(R.string.followers);
    }

    public void follow() {
        if (UserHelper.getUserId() == -1) {
            baseError = ResourceManager.getString(R.string.please_login_first);
            mMutableLiveData.setValue(Constants.ERROR);
        } else
            famousRepository.follow(id, type);
    }

    public FamousDetailsResponse getStory() {
        return famousDetailsResponse;
    }


    @BindingAdapter("story")
    public static void setStory(CircleImageView circleImageView, FamousDetailsResponse response) {
//        if(response!= null && response.isStory == 1){
//            circleImageView.setBorderColor(ResourceManager.getColor(R.color.multiple_image_select_primary));
//            circleImageView.setBorderWidth(8);
//            circleImageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(circleImageView.getContext(), BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.STORY);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable(Constants.STORY, response.story);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    intent.putExtra(Constants.NAME_BAR, response.story.mName);
//                    circleImageView.getContext().startActivity(intent);
//                }
//            });
//        }

//        if (response != null && response.isStory == 1) {
//            if (response.shopData.shop_details.storyView == 0) {

        ImageLoader.getInstance().displayImage(
                response.accountDetails.image, circleImageView,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        circleImageView.setImageDrawable(ResourceManager.getDrawable(R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if(response.isStory == 1) {
                            if (response.story.storyView == 0)
                                circleImageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_gradient));
                            else
                                circleImageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_silver));
                        }
                    }
                }
        );
//            }
        if (response.isStory == 1) {
            circleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(circleImageView.getContext(), BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.STORY);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.STORY, response.story);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, response.story.mName);
                    circleImageView.getContext().startActivity(intent);
                }
            });
        }
    }


    public void chatSubmit() {
        if (UserHelper.getUserId() == -1) {
            baseError = ResourceManager.getString(R.string.please_login_first);
            mMutableLiveData.setValue(Constants.ERROR);
        } else
            mMutableLiveData.setValue(Constants.CHAT_DETAILS);
    }

    public void shopSubmit() {
        mMutableLiveData.setValue(Constants.SHOP_DETAILS);
    }

    public void shareSubmit() {
        mMutableLiveData.setValue(Constants.SHARE);
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
