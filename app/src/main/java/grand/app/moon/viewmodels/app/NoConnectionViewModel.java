
package grand.app.moon.viewmodels.app;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class NoConnectionViewModel extends ParentViewModel {

    public void reload() {
        if (AppUtils.isNetworkAvailable()) {
            mMutableLiveData.setValue(Constants.RELOAD);
        } else {
            mMutableLiveData.setValue(Constants.FAILURE_CONNECTION);
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
