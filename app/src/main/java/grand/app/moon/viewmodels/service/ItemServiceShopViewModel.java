package grand.app.moon.viewmodels.service;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;

public class ItemServiceShopViewModel extends ParentViewModel {
    public ShopDetails shop;
    public int position = 0;
    public int width = 0;
    public boolean isAverage = true;
    public ObservableBoolean imageIsClose = new ObservableBoolean(false);
    public ObservableBoolean imageIsOffer = new ObservableBoolean(false);
    public ObservableBoolean imageIsVoucher = new ObservableBoolean(false);

    public ItemServiceShopViewModel(ShopDetails shop, int position) {
        this.shop = shop;
        this.position = position;
        if (shop.mAvg == null || shop.mAvg.equals(""))
            isAverage = false;
        if (shop.mWorkDays == 0) imageIsClose.set(true);
        if (shop.mVoucher == 1) imageIsVoucher.set(true);
        if (shop.mOffer == 1) imageIsOffer.set(true);

        notifyChange();
    }

//    public Drawable getStatusShop(){
//        if(shop.mWorkDays == 0 && shop.mVoucher != 1)
//            return ResourceManager.getDrawable(R.drawable.close);
//        else if(shop.mVoucher == 1)
//            return ResourceManager.getDrawable(R.drawable.voucher);
//        else
//            return ResourceManager.getDrawable(R.drawable.sale);
//    }

    @BindingAdapter("statusShop")
    public static void setStatusShop(ImageView imageView, Drawable drawable) {
        imageView.setImageDrawable(drawable);
    }

    public String getImageUrl() {
        return shop.mImage;
    }

    public void storySubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.STORY,position));
    }

    public void submit() {
        Log.d("shopDetails", "submit item adapter");
        if (shop.mWorkDays != 0)
            mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
        else
            Toasty.info(MyApplication.getInstance(), ResourceManager.getString(R.string.this_shop_is_close_now), Toasty.LENGTH_SHORT).show();
    }

}
