package grand.app.moon.viewmodels.adsCompanyFilter;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.adsCompanyFilter.AdsCompanyFilter;


public class ItemAdsCompanyFilterViewModel extends ParentViewModel {
    public AdsCompanyFilter adsCompanyFilter = null;
    private int position = 0;
    public ItemAdsCompanyFilterViewModel(AdsCompanyFilter adsCompanyFilter, int position) {
        this.adsCompanyFilter = adsCompanyFilter;
        this.position = position;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public String getImageUrl(){
        return adsCompanyFilter.image;
    }

    public AdsCompanyFilter getCountry() {
        return adsCompanyFilter;
    }
}
