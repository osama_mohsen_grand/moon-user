
package grand.app.moon.viewmodels.ads;

import java.util.ArrayList;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.repository.FamousRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CompanyAdsCategoriesViewModel extends ParentViewModel {

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
