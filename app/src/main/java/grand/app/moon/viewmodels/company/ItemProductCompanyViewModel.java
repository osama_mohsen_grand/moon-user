package grand.app.moon.viewmodels.company;


import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;

public class ItemProductCompanyViewModel extends ParentViewModel {
    public ProductList productList ;
    public int position = 0;
    public int width = 0;
    private String priceTotal;

    public ItemProductCompanyViewModel(ProductList productList, int position) {
        this.productList = productList;
        this.priceTotal = productList.price +" "+ UserHelper.retrieveCurrency();
        this.position = position;
        notifyChange();
    }

    public String getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(String priceTotal) {
        this.priceTotal = priceTotal;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
