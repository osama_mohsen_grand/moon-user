package grand.app.moon.viewmodels.reservation;

import androidx.databinding.ObservableBoolean;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.reservation.ReservationOrder;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;


public class ItemReservationViewModel extends ParentViewModel {

    public ReservationOrder order;
    public int position;
    public ObservableBoolean isAllowAction;
    public String cost,order_status = "";

    public ItemReservationViewModel(ReservationOrder order, int position, String status) {
        this.order = order;
        this.position = position;
        this.isAllowAction = new ObservableBoolean(status.equals("0"));
        cost = order.cost + " " + UserHelper.retrieveCurrency();
        if(order.order_status == 1){
            order_status = ResourceManager.getString(R.string.confirm);
        }else if(order.order_status == 2){
            order_status = ResourceManager.getString(R.string.finished);
        }else if(order.order_status == 3){
            order_status = ResourceManager.getString(R.string.cancelled);
        }
    }

    public void delete() {
        mMutableLiveData.setValue(new Mutable(Constants.DELETE, position));
    }

    public void submit() {
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT, position));
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
