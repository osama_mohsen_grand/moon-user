
package grand.app.moon.viewmodels.app;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;

import java.util.ArrayList;

import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.models.base.IdName;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.views.fragments.base.ZoomFragment;
import grand.app.moon.views.fragments.intro.Slide1Fragment;
import grand.app.moon.views.fragments.intro.Slide2Fragment;
import grand.app.moon.views.fragments.intro.Slide3Fragment;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ZoomViewModel extends ParentViewModel {

    public String image;
    public ZoomViewModel(){}
    public ZoomViewModel(String image) {
        this.image = image;
    }
    public String getZoomImageUrl() {
        return image;
    }
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    public void setImages(ArrayList<IdName> images) {
        tabModels.clear();
        for(IdName image : images){
            ZoomFragment zoomFragment = new ZoomFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.IMAGE,image.name);
            zoomFragment.setArguments(bundle);
            tabModels.add(new TabModel("",zoomFragment));
        }
    }



}
