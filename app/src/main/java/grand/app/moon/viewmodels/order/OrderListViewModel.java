package grand.app.moon.viewmodels.order;


import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.repository.OrderRepository;
import grand.app.moon.repository.service.TagsRepository;
import grand.app.moon.utils.Constants;

public class OrderListViewModel extends ParentViewModel {
    private OrderRepository orderRepository;
    public int delete_position = -1;
    public OrderListViewModel() {
        orderRepository = new OrderRepository(mMutableLiveData);
        orderRepository.getOrders();
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void deleteOrder(int position) {
        delete_position = position;
        orderRepository.deleteOrder(orderRepository.getOrderListResponse().orders.get(position).id);
    }
}
