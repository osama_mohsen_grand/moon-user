package grand.app.moon.viewmodels.service;


import android.graphics.drawable.Drawable;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.utils.resources.ResourceManager;

public class ItemServiceViewModel extends ParentViewModel {
    public Service service ;
    public int position = 0;
    public boolean selected = false;
    public int width = 0;
    public int drawable = -1;

    public ItemServiceViewModel(Service service, int position, boolean selected) {
        this.service = service;
        this.position = position;
        this.selected = selected;
        notifyChange();
    }


    public int getColor(){
        if(selected)
            return ResourceManager.getColor(R.color.colorPrimary);
        return ResourceManager.getColor(R.color.colorBlack);
    }

    public Drawable getDrawable(){
        if(selected)
            return ResourceManager.getDrawable(R.drawable.border_background_primary_strock);
        return ResourceManager.getDrawable(R.drawable.border_background_silver_strock);
    }


    public String getImageUrl(){
        return service.mImage;
    }

    public int getColorSrc(){
        if(selected)
            return ResourceManager.getColor(R.color.colorPrimary);
        return ResourceManager.getColor(R.color.colorBlack);
    }

    public int getWidth() {
        return 3;
    }


    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
