
package grand.app.moon.viewmodels.search;

import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.search.SearchRepository;
import grand.app.moon.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class SearchViewModel extends ParentViewModel {

    int type = -1,flag = -1 ,service_id = -1;
    public String text = "";
    public SearchRepository searchRepository;

    public SearchViewModel(int service_id, int flag, int type) {
        searchRepository = new SearchRepository(mMutableLiveData);
        this.service_id = service_id;
        this.flag = flag;
        this.type = type;
    }

    public SearchViewModel(){
        searchRepository = new SearchRepository(mMutableLiveData);
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void submitSearch(String search) {
        searchRepository.submitSearch(search, type, flag, service_id);
    }
}
