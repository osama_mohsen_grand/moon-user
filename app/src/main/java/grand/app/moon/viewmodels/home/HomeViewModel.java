
package grand.app.moon.viewmodels.home;

import java.util.Timer;
import java.util.TimerTask;

import grand.app.moon.base.ParentViewModel;
import grand.app.moon.repository.HomeRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.storage.SharedPreferenceHelper;
import grand.app.moon.models.location.LocationRequest;
import grand.app.moon.repository.LocationRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class HomeViewModel extends ParentViewModel {
    private static final String TAG = "HomeViewModel";
    HomeRepository homeRepository;
    public MapConfig mapConfig;
    public String[] images = new String[0];

    public HomeViewModel() {
        homeRepository = new HomeRepository(mMutableLiveData);
        homeRepository.home();
    }


    public String[] getImages(){
        if(homeRepository.getHomeResponse() != null) {
            images = new String[homeRepository.getHomeResponse().mSliders.size()];
            for (int i = 0; i < homeRepository.getHomeResponse().mSliders.size(); i++)
                images[i] = homeRepository.getHomeResponse().mSliders.get(i).mImage;
        }
        return images;
    }

    
    
    public HomeRepository getHomeRepository() {
        return homeRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
