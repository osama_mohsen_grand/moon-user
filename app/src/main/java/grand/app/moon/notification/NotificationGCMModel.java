package grand.app.moon.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationGCMModel implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("order_id")
    @Expose
    public int order_id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("body")
    @Expose
    public String body;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("audio")
    @Expose
    public String audio;
    @SerializedName("type")
    @Expose
    public int type;
    @SerializedName("notification_type")
    @Expose
    public int notification_type;
    @SerializedName("sender_id")
    @Expose
    public String senderId;
    @SerializedName("type_data")
    @Expose
    public int type_data;
    @SerializedName("receiver_type")
    @Expose
    public int receiver_type;
    @SerializedName("receiver_id")
    @Expose
    public int receiver_id;
    @SerializedName("sender_type")
    @Expose
    public int sender_type;

    @SerializedName("finish")
    @Expose
    public int finish;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("chats_count")
    @Expose
    public int chatCount;

}
