package grand.app.moon.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

import androidx.core.app.NotificationCompat;
import grand.app.moon.R;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.user.profile.User;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.SharedPreferenceHelper;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.TrackActivity;
import timber.log.Timber;

import static android.app.NotificationManager.IMPORTANCE_HIGH;


public class GCMNotificationIntentService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        SharedPreferenceHelper.saveKey("token", s);
        Log.e("token", s);
    }


    private static final String TAG = "GCMNotificationIntentSe";

    String CHANNEL_ID = "MOON";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //ar_text , en_text
        Timber.e("notification: start Here");
        if (UserHelper.getUserId() != -1) {
            String details = remoteMessage.getData().get(Constants.MESSAGE);
            Timber.e("details_notification:" + details);
            NotificationGCMModel notificationGCMModel = AppMoon.getNotification(details);
            String title = notificationGCMModel.title, message = notificationGCMModel.body;
            Intent intent = null;
            if ((notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_TRUCKS) || notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_TAXI))
                    ) {
                //&& notificationGCMModel.finish != 1
                Timber.e("activity:track");
                intent = new Intent(this, TrackActivity.class);
                intent.putExtra(Constants.TRIP_ID, notificationGCMModel.order_id);
                intent.putExtra(Constants.TYPE, notificationGCMModel.notification_type);

            } else {
                Timber.e("activity:base");
                intent = new Intent(this, BaseActivity.class);
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_NOTIFICATION, notificationGCMModel);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
            sendBroadCast(notificationGCMModel);

            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder notification = new NotificationCompat.Builder(this,CHANNEL_ID);

            notification.setColor(getResources().getColor(R.color.colorPrimary));
            notification.setPriority(IMPORTANCE_HIGH);
            notification.setContentTitle(title);
            notification.setColor(getResources().getColor(R.color.colorPrimary));
            notification.setContentText(message);
            notification.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            notification.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            //        notification.setAutoCancel(false);
            notification.setAutoCancel(true);
            notification.setShowWhen(true);

            try{
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), defaultSoundUri);
                r.play();
            }catch (Exception exception){
                exception.printStackTrace();
            }
            notification.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
            notification.setContentIntent(pendingIntent);

            int currentApiVersion = Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
                notification
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
            } else {
                notification.setSmallIcon(R.mipmap.ic_launcher);
            }

            int random = new Random().nextInt(10000000) + 1;
            //Android O
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                show_Notification(title, message, pendingIntent, random);
            } else
                notificationManager.notify(random, notification.build());
        }
    }


    public void show_Notification(String title, String body, PendingIntent pendingIntent,int number) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = null;

            notificationChannel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);

//            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 1, intent, 0);
            Notification notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                    .setContentText(title)
                    .setContentTitle(body)
                    .setContentIntent(pendingIntent)
                    .setChannelId(CHANNEL_ID)
                    .setNumber(number)
                    .setSmallIcon(R.drawable.ic_logo_original)
                    .build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(1, notification);
        }

    }


    private void sendBroadCast(NotificationGCMModel notificationGCMModel) {

        Log.e(TAG, "sendBroadCast: " );
        Intent intent = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_NOTIFICATION, notificationGCMModel);
        if (notificationGCMModel.notification_type == 1) {//new order
            intent = new Intent(Constants.NEW_ORDER);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
            getApplicationContext().sendBroadcast(intent);
        } else if (notificationGCMModel.notification_type == 2 || notificationGCMModel.notification_type == 5) {//chat
            intent = new Intent(Constants.CHAT);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
            getApplicationContext().sendBroadcast(intent);
        } else if ((notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) ||
                notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY))
                && notificationGCMModel.finish == 1) {
            Timber.e("details_notification:FINISH");
            intent = new Intent(Constants.RATE_NOTIFICATION);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
            getApplicationContext().sendBroadcast(intent);
        } else if ((notificationGCMModel.status == 2 || notificationGCMModel.status == 5) &&
                (notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_TRUCKS) || notificationGCMModel.notification_type == Integer.parseInt(Constants.DEFAULT_TAXI))) {
            intent = new Intent(Constants.CANCELED);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
            getApplicationContext().sendBroadcast(intent);
        }else if(notificationGCMModel.status == 6 &&
                (notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_TRUCKS) || notificationGCMModel.notification_type == Integer.parseInt(Constants.DEFAULT_TAXI))){
            Log.e(TAG, "sendBroadCast: HANDLE UI" );
            intent = new Intent(Constants.HANDLE_UI);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
            getApplicationContext().sendBroadcast(intent);
        }
        Log.d(TAG,"chat imagesCount");
        User user = UserHelper.getUserDetails();
        user.chats_count = notificationGCMModel.chatCount;
        UserHelper.saveUserDetails(user);
        Intent intent2 = new Intent(Constants.NOTIFICATION);
        getApplicationContext().sendBroadcast(intent2);
    }
}
