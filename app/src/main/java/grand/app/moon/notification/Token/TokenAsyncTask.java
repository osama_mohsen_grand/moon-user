package grand.app.moon.notification.Token;

import android.content.Context;
import android.os.AsyncTask;

import grand.app.moon.utils.storage.SharedPreferenceHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;


/**
 * Created by osama on 23/01/2018.
 */

public class TokenAsyncTask extends AsyncTask<Void,Void,Void> {
    Context context;

    public TokenAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String token = SharedPreferenceHelper.getKey("token");
        ConnectionHelper connectionHelper = new ConnectionHelper(new ConnectionListener(){
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        });
//        connectionHelper.requestJsonObject();
        return null;
    }
}
