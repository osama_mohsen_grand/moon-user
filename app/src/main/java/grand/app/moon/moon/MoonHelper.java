package grand.app.moon.moon;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.utils.Constants;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.ads.AdvertisementDetailsFragment;
import grand.app.moon.views.fragments.ads.AdvertisementFragment;

public class MoonHelper {

    public static void shopDetails(Context context, int type, int flag, int id, String name) {
        Service service = new Service();
        service.mType = type;
        service.mFlag = flag;
        if (service.mType == Integer.parseInt(Constants.TYPE_INSTITUTIONS)) {
            //SHOP_DETAILS
            Intent intent = new Intent(context, BaseActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.ID, id);
            bundle.putSerializable(Constants.SERVICE, service);
            intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
//            intent.putExtra(Constants.NAME_BAR, name);
            intent.putExtra(Constants.BUNDLE, bundle);
            context.startActivity(intent);
        } else if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) ||
                service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.CLINIC_DETAILS);
            intent.putExtra(Constants.NAME_BAR, name);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.SERVICE, service);
            bundle.putInt(Constants.ID, id);
            intent.putExtra(Constants.BUNDLE, bundle);
            context.startActivity(intent);
        }
//        else if (service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING)) {
//            Intent intent = new Intent(context, BaseActivity.class);
//            intent.putExtra(Constants.PAGE, Constants.ADS_DETAILS);
//            intent.putExtra(Constants.NAME_BAR, name);
//            Bundle bundle = new Bundle();
//            bundle.putInt(Constants.TYPE, service.mType);
//            bundle.putInt(Constants.ID, id);
//            intent.putExtra(Constants.BUNDLE, bundle);
//            context.startActivity(intent);
//        }
        else if ((service.mType == Integer.parseInt(Constants.TYPE_COMPANIES) && service.mFlag == 0)) {
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.NAME_BAR, name);
            intent.putExtra(Constants.PAGE, AdvertisementDetailsFragment.class.getName());
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.ID,id);
            bundle.putInt(Constants.TYPE,type);
            intent.putExtra(Constants.BUNDLE,bundle);
            context.startActivity(intent);
        } else if (service.mType == Integer.parseInt(Constants.TYPE_PHOTOGRAPHER) || service.mType == Integer.parseInt(Constants.TYPE_FAMOUS_PEOPLE)) {
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
            Bundle bundle = new Bundle();
            intent.putExtra(Constants.NAME_BAR, name);
            bundle.putSerializable(Constants.SERVICE, service);
            bundle.putInt(Constants.ID, id);
            intent.putExtra(Constants.BUNDLE, bundle);
            context.startActivity(intent);
        } else {
            Intent intent = new Intent(context, BaseActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.ID, id);
            bundle.putInt(Constants.TYPE, service.mType);
            bundle.putSerializable(Constants.SERVICE, service);
            intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
//            intent.putExtra(Constants.NAME_BAR, name);
            intent.putExtra(Constants.BUNDLE, bundle);
            context.startActivity(intent);
        }
    }

    private static final String TAG = "MoonHelper";
    public static void sortAds(List<AdsCompanyModel> adsList){


        Collections.sort(adsList, new Comparator<AdsCompanyModel>()
        {
            @Override
            public int compare(AdsCompanyModel lhs, AdsCompanyModel rhs) {
                return Integer.valueOf(rhs.id).compareTo(lhs.id);
            }
        });

        for(AdsCompanyModel ads : adsList){
            Log.d(TAG,ads.id+"");
        }

//        List<ToSort> sortList = new ArrayList<ToSort>();
//        for(AdsCompanyModel ads : adsList){
//            sortList.add(new ToSort(ads,""+ads.id));
//        }
//        Collections.sort(sortList);
//        for(ToSort toSort : sortList){
//            Log.d(TAG,toSort.id);
//        }
    }

    public static void getCities(String countryId) {

    }


    public static class ToSort implements Comparable<ToSort> {

        private AdsCompanyModel val;
        private String id;

        public ToSort(AdsCompanyModel val, String id){
            this.val = val;
            this.id = id;
        }

        @Override
        public int compareTo(ToSort f) {

            if (val.id > f.val.id) {
                return 1;
            }
            else if (val.id <  f.val.id) {
                return -1;
            }
            else {
                return 0;
            }
        }

        @Override
        public String toString(){
            return this.id;
        }
    }
}
