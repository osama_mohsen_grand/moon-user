package grand.app.moon.map;

import android.location.Location;

public interface FetchLocationListener {
    public void location(Location location);
}
