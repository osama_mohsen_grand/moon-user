package grand.app.moon.vollyutils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import grand.app.moon.BuildConfig;
import grand.app.moon.R;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.views.activities.MainActivity;

import static com.facebook.FacebookSdk.getCacheDir;

/**
 * Sketch Project Studio
 * Created by Angga on 12/04/2016 14.27.
 */
public class AppHelper {

    public static String getDeviceIdWithoutPermission() {
        return Settings.Secure.getString(MyApplication.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    /**
     * p
     * Turn drawable resource into byte array.
     *
     * @param context parent context
     * @param id      drawable resource id
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static boolean isAndroid30() {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.Q;
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }


    public static void shareCustom(Context context, String title, String message) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        final String appPackageName = MyApplication.getInstance().getPackageName();
        String messageDisplay = "https://play.google.com/store/apps/details?id=" + appPackageName + "\n\n" + message;
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, messageDisplay);
        context.startActivity(Intent.createChooser(intent, ResourceManager.getString(R.string.share)));
    }


    public static Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file = new File(MyApplication.getInstance().getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            // **Warning:** This will fail for API >= 24, use a FileProvider as shown below instead.
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    public static Uri getUriImage(String image) {
        return Uri.parse(image);
    }

    public static void shareDynamicLink(Activity activity, String title, String message, Uri image) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        if (image != null) {
            intent.putExtra(Intent.EXTRA_STREAM, image);
            intent.setType("image/*");
        } else
            intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.share)));
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }

    public static Uri getUriFromBitmap(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    private static final String TAG = "AppHelper";


    public static void shareCustom(Activity activity, String title, String message, ImageView imageView) {
        // save bitmap to cache directory
        try {
            imageView.invalidate();
            BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();

            File cachePath = new File(getCacheDir(), "images");
            if (!cachePath.exists())
                cachePath.mkdirs(); // don't forget to make the directory

            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
            if (bitmapDrawable != null && bitmapDrawable.getBitmap() != null) {
                bitmapDrawable.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, stream);
                stream.close();
                share(activity,title,message);
            } else
                shareCustom(activity, title, message);


        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "exe:" + e.getMessage());
        }
    }

    public static void share(Context context,String title , String message){
        File imagePath = new File(getCacheDir(), "images");
        File newFile = new File(imagePath, "image.png");
        Uri contentUri = FileProvider.getUriForFile(MyApplication.getInstance(), BuildConfig.APPLICATION_ID + ".fileprovider", newFile);
//
        if (contentUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
            shareIntent.setDataAndType(contentUri, context.getContentResolver().getType(contentUri));
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            shareIntent.setType("*/*");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, ResourceManager.getString(R.string.app_name));
            shareIntent.putExtra(Intent.EXTRA_TEXT, title + "\n" + message);
            context.startActivity(Intent.createChooser(shareIntent, ResourceManager.getString(R.string.share)));
        }
    }

    public static void shareCustom(Activity activity, String title, String message, Bitmap bitmap) {
        // save bitmap to cache directory
        try {

            File cachePath = new File(getCacheDir(), "images");
            if (!cachePath.exists())
                cachePath.mkdirs(); // don't forget to make the directory

            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.close();
            share(activity,title,message);

        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "exe:" + e.getMessage());
        }
    }

    public static class BitmapAsyncTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }
    }
}