package grand.app.moon.vollyutils;

public class URLS {
        /*
        google-site-verification=XD3nf2sTDc5nBykVnP8y1fFI-Z7FqiqRWvf1J8iBCGQ
        https://developers.google.com/digital-asset-links/tools/generator


        [{
  "relation": ["delegate_permission/common.handle_all_urls"],
  "target" : { "namespace": "android_app", "package_name": "grand.app.moon",
               "sha256_cert_fingerprints": ["BC:8C:82:09:B8:C1:09:B1:3E:33:FA:81:EA:11:9A:69:30:7A:39:2B"] }
}]


         */
        //https://moon4online.com?product_id=1&flag=2 ==> redirect limk
        ///https://moon4online.com//1/2/3
        //https://moonfouronline.page.link/.well-known/assetlinks.json
        public static String BASE_DOMAIN = "https://moon4online.com/";

        public static String BASE_URL_WEB = "https://moonfouronline.page.link";
        public static String BASE_URL = "https://moon4online.com/api/";
//        public static String BASE_URL = "http://demo.moon4online.com/api/";
        public  final static String DEFAULT_IMAGE = "https://moon4online.com/images/profile/user.png";

        //here
        public static String FAVOURITE = "user/favorite";
        public static String FAVOURITE_ADD = "user/favorite/add_or_remove";


        public static String ACCOUNT_INFO = "account-info?";

        public static String MEDIA_VIEW = "media-view";

        //======
        public static String FIREBASE = "firebase";

        public static String LOGIN = "login";

        public static String REGISTER_USER = "register";

        public static String REGISTER_SHOP = "shops/addShop";

        public static String GET_ALL_COUNTRIES = "getCountries";

        public static String CODE_SEND = "driver/code_send";

        public static String CHECK_PHONE = "checkPhone";
        public static String CHANGE_PASSWORD = "changeForgetPassword";

        public static String HOME = "services";



        public static String INSTITUTION_DETAILS = "Institution/institutions_list";

        public static String SERVICE_DETAILS = "shops_list";

        public static final String TAGS = "tags";
        public static final String DISCOVER = "discover";
        public static final String DISCOVER_ALL = "all_discovers";
        public static final String DISCOVER_ALL_WITH_SERVICES = "famous/main-service-categories";
        public static final String SHOPS_DISCOVER = "shops-discover";

        //famous/main-service-categories
        public static String ADD_CATEGORY = "shops/addCategory";

        public static String DELETE_CATEGORY = "shops/deleteCategory";

        public static String SHOP_SIZE_COLOR = "shops/SizesColors";
        public static String REGISTER = "register";

        public static String NOTIFICATION = "notification";
        public static String DELETE_NOTIFICATION = "delete-Notification/";

        public static String CLINIC_LIST = "clinics/clinics_list";
        public static String CLINIC_DETAILS = "clinics/clinicDetails";
        public static String CLINIC_DOCTOR_DETAILS = "clinics/doctorSchedules";
        public static String CLINIC_RESERVATION_ORDER = "clinics/reservation_order";
        public static String CLINIC_RESERVATION_LIST = "clinics/orders/list";
        public static String CLINIC_RESERVATION_DETAILS = "clinics/orderDetails";
        public static String CLINIC_DELETE = "clinics/confirmOrder";



        public static String LOG_CRASH = "logcrash";
        public static String TRUCK_LIST = "taxi/trucks_list";
        public static String TRUCK_CREATE_ORDER = "taxi/user/create";


        public static String SHOP_DETAILS = "shopDetails";
        public static String SHOP_PRODUCTS = "getProductFromCategory";
        public static String SHOP_PRODUCT_DETAILS = "getDetailsProduct";
        public static String FOLLOW = "create_follow";
        public static String RATE = "create_rate";
        public static String STORIES = "stories";

        public static String SERVICES = "shops_services";


        public static String ADS_LIST = "Advertise/advertisements_list";
        public static String ADS_FILTER = "companies/services_filter";

        public static String ADS_DETAILS = "Advertise/advertiseDetails";

        public static String ADD_ADV = "Advertise/add_advertise";

        public static String UPDATE_ADV = "Advertise/editAdvertise";

        public static String DELETE_ADV = "Advertise/deleteAdvertise";

        public static String FAMOUS = "famous";

        public static String FAMOUS_ADD_ALBUM = "famous/addAlbum";

        public static String FAMOUS_ALBUMS = "famous/albums";

        public static String FAMOUS_FILTER = "famous/filter";

        public static String FILTER_SHOP_ADS = "filter-shop-ads";

        public static String MY_ADS = "Advertise/advertisements";

        public static String FAMOUS_FILTER_SHOP_ADS = "famous/filterShopAdds";

        public static String COMPANIES_LIST = "companies/companies_list";

        public static String ALBUM_IMAGES = "famous/albumsImages";

        public static String FAMOUS_SEARCH_SHOP = "famous/searchShop";
        public static String SEARCH_SHOP = "searchShop";
        public static String SEARCH_HOME = "home-search";

        public static String FAMOUS_DETAILS = "famous/details";

        public static String FAMOUS_FOLLOW = "famous/create_follow";

        public static String FAMOUS_DELETE_IMAGE = "famous/deleteImage";

        public static String FAMOUS_EDIT_ALBUM = "famous/edit_album";

        public static String FAMOUS_DISCOVER = "famous/discover";

        public static String FAMOUS_ALBUM_SEARCH = "famous/searchAlbum";

        public static String FAMOUS_SEARCH = "famous/searchFamous";

        public static String SHOPS_CREDIT_CARD = "shops/creditCard";

        public static final String BE_SHOP = "famous/addShop";

        public static String ADVERTISE_DETAILS = "famous/advertisement_details";

        public static String FACEBOOK_LOGIN = "facebook_login";

        public static String UPDATE_PROFILE = "profile/edit";

        public static String PACKAGES = "packages";

        public static String ADD_PACKAGE = "addPackage";

        public static String PRODUCTS = "shops/products";

        public static String PRODUCT_DETAILS = "shops/productDetails";

        public static String ADD_PRODUCT = "shops/addProduct";

        public static String EDIT_PRODUCT = "shops/editProduct";

        public static String DELETE_PRODUCT = "shops/deleteProduct";

        public static String STATUS_LIST = "shops/Status";

        public static String ADD_STATUS = "shops/addStatus";

        public static String DELETE_STATUS = "shops/deleteStatus";

        public static String SETTINGS = "settings";
        public static String CONTACT_US = "contact-us";

        public static String STORY_VIEW = "story-view";


        public static String TRIP_HISTORY = "taxi/trip_history";


        public static String OFFERS = "shops/offers";

        public static String DELETE_OFFER = "shops/deleteOffer";

        public static String ADD_OFFER = "shops/addOffer";

        public static String SHOP_REVIEW = "userReviewShop";

        public static String SHOP_ADD_REVIEW = "addReviewShop";

        public static String TRIP_ARRIVE = "driver/arrive";

        public static String TRIP_ACTION = "taxi/driver/accept_trip";

        public static String TRIP_DETAILS = "taxi/trip_details";

        public static String CODE_CHECK = "driver/code_check";


        public static String ORDER_CONFIRM = "order/user/confirm";

        public static String ORDER_SHIPPING = "user/getShippingCompany";

        public static String ORDER_LIST = "order/user/list";

        public static String ORDER_DETAILS = "order/user/orderDetails";
        public static String ORDER_DELETE = "order/user/order-delete/";

        public static String CART_ADD = "user/cart/add_or_update";

        public static String CART_DELETE = "user/cart/delete";

        public static String CART_USER = "user/cart";

        public static String CART_SHIPPING = "user/cart";

        public static String CREDIT = "driver/credit";

        public static String UPDATE_PAYMENT = "driver/update_payment";

        public static String CHAT_GET = "chat/all_chats";

        public static String CHAT_DETAILS = "order/user/chat";
        public static String CHAT_DETAILS_SUBMIT = "order/user/orderChat";
        public static String CHAT = "chat";

        public static String CHAT_SEND = "chat/create_chat";
        public static String FAMOUS_COMPANY_ADS_CATEGORIES = "famous/famous-ads-filter";




    }

