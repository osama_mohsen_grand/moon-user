package grand.app.moon.vollyutils;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.upload.CompressObject;
import grand.app.moon.utils.upload.ImageHelper;

//n2es hena 2nna n3mel check 3la type al file swa2 image , video 34an n2dr nb3to be retrofit

public class VolleyFileObject implements Serializable {
    private String filePath = "",paramName;
    private int fileType;
    private Uri uri;
    private CompressObject compressObject;
    private File file;



    public VolleyFileObject(String paramName , String filePath, int fileType) {
        this.paramName=paramName;
        this.filePath=filePath;
        this.fileType=fileType;
        this.file = new File(filePath);
        if(file.exists()){
            setBitmap(BitmapFactory.decodeFile(filePath));
//            renameFile();
        }
//        compressImage();
    }

    public void renameFile(){
        Log.d("filePath",filePath);
        String[] parts = filePath.split("/");
        if (parts.length > 0) {
            Log.d("filePath","parts > 0");
            String paths = parts[0];

            for (int i = 1; i < parts.length - 1; i++) {
                paths += "/" + parts[i];
            }
            String fileName = parts[parts.length - 1];
            int x = fileName.indexOf(".");
            String value = fileName.substring(x + 1);
            String newPath = "application." + value;
            File from = new File(paths, fileName);
            File to = new File(paths, newPath);
            from.renameTo(to);
            setFile(to);
            setFilePath(newPath);
            Log.d("filePath",paths+newPath);
            Log.d("filePathNewAb",to.getAbsolutePath());
            Log.d("filePathNewPa",to.getPath());
            setBitmap(BitmapFactory.decodeFile(to.getAbsolutePath()));
        }
//        volleyFileObject.setCompressObject(compressObject);

    }


    public Bitmap getResizedBitmap(File file) {
        int maxSize = 400;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),bmOptions);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }
    public void compressImage() {
        if(fileType == Constants.FILE_TYPE_IMAGE) {
            File imageFile = new File(getFilePath());
            Bitmap bitmap = getResizedBitmap(imageFile);
            setBitmap(bitmap);
            OutputStream os;
            try {
                os = new FileOutputStream(imageFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, os);
                os.flush();
                os.close();
            } catch (Exception e) {
                Log.e("err_compress_image", e.getMessage());
            }
            setFile(imageFile);
            setBitmap(bitmap);
        }
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public int getFileType() {
        return fileType;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public void setCompressObject(CompressObject compressObject) {
        this.compressObject = compressObject;
    }

    public byte[] getBytes(){
        File file = new File(getFilePath());
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    private Bitmap bitmap;
    public void setBitmap(Bitmap bitmap){
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public CompressObject getCompressObject() {
        return compressObject;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public Uri getUri() {
        return uri;
    }

    public void setFile(File file) {
        this.file = file;
        setFilePath();
    }

    private void setFilePath() {
        if(file != null && file.exists()){
            setFilePath(file.getAbsolutePath());
        }
    }

    public File getFile() {
        return file;
    }
}
