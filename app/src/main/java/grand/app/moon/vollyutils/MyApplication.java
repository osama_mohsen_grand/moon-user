package grand.app.moon.vollyutils;


import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.multidex.MultiDexApplication;
import grand.app.moon.BuildConfig;
import grand.app.moon.utils.crash.ExceptionHandler;
import grand.app.moon.base.ApplicationComponent;
import grand.app.moon.utils.images.ImageLoaderHelper;
import timber.log.Timber;

/**
 * Created by Akshay Raj on 7/17/2016.
 * Snow Corporation Inc.
 * www.snowcorp.org
 */
public class MyApplication extends MultiDexApplication {
    public static final String TAG = MyApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        new ImageLoaderHelper(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashReportingTree());
        }
//        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
//        Fabric.with(this, new Crashlytics());
//        Crashlytics.getInstance().crash();
//        Crashlytics.log("Your log");
//        Crashlytics.logException(new Throwable("This your not-fatal name"));


//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(true)
//                .build();
//
//        Fabric.with(fabric);


//        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
//        crashlytics.log("work in crash");
//
//        FirebaseCrashlytics.getInstance().setCustomKey("int_key", 50);

//        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        DataBindingUtil.setDefaultComponent(new ApplicationComponent());
    }

    private static class CrashReportingTree extends Timber.Tree {
        @Override protected void log(int priority, String tag, @NonNull String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }
        }
    }



    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

}