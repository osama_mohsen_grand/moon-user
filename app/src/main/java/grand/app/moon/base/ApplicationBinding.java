package grand.app.moon.base;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import grand.app.moon.R;
import grand.app.moon.customviews.views.CustomWebView;
import grand.app.moon.customviews.views.HtmlAudioHelper;
import grand.app.moon.customviews.views.WebInterface;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.images.zoom.ImageMatrixTouchHandler;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.base.ZoomPagerFragment;
import grand.app.moon.vollyutils.AppHelper;
import grand.app.moon.vollyutils.MyApplication;
import libs.mjn.scaletouchlistener.ScaleTouchListener;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import timber.log.Timber;

public class ApplicationBinding {

    @BindingAdapter("animation_submit")
    public static void animation(View view, IAnimationSubmit animationSubmit) {
        view.setOnTouchListener(new ScaleTouchListener() {
            @Override
            public void onClick(View v) {
                animationSubmit.animationSubmit();
            }
        });
    }

    public Integer getWidthHeight(){
        return 120;
    }

    @BindingAdapter("imageWH3")
    public static void imageWidth(View view, Integer widthW) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((FragmentActivity)view.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int widthInt = displayMetrics.widthPixels;
        Log.d(TAG,"width:"+widthInt);
        view.getLayoutParams().width = widthInt/3;
        view.getLayoutParams().height = view.getLayoutParams().width;
    }

    //image loader have issue in display image on google map with http://...test. <== with no extention
    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, String image) {
        Glide
                .with(imageView.getContext())
                .load(image)
                .dontTransform()
//                .centerCrop()
//                .thumbnail(0.05f)
//                .transition(DrawableTransitionOptions.withCrossFade())
//                .placeholder(R.drawable.progress_animation)
                .into(imageView);
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(CircleImageView imageView, Drawable drawable) {
        Glide
                .with(imageView.getContext())
                .load(drawable)
                .dontTransform()
//                .centerCrop()
//                .thumbnail(0.05f)
//                .transition(DrawableTransitionOptions.withCrossFade())
//                .placeholder(R.drawable.progress_animation)
                .into(imageView);
    }


    @BindingAdapter({"app:adapter", "app:span", "app:orientation"})
    public static void getItemsV2Binding(RecyclerView recyclerView, RecyclerView.Adapter<?> itemsAdapter, String spanCount, String orientation) {
        if (orientation.equals("1"))
            AppUtils.initVerticalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        else
            AppUtils.initHorizontalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        recyclerView.setAdapter(itemsAdapter);
    }


    @BindingAdapter({"app:adapterList", "app:span", "app:orientation"})
    public static void getItemsV2BindingList(RecyclerView recyclerView, RecyclerView.Adapter<?> itemsAdapter, String spanCount, String orientation) {
        if (orientation.equals("1"))
            AppUtils.initVerticalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        else
            AppUtils.initHorizontalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        recyclerView.setAdapter(itemsAdapter);
    }


    @BindingAdapter("width")
    public static void setWidth(LinearLayout linearLayout, int span) {
        if(span > 0) {
            int width = AppHelper.getScreenWidth(linearLayout.getContext()) / span;
            linearLayout.getLayoutParams().width = width;
        }
    }


    @BindingAdapter("width")
    public static void setWidth(RelativeLayout relativeLayout, int span) {
        if(span > 0) {
            int width = AppHelper.getScreenWidth(relativeLayout.getContext()) / span;
            relativeLayout.getLayoutParams().width = width;
        }
    }


    @BindingAdapter("drawable")
    public static void setBackgroundDrawable(RelativeLayout relativeLayout, Drawable drawable) {
        relativeLayout.setBackground(drawable);
    }

    @BindingAdapter("backgroundColor")
    public static void setBackgroundColor(RelativeLayout relativeLayout, String colorHex) {
        if(colorHex != null && !colorHex.equals("") && colorHex.startsWith("#"))
            relativeLayout.setBackgroundColor(Color.parseColor(colorHex));
    }

    @BindingAdapter("imageZoomUrl")
    public static void loadZoomImage(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
        imageView.setOnTouchListener(new ImageMatrixTouchHandler(MyApplication.getInstance()));
    }


    @BindingAdapter("audio")
    public static void loadAudio(PlayerView playerView, String audio) {
        SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(playerView.getContext());
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                .createMediaSource(Uri.parse(audio));
        player.setPlayWhenReady(false);
        player.prepare(mediaSource, true, false);
        playerView.setPlayer(player);
    }

    @BindingAdapter("audioHtml")
    public static void media(final CustomWebView webView,String url) {
        if(url != null) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webView.addJavascriptInterface(new WebInterface(webView.getContext()), "Android");
            String html = new HtmlAudioHelper().html(url);
            webView.loadData(html, "text/html", "UTF-8");
//            setExoPlayerView(simpleExoPlayerView,video);
        }

    }


    @BindingAdapter("timeMin")
    public static void time(TextView textView, String text) {
        textView.setText(text+" "+ ResourceManager.getString(R.string.min));
    }




    @BindingAdapter("color")
    public static void color(ImageView imageView, String color) {
        Timber.e("color:"+color);
        if(color != null && !color.equals("") && color.charAt(0) == '#') {
            imageView.setBackgroundColor(Color.parseColor(color));
        }
    }


    @BindingAdapter("txtColor")
    public static void setColor(TextView textView , int color){
        textView.setTextColor(color);
    }

    @BindingAdapter("colorSrc")
    public static void setColorSrc(ImageView imageView , int color){
        imageView.setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY);
    }



    @BindingAdapter("rate")
    public static void setRate(final RatingBar ratingBar, float rate) {
        ratingBar.setRating(rate);
    }

    @BindingAdapter("rate")
    public static void setRateMaterial(final MaterialRatingBar ratingBar, float rate) {
        ratingBar.setRating(rate);
    }

    @BindingAdapter("rating")
    public static void setRating(final RatingBar ratingBar, float rate) {
        ratingBar.setRating(rate);
    }

    @BindingAdapter("images")
    public static void setImages(final SliderLayout sliderLayout, String[] images) {
        Log.e("slider","start");
        ArrayList<IdName> imgs = new ArrayList<>();
        if(images != null) {
            imgs.clear();
            sliderLayout.removeAllSliders();
            Log.e("setImageSlider", "" + images.length);
            try {
                for (int i = 0; i < images.length; i++) {
                    final String image = images[i];
                    try {

                        DefaultSliderView defaultSliderView;
                        defaultSliderView = new DefaultSliderView(sliderLayout.getContext());
                        defaultSliderView
                                .image(image)
                                .setScaleType(BaseSliderView.ScaleType.Fit);
                        defaultSliderView.bundle(new Bundle());
                        defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
//                                Intent intent = new Intent(sliderLayout.getContext(), BaseActivity.class);
//                                intent.putExtra(Constants.PAGE, Constants.ZOOM);
//                                Bundle bundle = new Bundle();
//                                bundle.putString(Constants.IMAGE, image);
//                                intent.putExtra(Constants.BUNDLE,bundle);
//                                sliderLayout.getContext().startActivity(intent);

                                Intent intent = new Intent(sliderLayout.getContext(), BaseActivity.class);
                                intent.putExtra(Constants.PAGE, ZoomPagerFragment.class.getName());
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(Constants.IMAGES, imgs);
                                bundle.putInt(Constants.POSITION,sliderLayout.getCurrentPosition());
                                intent.putExtra(Constants.BUNDLE,bundle);
                                sliderLayout.getContext().startActivity(intent);
                            }
                        });
                        sliderLayout.addSlider(defaultSliderView);
                        sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
                    } catch (Exception e) {
                        Log.e("setImageSliderException", "" + e.getMessage());
                        //  Toast.makeText(getApplicationContext(),"",Toast.LENGTH_LONG).show();
                    }
                    imgs.add(new IdName("0",image));
                }

                sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                sliderLayout.getPagerIndicator().setDefaultIndicatorColor(sliderLayout.getContext().getResources().getColor(R.color.colorAccent), sliderLayout.getContext().getResources().getColor(R.color.colorSilver));
                sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
            } catch (Exception e) {
                Log.e("Exception", "" + e.getMessage());
                e.getStackTrace();
            }
        }
    }


    private static final String TAG = "ApplicationBinding";

    @BindingAdapter("imagesVideoSlider")
    public static void setImagesVideoSlider(final SliderLayout sliderLayout, List<ImageVideo> images) {
        Log.e("slider","start");
        if(images != null) {
            sliderLayout.removeAllSliders();
            Log.e("setImageSlider", "" + images.size());
            try {
                for (int i = 0; i < images.size(); i++) {
                    String image =  images.get(i).image;;
                    if(images.get(i).type == 2) image = images.get(i).capture;
                    try {

                        DefaultSliderView defaultSliderView;
                        defaultSliderView = new DefaultSliderView(sliderLayout.getContext());
                        defaultSliderView
                                .image(image)
                                .setScaleType(BaseSliderView.ScaleType.Fit);
                        defaultSliderView.bundle(new Bundle());
                        int finalI = i;
                        defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                                Intent intent = new Intent(sliderLayout.getContext(), BaseActivity.class);

                                Bundle bundle = new Bundle();
                                Log.d(TAG, images.get(finalI).image);
                                if(images.get(finalI).type == 1) {
                                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                                    bundle.putString(Constants.IMAGE, images.get(finalI).image);
                                }else {
                                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                                    bundle.putString(Constants.VIDEO, images.get(finalI).image);
                                }
                                intent.putExtra(Constants.BUNDLE,bundle);
                                sliderLayout.getContext().startActivity(intent);

                            }
                        });
                        sliderLayout.addSlider(defaultSliderView);
                        sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
                    } catch (Exception e) {
                        Log.e("setImageSliderException", "" + e.getMessage());
                        //  Toast.makeText(getApplicationContext(),"",Toast.LENGTH_LONG).show();
                    }

                }

                sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                sliderLayout.getPagerIndicator().setDefaultIndicatorColor(sliderLayout.getContext().getResources().getColor(R.color.colorAccent),
                        sliderLayout.getContext().getResources().getColor(R.color.colorSilver));
                sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
            } catch (Exception e) {
                Log.e("Exception", "" + e.getMessage());
                e.getStackTrace();
            }
        }
    }


    public static DrawableCrossFadeFactory factory =
            new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();

//    public static void loadImage(ImageView imageView,String imageUrl){
//        Glide.with(imageView.getContext())
//                .load(imageUrl)
//                .transition(withCrossFade(factory))
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .placeholder(R.mipmap.ic_launcher)
//                .into(imageView);
//    }

}
