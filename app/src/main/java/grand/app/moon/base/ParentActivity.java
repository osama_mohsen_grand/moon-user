package grand.app.moon.base;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.customviews.dialog.rate.DialogRateModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.notification.NotificationGCMModel;
import grand.app.moon.repository.FirebaseRepository;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.LanguagesHelper;
import grand.app.moon.utils.MyContextWrapper;
import grand.app.moon.utils.crash.MyExceptionHandler;
import grand.app.moon.utils.maputils.background.LocationBackground;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.location.LocationHelper;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import timber.log.Timber;

import static com.facebook.FacebookSdk.setAdvertiserIDCollectionEnabled;
import static com.facebook.FacebookSdk.setAutoLogAppEventsEnabled;

//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;


public class ParentActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener,
        View.OnClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,
//                ParentActivity.class));
        initializeLanguage();
//        initializeToken();
        initializeProgress();
    }


    protected void initFacebook() {
        if(!FacebookSdk.isInitialized()) {
            FacebookSdk.setApplicationId("353837505660520");
            FacebookSdk.sdkInitialize(getApplicationContext());
            setAutoLogAppEventsEnabled(true);
            FacebookSdk.fullyInitialize();
            FacebookSdk.setAutoInitEnabled(true);
            setAdvertiserIDCollectionEnabled(true);

        }
    }


    protected void initializeLanguage() {
        LanguagesHelper.getCurrentLanguage();
    }


    protected void initializeToken() {
        Log.d(TAG,"initializeToken");
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(task.isComplete() && task.getResult() != null){
                    String token = task.getResult();
                    UserHelper.saveKey(Constants.TOKEN,token);
                    new FirebaseRepository().updateToken(token);
                }
            }
        }) ;
//    {
//            if(it.isComplete){
//                fbToken = it.result.toString()
//                // DO your thing with your firebase token
//            }
//        }

//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(ParentActivity.this,
//                new OnSuccessListener<InstanceIdResult>() {
//                    @Override
//                    public void onSuccess(InstanceIdResult instanceIdResult) {
//                        String newToken = instanceIdResult.getToken();
//                        UserHelper.saveKey(Constants.TOKEN,newToken);
//                        if(UserHelper.getUserId() != -1) new FirebaseRepository().updateToken(newToken);
//                    }
//                });
    }

    Intent backgroundService = null;

    public void startBackgroundService() {
        if (!AppUtils.isServiceRunning(this, LocationBackground.class)) {
            backgroundService = new Intent(this, LocationBackground.class);
            stopService(backgroundService);
            startService(backgroundService);
        }
    }


    public void stopBackgroundService(){
        if(backgroundService != null) {
            stopService(backgroundService);
        }
    }

    BroadcastReceiver rate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent broadIntentExtra) {
            Bundle bundle = broadIntentExtra.getBundleExtra(Constants.BUNDLE_NOTIFICATION);
            NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
            DialogRateModel dialogRateModel = AppMoon.getDialogRateModel(notificationGCMModel);
            Intent intent = new Intent(context,BaseActivity.class);
            bundle.putSerializable(Constants.RATE_NOTIFICATION,dialogRateModel);
            intent.putExtra(Constants.PAGE,Constants.RATE_NOTIFICATION);
            intent.putExtra(Constants.NAME_BAR,getString(R.string.rate));
            intent.putExtra(Constants.BUNDLE,bundle);
            startActivity(intent);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(rate, new IntentFilter(Constants.RATE_NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(rate);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(MyContextWrapper.wrap(newBase, LanguagesHelper.getCurrentLanguage()));
        } else {
            super.attachBaseContext(newBase);
        }
    }


    protected Dialog dialogLoader;


    public void initializeProgress() {
        View view = LayoutInflater.from(this).inflate(R.layout.loader_animation, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.customDialog);
        builder.setView(view);
        dialogLoader = builder.create();
        dialogLoader.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                dialogLoader.dismiss();
                onBackPressed();
            }
        });
    }

    public void showProgress() {
        //show dialog
        if (dialogLoader != null && !this.isFinishing()) {
            Log.e("dialog", "HERE2");
            dialogLoader.show();
        }

    }

    public void hideProgress() {
        if (dialogLoader != null && dialogLoader.isShowing() && !this.isFinishing())
            dialogLoader.dismiss();
    }

    private static final String TAG = "ParentActivity";
    public void handleActions(String action, String baseError) {
        Timber.e(action);
        if (action.equals(Constants.SHOW_PROGRESS)) showProgress();
        else if (action.equals(Constants.HIDE_PROGRESS)) hideProgress();
        else if (action.equals(Constants.ERROR_RESPONSE) && !baseError.equals("")) showError(baseError);
        else if (action.equals(Constants.SERVER_ERROR)){
            hideProgress();
            showError(ResourceManager.getString(R.string.msg_server_error));
        }
        else if (action.equals(Constants.FAILURE_CONNECTION)) {
            hideProgress();
            noConnection();
        } else if (action.equals(Constants.LOGOUT)) {
            if(UserHelper.getUserId() != -1) {
                UserHelper.clearUserDetails();
                finishAffinity();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LOGIN);
                startActivity(intent);
            }
        }
    }

    public void noConnection() {
//        Snackbar snackBar = Snackbar.make(findViewById(R.id.ll_base_container),
//                getString(R.string.please_check_connection), Snackbar.LENGTH_LONG);
//        View view = snackBar.getView();
//        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
//        TextView textView = view.findViewById(R.id.snackbar_text);
//        textView.setGravity(Gravity.CENTER_VERTICAL);
//        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning, 0, 0, 0);
//        snackBar.show();
        Intent intent = new Intent(this,BaseActivity.class);
        intent.putExtra(Constants.PAGE,Constants.FAILURE_CONNECTION);
        finishAffinity();
        startActivity(intent);
    }

    public void showError(String msg) {
        Timber.e(msg);
        if (msg.equals(Constants.ERROR_LOGIN_RESPONSE))
            msg = getString(R.string.please_login_first);
        Snackbar snackBar = Snackbar.make(findViewById(R.id.ll_base_container),
                msg, Snackbar.LENGTH_LONG);
        View view = snackBar.getView();
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack));
        TextView textView = view.findViewById(R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning, 0, 0, 0);
        snackBar.show();
    }

    public void toastMessage(String message, int icon, int color) {
        Toasty.custom(this, message, icon, color, Toasty.LENGTH_SHORT, true, true).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if(permissions.length > 0 && grantResults.length > 0) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
                assert fragment != null;
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } catch (Exception ex) {
            Log.e("exceptionBase", ex.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if(resultCode == RESULT_OK) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
                assert fragment != null;
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception ex) {
            Log.e("exceptionBase", ex.getMessage());
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Timber.e("location "+location.getLatitude()+","+location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {

    }
}
