package grand.app.moon.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.upload.FileOperations;
import libs.mjn.scaletouchlistener.ScaleTouchListener;

public class BaseFragment extends Fragment {
    protected Context context;
    ScaleTouchListener.Config config;


    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        DataBindingUtil.setDefaultComponent(new ApplicationComponent());
        config = new ScaleTouchListener.Config(
                300,    // Duration
                0.75f,  // ScaleDown
                0.75f); // Alpha

        return null;
    }

    protected void pickImageDialogSelect(){
        FileOperations.pickImage(context,BaseFragment.this,Constants.FILE_TYPE_IMAGE);
    }

    protected void pickVideoDialogSelect(){
        FileOperations.pickVideo(context,BaseFragment.this, Constants.VIDEO_REQUEST_CODE);
    }


    public void handleActions(String action,String baseError){
        ((ParentActivity)context).handleActions(action,baseError);
    }


    public void showError(String msg){
        if(getActivity() != null) {
            ((ParentActivity)getActivity()).showError(msg);
        }
    }

    public ParentActivity getActivityBase(){
        return ((ParentActivity)getActivity());
    }

    public void toastMessage(String message , int icon , int color){
        ((ParentActivity)context).toastMessage(message,icon,color);
    }

    public void toastMessage(String message){
        ((ParentActivity)context).toastMessage(message, R.drawable.ic_check_white, R.color.colorPrimary);
    }

    public void toastInfo(String message ){
        Toasty.info(context, message, Toasty.LENGTH_SHORT).show();
    }

    public boolean isReloadPage(){
        if(UserHelper.retrieveKey(Constants.RELOAD).equals(Constants.TRUE)){
            UserHelper.saveKey(Constants.RELOAD,Constants.FALSE);
            return true;
        }
        return false;
    }

    public void reloadPage(){
        UserHelper.saveKey(Constants.RELOAD,Constants.TRUE);
    }

    public void restorePage(){
        UserHelper.saveKey(Constants.RELOAD,Constants.FALSE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void reloadPrevious(){
        Intent intent = new Intent();
        intent.putExtra(Constants.RELOAD,true);
        ((ParentActivity)context).setResult(Activity.RESULT_OK,intent);
        ((ParentActivity)context).finish();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

}
