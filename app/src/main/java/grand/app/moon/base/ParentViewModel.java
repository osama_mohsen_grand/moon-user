
package grand.app.moon.base;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.List;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import grand.app.moon.R;
import grand.app.moon.models.base.IdName;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public abstract class ParentViewModel extends ParentBaseObservableViewModel {
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    public ObservableField<String> noDataText = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.data));
    public ObservableBoolean noDataTextDisplay = new ObservableBoolean(false);
    public ObservableBoolean show = new ObservableBoolean(false);



    public static ObservableInt progress;
    public String baseError = "";

    Context context = null;
    Animation hyperspaceJumpAnimation;

    public ParentViewModel(Context context) {
        this.context = context;
        hyperspaceJumpAnimation = AnimationUtils.loadAnimation(MyApplication.getInstance(), R.anim.bonuce);
        progress = new ObservableInt(View.GONE);
    }

    public ParentViewModel(){

    }


    public void noData(){
        noDataTextDisplay.set(true);
        notifyChange();
    }

    public void haveData(List<?> list){
        noDataTextDisplay.set(list.size() == 0);
        notifyChange();
    }

    public void haveData(){
        noDataTextDisplay.set(false);
        notifyChange();
    }
    public void showPage(boolean show){
        this.show.set(show);
        notifyChange();
    }

    public MutableLiveData<Object> getMutableLiveData() {
        return mMutableLiveData;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }


}
