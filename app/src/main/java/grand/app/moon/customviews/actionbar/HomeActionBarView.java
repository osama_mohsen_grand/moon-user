package grand.app.moon.customviews.actionbar;

/**
 * Created by mohamedatef on 12/30/18.
 */


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import grand.app.moon.R;
import grand.app.moon.customviews.menu.NavigationDrawerView;
import grand.app.moon.databinding.LayoutActionBarHomeBinding;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.scan.ScanFragment;
import grand.app.moon.vollyutils.AppHelper;
import timber.log.Timber;

public class HomeActionBarView extends RelativeLayout {
    public LayoutActionBarHomeBinding layoutActionBarHomeBinding;
    NavigationDrawerView navigationDrawerView;
    DrawerLayout drawerLayout;
    ScrollView svMenu;
    public SearchAction searchAction;

    public HomeActionBarView(Context context) {
        super(context);
        init();
    }


    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarHomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_home, this, true);
        setEvents();
    }

    private void setEvents() {
        layoutActionBarHomeBinding.imgHomeBarMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                connectDrawer(HomeActionBarView.this.drawerLayout, false);
            }
        });
        layoutActionBarHomeBinding.imgHomeBarSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                searchAction.searchSubmit();
                Intent intent = new Intent(getContext(), BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.SEARCH);
                Bundle bundle = new Bundle();
                intent.putExtra(Constants.BUNDLE,bundle);
                getContext().startActivity(intent);


            }
        });

        layoutActionBarHomeBinding.imgHomeBarScan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                    Intent intent = new Intent(getContext(), BaseActivity.class);
                    intent.putExtra(Constants.PAGE, ScanFragment.class.getName());
                    getContext().startActivity(intent);
                }else{
                    ActivityCompat.requestPermissions((Activity) getContext(),
                            new String[]{Manifest.permission.CAMERA},
                            1007);
                }
//                    ActivityCompat.requestPermissions(getContext().getApplicationContext().re, new String[]{ Manifest.permission.CAMERA}, 1007);

            }
        });
    }

    public void setTitle(String title) {
        layoutActionBarHomeBinding.tvHomeBarText.setText(title);
    }

    public void connectDrawer(DrawerLayout drawerLayout, boolean firstConnect) {

        if (firstConnect) {
            this.drawerLayout = drawerLayout;
            return;
        } else {
            if (drawerLayout.isDrawerOpen(GravityCompat.END))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                drawerLayout.openDrawer(GravityCompat.START);
        }
    }


    public void setScroll(ScrollView svMenu) {
        this.svMenu = svMenu;
    }

    public void setSearchVisibility(String id) {
        Timber.e("id:"+id);
        if (AppMoon.searchVisibility(id))
            layoutActionBarHomeBinding.imgHomeBarSearch.setVisibility(VISIBLE);
        else
            layoutActionBarHomeBinding.imgHomeBarSearch.setVisibility(GONE);
    }

    public void setNavigation(NavigationDrawerView navigationDrawerView) {
        this.navigationDrawerView = navigationDrawerView;
    }
}
