package grand.app.moon.customviews.dialog.rate;

import java.io.Serializable;

public class DialogRateModel implements Serializable {
    public int id;
    public int order_id;
    public String name;
    public String image;
    public String type;
}
