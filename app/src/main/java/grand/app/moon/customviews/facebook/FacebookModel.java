package grand.app.moon.customviews.facebook;

import java.io.Serializable;

public class FacebookModel implements Serializable {
    public String social_id;
    public String name;
    public String email;
    public String image;

    public FacebookModel(String social_id, String name, String email, String image) {
        this.social_id = social_id;
        this.name = name;
        this.email = email;
        this.image = image;
    }
}
