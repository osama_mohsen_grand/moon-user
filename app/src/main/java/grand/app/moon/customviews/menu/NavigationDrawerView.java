package grand.app.moon.customviews.menu;

/**
 * Created by mohamedatef on 12/30/18.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.customviews.actionbar.HomeActionBarView;
import grand.app.moon.databinding.LayoutNavigationDrawerBinding;

import grand.app.moon.models.user.profile.User;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.MovementHelper;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.fragments.HomeFragment;
import grand.app.moon.views.fragments.LanguageFragment;
import grand.app.moon.views.fragments.contactAndSupport.ContactUsFragment;
import grand.app.moon.views.fragments.contactAndSupport.TechnicalSupportFragment;
import grand.app.moon.views.fragments.favourite.FavouriteFragment;
import grand.app.moon.views.fragments.follower.FollowersFragment;
import grand.app.moon.views.fragments.gallery.DiscoverMainFragment;
import grand.app.moon.views.fragments.settings.CountryFragment;
import grand.app.moon.views.fragments.settings.PrivacyListFragment;
import grand.app.moon.views.fragments.settings.SettingsFragment;
import grand.app.moon.views.fragments.settings.SettingsMainFragment;
import grand.app.moon.views.fragments.trip.TripHistoryFragment;
import grand.app.moon.views.fragments.ads.MyAdsFragment;
import grand.app.moon.views.fragments.auth.LoginFragment;
import grand.app.moon.views.fragments.NotificationFragment;
import grand.app.moon.views.fragments.auth.ProfileFragment;
import grand.app.moon.views.fragments.cart.CartFragment;
import grand.app.moon.views.fragments.chat.ChatListFragment;
import grand.app.moon.views.fragments.moon.MoonMapFragment;
import grand.app.moon.views.fragments.order.MyOrdersFragment;
import grand.app.moon.views.fragments.reservation.ReservationFragment;
import grand.app.moon.vollyutils.AppHelper;
import timber.log.Timber;


public class NavigationDrawerView extends RelativeLayout {
    public MutableLiveData<Object> mutableLiveData = new MutableLiveData<>();
    public LayoutNavigationDrawerBinding layoutNavigationDrawerBinding;
    Context context;
    HomeActionBarView homeActionBarView;

    public NavigationDrawerView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public NavigationDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public NavigationDrawerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {


//                MovementHelper.addFragment(getContext(),new Fragment(),"fda");
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true);
        setHeader();
        setList();
        setEvents();
    }

    public void setHeader() {
        if (UserHelper.getUserId() != -1) {
            User user = UserHelper.getUserDetails();
            layoutNavigationDrawerBinding.tvNavigationName.setText(user.name);
            ImageLoaderHelper.ImageLoaderLoad(getContext(), user.image, layoutNavigationDrawerBinding.civMainUserImage);
        }
    }

    MenuAdapter menuAdapter = null;
    List<MenuModel> menu = new ArrayList<>();

    private void setList() {
        //Add List Here
        menuAdapter = new MenuAdapter(menu);
        menu.add(new MenuModel(Constants.HOME, context.getString(R.string.label_home), ResourceManager.getDrawable(R.drawable.ic_home_primary), ""));

        if (UserHelper.getUserId() != -1) {
            menu.add(new MenuModel(Constants.PROFILE, context.getString(R.string.profile), ResourceManager.getDrawable(R.drawable.ic_profile_primary), ""));
            menu.add(new MenuModel(Constants.RESERVATION, context.getString(R.string.reservation), ResourceManager.getDrawable(R.drawable.ic_flag_primary), ""));
            menu.add(new MenuModel(Constants.ORDERS, context.getString(R.string.orders), ResourceManager.getDrawable(R.drawable.ic_order), ""));
            menu.add(new MenuModel(Constants.NOTIFICATION, context.getString(R.string.label_notification), ResourceManager.getDrawable(R.drawable.ic_notification_strock), ""));
//            menu.add(new MenuModel(Constants.CHAT, context.getString(R.string.chat), ResourceManager.getDrawable(R.drawable.ic_chat), ""));
//            menu.add(new MenuModel(Constants.MY_ADS, context.getString(R.string.ads), ResourceManager.getDrawable(R.drawable.ic_ads), ""));

//            menu.add(new MenuModel(Constants.TAXI, context.getString(R.string.taxi_history), ResourceManager.getDrawable(R.drawable.ic_followers_primary), ""));
//            menu.add(new MenuModel(Constants.TRUCKS, context.getString(R.string.trucks_history), ResourceManager.getDrawable(R.drawable.ic_truck), ""));
            menu.add(new MenuModel(Constants.FOLLOWING, context.getString(R.string.following), ResourceManager.getDrawable(R.drawable.ic_followed), ""));
            menu.add(new MenuModel(Constants.FAVOURITE, context.getString(R.string.favourite), ResourceManager.getDrawable(R.drawable.ic_favourite), ""));
        }else
            menu.add(new MenuModel(Constants.NOTIFICATION, context.getString(R.string.label_notification), ResourceManager.getDrawable(R.drawable.ic_notification_strock), ""));

//        menu.add(new MenuModel(Constants.MOON_MAP, context.getString(R.string.moon_map), ResourceManager.getDrawable(R.drawable.ic_moon_map), ""));
        menu.add(new MenuModel(Constants.BE_SHOP, context.getString(R.string.be_shop), ResourceManager.getDrawable(R.drawable.ic_be_shop_primary), ""));
        menu.add(new MenuModel(Constants.BE_DELEGATE, context.getString(R.string.be_delegate), ResourceManager.getDrawable(R.drawable.ic_be_delegate_primary), ""));
//        menu.add(new MenuModel(Constants.BE_DRIVER, context.getString(R.string.be_driver), ResourceManager.getDrawable(R.drawable.ic_be_driver), ""));
        menu.add(new MenuModel(Constants.PRIVACY_POLICY, context.getString(R.string.label_privacy_policy), ResourceManager.getDrawable(R.drawable.ic_be_privacy_primary), ""));
        menu.add(new MenuModel(Constants.TERMS, context.getString(R.string.terms_and_conditions), ResourceManager.getDrawable(R.drawable.ic_terms), ""));
        menu.add(new MenuModel(Constants.FAQ, context.getString(R.string.faq), ResourceManager.getDrawable(R.drawable.ic_faq), ""));

        menu.add(new MenuModel(Constants.SHARE, context.getString(R.string.label_share_app), ResourceManager.getDrawable(R.drawable.ic_share_primary), ""));
        menu.add(new MenuModel(Constants.RATE, context.getString(R.string.rate_app), ResourceManager.getDrawable(R.drawable.ic_star_primary), ""));


        menu.add(new MenuModel(Constants.COUNTRIES, context.getString(R.string.country), ResourceManager.getDrawable(R.drawable.ic_world), ""));
        menu.add(new MenuModel(Constants.LANG, context.getString(R.string.label_language), ResourceManager.getDrawable(R.drawable.ic_language_primary), ""));
        menu.add(new MenuModel(Constants.CONTACT_US, context.getString(R.string.contact_us), ResourceManager.getDrawable(R.drawable.ic_contact), ""));

        if (UserHelper.getUserId() == -1) {
            menu.add(new MenuModel(Constants.LOGIN, context.getString(R.string.label_login), ResourceManager.getDrawable(R.drawable.ic_logout), ""));
        } else {
            menu.add(new MenuModel(Constants.TECHNICAL_SUPPORT, context.getString(R.string.technical_support), ResourceManager.getDrawable(R.drawable.ic_support), ""));
            menu.add(new MenuModel(Constants.LOGOUT, context.getString(R.string.label_logout), ResourceManager.getDrawable(R.drawable.ic_logout_primary), ""));
        }
        AppUtils.initVerticalRV(layoutNavigationDrawerBinding.rvNavigationDrawerList, context, 1);
        layoutNavigationDrawerBinding.rvNavigationDrawerList.setAdapter(menuAdapter);
    }

    public void updateChatCount(int count){
        if(menuAdapter != null) menuAdapter.updateItemCount(Constants.CHAT , count);
    }

    private static final String TAG = "NavigationDrawerView";


    public void homePage() {
        MovementHelper.replaceFragmentTag(getContext(), new HomeFragment(), Constants.HOME, "");
        MovementHelper.popAllFragments(context);
        homeActionBarView.setSearchVisibility(Constants.HOME);
        homeActionBarView.setTitle(context.getString(R.string.app_name));
        mutableLiveData.setValue(context.getString(R.string.app_name));
    }

    public void loginPage() {
        MovementHelper.replaceFragmentTag(getContext(), new LoginFragment(), Constants.LOGIN, "");
        MovementHelper.popAllFragments(context);
        homeActionBarView.setSearchVisibility(Constants.LOGIN);
        mutableLiveData.setValue(context.getString(R.string.label_login));
    }

    public void myAdsPage() {
        MovementHelper.replaceFragmentTag(getContext(), new MyAdsFragment(), Constants.MY_ADS, "");
        MovementHelper.popAllFragments(context);
        homeActionBarView.setSearchVisibility(Constants.MY_ADS);
        mutableLiveData.setValue(context.getString(R.string.ads));
    }

    public void profilePage() {
        MovementHelper.replaceFragmentTag(getContext(), new ProfileFragment(), Constants.PROFILE, "");
        MovementHelper.popAllFragments(context);
        homeActionBarView.setSearchVisibility(Constants.PROFILE);
        mutableLiveData.setValue(context.getString(R.string.profile));
    }

    public void cartPage() {
        MovementHelper.replaceFragmentTag(getContext(), new CartFragment(), Constants.CART, "");
        MovementHelper.popAllFragments(context);
        homeActionBarView.setSearchVisibility(Constants.CART);
        mutableLiveData.setValue(context.getString(R.string.cart));
    }

    public MenuAdapter getAdapter() {
        return menuAdapter;
    }

    //home - profile - ads - stories - chat_history - gallery - orders - reviews - famous_people - credit_card
    //label_notification - photographer_people - help_and_support - label_privacy_policy - label_language
    // label_share_app - rate_app - be_shop - label_logout
    private void setEvents() {

        menuAdapter.mutableLiveDataAdapter.observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer position) {

                // select menu
                int pos = position;
                Timber.e("pos:" + pos);
                String id = menu.get(pos).id;
                Timber.e("language:" + id);
                Intent intent = null;
                if (id.equals(Constants.HOME)) {
                    homePage();
                } else if (id.equals(Constants.PROFILE)) {
                    //profile
                    profilePage();
                } else if (id.equals(Constants.RESERVATION)) {
                    MovementHelper.replaceFragmentTag(context, new ReservationFragment(), Constants.RESERVATION, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.reservation));
                } else if (id.equals(Constants.ORDERS)) {
                    //profile
                    MovementHelper.replaceFragmentTag(context, new MyOrdersFragment(), Constants.ORDERS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.orders));
                } else if (id.equals(Constants.CHAT)) {
                   chatList();
                } else if (id.equals(Constants.MY_ADS)) {
                    myAdsPage();
                } else if (id.equals(Constants.TAXI)) {

                    TripHistoryFragment tripHistoryFragment = new TripHistoryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TYPE,Constants.TYPE_TAXI);
                    tripHistoryFragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, tripHistoryFragment, Constants.TAXI, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.taxi_history));

                } else if (id.equals(Constants.TRUCKS)) {

                    TripHistoryFragment tripHistoryFragment = new TripHistoryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TYPE,Constants.TYPE_TRUCKS);
                    tripHistoryFragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, tripHistoryFragment, Constants.TRUCKS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.trucks_history));

                }else if (id.equals(Constants.FOLLOWING)) {
                    followers();
                }else if (id.equals(Constants.FAVOURITE)) {

                    FavouriteFragment favouriteFragment = new FavouriteFragment();
                    MovementHelper.replaceFragmentTag(context, favouriteFragment, Constants.FAVOURITE, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.favourite));

                } else if (id.equals(Constants.NOTIFICATION)) {
                    //notifications
                    NotificationFragment notificationFragment = new NotificationFragment();
                    MovementHelper.replaceFragmentTag(context, notificationFragment, Constants.NOTIFICATION, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.label_notification));
//
                } else if (id.equals(Constants.MOON_MAP)) {
                    //reviews
                    moonMap();
                } else if (id.equals(Constants.BE_DELEGATE)) {
//                    BeDelegateFragment beDelegateFragment = new BeDelegateFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Constants.TYPE, Constants.BE_DRIVER);
//                    beDelegateFragment.setArguments(bundle);
//                    MovementHelper.replaceFragmentTag(context, beDelegateFragment, Constants.BE_DELEGATE, "");
//                    MovementHelper.popAllFragments(context);
//                    mutableLiveData.setValue(context.getString(R.string.be_delegate));
                    final String appPackageName ="grand.app.moondelegate"; // getPackageName() from Context or Activity object
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else if (id.equals(Constants.BE_SHOP)) {
//                    BeShopFragment beShopFragment = new BeShopFragment();
//                    MovementHelper.replaceFragmentTag(context, beShopFragment, Constants.BE_SHOP, "");
//                    MovementHelper.popAllFragments(context);
//                    mutableLiveData.setValue(context.getString(R.string.be_shop));

                    final String appPackageName ="grand.app.moonshop"; // getPackageName() from Context or Activity object
                    try {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }

                }else if (id.equals(Constants.PRIVACY_POLICY)) {
                    //privacy
                    goToSettings(1,context.getString(R.string.label_privacy_policy));
                }else if (id.equals(Constants.TERMS)) {
                    //privacy
                    goToSettings(2,context.getString(R.string.terms_and_conditions));
                }else if (id.equals(Constants.FAQ)) {
                    //privacy
                    goToSettings(3,context.getString(R.string.faq));
                }else if (id.equals(Constants.LANG)) {
                    //language
                    MovementHelper.replaceFragmentTag(context, new LanguageFragment(), Constants.LANG, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.label_language));
                    homeActionBarView.setSearchVisibility(Constants.LANG);
                } else if (id.equals(Constants.RATE)) {
                    //rate app
                    AppUtils.startWebPage(context, AppUtils.getPlayStoreLink(context));
                } else if (id.equals(Constants.SHARE)) {
                    //share app
                    AppHelper.shareCustom(context,ResourceManager.getString(R.string.app_name), AppUtils.getPlayStoreLink(context));
//                } else if (id.equals(Constants.BE_SHOP)) {
//                    //logout
//                    MovementHelper.replaceFragmentTag(getContext(), new BeShopFragment(), Constants.BE_SHOP, "");
//                    MovementHelper.popAllFragments(context);
//                    mutableLiveData.setValue(context.getString(R.string.be_shop));
                } else if (id.equals(Constants.COUNTRIES)) {
                    //logout
                    MovementHelper.replaceFragmentTag(context, new CountryFragment(), Constants.COUNTRIES, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.country));
                    homeActionBarView.setSearchVisibility(Constants.COUNTRIES);
                }else if (id.equals(Constants.CONTACT_US)) {
                    //logout
                    MovementHelper.replaceFragmentTag(context, new ContactUsFragment(), Constants.CONTACT_US, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.contact_us));
                    homeActionBarView.setSearchVisibility(Constants.CONTACT_US);
                }else if (id.equals(Constants.TECHNICAL_SUPPORT)) {
                    //logout
                    MovementHelper.replaceFragmentTag(context, new TechnicalSupportFragment(), Constants.TECHNICAL_SUPPORT, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.technical_support));
                    homeActionBarView.setSearchVisibility(Constants.TECHNICAL_SUPPORT);
                }
                else if (id.equals(Constants.LOGOUT)) {
                    //logout
                    UserHelper.clearUserDetails();
                    ((ParentActivity) context).finishAffinity();
                    ((ParentActivity) context).startActivity(new Intent(context, MainActivity.class));
                } else if (id.equals(Constants.LOGIN)) {
                    loginPage();
                }
            }
        });

    }

    public void followers(){
        FollowersFragment followersFragment = new FollowersFragment();
        MovementHelper.replaceFragmentTag(context, followersFragment, Constants.FOLLOWING, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.followers));
    }

    private void goToSettings(int type,String name) {
        SettingsMainFragment settingsFragment = new SettingsMainFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TYPE,type);
        bundle.putString(Constants.NAME_BAR,name);
        settingsFragment.setArguments(bundle);
        MovementHelper.replaceFragmentTag(context, settingsFragment, Constants.SETTINGS, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(name);
    }


    public void setActionBar(HomeActionBarView homeActionBarView) {
        this.homeActionBarView = homeActionBarView;
    }


    public void moonMap() {
        MoonMapFragment moonMapFragment = new MoonMapFragment();
        MovementHelper.replaceFragmentTag(context, moonMapFragment, Constants.MOON_MAP, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.moon_map));
    }

    public void discoverPage() {
        DiscoverMainFragment fragment = new DiscoverMainFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.OFFLINE,true);
        fragment.setArguments(bundle);
        MovementHelper.replaceFragmentTag(getContext(),fragment , Constants.DISCOVER, "");
        MovementHelper.popAllFragments(context);
        homeActionBarView.setSearchVisibility(Constants.DISCOVER);
        mutableLiveData.setValue(context.getString(R.string.discover));
    }

    public void chatList() {
        MovementHelper.replaceFragmentTag(context, new ChatListFragment(), Constants.CHAT, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.chat));
    }
}
