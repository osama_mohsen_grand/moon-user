package grand.app.moon.customviews.actionbar;

/**
 * Created by mohamedatef on 12/30/18.
 */


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.databinding.DataBindingUtil;
import grand.app.moon.R;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.LayoutActionBarBackBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.vollyutils.MyApplication;


public class BackActionBarView extends RelativeLayout {
    public LayoutActionBarBackBinding layoutActionBarBackBinding;
    public int service_id = 0;
    public int type = 0;
    public int flag = 0;

    public BackActionBarView(Context context) {
        super(context);
        init();
    }

    public BackActionBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public BackActionBarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarBackBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_back, this, true);
        setEvents();
    }

    private void setEvents() {
        layoutActionBarBackBinding.imgActionBarCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) getContext()).finish();
            }
        });
    }

    public void setSearchVisibility(String id, Bundle bundle){

        if(id.equals(Constants.SERVICE_DETAILS) || id.equals(Constants.COMPANIES) || id.equals(Constants.ADS_MAIN)
                || id.equals(Constants.FAMOUS)) {
            layoutActionBarBackBinding.imgBaseBarSearch.setVisibility(VISIBLE);
//            layoutActionBarBackBinding.imgBaseBarSearch.setOnClickListener(view -> {
//                layoutActionBarBackBinding.imgBaseBarSearch.getContext().startActivity(intent);
//            });
        }
    }

    public void setTitle(String title) {
        layoutActionBarBackBinding.tvActionBarTitle.setText(title);
    }


    public void showAddress() {
        layoutActionBarBackBinding.tvActionBarTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
        MapConfig mapConfig = new MapConfig(MyApplication.getInstance(),null);
        String address = "";
        if(UserHelper.retrieveKey(Constants.LAT) != null && !UserHelper.retrieveKey(Constants.LAT).equals("") &&
                UserHelper.retrieveKey(Constants.LNG) != null && !UserHelper.retrieveKey(Constants.LNG).equals("")){
            double lat = Double.parseDouble(UserHelper.retrieveKey(Constants.LAT));
            double lng = Double.parseDouble(UserHelper.retrieveKey(Constants.LNG));
            address =    mapConfig.getAddress(lat,lng);
//            layoutActionBarBackBinding.tvActionBarTitle.setTextSize(10);

            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.START_OF,layoutActionBarBackBinding.imgBaseBarSearch.getId());
            lp.addRule(RelativeLayout.END_OF,layoutActionBarBackBinding.imgActionBarCancel.getId());
            layoutActionBarBackBinding.tvActionBarTitle.setLayoutParams(lp);

            layoutActionBarBackBinding.tvActionBarTitle.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), MapAddressActivity.class);
                    ((ParentActivity)getContext()).startActivityForResult(intent, Constants.ADDRESS_RESULT);
                }
            });
        }
        setTitle(address);
    }

}
