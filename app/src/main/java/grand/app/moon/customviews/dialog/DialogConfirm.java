package grand.app.moon.customviews.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import grand.app.moon.R;
import grand.app.moon.databinding.LayoutDialogRemoveBinding;
import grand.app.moon.utils.dialog.DialogHelperActionInterface;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import timber.log.Timber;


public class DialogConfirm {
    Context context;
    String title = ResourceManager.getString(R.string.title), message = ResourceManager.getString(R.string.do_you_want_delete_product);
    Float actionTextSize = ResourceManager.getDimens(R.dimen.sp4);
    Float titleTextSize = ResourceManager.getDimens(R.dimen.sp6);
    Float messageTextSize = ResourceManager.getDimens(R.dimen.sp5);
    String actionText = ResourceManager.getString(R.string.remove_item);
    String actionCancel = ResourceManager.getString(R.string.cancel);
    int image = -1;
    float width=-1,height=-1;

    public DialogConfirm(Context context) {
        this.context = context;
        init();
    }

    public DialogConfirm setTitle(String title){
        this.title = title;
        return this;
    }


    public DialogConfirm setMessage(String message){
        this.message = message;
        return this;
    }

    public DialogConfirm setActionTextSize(Float actionTextSize) {
        this.actionTextSize = actionTextSize;
        return this;
    }
    public DialogConfirm setTitleTextSize(Float titleTextSize){
        this.titleTextSize = titleTextSize;
        return this;
    }

    public DialogConfirm setMessageTextSize(Float messageTextSize){
        this.messageTextSize = messageTextSize;
        return this;
    }

    public DialogConfirm setActionText(String actionText) {
        this.actionText = actionText;
        return this;
    }

    public DialogConfirm setActionCancel(String actionCancel) {
        this.actionCancel = actionCancel;
        return this;
    }

    public DialogConfirm setImage(int image,float width,float height) {
        this.image = image;
        this.width = width;
        this.height = height;
        return this;
    }

    LayoutDialogRemoveBinding layoutDialogRemoveBinding = null;

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutDialogRemoveBinding  = DataBindingUtil.inflate(layoutInflater, R.layout.layout_dialog_remove, null, true);
    }

    public void show(DialogHelperInterface dialogHelperInterface){
        View view = layoutDialogRemoveBinding.getRoot();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialogAnimationUpBottom;
        dialog.setView(view);
        dialog.show();

        layoutDialogRemoveBinding.tvDialogTitle.setText(title);
        layoutDialogRemoveBinding.tvDialogTitle.setTextSize(titleTextSize);

        layoutDialogRemoveBinding.tvDialogMessage.setTextSize(messageTextSize);
        layoutDialogRemoveBinding.tvDialogMessage.setText(message);

        layoutDialogRemoveBinding.btnDialogSubmit.setText(actionText);
        layoutDialogRemoveBinding.btnDialogSubmit.setTextSize(actionTextSize);

        layoutDialogRemoveBinding.btnDialogCancel.setText(actionCancel);
        layoutDialogRemoveBinding.btnDialogCancel.setTextSize(actionTextSize);

        if(image != -1){
            layoutDialogRemoveBinding.imgDialog.setVisibility(View.VISIBLE);
            layoutDialogRemoveBinding.imgDialog.setImageResource(image);
            layoutDialogRemoveBinding.imgDialog.setMinimumWidth((int) width);
            layoutDialogRemoveBinding.imgDialog.setMaxWidth((int) width);
            layoutDialogRemoveBinding.imgDialog.setMaxHeight((int) height);
            layoutDialogRemoveBinding.imgDialog.setMinimumHeight((int) height);
        }

        layoutDialogRemoveBinding.btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        layoutDialogRemoveBinding.imgDialogClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        layoutDialogRemoveBinding.btnDialogSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialogHelperInterface.OnClickListenerContinue(dialog,v);
            }
        });
    }

    public void show(DialogHelperActionInterface dialogHelperInterface){
        View view = layoutDialogRemoveBinding.getRoot();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialogAnimationUpBottom;
        dialog.setView(view);
        dialog.show();

        layoutDialogRemoveBinding.tvDialogTitle.setText(title);
        layoutDialogRemoveBinding.tvDialogTitle.setTextSize(titleTextSize);

        layoutDialogRemoveBinding.tvDialogMessage.setTextSize(messageTextSize);
        layoutDialogRemoveBinding.tvDialogMessage.setText(message);

        layoutDialogRemoveBinding.btnDialogSubmit.setText(actionText);
        layoutDialogRemoveBinding.btnDialogSubmit.setTextSize(actionTextSize);

        layoutDialogRemoveBinding.btnDialogCancel.setText(actionCancel);
        layoutDialogRemoveBinding.btnDialogCancel.setTextSize(actionTextSize);

        if(image != -1){
            layoutDialogRemoveBinding.imgDialog.setVisibility(View.VISIBLE);
            layoutDialogRemoveBinding.imgDialog.setImageResource(image);
            layoutDialogRemoveBinding.imgDialog.setMinimumWidth((int) width);
            layoutDialogRemoveBinding.imgDialog.setMaxWidth((int) width);
            layoutDialogRemoveBinding.imgDialog.setMaxHeight((int) height);
            layoutDialogRemoveBinding.imgDialog.setMinimumHeight((int) height);
        }

        layoutDialogRemoveBinding.btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Timber.e("cancel Dialog: Done");
                dialogHelperInterface.onCancel(dialog,v);
            }
        });
        layoutDialogRemoveBinding.imgDialogClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        layoutDialogRemoveBinding.btnDialogSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialogHelperInterface.onSubmit(dialog,v);
            }
        });
    }

}
