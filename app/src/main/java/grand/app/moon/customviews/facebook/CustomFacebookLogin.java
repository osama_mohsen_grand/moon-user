package grand.app.moon.customviews.facebook;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import androidx.databinding.DataBindingUtil;
import grand.app.moon.R;
import grand.app.moon.databinding.CustomFacebookLoginBinding;

public class CustomFacebookLogin extends RelativeLayout {
    CustomFacebookLoginBinding customFacebookLoginBinding;
    public CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private static final String PUBLIC_PROFILE = "public_profile";
    private FacebookResponseInterface facebookResponseInterface = null;

    public CustomFacebookLogin(Context context) {
        super(context);
        init();
    }

    public CustomFacebookLogin(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFacebookLogin(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void  init(){
        FacebookSdk.setApplicationId("353837505660520");
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());
//        AppEventsLogger.activateApp(getContext());
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        customFacebookLoginBinding  = DataBindingUtil.inflate(layoutInflater, R.layout.custom_facebook_login, null, true);
        initFacebook();
    }

    private void initFacebook() {
//        printKeyHash();
        LoginManager.getInstance().logOut();
        callbackManager = CallbackManager.Factory.create();
        customFacebookLoginBinding.loginFacebook.setReadPermissions(Arrays.asList(PUBLIC_PROFILE,EMAIL));
        loginFacebookListeners();
    }

    private void loginFacebookListeners() {

        customFacebookLoginBinding.loginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e("start","start here success login");
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.e("facebook","completed");
                                try {
                                    String image = "https://graph.facebook.com/"+object.getString("id")+"/picture?width=250&height=250";
                                    FacebookModel facebookModel = new FacebookModel(object.getString("id"),object.getString("email"),object.getString("name"),image);
                                    facebookResponseInterface.response(facebookModel);
                                    LoginManager.getInstance().logOut();
                                } catch (JSONException e) {
                                    Log.e("start","error");
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }



            @Override
            public void onCancel() {
                // App code
                Log.e("cancel_facebook","cancel facebook");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.e("error_facebook",exception.getMessage().toString());
            }
        });
    }

    public void submitFacebook(FacebookResponseInterface facebookResponseInterface) {
        this.facebookResponseInterface = facebookResponseInterface;
        customFacebookLoginBinding.loginFacebook.performClick();
    }
}
