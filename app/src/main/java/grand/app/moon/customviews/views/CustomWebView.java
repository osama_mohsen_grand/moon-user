package grand.app.moon.customviews.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CustomWebView extends WebView {
    public CustomWebView(Context context) {
        super(context);
        init();
    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        getSettings().setAppCachePath(getContext().getCacheDir().getPath());
        getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setDisplayZoomControls(true);
        getSettings().setAppCacheEnabled(true);
        getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        setWebViewClient(new MyAppWebViewClient());

    }

    public static class MyAppWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }


}
