package grand.app.moon.customviews.menu;

import android.graphics.drawable.Drawable;
import android.widget.RelativeLayout;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import grand.app.moon.base.ParentBaseObservableViewModel;


public class ItemMenuViewModel extends ParentBaseObservableViewModel {
    private MenuModel menuModel = null;
    private int position = 0;
    public int height = 0;

    public ObservableBoolean visibleCount = new ObservableBoolean(false);

    public ItemMenuViewModel(MenuModel menuModel, int position) {
        this.menuModel = menuModel;
        this.position = position;
        if(!menuModel.price.equals("")){
            int price = Integer.parseInt(menuModel.price);
            if(price > 0) visibleCount.set(true);
        }
    }


    public void submitMenu(){
        mMutableLiveDataBaseObservable.setValue(position);
    }

    public MutableLiveData<Object> getOnGetDataListener() {
        if(mMutableLiveDataBaseObservable==null)mMutableLiveDataBaseObservable = new MutableLiveData<>();
        return mMutableLiveDataBaseObservable;
    }



    @Bindable
    public String getTitle(){
        return menuModel.title;
    }

    @Bindable
    public String getPrice(){
        return menuModel.price;
    }


    @Bindable
    public Drawable getIcon(){
        return menuModel.icon;
    }

}
