package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemCheckedPriceBinding;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.viewmodels.moon.ItemCheckPriceViewModel;
import timber.log.Timber;


public class CheckPriceAdapter extends RecyclerView.Adapter<CheckPriceAdapter.CheckView> {
    private List<IdNamePrice> checks;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private  ArrayList<String> selected;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public CheckPriceAdapter(List<IdNamePrice> checks, ArrayList<String> selected) {
        this.checks = checks;
        this.selected = selected;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CheckView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemCheckedPriceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_checked_price,parent,false);
        return new CheckView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckView holder, final int position) {
        ItemCheckPriceViewModel itemCheckViewModel = new ItemCheckPriceViewModel(checks.get(position),position,selected.contains(checks.get(position).id));
        holder.itemCheckedBinding.setItemCheckPriceViewModel(itemCheckViewModel);
        setEvent(itemCheckViewModel);
    }

    private void setEvent(ItemCheckPriceViewModel itemCheckViewModel) {
        itemCheckViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                int pos = (int) aVoid;
                IdNamePrice idNamePrice = checks.get(pos);
                if(selected.contains(idNamePrice.id)){
                    selected.remove(selected.indexOf(idNamePrice.id));
                }else{
                    selected.add(idNamePrice.id);
                }
                notifyDataSetChanged();
                mMutableLiveData.setValue(selected);
                Timber.e("selected:"+selected.toString());
            }
        });
    }


    @Override
    public int getItemCount() {
        return checks.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class CheckView extends RecyclerView.ViewHolder{

        private ItemCheckedPriceBinding itemCheckedBinding;
        public CheckView(@NonNull ItemCheckedPriceBinding itemCheckedBinding) {
            super(itemCheckedBinding.getRoot());
            this.itemCheckedBinding = itemCheckedBinding;
        }
    }
}
