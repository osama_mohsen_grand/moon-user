package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemTagBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.viewmodels.tags.ItemTagViewModel;


public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.TagView> {
    private List<IdNameImage> tags;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private  ArrayList<Integer> selected;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public TagsAdapter(List<IdNameImage> tags, ArrayList<Integer> selected) {
        this.tags = tags;
        this.selected = selected;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public TagView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemTagBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_tag,parent,false);
        return new TagView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TagView holder, final int position) {
        ItemTagViewModel itemTagViewModel = new ItemTagViewModel(tags.get(position),position,selected.contains(Integer.parseInt(tags.get(position).id)));
        holder.itemTagBinding.setItemTagViewModel(itemTagViewModel);
        setEvent(itemTagViewModel);
    }

    private void setEvent(ItemTagViewModel itemTagViewModel) {
        itemTagViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return tags.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(ArrayList<Integer> selected) {
        this.selected = new ArrayList<>(selected);
        notifyDataSetChanged();
    }

    public class TagView extends RecyclerView.ViewHolder{

        private ItemTagBinding itemTagBinding;
        public TagView(@NonNull ItemTagBinding itemTagBinding) {
            super(itemTagBinding.getRoot());
            this.itemTagBinding = itemTagBinding;
        }
    }
}
