package grand.app.moon.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemCompanyAdsBinding;
import grand.app.moon.models.famous.details.AlbumModel;
import grand.app.moon.models.famous.details.Category;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.ads.ItemCompanyAdsViewModel;
import grand.app.moon.views.activities.BaseActivity;


public class CompanyAdsAdapter extends RecyclerView.Adapter<CompanyAdsAdapter.AdsCompanyView> {
    public int famous_id;
    private List<AlbumModel> companyAds;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public CompanyAdsAdapter(List<AlbumModel> companyAds) {
        this.companyAds = companyAds;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AdsCompanyView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemCompanyAdsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_company_ads, parent, false);
        return new AdsCompanyView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdsCompanyView holder, final int position) {
        ItemCompanyAdsViewModel itemAdsCompanyViewModel = new ItemCompanyAdsViewModel(companyAds.get(position), position);
        holder.itemAdsCompanyBinding.setViewModel(itemAdsCompanyViewModel);
        setEvent(holder.itemAdsCompanyBinding.getRoot().getContext() , itemAdsCompanyViewModel);
    }

    @Override
    public int getItemCount() {
        return companyAds.size();
    }

    private static final String TAG = "CompanyAdsAdapter";

    private void setEvent(Context context , ItemCompanyAdsViewModel itemAdsCompanyViewModel) {
        itemAdsCompanyViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Log.d(TAG,"adapter");
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.CATEGORY_COMPANY_ADS);
                intent.putExtra(Constants.NAME_BAR,companyAds.get(itemAdsCompanyViewModel.position).name);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.CATEGORIES,companyAds.get(itemAdsCompanyViewModel.position));
                bundle.putInt(Constants.FAMOUS_ID,famous_id);
                intent.putExtra(Constants.BUNDLE,bundle);
                context.startActivity(intent);
            }
        });
    }

    public void update(List<AlbumModel> companyAds) {
        this.companyAds = companyAds;
        notifyDataSetChanged();
    }

    public class AdsCompanyView extends RecyclerView.ViewHolder {

        private ItemCompanyAdsBinding itemAdsCompanyBinding;

        public AdsCompanyView(@NonNull ItemCompanyAdsBinding itemAdsCompanyBinding) {
            super(itemAdsCompanyBinding.getRoot());
            this.itemAdsCompanyBinding = itemAdsCompanyBinding;
        }
    }
}
