package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemShippingBinding;
import grand.app.moon.databinding.ItemShippingBinding;
import grand.app.moon.models.cart.Item;
import grand.app.moon.models.shipping.Shipping;
import grand.app.moon.viewmodels.shipping.ItemShippingViewModel;


public class ShippingAdapter extends RecyclerView.Adapter<ShippingAdapter.ShippingView> {
    private List<Shipping> shipping;
    private LayoutInflater layoutInflater;
    public int selected = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();

    public ShippingAdapter(List<Shipping> shipping) {
        this.shipping = shipping;
        if(shipping.size() > 0) selected = 0;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ShippingView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemShippingBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_shipping, parent, false);
        return new ShippingView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ShippingView holder, final int position) {
        ItemShippingViewModel itemShippingViewModel = new ItemShippingViewModel(shipping.get(position), position, selected == position);
        holder.itemShippingBinding.setItemShippingViewModel(itemShippingViewModel);
        setEvent(itemShippingViewModel);
    }

    private void setEvent(ItemShippingViewModel itemShippingViewModel) {
        itemShippingViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                selected = (int) aVoid;
                notifyDataSetChanged();
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return shipping.size();
    }

    public void update(List<Shipping> shipping) {
        this.shipping.clear();
        this.shipping.addAll(shipping);
    }

    public class ShippingView extends RecyclerView.ViewHolder {

        private ItemShippingBinding itemShippingBinding;

        public ShippingView(@NonNull ItemShippingBinding itemShippingBinding) {
            super(itemShippingBinding.getRoot());
            this.itemShippingBinding = itemShippingBinding;
        }
    }
}
