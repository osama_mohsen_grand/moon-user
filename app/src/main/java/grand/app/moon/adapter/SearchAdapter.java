package grand.app.moon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.databinding.ItemSearchBinding;
import grand.app.moon.models.search.SearchModel;
import grand.app.moon.viewmodels.search.ItemSearchViewModel;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ItemSearchViewHolder> {
    private List<SearchModel> mDataList;
    private MutableLiveData<SearchModel> mutableLiveData;

    public SearchAdapter() {
        mutableLiveData = new MutableLiveData<>();
    }

    public void setList(@NonNull List<SearchModel> mDataList) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList<>();
        }
        this.mDataList.clear();
        this.mDataList.addAll(mDataList);
        notifyDataSetChanged();
    }

    public void updateList(@NonNull List<SearchModel> mDataList) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList<>();
        }
        this.mDataList.addAll(mDataList);
        notifyDataSetChanged();
    }

    public void clear(){
        if(mDataList != null)
        mDataList.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemSearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemSearchBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_search, parent, false);
        return new ItemSearchViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemSearchViewHolder holder, int position) {
        holder.setAnimation();
        ItemSearchViewModel viewModel = new ItemSearchViewModel(getCurrentItem(position),position);
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    private SearchModel getCurrentItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public int getItemCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    public MutableLiveData<SearchModel> getMutableLiveData() {
        return mutableLiveData;
    }

    class ItemSearchViewHolder extends RecyclerView.ViewHolder {
        ItemSearchBinding binding;

        public ItemSearchViewHolder(ItemSearchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setAnimation() {
            //set animation here
        }

        public void setViewModel(ItemSearchViewModel viewModel) {
            binding.setViewModel(viewModel);
        }
    }
}