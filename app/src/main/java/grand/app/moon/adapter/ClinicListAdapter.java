package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemClinicListBinding;
import grand.app.moon.models.service.clinic.ClinicModel;
import grand.app.moon.viewmodels.reservation.ItemClinicListViewModel;


public class ClinicListAdapter extends RecyclerView.Adapter<ClinicListAdapter.ClinicModelView> {
    private List<ClinicModel> clinicModels;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private String type;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ClinicListAdapter(List<ClinicModel> clinicModels, String type) {
        this.clinicModels = clinicModels;
        this.type = type;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ClinicModelView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemClinicListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_clinic_list,parent,false);
        return new ClinicModelView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ClinicModelView holder, final int position) {
        ItemClinicListViewModel itemCheckViewModel = new ItemClinicListViewModel(clinicModels.get(position),type,position);
        holder.itemClinicListBinding.setItemClinicListViewModel(itemCheckViewModel);
        setEvent(itemCheckViewModel);
    }

    private void setEvent(ItemClinicListViewModel itemClinicModelViewModel) {
        itemClinicModelViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return clinicModels.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ClinicModel> clinicModels) {
        this.clinicModels = clinicModels;
        notifyDataSetChanged();
    }

    public class ClinicModelView extends RecyclerView.ViewHolder{

        private ItemClinicListBinding itemClinicListBinding;
        public ClinicModelView(@NonNull ItemClinicListBinding itemClinicListBinding) {
            super(itemClinicListBinding.getRoot());
            this.itemClinicListBinding = itemClinicListBinding;
        }
    }
}
