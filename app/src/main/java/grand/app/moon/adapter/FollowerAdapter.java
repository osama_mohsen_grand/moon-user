package grand.app.moon.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.databinding.ItemFollowerBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.followers.Follower;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.followers.ItemFollowerViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.FollowerView> {
    private List<Follower> mDataList;
    private MutableLiveData<Follower> mutableLiveData;
    int position_selected = -1;
    public String type = "";

    public FollowerAdapter(List<Follower> followers) {
        mutableLiveData = new MutableLiveData<>();
        this.mDataList = followers;
    }

    public void setList(@NonNull List<Follower> mDataList) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList<>();
        }
        this.mDataList.clear();
        this.mDataList.addAll(mDataList);
        notifyDataSetChanged();
    }

    public void updateList(@NonNull List<Follower> mDataList) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList<>();
        }
        this.mDataList.addAll(mDataList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FollowerView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemFollowerBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_follower, parent, false);
        return new FollowerView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FollowerView holder, int position) {
        holder.setAnimation();
        ItemFollowerViewModel viewModel = new ItemFollowerViewModel(getCurrentItem(position), position);
        holder.setViewModel(viewModel);
        viewModel.getMutableLiveData().observeForever(o -> {
            Mutable mutable = (Mutable) o;
            type = mutable.type;
            position_selected = mutable.position;
            mutableLiveData.setValue(getCurrentItem(position));
        });
    }

    private Follower getCurrentItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public int getItemCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    public MutableLiveData<Follower> getMutableLiveData() {
        return mutableLiveData;
    }

    public void updateItem() {
        if (position_selected != -1) {
            mDataList.get(position_selected).isFollow = !mDataList.get(position_selected).isFollow;
            notifyItemChanged(position_selected);
        }
    }

    class FollowerView extends RecyclerView.ViewHolder {
        ItemFollowerBinding binding;

        public FollowerView(ItemFollowerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setAnimation() {
            //set animation here
        }

        public void setViewModel(ItemFollowerViewModel viewModel) {
            binding.setItemFollowerViewModel(viewModel);
        }
    }
}