package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemCartBinding;
import grand.app.moon.databinding.ItemCartBinding;
import grand.app.moon.models.cart.Item;
import grand.app.moon.models.product.Color;
import grand.app.moon.viewmodels.cart.ItemCartViewModel;



public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartView> {
    private List<Item> carts;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public CartAdapter(List<Item> carts) {
        this.carts = carts;
    }

    
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CartView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemCartBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_cart, parent, false);
        return new CartView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CartView holder, final int position) {
        ItemCartViewModel itemCartViewModel = new ItemCartViewModel(carts.get(position), position);
        holder.itemColorBinding.setItemCartViewModel(itemCartViewModel);
        setEvent(itemCartViewModel);
    }

    private void setEvent(ItemCartViewModel itemCartViewModel) {
        itemCartViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return carts.size();
    }

    public void update(List<Item> carts) {
        this.carts = carts;
        notifyDataSetChanged();
    }

    public class CartView extends RecyclerView.ViewHolder {

        private ItemCartBinding itemColorBinding;

        public CartView(@NonNull ItemCartBinding itemColorBinding) {
            super(itemColorBinding.getRoot());
            this.itemColorBinding = itemColorBinding;
        }
    }
}
