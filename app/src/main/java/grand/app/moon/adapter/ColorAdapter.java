package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemColorBinding;
import grand.app.moon.databinding.ItemColorBinding;
import grand.app.moon.models.product.Color;
import grand.app.moon.viewmodels.color.ItemColorViewModel;


public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ColorView> {
    private List<Color> colors;
    private LayoutInflater layoutInflater;
    public int selected = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public ColorAdapter(List<Color> colors) {
        if(colors.size() > 0) selected = 0;
        this.colors = colors;
    }



    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ColorView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemColorBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_color, parent, false);
        return new ColorView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorView holder, final int position) {
        ItemColorViewModel itemColorViewModel = new ItemColorViewModel(colors.get(position), position, position == selected);
        holder.itemColorBinding.setItemColorViewModel(itemColorViewModel);
        setEvent(itemColorViewModel);
    }

    private void setEvent(ItemColorViewModel itemColorViewModel) {
        itemColorViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                selected = (int) aVoid;
                mMutableLiveData.setValue(selected);
                notifyDataSetChanged();
            }
        });
    }


    @Override
    public int getItemCount() {
        return colors.size();
    }

    public void update(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    public class ColorView extends RecyclerView.ViewHolder {

        private ItemColorBinding itemColorBinding;

        public ColorView(@NonNull ItemColorBinding itemColorBinding) {
            super(itemColorBinding.getRoot());
            this.itemColorBinding = itemColorBinding;
        }
    }
}
