package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemProductBinding;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.viewmodels.shop.ItemShopProductViewModel;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.AlbumView> {
    public List<ProductList> products;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ProductAdapter(List<ProductList> products) {
        this.products = products;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemProductBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_product,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemShopProductViewModel itemShopProductViewModel = new ItemShopProductViewModel(products.get(position),position);
        holder.itemProductBinding.setItemProductProductViewModel(itemShopProductViewModel);
        setEvent(itemShopProductViewModel);
    }

    private void setEvent(ItemShopProductViewModel ItemShopProductViewModel) {
        ItemShopProductViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ProductList> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemProductBinding itemProductBinding;
        public AlbumView(@NonNull ItemProductBinding itemProductBinding) {
            super(itemProductBinding.getRoot());
            this.itemProductBinding = itemProductBinding;
        }
    }
}
