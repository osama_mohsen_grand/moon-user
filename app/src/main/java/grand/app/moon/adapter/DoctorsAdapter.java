package grand.app.moon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemDoctorListBinding;
import grand.app.moon.models.clinic.DoctorsList;
import grand.app.moon.viewmodels.clinic.ItemDoctorListViewModel;


public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.DoctorsListView> {
    private List<DoctorsList> doctors;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveDataAdapter  = new MutableLiveData<>();;

    public DoctorsAdapter(List<DoctorsList> doctors) {
        this.doctors = doctors;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public DoctorsListView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemDoctorListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_doctor_list,parent,false);
        return new DoctorsListView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorsListView holder, final int position) {
        ItemDoctorListViewModel itemDoctorsListViewModel = new ItemDoctorListViewModel(doctors.get(position),position);
        holder.itemDoctorListBinding.setItemDoctorListViewModel(itemDoctorsListViewModel);
        setEvent(itemDoctorsListViewModel);
    }

    private void setEvent(ItemDoctorListViewModel itemDoctorsListViewModel) {
        itemDoctorsListViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Log.d("adapter","here change");
                mMutableLiveDataAdapter.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return (doctors != null ? doctors.size() : 0);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        doctors.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<DoctorsList> doctorsList) {
        this.doctors = doctorsList;
        notifyDataSetChanged();
    }

    public class DoctorsListView extends RecyclerView.ViewHolder{

        private ItemDoctorListBinding itemDoctorListBinding;
        public DoctorsListView(@NonNull ItemDoctorListBinding itemDoctorListBinding) {
            super(itemDoctorListBinding.getRoot());
            this.itemDoctorListBinding = itemDoctorListBinding;
        }
    }
}
