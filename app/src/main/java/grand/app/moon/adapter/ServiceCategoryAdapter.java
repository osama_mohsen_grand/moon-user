package grand.app.moon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemServiceCategoryBinding;
import grand.app.moon.databinding.ItemServiceCategoryBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.viewmodels.service.ItemServiceViewModel;


public class ServiceCategoryAdapter extends RecyclerView.Adapter<ServiceCategoryAdapter.ServiceCategoryView> {
    public List<Service> services;
    private LayoutInflater layoutInflater;
    public int selected = 0;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ServiceCategoryAdapter(List<Service> services) {
        this.services = services;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ServiceCategoryView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemServiceCategoryBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_service_category,parent,false);
        return new ServiceCategoryView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceCategoryView holder, final int position) {
        ItemServiceViewModel itemServiceViewModel = new ItemServiceViewModel(services.get(position),position,position==selected);
        holder.itemServiceCategoryBinding.setItemServiceViewModel(itemServiceViewModel);
        setEvent(itemServiceViewModel);
    }

    private void setEvent(ItemServiceViewModel itemServiceViewModel) {
        itemServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return services.size();
    }

    public void update(List<Service> services) {
        this.services = services;
        notifyDataSetChanged();
    }

    public class ServiceCategoryView extends RecyclerView.ViewHolder{

        private ItemServiceCategoryBinding itemServiceCategoryBinding;
        public ServiceCategoryView(@NonNull ItemServiceCategoryBinding itemServiceCategoryBinding) {
            super(itemServiceCategoryBinding.getRoot());
            this.itemServiceCategoryBinding = itemServiceCategoryBinding;
        }
    }
}
