package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemTimeBinding;
import grand.app.moon.databinding.ItemTimeBinding;
import grand.app.moon.models.reservation.DateTimeModel;
import grand.app.moon.viewmodels.time.ItemTimeViewModel;

//(activity) => created - start - resume - pause - stop - destroy
//(fragment) =>
public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.TimeView> {
    private List<DateTimeModel> time;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    List<String> reservedTime;
    int position_selected = -1;

    public TimeAdapter(List<DateTimeModel> time, List<String> reservedTime) {
        this.time = time;
        this.reservedTime = reservedTime;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public TimeView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemTimeBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_time, parent, false);
        return new TimeView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TimeView holder, final int position) {
        ItemTimeViewModel itemTimeViewModel = new ItemTimeViewModel(time.get(position), position, (position == position_selected) , reservedTime.contains(time.get(position).time));
        holder.itemTimeBinding.setItemTimeViewModel(itemTimeViewModel);
        setEvent(itemTimeViewModel);
    }

    private void setEvent(ItemTimeViewModel itemTimeViewModel) {
        itemTimeViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }

    public void update(int pos){
        this.position_selected = pos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return time.size();
    }

    public void updateAll(List<DateTimeModel> times) {
        this.time = times;
        notifyDataSetChanged();
    }

    public class TimeView extends RecyclerView.ViewHolder {

        private ItemTimeBinding itemTimeBinding;

        public TimeView(@NonNull ItemTimeBinding itemTimeBinding) {
            super(itemTimeBinding.getRoot());
            this.itemTimeBinding = itemTimeBinding;
        }
    }
}
