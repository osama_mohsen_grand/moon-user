package grand.app.moon.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.ItemAlbumBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.album.ItemAlbumViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.famous.FamousDetailsFragment;


public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumView> {
    public List<ImageVideo> imageVideosTmp;
    public List<ImageVideo> imageVideos;
    public int flag = 2;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;
    boolean default_submit;
    public boolean content = false;

    public AlbumAdapter(List<ImageVideo> imageVideos, boolean default_submit) {
        this.imageVideos = imageVideos;
        this.default_submit = default_submit;
        imageVideosTmp = imageVideos;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemAlbumBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_album, parent, false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemAlbumViewModel itemAlbumViewModel = new ItemAlbumViewModel(imageVideos.get(position), position);
        holder.itemAlbumBinding.setItemAlbumViewModel(itemAlbumViewModel);
        setEvent(holder);
    }

    private void setEvent(AlbumView view) {
        view.itemAlbumBinding.getItemAlbumViewModel().mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
//                mMutableLiveData.setValue(aVoid);
                Context context = view.itemAlbumBinding.getRoot().getContext();
                Mutable mutable = (Mutable) aVoid;
                Intent intent = new Intent(context, BaseActivity.class);
                if (default_submit) {
                    if (!content) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.ID, imageVideos.get(mutable.position).shopId);
                        bundle.putInt(Constants.TYPE, imageVideos.get(mutable.position).accountType);
                        intent.putExtra(Constants.PAGE, FamousDetailsFragment.class.getName());
                        intent.putExtra(Constants.NAME_BAR, imageVideos.get(mutable.position).name);
                        bundle.putInt(Constants.FLAG, flag);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        context.startActivity(intent);
                    }
                    if (mutable.type.equals(Constants.IMAGE)) {
                        intent.putExtra(Constants.PAGE, Constants.ZOOM);
                        intent.putExtra(Constants.NAME_BAR, imageVideos.get(mutable.position).name);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.IMAGE, imageVideos.get(mutable.position).image);
                        bundle.putInt(Constants.ID, imageVideos.get(mutable.position).id);
                        bundle.putInt(Constants.FLAG, flag);

                        intent.putExtra(Constants.BUNDLE, bundle);
                        context.startActivity(intent);
                    } else if (mutable.type.equals(Constants.VIDEO)) {
                        intent.putExtra(Constants.PAGE, Constants.VIDEO);
                        intent.putExtra(Constants.NAME_BAR, imageVideos.get(mutable.position).name);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.VIDEO, imageVideos.get(mutable.position).image);
                        bundle.putInt(Constants.ID, imageVideos.get(mutable.position).id);
                        bundle.putInt(Constants.FLAG, flag);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        context.startActivity(intent);
                    }
                }
                mMutableLiveData.setValue(aVoid);

            }
        });
    }


    @Override
    public int getItemCount() {
        return (imageVideos != null ? imageVideos.size() : 0);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ImageVideo> imageVideos) {
        this.imageVideos = imageVideos;
        notifyDataSetChanged();
    }

    private List<ImageVideo> filterList = new ArrayList<>();

    public void submitFilter(ArrayList<Integer> list) {
        filterList.clear();
        if (!list.isEmpty()) {
            for (ImageVideo item : imageVideosTmp) {
                if (list.contains(item.famous_id))
                    filterList.add(item);
            }
            imageVideos = filterList;
        } else {
            imageVideos = imageVideosTmp;
        }
        notifyDataSetChanged();
    }

    public void clear() {
        imageVideos.clear();
        notifyDataSetChanged();
    }

    private static final String TAG = "AlbumAdapter";

    public void find(String s, List<ImageVideo> originalList) {
        imageVideosTmp.clear();
        for (ImageVideo imageVideo : originalList) {
            if (imageVideo.shopName.toLowerCase().contains(s.toLowerCase()))
                imageVideosTmp.add(imageVideo);
        }
        Log.d(TAG, "tmpSize:" + imageVideosTmp.size());
        Log.d(TAG, "searchAfterTextChange_tmpSize:" + imageVideosTmp.size());
        update(imageVideosTmp);
    }


    public class AlbumView extends RecyclerView.ViewHolder {

        private ItemAlbumBinding itemAlbumBinding;

        public AlbumView(@NonNull ItemAlbumBinding itemAlbumBinding) {
            super(itemAlbumBinding.getRoot());
            this.itemAlbumBinding = itemAlbumBinding;
        }
    }
}
