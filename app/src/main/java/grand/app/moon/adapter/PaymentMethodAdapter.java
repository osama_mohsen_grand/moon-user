package grand.app.moon.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemPaymentMethodBinding;
import grand.app.moon.databinding.ItemPaymentMethodBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.viewmodels.checkout.ItemPaymentMethodViewModel;
import grand.app.moon.viewmodels.institution.ItemInstitutionServiceViewModel;


public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ServiceView> {
    private List<IdNameImage> items;
    private LayoutInflater layoutInflater;
    public int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    Context context;

    public PaymentMethodAdapter(List<IdNameImage> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ServiceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemPaymentMethodBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_payment_method,parent,false);
        return new ServiceView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceView holder, final int position) {
        ItemPaymentMethodViewModel viewModel = new ItemPaymentMethodViewModel(items.get(position),position,this.position == position);
        holder.ItemPaymentMethodBinding.setViewmodel(viewModel);
        setEvent(viewModel);
    }

    public void setEvent(ItemPaymentMethodViewModel viewModel) {
        viewModel.mMutableLiveData.observeForever( new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
                notifyDataSetChanged();
            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getPosition() {
        return position;
    }

    private static final String TAG = "PaymentMethodAdapter";
    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<IdNameImage> paymentList) {
        items.clear();
        items.addAll(paymentList);
        if(paymentList.size() > 0) {
            position = 0;
            mMutableLiveData.setValue(0);
        }
        notifyDataSetChanged();
    }

    public class ServiceView extends RecyclerView.ViewHolder{

        private ItemPaymentMethodBinding ItemPaymentMethodBinding;
        public ServiceView(@NonNull ItemPaymentMethodBinding ItemPaymentMethodBinding) {
            super(ItemPaymentMethodBinding.getRoot());
            this.ItemPaymentMethodBinding = ItemPaymentMethodBinding;
        }
    }
}
