package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemInstitutionServiceBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.viewmodels.institution.ItemInstitutionServiceViewModel;
import grand.app.moon.viewmodels.institution.ItemInstitutionViewModel;


public class InstitutionServicesAdapter extends RecyclerView.Adapter<InstitutionServicesAdapter.ServiceView> {
    private List<IdNameImage> services;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public InstitutionServicesAdapter(List<IdNameImage> services) {
        this.services = services;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ServiceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemInstitutionServiceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_institution_service,parent,false);
        return new ServiceView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceView holder, final int position) {
        ItemInstitutionServiceViewModel itemInstitutionServiceViewModel = new ItemInstitutionServiceViewModel(services.get(position),position);
        holder.itemInstitutionServiceBinding.setItemInstitutionServiceViewModel(itemInstitutionServiceViewModel);
        setEvent(itemInstitutionServiceViewModel);
    }

    private void setEvent(ItemInstitutionServiceViewModel itemInstitutionServiceViewModel) {
        itemInstitutionServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return services.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class ServiceView extends RecyclerView.ViewHolder{

        private ItemInstitutionServiceBinding itemInstitutionServiceBinding;
        public ServiceView(@NonNull ItemInstitutionServiceBinding itemInstitutionServiceBinding) {
            super(itemInstitutionServiceBinding.getRoot());
            this.itemInstitutionServiceBinding = itemInstitutionServiceBinding;
        }
    }
}
