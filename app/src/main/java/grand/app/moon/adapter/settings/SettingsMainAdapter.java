package grand.app.moon.adapter.settings;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemSettingsMainBinding;
import grand.app.moon.databinding.ItemSettingsMainBinding;
import grand.app.moon.models.settings.Settings;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.settings.ItemSettingsMainViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.settings.SettingsFragment;
import grand.app.moon.views.fragments.shop.ShopDetailsInfoFragment;


public class SettingsMainAdapter extends RecyclerView.Adapter<SettingsMainAdapter.SettingsView> {
    private List<Settings> SettingsList;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public SettingsMainAdapter(List<Settings> SettingsList) {
        this.SettingsList = SettingsList;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public SettingsView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSettingsMainBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_settings_main,parent,false);
        return new SettingsView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingsView holder, final int position) {
        ItemSettingsMainViewModel itemPrivacyViewModel = new ItemSettingsMainViewModel(SettingsList.get(position),position);
        holder.itemPrivacyBinding.setViewmodel(itemPrivacyViewModel);
        setEvent(itemPrivacyViewModel,holder);
    }

    private void setEvent(ItemSettingsMainViewModel itemPrivacyViewModel, SettingsView holder) {
        itemPrivacyViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Intent intent = new Intent(holder.itemPrivacyBinding.getRoot().getContext(), BaseActivity.class);
                intent.putExtra(Constants.PAGE, SettingsFragment.class.getName());
                intent.putExtra(Constants.NAME_BAR,itemPrivacyViewModel.model.name);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.SETTINGS, itemPrivacyViewModel.model);
                intent.putExtra(Constants.BUNDLE, bundle);
                holder.itemPrivacyBinding.getRoot().getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return SettingsList.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        SettingsList.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<Settings> data) {
        this.SettingsList = data;
        notifyDataSetChanged();
    }

    public class SettingsView extends RecyclerView.ViewHolder{

        private ItemSettingsMainBinding itemPrivacyBinding;
        public SettingsView(@NonNull ItemSettingsMainBinding itemPrivacyBinding) {
            super(itemPrivacyBinding.getRoot());
            this.itemPrivacyBinding = itemPrivacyBinding;
        }
    }
}
