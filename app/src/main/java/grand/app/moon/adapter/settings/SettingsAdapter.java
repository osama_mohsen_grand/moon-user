package grand.app.moon.adapter.settings;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemSettingsBinding;
import grand.app.moon.models.settings.SettingsModel;
import grand.app.moon.models.settings.SettingsResponse;
import grand.app.moon.viewmodels.settings.ItemSettingsViewModel;


public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.SettingsView> {
    private List<SettingsModel> settingsModel;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public SettingsAdapter(List<SettingsModel> settingsModel) {
        this.settingsModel = settingsModel;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public SettingsView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSettingsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_settings,parent,false);
        return new SettingsView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingsView holder, final int position) {
        ItemSettingsViewModel itemPrivacyViewModel = new ItemSettingsViewModel(settingsModel.get(position),position);
        holder.itemPrivacyBinding.setViewmodel(itemPrivacyViewModel);
        setEvent(itemPrivacyViewModel);
    }

    private void setEvent(ItemSettingsViewModel itemPrivacyViewModel) {
        itemPrivacyViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return settingsModel.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        settingsModel.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<SettingsModel> data) {
        this.settingsModel = data;
        notifyDataSetChanged();
    }

    public class SettingsView extends RecyclerView.ViewHolder{

        private ItemSettingsBinding itemPrivacyBinding;
        public SettingsView(@NonNull ItemSettingsBinding itemPrivacyBinding) {
            super(itemPrivacyBinding.getRoot());
            this.itemPrivacyBinding = itemPrivacyBinding;
        }
    }
}
