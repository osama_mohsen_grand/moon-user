package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemReviewBinding;
import grand.app.moon.models.review.Review;
import grand.app.moon.viewmodels.review.ItemReviewViewModel;
import timber.log.Timber;


public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewView> {
    private List<Review> reviews;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ReviewAdapter(List<Review> reviews) {
        this.reviews = reviews;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ReviewView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemReviewBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_review,parent,false);
        return new ReviewView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewView holder, final int position) {
        ItemReviewViewModel itemReviewViewModel = new ItemReviewViewModel(reviews.get(position),position);
        Timber.e("itemReviewViewModel:"+reviews.get(position).mComment);
        holder.itemReviewBinding.setItemReviewViewModel(itemReviewViewModel);
        setEvent(itemReviewViewModel);
    }

    private void setEvent(ItemReviewViewModel itemReviewViewModel) {
        itemReviewViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }


    public class ReviewView extends RecyclerView.ViewHolder{

        private ItemReviewBinding itemReviewBinding;
        public ReviewView(@NonNull ItemReviewBinding itemReviewBinding) {
            super(itemReviewBinding.getRoot());
            this.itemReviewBinding = itemReviewBinding;
        }
    }
}
