package grand.app.moon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemServiceBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.viewmodels.service.ItemServiceViewModel;


public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.CountryView> {
    private List<Service> services;
    private LayoutInflater layoutInflater;
    private int selected = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ServiceAdapter(List<Service> services) {
        this.services = services;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CountryView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemServiceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_service,parent,false);
        return new CountryView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryView holder, final int position) {
        ItemServiceViewModel itemServiceViewModel = new ItemServiceViewModel(services.get(position),position, position == selected);
        holder.itemServiceBinding.setItemServiceViewModel(itemServiceViewModel);
        setEvent(holder.itemView, itemServiceViewModel);
    }

    private void setEvent(View view , ItemServiceViewModel itemServiceViewModel) {
        itemServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return services.size();
    }


    public void update(List<Service> services) {
        this.services = services;
        notifyDataSetChanged();
    }

    public class CountryView extends RecyclerView.ViewHolder{

        private ItemServiceBinding itemServiceBinding;
        public CountryView(@NonNull ItemServiceBinding itemServiceBinding) {
            super(itemServiceBinding.getRoot());
            this.itemServiceBinding = itemServiceBinding;
        }
    }
}
