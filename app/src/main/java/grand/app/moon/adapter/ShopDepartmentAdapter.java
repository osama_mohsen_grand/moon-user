package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemShopDetailsDepartmentBinding;
import grand.app.moon.databinding.ItemShopDetailsDepartmentBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.shop.ShopDepartment;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.shop.ItemShopDepartmentViewModel;

public class ShopDepartmentAdapter extends RecyclerView.Adapter<ShopDepartmentAdapter.AlbumView> {
    public List<ShopDepartment> imageVideos;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ShopDepartmentAdapter(List<ShopDepartment> imageVideos) {
        this.imageVideos = imageVideos;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemShopDetailsDepartmentBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_shop_details_department,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemShopDepartmentViewModel itemAdsViewModel = new ItemShopDepartmentViewModel(imageVideos.get(position),position);
        holder.itemBinding.setViewmodel(itemAdsViewModel);
        setEvent(itemAdsViewModel);
    }

    private void setEvent(ItemShopDepartmentViewModel ItemShopDepartmentViewModel) {
        ItemShopDepartmentViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return imageVideos.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ShopDepartment> imageVideos) {
        this.imageVideos = imageVideos;
        notifyDataSetChanged();
    }

    public void updateFollow(boolean isFollow) {
        for(int i=0; i<imageVideos.size() ; i++){
            if(imageVideos.get(i).constant.equals(Constants.FOLLOW)){
                imageVideos.get(i).drawable = (isFollow ? ResourceManager.getDrawable(R.drawable.ic_followed) :ResourceManager.getDrawable(R.drawable.ic_follow) );
                notifyItemChanged(i);
                break;
            }
        }
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemShopDetailsDepartmentBinding itemBinding;
        public AlbumView(@NonNull ItemShopDetailsDepartmentBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}
