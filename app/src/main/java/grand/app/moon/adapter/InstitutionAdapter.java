package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemInstitutionListBinding;
import grand.app.moon.databinding.ItemInstitutionListBinding;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.viewmodels.institution.ItemInstitutionViewModel;


public class InstitutionAdapter extends RecyclerView.Adapter<InstitutionAdapter.ShopView> {
    private List<ShopDetails> Shops;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public InstitutionAdapter(List<ShopDetails> Shops) {
        this.Shops = Shops;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ShopView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemInstitutionListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_institution_list,parent,false);
        return new ShopView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopView holder, final int position) {
        ItemInstitutionViewModel itemInstitutionViewModel = new ItemInstitutionViewModel(Shops.get(position),position);
        holder.ItemInstitutionListBinding.setItemInstitutionViewModel(itemInstitutionViewModel);
        setEvent(itemInstitutionViewModel);
    }

    private void setEvent(ItemInstitutionViewModel itemInstitutionViewModel) {
        itemInstitutionViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return Shops.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class ShopView extends RecyclerView.ViewHolder{

        private ItemInstitutionListBinding ItemInstitutionListBinding;
        public ShopView(@NonNull ItemInstitutionListBinding ItemInstitutionListBinding) {
            super(ItemInstitutionListBinding.getRoot());
            this.ItemInstitutionListBinding = ItemInstitutionListBinding;
        }
    }
}
