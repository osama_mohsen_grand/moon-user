package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemBeautyServiceBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.service.shop.Service;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.beauty.ItemBeautyServiceViewModel;
import timber.log.Timber;


public class BeautyServiceAdapter extends RecyclerView.Adapter<BeautyServiceAdapter.IdNamePriceView> {
    public List<IdNamePrice> services;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public ArrayList<Integer> ids = new ArrayList<>();

    public BeautyServiceAdapter(List<IdNamePrice> services) {
        this.services = services;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public IdNamePriceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemBeautyServiceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_beauty_service,parent,false);
        return new IdNamePriceView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull IdNamePriceView holder, final int position) {
        ItemBeautyServiceViewModel itemBeautyServiceViewModel = new ItemBeautyServiceViewModel(services.get(position),position,ids.contains(Integer.parseInt(services.get(position).id)));
        holder.itemBeautyServiceBinding.setItemBeautyServiceViewModel(itemBeautyServiceViewModel);
        setEvent(itemBeautyServiceViewModel);
    }

    private void setEvent(ItemBeautyServiceViewModel ItemBeautyServiceViewModel) {
        ItemBeautyServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Mutable mutable = (Mutable) aVoid;
                int id = Integer.parseInt(services.get(mutable.position).id);
                if(ids.contains(id)){
                    ids.remove(ids.indexOf(id));
                }else
                    ids.add(id);
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return services.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<IdNamePrice> services) {
        Timber.e("update list");
        this.services = services;
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        services.remove(category_delete_position);
        notifyDataSetChanged();
    }

    public void add(IdNamePrice reservationOrder) {
        services.add(reservationOrder);
        notifyDataSetChanged();
    }

    public void setIds(ArrayList<Integer> ids) {
        this.ids = ids;
        notifyDataSetChanged();
    }

    public class IdNamePriceView extends RecyclerView.ViewHolder{

        private ItemBeautyServiceBinding itemBeautyServiceBinding;
        public IdNamePriceView(@NonNull ItemBeautyServiceBinding itemBeautyServiceBinding) {
            super(itemBeautyServiceBinding.getRoot());
            this.itemBeautyServiceBinding = itemBeautyServiceBinding;
        }
    }
}
