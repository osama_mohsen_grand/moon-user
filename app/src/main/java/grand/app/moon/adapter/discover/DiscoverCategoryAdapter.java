package grand.app.moon.adapter.discover;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemDiscoverCategoryBinding;
import grand.app.moon.models.base.IdName;
import grand.app.moon.viewmodels.discover.ItemDiscoverCategoryViewModel;


public class DiscoverCategoryAdapter extends RecyclerView.Adapter<DiscoverCategoryAdapter.AlbumView> {
    public List<IdName> discoverService;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public int selected = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public DiscoverCategoryAdapter(List<IdName> discoverService) {
        this.discoverService = discoverService;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemDiscoverCategoryBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_discover_category,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemDiscoverCategoryViewModel itemViewModel = new ItemDiscoverCategoryViewModel(discoverService.get(position),position,(position == selected));
        holder.binding.setViewModel(itemViewModel);
        setEvent(holder);
    }

    private void setEvent(AlbumView view) {
        view.binding.getViewModel().mMutableLiveData.observeForever(aVoid -> {
            selected = view.binding.getViewModel().position;
            mMutableLiveData.setValue(aVoid);
            notifyDataSetChanged();
        });
    }


    @Override
    public int getItemCount() {
        return discoverService.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<IdName> discoverService) {
        this.discoverService = discoverService;
        selected = 0;
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemDiscoverCategoryBinding binding;
        public AlbumView(@NonNull ItemDiscoverCategoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
