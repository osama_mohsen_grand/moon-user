package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.models.base.IdNamePrice;


public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.IdNamePriceView> {
    private List<IdNamePrice> size;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private int selected = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public SizeAdapter(List<IdNamePrice> size) {
        this.size = size;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public IdNamePriceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
//        ItemNamePriceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_name, parent, false);
//        return new IdNameView(binding);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull IdNamePriceView holder, final int position) {
//        ItemIdNamePriceViewModel itemIdNamePriceViewModel = new ItemIdNamePriceViewModel(size.get(position), position, position == selected);
//        holder.itemIdNamePriceBinding.setItemIdNamePriceViewModel(itemIdNamePriceViewModel);
//        setEvent(itemIdNamePriceViewModel);
    }

//    private void setEvent(ItemIdNamePriceViewModel itemIdNamePriceViewModel) {
//        itemIdNamePriceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
//            @Override
//            public void onChanged(@Nullable Object aVoid) {
//                mMutableLiveData.setValue(aVoid);
//            }
//        });
//    }


    @Override
    public int getItemCount() {
        return size.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    public class IdNamePriceView extends RecyclerView.ViewHolder {
        public IdNamePriceView(@NonNull View itemView) {
            super(itemView);
        }

//        private ItemIdNamePriceBinding itemIdNamePriceBinding;
//
//        public IdNameView(@NonNull ItemIdNamePriceBinding itemIdNamePriceBinding) {
//            super(itemIdNamePriceBinding.getRoot());
//            this.itemIdNamePriceBinding = itemIdNamePriceBinding;
//        }
    }
}
