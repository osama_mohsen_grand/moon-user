package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemInfoServiceBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.viewmodels.profile.ItemInfoServiceViewModel;


public class ServiceInfoAdapter extends RecyclerView.Adapter<ServiceInfoAdapter.NotificationView> {
    private List<IdNameImage> notifications;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ServiceInfoAdapter(List<IdNameImage> notifications) {
        this.notifications = notifications;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public NotificationView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemInfoServiceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_info_service,parent,false);
        return new NotificationView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationView holder, final int position) {
        ItemInfoServiceViewModel itemInfoServiceViewModel = new ItemInfoServiceViewModel(notifications.get(position),position);
        holder.itemInfoServiceBinding.setViewmodel(itemInfoServiceViewModel);
        setEvent(itemInfoServiceViewModel);
    }

    private void setEvent(ItemInfoServiceViewModel itemInfoServiceViewModel) {
        itemInfoServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        notifications.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<IdNameImage> infoServices) {
        this.notifications = infoServices;
        notifyDataSetChanged();
    }

    public class NotificationView extends RecyclerView.ViewHolder{

        private ItemInfoServiceBinding itemInfoServiceBinding;
        public NotificationView(@NonNull ItemInfoServiceBinding itemInfoServiceBinding) {
            super(itemInfoServiceBinding.getRoot());
            this.itemInfoServiceBinding = itemInfoServiceBinding;
        }
    }
}
