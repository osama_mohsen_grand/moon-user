package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemAdsBinding;
import grand.app.moon.databinding.ItemAdsBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.viewmodels.ads.ItemAdsViewModel;


public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.AlbumView> {
    public List<ImageVideo> imageVideos;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public AdsAdapter(List<ImageVideo> imageVideos) {
        this.imageVideos = imageVideos;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemAdsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_ads,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemAdsViewModel itemAdsViewModel = new ItemAdsViewModel(imageVideos.get(position),position);
        holder.itemAdsBinding.setItemAdsViewModel(itemAdsViewModel);
        setEvent(itemAdsViewModel);
    }

    private void setEvent(ItemAdsViewModel ItemAdsViewModel) {
        ItemAdsViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return imageVideos.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ImageVideo> imageVideos) {
        this.imageVideos = imageVideos;
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemAdsBinding itemAdsBinding;
        public AlbumView(@NonNull ItemAdsBinding itemAdsBinding) {
            super(itemAdsBinding.getRoot());
            this.itemAdsBinding = itemAdsBinding;
        }
    }
}
