package grand.app.moon.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemBranchAccountInfoBinding;
import grand.app.moon.models.shop.Branch;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.branch.ItemBranchAccountInfoViewModel;
import grand.app.moon.views.activities.MapAddressActivity;

/*
1- BranchAdapter
2- BranchView (itemRow)
3- Branch (list)
4- ItemBranchAccountInfoBinding
 */


public class BranchAccountInfoAdapter extends RecyclerView.Adapter<BranchAccountInfoAdapter.BranchAccountInfoView> {
    public List<Branch> branches;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public BranchAccountInfoAdapter(List<Branch> branches) {
        this.branches = branches;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public BranchAccountInfoView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemBranchAccountInfoBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_branch_account_info, parent, false);
        return new BranchAccountInfoView(binding);
    }

    private static final String TAG = "BranchAccountInfoAdapte";

    @Override
    public void onBindViewHolder(@NonNull BranchAccountInfoView holder, final int position) {
        Log.d(TAG,"doner");
        ItemBranchAccountInfoViewModel itemBranchViewModel = new ItemBranchAccountInfoViewModel(branches.get(position), position);
//        ItemBranchAccountInfoViewModel itemBranchViewModel = new ItemBranchAccountInfoViewModel();
        holder.ItemBranchAccountInfoBinding.setItemBranchViewModel(itemBranchViewModel);
        setEvent(holder.itemView,itemBranchViewModel);
    }

    private void setEvent(View itemView, ItemBranchAccountInfoViewModel ItemBranchAccountInfoViewModel) {
        ItemBranchAccountInfoViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
                int pos = (int) aVoid;
                Intent intent = new Intent(itemView.getContext(), MapAddressActivity.class);
                intent.putExtra(Constants.LAT, branches.get(pos).lat);
                intent.putExtra(Constants.LNG, branches.get(pos).lng);
                intent.putExtra(Constants.NAME, branches.get(pos).address);
                itemView.getContext().startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return (branches == null ? 0 : branches.size());
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<Branch> branches) {
        this.branches = branches;
        Log.d(TAG,"branches_size:"+branches.size());
        notifyDataSetChanged();
    }

    public class BranchAccountInfoView extends RecyclerView.ViewHolder {

        private ItemBranchAccountInfoBinding ItemBranchAccountInfoBinding;

        public BranchAccountInfoView(@NonNull ItemBranchAccountInfoBinding ItemBranchAccountInfoBinding) {
            super(ItemBranchAccountInfoBinding.getRoot());
            this.ItemBranchAccountInfoBinding = ItemBranchAccountInfoBinding;
        }
    }
}
