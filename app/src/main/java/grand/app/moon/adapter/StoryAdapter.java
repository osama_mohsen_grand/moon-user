package grand.app.moon.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemStoryBinding;
import grand.app.moon.databinding.ItemStoryBinding;

import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.moon.MoonHelper;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.story.ItemStoryViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;


public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.CountryView> {
    private List<Story> stories;
    private LayoutInflater layoutInflater;
    public int position = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    Fragment fragment;

    public StoryAdapter(List<Story> stories, Fragment fragment) {
        this.stories = stories;
        this.fragment = fragment;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CountryView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemStoryBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_story, parent, false);
        return new CountryView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryView holder, final int position) {
        ItemStoryViewModel itemStoryViewModel = new ItemStoryViewModel(stories.get(position), position);
        holder.itemStoryBinding.setItemStoryViewModel(itemStoryViewModel);
        itemStoryViewModel.mMutableLiveData.observeForever(aVoid -> {
            this.position = position;
            mMutableLiveData.setValue(aVoid);
            int pos = ((Mutable) aVoid).position;
            Story story = stories.get(pos);

            if (story.mId == -1) {
                ((MainActivity) holder.itemStoryBinding.getRoot().getContext()).navigationDrawerView.followers();
            } else {
                if (((Mutable) aVoid).type.equals(Constants.STORY)) {
                    Intent intent = new Intent(holder.itemStoryBinding.getRoot().getContext(), BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.STORY);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.STORY, story);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, story.mName);
                    fragment.startActivityForResult(intent,Constants.VIEW_REQUEST);
                }else{
                    if(story.workDays != 0)
                        MoonHelper.shopDetails(holder.itemStoryBinding.getRoot().getContext(), story.type, story.flag, story.mId, story.mName);
                    else
                        Toasty.info(holder.itemView.getContext(), ResourceManager.getString(R.string.this_shop_is_close_now),Toasty.LENGTH_SHORT).show();                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return stories.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }




    public void swapLast() {

//        Log.d("result_ok","swapLast");
//        Log.d("result_ok",position+"");
//        Log.d("result_ok",stories.size() -2+"");
//        if(position != -1 && position < stories.size() -2){
//            Log.d("result_ok","position_swap");
//            Log.d("result_ok",stories.get(position).mId+"");
//            Log.d("result_ok",stories.get(stories.size() -2).mId+"");
//            Collections.swap(stories, position, stories.size() -2);
//            Log.d("result_ok",stories.get(position).mId+"");
//            Log.d("result_ok",stories.get(stories.size() -2).mId+"");
////            notifyItemChanged(position);
////            notifyItemChanged(storiesTmp.size() -2);
//        }
    }

    ArrayList<Story> storiesTmp = new ArrayList<>();
    ArrayList<Story> storiesViews = new ArrayList<>();
    Story more; int last_position=0;
    private static final String TAG = "StoryAdapter";

    public void swapUpdate() {
        storiesTmp.clear();
        storiesViews.clear();
        for (Story story: stories) {
            if (story.storyView == 1)
                storiesViews.add(story);
            else
                storiesTmp.add(story);
        }

        last_position = storiesTmp.size() - 1;
        more = storiesTmp.get(last_position);
        storiesTmp.remove(last_position);

        Log.d(TAG,storiesTmp.size()+"");
        Log.d(TAG,storiesViews.size()+"");

        stories.clear();
        stories.addAll(storiesTmp);
        stories.addAll(storiesViews);
        stories.add(more);

        notifyDataSetChanged();
        mMutableLiveData.setValue(new Mutable(Constants.SWAP,-1));
    }

    public void viewed() {
        if(position != -1 && position < stories.size()){
            stories.get(position).storyView = 1;
            notifyItemChanged(position);
        }
    }

    public void updateCount(int storyViewCount) {
        if(position != -1 && position < stories.size()){
            stories.get(position).storyViewCount = storyViewCount;
            stories.get(position).storyView = 1;
            notifyItemChanged(position);
        }
    }


    public class CountryView extends RecyclerView.ViewHolder {

        public ItemStoryBinding itemStoryBinding;

        public CountryView(@NonNull ItemStoryBinding itemStoryBinding) {
            super(itemStoryBinding.getRoot());
            this.itemStoryBinding = itemStoryBinding;
        }
    }
}
