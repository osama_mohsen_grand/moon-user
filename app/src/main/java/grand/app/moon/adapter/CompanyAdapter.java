package grand.app.moon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemCompanyListBinding;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.viewmodels.company.ItemCompanyListViewModel;



public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.CompanyView> {
    private List<AdsCompanyModel> adsCompanyModels;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public CompanyAdapter(List<AdsCompanyModel> adsCompanyModels) {
        this.adsCompanyModels = adsCompanyModels;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CompanyView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemCompanyListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_company_list,parent,false);
        return new CompanyView(binding);
    }

    private static final String TAG = "CompanyAdapter";

    @Override
    public void onBindViewHolder(@NonNull CompanyView holder, final int position) {
        ItemCompanyListViewModel itemInstitutionViewModel = new ItemCompanyListViewModel(adsCompanyModels.get(position),position);
        Log.d(TAG,adsCompanyModels.get(position).name);
        holder.ItemCompanyListBinding.setItemCompanyListViewModel(itemInstitutionViewModel);
        setEvent(itemInstitutionViewModel);
    }

    private void setEvent(ItemCompanyListViewModel itemInstitutionViewModel) {
        itemInstitutionViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return adsCompanyModels.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(ArrayList<AdsCompanyModel> ads) {
        this.adsCompanyModels = ads;
        Log.d(TAG,adsCompanyModels.size()+"");
        this.notifyDataSetChanged();
    }

    public class CompanyView extends RecyclerView.ViewHolder{

        private ItemCompanyListBinding ItemCompanyListBinding;
        public CompanyView(@NonNull ItemCompanyListBinding ItemCompanyListBinding) {
            super(ItemCompanyListBinding.getRoot());
            this.ItemCompanyListBinding = ItemCompanyListBinding;
        }
    }
}
