package grand.app.moon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.databinding.ItemDriverTruckBinding;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.taxi.TruckModel;
import grand.app.moon.viewmodels.clinic.ItemDoctorListViewModel;
import grand.app.moon.viewmodels.trucks.ItemDriverTruckViewModel;

public class DriverTrucksAdapter extends RecyclerView.Adapter<DriverTrucksAdapter.DriverTruckViewHolder> {
    private List<IdNamePrice> mDataList;
    private MutableLiveData<IdNamePrice> mutableLiveData;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public int selected = -1;
    List<TruckModel> mTruckModels;
    public void setList(@NonNull List<IdNamePrice> mDataList) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList<>();
        }
        this.mDataList.clear();
        this.mDataList.addAll(mDataList);
        notifyDataSetChanged();
    }

    public void updateList(@NonNull List<IdNamePrice> mDataList) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList<>();
        }
        this.mDataList.addAll(mDataList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DriverTruckViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemDriverTruckBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_driver_truck, parent, false);
        return new DriverTruckViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DriverTruckViewHolder holder, int position) {
        holder.setAnimation();
        ItemDriverTruckViewModel viewModel = new ItemDriverTruckViewModel(getCurrentItem(position),position);
        holder.setViewModel(viewModel);
        setEvent(viewModel);
    }

    private void setEvent(ItemDriverTruckViewModel viewModel) {
        viewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }
    private IdNamePrice getCurrentItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public int getItemCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    public MutableLiveData<IdNamePrice> getMutableLiveData() {
        return mutableLiveData;
    }

    class DriverTruckViewHolder extends RecyclerView.ViewHolder {
        ItemDriverTruckBinding binding;

        public DriverTruckViewHolder(ItemDriverTruckBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setAnimation() {
            //set animation here
        }

        public void setViewModel(ItemDriverTruckViewModel viewModel) {
            binding.setItemDriverTruckViewModel(viewModel);
        }
    }
}