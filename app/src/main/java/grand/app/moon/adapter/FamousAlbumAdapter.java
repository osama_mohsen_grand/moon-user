package grand.app.moon.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemFamousAlbumBinding;
import grand.app.moon.databinding.ItemFamousAlbumBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.album.ItemAlbumCoverViewModel;
import grand.app.moon.viewmodels.album.ItemAlbumViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.famous.FamousAlbumMainDetailsFragment;


public class FamousAlbumAdapter extends RecyclerView.Adapter<FamousAlbumAdapter.AlbumView> {
    public List<ImageVideo> imageVideosTmp;
    public List<ImageVideo> imageVideos;
    public int flag = 1;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public boolean isAds = false;

    public FamousAlbumAdapter() {
        imageVideos = new ArrayList<>();
    }

    public void setData(List<ImageVideo> imageVideos){
        this.imageVideos = imageVideos;
        imageVideosTmp = imageVideos;
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemFamousAlbumBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_famous_album,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemAlbumCoverViewModel itemAlbumViewModel = new ItemAlbumCoverViewModel(imageVideos.get(position),position);
        holder.itemAlbumBinding.setViewModel(itemAlbumViewModel);
        setEvent(holder.itemAlbumBinding.getRoot().getContext() , itemAlbumViewModel);
    }

    private static final String TAG = "FamousAlbumAdapter";

    private void setEvent(Context context,ItemAlbumCoverViewModel itemAlbumCoverViewModel) {
        itemAlbumCoverViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
//                mMutableLiveData.setValue(aVoid);

                Log.d(TAG,"onChange");
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, FamousAlbumMainDetailsFragment.class.getName());
                intent.putExtra(Constants.NAME_BAR,context.getString(R.string.details));
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TYPE,"1");
                bundle.putInt(Constants.ID,imageVideos.get(itemAlbumCoverViewModel.position).id);
                bundle.putInt(Constants.FLAG,flag);
                bundle.putInt(Constants.SHOP_ID,imageVideos.get(itemAlbumCoverViewModel.position).shopId);
                bundle.putBoolean(Constants.ADS,isAds);
                bundle.putBoolean(Constants.CONTENT,true);
                intent.putExtra(Constants.BUNDLE,bundle);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return imageVideos.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ImageVideo> imageVideos) {
        this.imageVideos = imageVideos;
        notifyDataSetChanged();
    }

    private List<ImageVideo> filterList = new ArrayList<>();
    public void submitFilter(ArrayList<Integer> list) {
        filterList.clear();
        if(!list.isEmpty()) {
            for (ImageVideo item : imageVideosTmp) {
                if (list.contains(item.famous_id))
                    filterList.add(item);
            }
            imageVideos = filterList;
        } else {
            imageVideos = imageVideosTmp;
        }
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemFamousAlbumBinding itemAlbumBinding;
        public AlbumView(@NonNull ItemFamousAlbumBinding itemAlbumBinding) {
            super(itemAlbumBinding.getRoot());
            this.itemAlbumBinding = itemAlbumBinding;
        }
    }
}
