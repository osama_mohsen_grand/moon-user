package grand.app.moon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemCityRegionBinding;
import grand.app.moon.databinding.ItemCountryBinding;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.Region;
import grand.app.moon.viewmodels.city.ItemCityRegionViewModel;
import grand.app.moon.viewmodels.country.ItemCountryViewModel;


public class CityRegionAdapter extends RecyclerView.Adapter<CityRegionAdapter.CityRegionView> {
    public List<IdName> cityRegion;
    private LayoutInflater layoutInflater;
    int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public CityRegionAdapter(List<IdName> cityRegion) {
        this.cityRegion = cityRegion;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CityRegionView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemCityRegionBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_city_region,parent,false);
        return new CityRegionView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CityRegionView holder, final int position) {
        ItemCityRegionViewModel itemCountryViewModel = new ItemCityRegionViewModel(cityRegion.get(position),position,(this.position == position));
        holder.binding.setViewModel(itemCountryViewModel);
        setEvent(itemCountryViewModel);
    }

    private static final String TAG = "CityRegionAdapter";

    private void setEvent(ItemCityRegionViewModel itemCityRegionViewModel) {
        itemCityRegionViewModel.mMutableLiveData.observeForever(new Observer<Object>() {

            @Override
            public void onChanged(@Nullable Object aVoid) {
                int position = (int) aVoid;
                mMutableLiveData.setValue(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return cityRegion.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void updateCities(List<City> cities) {
        List<IdName> list = new ArrayList<>();
        for(City city  : cities){
            list.add(new IdName(city.id+"",city.cityName));
        }
        position = -1;
        cityRegion.clear();
        cityRegion.addAll(list);
        notifyDataSetChanged();
    }

    public void updateRegions(List<Region> regions) {
        List<IdName> list = new ArrayList<>();
        for(Region region  : regions){
            list.add(new IdName(region.id+"",region.regionName));
        }
        position = -1;
        cityRegion.clear();
        cityRegion.addAll(list);
        notifyDataSetChanged();
    }


    public class CityRegionView extends RecyclerView.ViewHolder{

        private ItemCityRegionBinding binding;
        public CityRegionView(@NonNull ItemCityRegionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
