package grand.app.moon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemNotificationBinding;
import grand.app.moon.models.notifications.Notification;
import grand.app.moon.repository.NotificationRepository;
import grand.app.moon.utils.dialog.DialogHelper;
import grand.app.moon.viewmodels.notification.ItemNotificationViewModel;
import grand.app.moon.vollyutils.MyApplication;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationView> {
    public NotificationRepository repository;
    private List<Notification> notifications;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public NotificationAdapter(List<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public NotificationView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemNotificationBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_notification, parent, false);
        return new NotificationView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationView holder, final int position) {
        ItemNotificationViewModel itemNotificationViewModel = new ItemNotificationViewModel(notifications.get(position), position);
        holder.itemNotificationBinding.setItemNotificationViewModel(itemNotificationViewModel);
        setEvent(holder.itemNotificationBinding.getRoot().getContext(), itemNotificationViewModel);
    }

    private void setEvent(Context context, ItemNotificationViewModel itemNotificationViewModel) {
        itemNotificationViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                showDeleteDialog(context, (int) aVoid);
            }
        });
    }

    private void showDeleteDialog(Context context, int position) {
        int[] ids = {R.id.rl_yes, R.id.rl_no};
        DialogHelper.showDialogHelper(context, R.layout.dialog_delete, ids, (dialog, view) -> {
            switch (view.getId()) {
                case R.id.rl_yes:
                    dialog.dismiss();
                    repository.deleteNotification(notifications.get(position).getmId());
                    notifications.remove(position);
                    notifyItemRemoved(position);
                    if(notifications.size() == 0)
                        mMutableLiveData.setValue(-1);
                    break;
                case R.id.rl_no:
                    dialog.dismiss();
                    break;
            }

        });
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void remove(int item_delete_position) {
        notifications.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<Notification> notifications) {
        this.notifications.addAll(notifications);
        notifyDataSetChanged();
    }

    public List<Notification> getItems() {
        return notifications;
    }

    public class NotificationView extends RecyclerView.ViewHolder {

        private ItemNotificationBinding itemNotificationBinding;

        public NotificationView(@NonNull ItemNotificationBinding itemNotificationBinding) {
            super(itemNotificationBinding.getRoot());
            this.itemNotificationBinding = itemNotificationBinding;
        }
    }
}
