package grand.app.moon.adapter.discover;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemAlbumBinding;
import grand.app.moon.databinding.ItemDiscoverServiceBinding;
import grand.app.moon.databinding.ItemServiceCategoryBinding;
import grand.app.moon.models.discover.DiscoverService;
import grand.app.moon.viewmodels.discover.ItemDiscoverServiceViewModel;


public class DiscoverServiceAdapter extends RecyclerView.Adapter<DiscoverServiceAdapter.AlbumView> {
    public List<DiscoverService> discoverService;
    private LayoutInflater layoutInflater;
    private int position = 0;
    public int selected = 0;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public DiscoverServiceAdapter(List<DiscoverService> discoverService) {
        this.discoverService = discoverService;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemDiscoverServiceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_discover_service,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemDiscoverServiceViewModel itemViewModel = new ItemDiscoverServiceViewModel(discoverService.get(position),position,(position == selected));
        holder.binding.setViewModel(itemViewModel);
        setEvent(holder);
    }

    private static final String TAG = "DiscoverServiceAdapter";
    private void setEvent(AlbumView view) {
        view.binding.getViewModel().mMutableLiveData.observeForever(aVoid -> {
            selected = view.binding.getViewModel().position;
            Log.d(TAG,selected+"");
            mMutableLiveData.setValue(aVoid);
            notifyDataSetChanged();
        });
    }


    @Override
    public int getItemCount() {
        return discoverService.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<DiscoverService> discoverService) {
        this.discoverService = discoverService;
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemDiscoverServiceBinding binding;
        public AlbumView(@NonNull ItemDiscoverServiceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
