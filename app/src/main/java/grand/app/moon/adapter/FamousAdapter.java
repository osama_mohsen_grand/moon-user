package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemFamousBinding;
import grand.app.moon.models.famous.list.Famous;
import grand.app.moon.viewmodels.search.ItemFamousViewModel;


public class FamousAdapter extends RecyclerView.Adapter<FamousAdapter.FamousView> {
    private List<Famous> famousList;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public FamousAdapter(List<Famous> famousList) {
        this.famousList = famousList;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public FamousView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemFamousBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_famous,parent,false);
        return new FamousView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FamousView holder, final int position) {
        ItemFamousViewModel itemFamousViewModel = new ItemFamousViewModel(famousList.get(position),position);
        holder.itemFamousBinding.setItemFamousViewModel(itemFamousViewModel);
        setEvent(itemFamousViewModel);
    }

    private void setEvent(ItemFamousViewModel itemFamousViewModel) {
        itemFamousViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return famousList.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class FamousView extends RecyclerView.ViewHolder{

        private ItemFamousBinding itemFamousBinding;
        public FamousView(@NonNull ItemFamousBinding itemFamousBinding) {
            super(itemFamousBinding.getRoot());
            this.itemFamousBinding = itemFamousBinding;
        }
    }
}
