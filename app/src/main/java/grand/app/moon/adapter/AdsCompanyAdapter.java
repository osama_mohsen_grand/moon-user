package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemAdsCompanyBinding;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.viewmodels.ads.ItemAdsCompanyViewModel;
import timber.log.Timber;


public class AdsCompanyAdapter extends RecyclerView.Adapter<AdsCompanyAdapter.AdsCompanyView> {
    private List<AdsCompanyModel> adsCompany;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public AdsCompanyAdapter(List<AdsCompanyModel> adsCompany) {
        this.adsCompany = adsCompany;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AdsCompanyView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemAdsCompanyBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_ads_company, parent, false);
        return new AdsCompanyView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdsCompanyView holder, final int position) {
        ItemAdsCompanyViewModel itemAdsCompanyViewModel = new ItemAdsCompanyViewModel(adsCompany.get(position), position);
        holder.itemAdsCompanyBinding.setItemAdsCompanyViewModel(itemAdsCompanyViewModel);
        setEvent(itemAdsCompanyViewModel);
    }

    @Override
    public int getItemCount() {
        return adsCompany.size();
    }


    private void setEvent(ItemAdsCompanyViewModel itemAdsCompanyViewModel) {
        itemAdsCompanyViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }

    public void update(List<AdsCompanyModel> adsCompany) {
        this.adsCompany = adsCompany;
        notifyDataSetChanged();
    }

    public class AdsCompanyView extends RecyclerView.ViewHolder {

        private ItemAdsCompanyBinding itemAdsCompanyBinding;

        public AdsCompanyView(@NonNull ItemAdsCompanyBinding itemAdsCompanyBinding) {
            super(itemAdsCompanyBinding.getRoot());
            this.itemAdsCompanyBinding = itemAdsCompanyBinding;
        }
    }
}
