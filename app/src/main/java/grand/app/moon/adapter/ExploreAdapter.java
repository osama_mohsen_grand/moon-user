package grand.app.moon.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemExploreBinding;
import grand.app.moon.models.explore.Explore;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.repository.CommonRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.explore.ItemExploreViewModel;


public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.AlbumView> {
    public List<Explore> items;
    private LayoutInflater layoutInflater;
    private int position = 0;
    public int selected = 0;
    public int follow_selected = -1;
    public CommonRepository commonRepository;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public ExploreAdapter(List<Explore> items) {
        this.items = items;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemExploreBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_explore, parent, false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemExploreViewModel itemViewModel = new ItemExploreViewModel(items.get(position), position);
        holder.binding.setViewmodel(itemViewModel);
        setEvent(holder);
    }

    private static final String TAG = "ExploreAdapter";

    private void setEvent(AlbumView view) {
        view.binding.getViewmodel().mMutableLiveData.observeForever(aVoid -> {
            selected = view.binding.getViewmodel().position;
            String action = (String) aVoid;
            if (action.equals(Constants.FOLLOW) && commonRepository != null) {
                follow_selected = selected;
                commonRepository.follow(new FollowRequest(items.get(selected).shopId,items.get(selected).type));
            } else if (action.equals(Constants.ERROR)) {
                Toasty.info(view.binding.getRoot().getContext(), ResourceManager.getString(R.string.please_login_first), Toasty.LENGTH_SHORT).show();
            }
            mMutableLiveData.setValue(aVoid);
            notifyDataSetChanged();
        });
    }

    public void setCommonRepository(CommonRepository commonRepository) {
        this.commonRepository = commonRepository;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<Explore> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void updateFollow(int follow_selected) {
        Explore explore = items.get(follow_selected);
        Log.d(TAG,"explore:"+explore.isFollow);
        explore.isFollow = (explore.isFollow.equals("0") ? "1" : "0");
        Log.d(TAG,"explore_update:"+explore.isFollow);
        notifyItemChanged(follow_selected,explore);
    }

    public class AlbumView extends RecyclerView.ViewHolder {

        private ItemExploreBinding binding;

        public AlbumView(@NonNull ItemExploreBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
