package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.viewmodels.cartype.ItemCarTypeViewModel;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemCarTypeBinding;

public class CarTypeAdapter extends RecyclerView.Adapter<CarTypeAdapter.CarTypeView> {
    private List<String> slider;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public CarTypeAdapter(List<String> slider) {
      this.slider = slider;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CarTypeView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemCarTypeBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_car_type,parent,false);
        return new CarTypeView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CarTypeView holder, final int position) {
        ItemCarTypeViewModel itemCarTypeViewModel = new ItemCarTypeViewModel(slider.get(position),position);
        holder.itemCarTypeBinding.setItemCarTypeViewModel(itemCarTypeViewModel);
        setEvent(itemCarTypeViewModel);
    }

    private void setEvent(ItemCarTypeViewModel itemCarTypeViewModel) {
        itemCarTypeViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                int pos = (int) aVoid;
                mMutableLiveData.setValue(pos);
            }
        });
    }


    @Override
    public int getItemCount() {
        return slider.size();
    }

    public class CarTypeView extends RecyclerView.ViewHolder{

        private ItemCarTypeBinding itemCarTypeBinding;
        public CarTypeView(@NonNull ItemCarTypeBinding itemCarTypeBinding) {
            super(itemCarTypeBinding.getRoot());
                this.itemCarTypeBinding = itemCarTypeBinding;
        }
    }
}
