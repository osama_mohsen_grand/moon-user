package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemRadioPriceBinding;
import grand.app.moon.databinding.ItemRadioPriceBinding;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.viewmodels.moon.ItemCheckPriceViewModel;


public class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.IdNamePriceView> {
    private List<IdNamePrice> idNamePrice;
    private LayoutInflater layoutInflater;
    public int selected = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public RadioAdapter(List<IdNamePrice> idNamePrice) {
        if(idNamePrice.size() > 0) selected = 0;
        this.idNamePrice = idNamePrice;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public IdNamePriceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemRadioPriceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_radio_price, parent, false);
        return new IdNamePriceView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull IdNamePriceView holder, final int position) {
        ItemCheckPriceViewModel itemIdNamePriceViewModel = new ItemCheckPriceViewModel(idNamePrice.get(position), position, position == selected);
        holder.itemRadioPriceBinding.setItemCheckPriceViewModel(itemIdNamePriceViewModel);
        setEvent(itemIdNamePriceViewModel);
    }

    private void setEvent(ItemCheckPriceViewModel itemIdNamePriceViewModel) {
        itemIdNamePriceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                selected = (int) aVoid;
                notifyDataSetChanged();
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return idNamePrice.size();
    }


    public void update(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    public class IdNamePriceView extends RecyclerView.ViewHolder {

        private ItemRadioPriceBinding itemRadioPriceBinding;

        public IdNamePriceView(@NonNull ItemRadioPriceBinding itemRadioPriceBinding) {
            super(itemRadioPriceBinding.getRoot());
            this.itemRadioPriceBinding = itemRadioPriceBinding;
        }
    }
}
