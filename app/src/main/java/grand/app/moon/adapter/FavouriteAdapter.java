package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemFavouriteBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.favourite.FavouriteModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.favourite.ItemFavouriteViewModel;


public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.FavouriteModelView> {
    private List<FavouriteModel> favourites;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public FavouriteAdapter() {
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public FavouriteModelView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemFavouriteBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_favourite,parent,false);
        return new FavouriteModelView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouriteModelView holder, final int position) {
        ItemFavouriteViewModel itemFavouriteModelViewModel = new ItemFavouriteViewModel(favourites.get(position),position);
        holder.itemFavouriteModelBinding.setViewmodel(itemFavouriteModelViewModel);
        setEvent(itemFavouriteModelViewModel);
    }

    private void setEvent(ItemFavouriteViewModel itemFavouriteModelViewModel) {
        itemFavouriteModelViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return favourites.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void setList(List<FavouriteModel> favourites){
        this.favourites = favourites;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        favourites.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public List<FavouriteModel> getList() {
        return favourites;
    }

    public void clear() {
        if(favourites != null)
        favourites.clear();
        notifyDataSetChanged();
    }

    public class FavouriteModelView extends RecyclerView.ViewHolder{

        private ItemFavouriteBinding itemFavouriteModelBinding;
        public FavouriteModelView(@NonNull ItemFavouriteBinding itemFavouriteModelBinding) {
            super(itemFavouriteModelBinding.getRoot());
            this.itemFavouriteModelBinding = itemFavouriteModelBinding;
        }
    }
}
