package grand.app.moon.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemServiceShopBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.service.ItemServiceShopViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;


public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ShopView> {
    private final Fragment fragment;
    public Service service = null;
    private List<ShopDetails> Shops = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();

    public ShopAdapter(Fragment fragment) {
        this.fragment = fragment;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ShopView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemServiceShopBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_service_shop, parent, false);
        return new ShopView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopView holder, final int position) {
        ItemServiceShopViewModel itemServiceShopViewModel = new ItemServiceShopViewModel(Shops.get(position), position);
        holder.itemServiceShopBinding.setItemServiceShopViewModel(itemServiceShopViewModel);
        setEvent(holder.itemView.getContext(), itemServiceShopViewModel);
    }

    private void setEvent(Context context, ItemServiceShopViewModel itemServiceShopViewModel) {
        itemServiceShopViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Log.d("shopDetails", "here adapter");
                Mutable mutable = (Mutable) aVoid;
                position = mutable.position;
                ShopDetails shop = Shops.get(mutable.position);
                if (mutable.type.equals(Constants.SUBMIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    Bundle bundle = new Bundle();
                    if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) || service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
                        intent.putExtra(Constants.PAGE, Constants.CLINIC_DETAILS);
                        intent.putExtra(Constants.NAME_BAR, shop.mName);
                        bundle.putInt(Constants.ID, shop.mId);
                        bundle.putSerializable(Constants.SERVICE, service);
                        intent.putExtra(Constants.BUNDLE, bundle);
                    } else {
                        ShopDetailsFragment shopDetailsFragment = new ShopDetailsFragment();
                        bundle.putInt(Constants.ID, shop.mId);
                        bundle.putSerializable(Constants.SERVICE, service);
                        shopDetailsFragment.setArguments(bundle);
                        intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
                        intent.putExtra(Constants.BUNDLE, bundle);
                    }
                    context.startActivity(intent);
                } else if (mutable.type.equals(Constants.STORY)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.STORY);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.STORY, shop.stories);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, shop.stories.mName);
                    fragment.startActivityForResult(intent, Constants.VIEW_REQUEST);
                }
            }
        });
    }

    public void update(List<ShopDetails> Shops) {
        this.Shops = Shops;
    }

    @Override
    public int getItemCount() {
        return Shops.size();
    }

    public int getPosition() {
        return position;
    }

    public List<ShopDetails> getShops(){
        return Shops;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void storyViewed() {
        Shops.get(position).stories.storyView = 1;
        notifyItemChanged(position);
    }

    public class ShopView extends RecyclerView.ViewHolder {

        private ItemServiceShopBinding itemServiceShopBinding;

        public ShopView(@NonNull ItemServiceShopBinding ItemServiceShopBinding) {
            super(ItemServiceShopBinding.getRoot());
            this.itemServiceShopBinding = ItemServiceShopBinding;
        }
    }
}
