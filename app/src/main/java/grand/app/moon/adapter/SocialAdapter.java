package grand.app.moon.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemSocialBinding;
import grand.app.moon.databinding.ItemSocialBinding;
import grand.app.moon.models.famous.list.Famous;
import grand.app.moon.models.personalnfo.Profile;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.profile.ItemSocialViewModel;
import grand.app.moon.viewmodels.profile.ItemSocialViewModel;
import grand.app.moon.vollyutils.AppHelper;


public class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.NotificationView> {
    private List<Profile> notifications;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public SocialAdapter(List<Profile> notifications) {
        this.notifications = notifications;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public NotificationView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSocialBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_social,parent,false);
        return new NotificationView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationView holder, final int position) {
        ItemSocialViewModel itemViewModel = new ItemSocialViewModel(notifications.get(position),position);
        holder.itemSocialBinding.setViewmodel(itemViewModel);
        setEvent(holder.itemSocialBinding.getRoot().getRootView().getContext() , itemViewModel);
    }

    private static final String TAG = "SocialAdapter";

    private void setEvent(Context context , ItemSocialViewModel itemViewModel) {
        itemViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Profile profile = notifications.get((int)aVoid);
                Log.d(TAG,profile.type);
                if(profile.type.equals("whats")){
                    AppUtils.openWhatsApp(context,profile.media);
                }else{
                    AppUtils.openBrowser(context,profile.media);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        notifications.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<Profile> profile) {
        this.notifications = profile;
        notifyDataSetChanged();
    }

    public class NotificationView extends RecyclerView.ViewHolder{

        private ItemSocialBinding itemSocialBinding;
        public NotificationView(@NonNull ItemSocialBinding itemSocialBinding) {
            super(itemSocialBinding.getRoot());
            this.itemSocialBinding = itemSocialBinding;
        }
    }
}
