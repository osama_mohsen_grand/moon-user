package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemMyAdsBinding;
import grand.app.moon.databinding.ItemMyAdsBinding;
import grand.app.moon.models.ads.MyAdsModel;
import grand.app.moon.models.ads.MyAdsModel;
import grand.app.moon.viewmodels.ads.ItemMyAdsViewModel;
import grand.app.moon.viewmodels.ads.ItemMyAdsViewModel;


public class MyAdsAdapter extends RecyclerView.Adapter<MyAdsAdapter.MyAdsView> {
    private List<MyAdsModel> myAdsModels;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    public int delete_position = -1;

    public MyAdsAdapter(List<MyAdsModel> myAdsModels) {
        this.myAdsModels = myAdsModels;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public MyAdsView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemMyAdsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_my_ads, parent, false);
        return new MyAdsView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdsView holder, final int position) {
        ItemMyAdsViewModel itemMyAdsViewModel = new ItemMyAdsViewModel(myAdsModels.get(position), position);
        holder.itemMyAdsBinding.setItemMyAdsViewModel(itemMyAdsViewModel);
        setEvent(itemMyAdsViewModel);
    }

    @Override
    public int getItemCount() {
        return myAdsModels.size();
    }


    private void setEvent(ItemMyAdsViewModel itemMyAdsViewModel) {
        itemMyAdsViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }

    public void update(List<MyAdsModel> myAdsModels) {
        this.myAdsModels = myAdsModels;
        notifyDataSetChanged();
    }

    public void deleteItem() {
        this.myAdsModels.remove(delete_position);
        notifyDataSetChanged();
    }

    public class MyAdsView extends RecyclerView.ViewHolder {

        private ItemMyAdsBinding itemMyAdsBinding;

        public MyAdsView(@NonNull ItemMyAdsBinding itemMyAdsBinding) {
            super(itemMyAdsBinding.getRoot());
            this.itemMyAdsBinding = itemMyAdsBinding;
        }
    }
}
