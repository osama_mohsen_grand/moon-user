package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemNameBinding;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.viewmodels.size.ItemNameViewModel;


public class TextAdapter extends RecyclerView.Adapter<TextAdapter.IdNameView> {
    private List<IdName> idNames;
    private LayoutInflater layoutInflater;
    public int selected = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public TextAdapter(List<IdName> idNames) {
        this.idNames = idNames;
        if(idNames.size() > 0)
            selected = 0;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public IdNameView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemNameBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_name, parent, false);
        return new IdNameView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull IdNameView holder, final int position) {
        ItemNameViewModel itemIdNamePriceViewModel = new ItemNameViewModel(idNames.get(position), position, position == selected);
        holder.itemIdNameBinding.setItemNameViewModel(itemIdNamePriceViewModel);
        setEvent(itemIdNamePriceViewModel);
    }

    private void setEvent(ItemNameViewModel itemNameViewModel) {
        itemNameViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                selected = (int) aVoid;
                notifyDataSetChanged();
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return idNames.size();
    }

    public void update(List<IdName> idNames) {
        this.idNames = idNames;
        selected = 0;
        notifyDataSetChanged();
    }

    public class IdNameView extends RecyclerView.ViewHolder {

        private ItemNameBinding itemIdNameBinding;

        public IdNameView(@NonNull ItemNameBinding itemIdNameBinding) {
            super(itemIdNameBinding.getRoot());
            this.itemIdNameBinding = itemIdNameBinding;
        }
    }
}
