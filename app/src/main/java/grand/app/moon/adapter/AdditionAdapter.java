package grand.app.moon.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.databinding.ItemProductAdditionBinding;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.product.Addition;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.viewmodels.product.ItemAdditionViewModel;
import timber.log.Timber;


public class AdditionAdapter extends RecyclerView.Adapter<AdditionAdapter.AdditionView> {
    private List<Addition> addition;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public ArrayList<String> selected;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public AdditionAdapter(List<Addition> addition, ArrayList<String> selected) {
        this.addition = addition;
        this.selected = selected;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AdditionView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemProductAdditionBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_product_addition,parent,false);
        return new AdditionView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdditionView holder, final int position) {
        ItemAdditionViewModel itemAdditionViewModel = new ItemAdditionViewModel(addition.get(position),position,selected.contains(addition.get(position).id));
        AppUtils.initVerticalRV(holder.itemCheckedBinding.rvProductAdditionCategory, holder.itemCheckedBinding.rvProductAdditionCategory.getContext(), 1);
        CheckPriceAdapter checkAdapter = new CheckPriceAdapter(addition.get(position).adds,selected);
        holder.itemCheckedBinding.rvProductAdditionCategory.setAdapter(checkAdapter);
        checkAdapter.mMutableLiveData.observe((LifecycleOwner) holder.itemCheckedBinding.rvProductAdditionCategory.getContext(), new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Timber.e("additionAdapter true");
                selected = (ArrayList<String>) o;
                Timber.e("ids:"+selected);
//                if(selected.contains(id)){
//                    selected.remove(selected.indexOf(id));
//                }else{
//                    selected.add(id);
//                }
//                notifyDataSetChanged();
            }
        });

        holder.itemCheckedBinding.setItemAdditionViewModel(itemAdditionViewModel);
//        setEvent(itemAdditionViewModel);
    }

    private void setEvent(ItemAdditionViewModel itemAdditionViewModel) {
        itemAdditionViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                int pos = (int) aVoid;
                Addition idNamePrice = addition.get(pos);
                if(selected.contains(idNamePrice.id)){
                    selected.remove(selected.indexOf(idNamePrice.id));
                }else{
                    selected.add(idNamePrice.id);
                }
                notifyDataSetChanged();
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return addition.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class AdditionView extends RecyclerView.ViewHolder{

        private ItemProductAdditionBinding itemCheckedBinding;
        public AdditionView(@NonNull ItemProductAdditionBinding itemCheckedBinding) {
            super(itemCheckedBinding.getRoot());
            this.itemCheckedBinding = itemCheckedBinding;
        }
    }
}
