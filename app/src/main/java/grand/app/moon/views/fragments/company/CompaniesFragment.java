package grand.app.moon.views.fragments.company;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ServiceCategoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentCompaniesBinding;
import grand.app.moon.databinding.FragmentServiceDetailsBinding;
import grand.app.moon.models.app.AppMoonServices;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.viewmodels.company.CompaniesDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.gallery.DiscoverMainFragment;
import grand.app.moon.views.fragments.institution.InstitutionShopFragment;
import grand.app.moon.views.fragments.service.ServiceMapFragment;
import grand.app.moon.views.fragments.service.ServiceShopFragment;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 */
public class CompaniesFragment extends BaseFragment {

    private FragmentCompaniesBinding fragmentCompaniesBinding;
    private CompaniesDetailsViewModel companiesDetailsViewModel;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    public ServiceCategoryAdapter serviceCategoryAdapter;
    public ArrayList<Integer> tags = new ArrayList<>();
    public int filter = 1;
    boolean setServiceCategory = false;
    SwapAdapter adapter = null;
    public String service_id = "";
    List<Service> services = null;
    Service service;
    public int type = -1;
    double lat, lng;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentCompaniesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_companies, container, false);
        getData();
        lat = UserHelper.getLat();
        lng = UserHelper.getLng();
        bind();
        setEvent();
        setSearchAction();
        return fragmentCompaniesBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE))
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
    }

    private void setSearchAction() {
        ((BaseActivity) getActivityBase()).backActionBarView.layoutActionBarBackBinding.imgBaseBarSearch.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.SEARCH);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.FLAG, service.mFlag);
            bundle.putInt(Constants.TYPE, service.mType);
            bundle.putSerializable(Constants.SERVICE, service);
            bundle.putInt(Constants.SERVICE_ID, Integer.parseInt(service_id));
            intent.putExtra(Constants.BUNDLE, bundle);
            startActivity(intent);
        });
    }


    @SuppressLint("ClickableViewAccessibility")
    private void bind() {
        companiesDetailsViewModel = new CompaniesDetailsViewModel(service);
        serviceCategoryAdapter = new ServiceCategoryAdapter(new ArrayList<>());
        setTabs();
        AppUtils.initHorizontalRV(fragmentCompaniesBinding.rvServiceCategories, fragmentCompaniesBinding.rvServiceCategories.getContext(), 1);
        fragmentCompaniesBinding.setCompaniesDetailsViewModel(companiesDetailsViewModel);
    }


    public void setTabs() {
        tabModels = AppMoonServices.getTabCompanies(getArguments());
        adapter = new SwapAdapter(getChildFragmentManager(), tabModels);
        fragmentCompaniesBinding.viewpager.setAdapter(adapter);
        fragmentCompaniesBinding.viewpager.setOffscreenPageLimit(tabModels.size());
        fragmentCompaniesBinding.slidingTabs.setupWithViewPager(fragmentCompaniesBinding.viewpager);
        adapter.notifyDataSetChanged();
    }

    private static final String TAG = "CompaniesFragment";

    private void setEvent() {
        companiesDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                Log.d(TAG, "" + action);
                handleActions(action, companiesDetailsViewModel.getCompanyRepository().getMessage());
//                handleActions(action, companiesDetailsViewModel.getDiscoverRepository().getMessage());
                if (action.equals(Constants.TAGS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.TAGS);
                    Bundle bundle = getArguments();
                    assert bundle != null;
                    bundle.putString(Constants.ID, service_id);
                    bundle.putIntegerArrayList(Constants.SELECTED, tags);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.tags));
                    startActivityForResult(intent, Constants.TAGS_RESULT);
                }
                if (action.equals(Constants.CITY)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.CITY);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.city));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, companiesDetailsViewModel.city_id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.CITY_RESULT);
                } else if (action.equals(Constants.FILTER) && !service_id.equals("")) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADS_COMPANY_FILTER);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FROM, Constants.ADS_COMPANY_FILTER);
                    bundle.putInt(Constants.ID, Integer.parseInt(service_id));
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.FILTER_RESULT);
                } else if (action.equals(Constants.DISCOVER)) {
                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.GALLERY);
//                    intent.putExtra(Constants.TYPE, Constants.DISCOVER);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable(Constants.SERVICE,service);
//                    bundle.putString(Constants.SERVICE_ID,service_id);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.discover));

                    intent.putExtra(Constants.PAGE, DiscoverMainFragment.class.getName());
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.discover));
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.OFFLINE, true);
                    bundle.putSerializable(Constants.SERVICE, service);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);


                    startActivity(intent);
                } else if (action.equals(Constants.SUCCESS)) {
                    companiesDetailsViewModel.response = companiesDetailsViewModel.getCompanyRepository().getCompanyMainResponse();
                    setService();
                    resetAdapter();
                }
            }
        });

    }

    public void setService() {
        this.services = companiesDetailsViewModel.response.mData.mServices;
        if (!setServiceCategory) {
            serviceCategoryAdapter = new ServiceCategoryAdapter(services);
            fragmentCompaniesBinding.rvServiceCategories.setAdapter(serviceCategoryAdapter);
            setServiceCategory = true;
            if (services.size() > 0)
                service_id = services.get(0).mId + "";
            setEventAdapter();
        } else {
            serviceCategoryAdapter.update(services);
        }
    }

    private void setEventAdapter() {
        serviceCategoryAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                serviceCategoryAdapter.selected = position;
                companiesDetailsViewModel.service_id = services.get(position).mId + "";
                serviceCategoryAdapter.notifyDataSetChanged();
                companiesDetailsViewModel.callService();
            }
        });
    }


    public void resetAdapter() {
        adapter = (SwapAdapter) fragmentCompaniesBinding.viewpager.getAdapter();
        try {
            for (int i = 0; i < 3; i++) {
                if (i < 2) {
                    CompaniesListFragment companiesListFragment = (CompaniesListFragment) adapter.getItem(i);
                    if (companiesListFragment != null) {
                        Log.d(TAG, companiesDetailsViewModel.response.mData.companies.size() + "");
                        companiesListFragment.companiesListViewModel.updateView(companiesDetailsViewModel.response.mData.companies);
//                        Timber.e("category_id:"+companiesDetailsViewModel.category_id);
//                        Timber.e("sub_category_id:"+companiesDetailsViewModel.sub_category_id);
//                        companiesDetailsViewModel.filter = filter;
//                        companiesDetailsViewModel.tags = tags;
//                        companiesDetailsViewModel.service_id = service_id;
//                        companiesDetailsViewModel.city_id = companiesDetailsViewModel.city_id;
//                        companiesDetailsViewModel.category_id = companiesDetailsViewModel.category_id;
//                        companiesDetailsViewModel.sub_category_id = companiesDetailsViewModel.sub_category_id;
//                        companiesDetailsViewModel.callService();
                    }
                } else {
                    ServiceMapFragment serviceMapFragment = (ServiceMapFragment) adapter.getItem(i);
                    if (serviceMapFragment != null)
                        serviceMapFragment.mapViewModel.updateViewCompanies(companiesDetailsViewModel.response.mData.companies);
//                    companyMapFragment.mapViewModel.filter = filter;
//                    companyMapFragment.mapViewModel.tags = tags;
//                    companyMapFragment.mapViewModel.service_id = service_id;
//                    companyMapFragment.mapViewModel.city_id = companiesDetailsViewModel.city_id;
//                    companyMapFragment.mapViewModel.category_id = companiesDetailsViewModel.category_id;
//                    companyMapFragment.mapViewModel.sub_category_id = companiesDetailsViewModel.sub_category_id;
//                    companyMapFragment.mapViewModel.callService();
                }
            }
        } catch (Exception ex) {
            Timber.e("Error:" + ex.getMessage());
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if (requestCode == Constants.CITY_RESULT && resultCode == Constants.CITY_RESULT) {
            companiesDetailsViewModel.city_id = data.getIntExtra(Constants.SELECTED, -1);
            companiesDetailsViewModel.callService();
        }
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            Timber.e("CallService");
            companiesDetailsViewModel.category_id = data.getIntExtra(Constants.CATEGORY_ID, -1);
            companiesDetailsViewModel.sub_category_id = data.getIntExtra(Constants.SUB_CATEGORY_ID, -1);
            companiesDetailsViewModel.callService();
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            UserHelper.saveKey(Constants.LAT, "" + data.getDoubleExtra("lat", 0));
            UserHelper.saveKey(Constants.LNG, "" + data.getDoubleExtra("lng", 0));
            Timber.e("lat:" + data.getDoubleExtra("lat", 0));
            Timber.e("lng:" + data.getDoubleExtra("lng", 0));
            ((BaseActivity) context).backActionBarView.showAddress();
            companiesDetailsViewModel.callService();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (companiesDetailsViewModel != null) {
            companiesDetailsViewModel.reset();
            UserHelper.saveKey(Constants.LAT, lat + "");
            UserHelper.saveKey(Constants.LNG, lng + "");
        }
    }

}
