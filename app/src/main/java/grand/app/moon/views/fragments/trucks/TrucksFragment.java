package grand.app.moon.views.fragments.trucks;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.DriverTrucksAdapter;
import grand.app.moon.adapter.ServiceCategoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentTrucksBinding;
import grand.app.moon.models.ads.AdsDetails;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.taxi.TruckModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.trucks.TrucksViewModel;
import grand.app.moon.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrucksFragment extends BaseFragment {
    public DriverTrucksAdapter driverTrucksAdapter;
    public ServiceCategoryAdapter serviceCategoryAdapter;
    public FragmentTrucksBinding fragmentTrucksBinding;
    public TrucksViewModel trucksViewModel;
    AdsDetails adsDetails;
    int service_id = -1;
    public List<Service> services;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentTrucksBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trucks, container, false);
        bind();
        setEvent();
        return fragmentTrucksBinding.getRoot();
    }

    private void bind() {
        trucksViewModel = new TrucksViewModel();
        AppUtils.initHorizontalRV(fragmentTrucksBinding.rvServices, fragmentTrucksBinding.rvServices.getContext(), 1);
        AppUtils.initVerticalRV(fragmentTrucksBinding.rvDrivers, fragmentTrucksBinding.rvDrivers.getContext(), 1);
        fragmentTrucksBinding.setTrucksViewModel(trucksViewModel);
    }



    private void setEvent() {
        trucksViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, trucksViewModel.getTrucksRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.TRUCKS)) {
                    setServices();
                    trucksViewModel.showPage(true);
                    if(trucksViewModel.getTrucksRepository().getTrucksResponse().truckModels.size() > 0) {
                        driverTrucksAdapter = new DriverTrucksAdapter();
                        driverTrucksAdapter.setList(trucksViewModel.getTrucksRepository().getTrucksResponse().truckModels.get(0).drivers);
                        fragmentTrucksBinding.rvDrivers.setAdapter(driverTrucksAdapter);
                        setEventAdapter();
                    }
                }
            }
        });
    }

    private void setServices() {
        services = new ArrayList<>();
        for(TruckModel truckModel : trucksViewModel.getTrucksRepository().getTrucksResponse().truckModels){
            Service service = new Service();
            service.mId = truckModel.id;
            service.mName = truckModel.name;
            service.mImage = truckModel.image;
            services.add(service);
        }
        serviceCategoryAdapter = new ServiceCategoryAdapter(services);
        fragmentTrucksBinding.rvServices.setAdapter(serviceCategoryAdapter);
    }

    private void setEventAdapter(){
        driverTrucksAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if(mutable != null && mutable.type.equals(Constants.SUBMIT)) {
                    int position = mutable.position;
//                Timber.e("pos:"+position);
//                driverTrucksAdapter.selected = -1;
//                service_id = adsDetails.mServices.get(position).mId;
//
//                Intent intent = new Intent(context, BaseActivity.class);
//                intent.putExtra(Constants.PAGE, Constants.ADS_COMPANY_FILTER);
//                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
//                Bundle bundle = new Bundle();
//                bundle.putString(Constants.FROM,Constants.ADD_ADS);
//                bundle.putInt(Constants.ID,service_id);
//                intent.putExtra(Constants.BUNDLE,bundle);
//                startActivityForResult(intent, Constants.FILTER_RESULT);

                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.MAP_BOOKING);

                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.DRIVER_ID, Integer.parseInt(trucksViewModel.getTrucksRepository().getTrucksResponse().truckModels.get(serviceCategoryAdapter.selected).drivers.get(position).id));
                    bundle.putInt(Constants.PRICE, Integer.parseInt(trucksViewModel.getTrucksRepository().getTrucksResponse().truckModels.get(serviceCategoryAdapter.selected).drivers.get(position).price));
                    bundle.putInt(Constants.START_COUNTER, trucksViewModel.getTrucksRepository().getTrucksResponse().truckModels.get(serviceCategoryAdapter.selected).start_counter);
                    bundle.putString(Constants.TYPE, Constants.TYPE_TRUCKS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.transporation));
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
            }
        });


        serviceCategoryAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                serviceCategoryAdapter.selected = position;
                driverTrucksAdapter.setList(trucksViewModel.getTrucksRepository().getTrucksResponse().truckModels.get(position).drivers);
                serviceCategoryAdapter.notifyDataSetChanged();
            }
        });
    }
}
