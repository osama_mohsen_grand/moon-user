package grand.app.moon.views.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.maputils.location.GPSAllowListener;
import grand.app.moon.utils.maputils.location.GPSLocation;
import grand.app.moon.utils.maputils.location.LocationListenerEvent;
import grand.app.moon.utils.maputils.location.MapActivityHelper;
import grand.app.moon.utils.maputils.permission.MapPermissionFragment;
import grand.app.moon.utils.storage.location.LocationHelper;
import grand.app.moon.R;
import grand.app.moon.databinding.FragmentGoHomeBinding;
import grand.app.moon.viewmodels.gohome.GoHomeViewModel;
import grand.app.moon.vollyutils.MyApplication;
import static android.app.Activity.RESULT_OK;

public class GoHomeFragment extends BaseFragment implements LocationListener {
    private View rootView;
    private FragmentGoHomeBinding fragmentGoHomeBinding;
    private GoHomeViewModel goHomeViewModel;
    public static double lat = 0, lng = 0;
    private static final String TAG = "ServiceMapFragment";
    private boolean async = false;
    public FusedLocationProviderClient fusedLocationClient;
    MapPermissionFragment mapPermission;
    MapActivityHelper mapActivityHelper;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentGoHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_go_home, container, false);
        LocationHelper.clear();
        if (fragmentGoHomeBinding != null) {
            bind(savedInstanceState);
            setEvent();
        }
        return rootView;
    }

    private void bind(Bundle savedInstanceState) {
        goHomeViewModel = new GoHomeViewModel();
        mapPermission = new MapPermissionFragment(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        fragmentGoHomeBinding.mapView.onCreate(savedInstanceState);
        fragmentGoHomeBinding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }

        fragmentGoHomeBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                goHomeViewModel.mapConfig = new MapConfig(context, mMap);
                mapActivityHelper = new MapActivityHelper(getActivity(), goHomeViewModel.mapConfig);
                goHomeViewModel.mapConfig.setMapStyle();//set style google map
                goHomeViewModel.mapConfig.setSettings();//add setting google map
                goHomeViewModel.mapConfig.checkLastLocation();
//                goHomeViewModel.mapConfig.setLocationButtonListeners();//enable button location listeners
                goHomeViewModel.mapConfig.getGoogleMap().setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        lat = goHomeViewModel.mapConfig.getGoogleMap().getCameraPosition().target.latitude;
                        lng = goHomeViewModel.mapConfig.getGoogleMap().getCameraPosition().target.longitude;
                    }
                });//addMarkerOnMap_
                if (!mapPermission.validLocationPermission()) {
                    mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST);
                } else {
//                    goHomeViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                    mapActivityHelper.startUpdateLocation(new LocationListenerEvent() {
                        @Override
                        public void update(LatLng latLng, Object object) {
                            async = false;
                            goHomeViewModel.mapConfig.saveLastLocation(latLng);
                        }
                    });
                    onResume();
                    enableMyLocationIfPermitted();
                }
            }
        });
        fragmentGoHomeBinding.setGoHomeViewModel(goHomeViewModel);
        rootView = fragmentGoHomeBinding.getRoot();
    }

    private void showAutoComplete() {
        if (!Places.isInitialized()) {
            Places.initialize(context, getString(R.string.google_direction_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(context);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    private void enableMyLocationIfPermitted() {
        if (!mapPermission.validLocationPermission()) {
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        } else if (goHomeViewModel.mapConfig != null && goHomeViewModel.mapConfig.getGoogleMap() != null) {
            allowGPS();
            onResume();
        }
    }

    @SuppressLint("MissingPermission")
    private void addCircleLocation(LatLng latLng) {
        if (!mapPermission.validLocationPermission()) {
            return;
        }
//        goHomeViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
        goHomeViewModel.mapConfig.moveCamera(latLng);
        onResume();
    }

    private void setEvent() {
        goHomeViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,goHomeViewModel.baseError);
                assert action != null;
                if(action.equals(Constants.DEST_SUBMIT)){
                    showAutoComplete();
                }else if(action.equals(Constants.GO_HOME)){
                    toastMessage("Request has been sent Successfully",R.drawable.ic_check_white,R.color.colorPrimary);
                    ((ParentActivity)context).finish();
                }
            }
        });

    }


    @SuppressLint("MissingPermission")
    public void allowGPS() {
        if (!GPSLocation.checkGps(context)) {
            GPSLocation.EnableGPSAutoMatically((ParentActivity) context, new GPSAllowListener() {
                @Override
                public void GPSStatus(boolean isOpen) {
                    if (isOpen) {
//                        goHomeViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                        updateLocation();
                    }
                }
            });

        } else {
            if (mapPermission.validLocationPermission()) {
                Location location = GPSLocation.getLocation((ParentActivity) context);
                if (location != null && !async) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    goHomeViewModel.mapConfig.saveLastLocation(latLng);
                    goHomeViewModel.mapConfig.moveCamera(latLng);
                    async = true;
                }
                updateLocation();
            }
        }
    }


    private LocationManager mLocationManager;


    @SuppressLint("MissingPermission")
    public void updateLocation() {
        Log.e(TAG, "updateLocation: ");
        if (!mapPermission.validLocationPermission()) {
            return;
        }
//        goHomeViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        Task<Location> task = fusedLocationClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                Log.e(TAG, "onSuccess: ");
                if (location != null && !async) {
                    //Write your implemenation here
                    Log.e(TAG, location.getLatitude() + " " + location.getLongitude());
                }
            }
        });
        onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mLocationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: Before super");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onRequestPermissionsResult: " + Constants.LOCATION_REQUEST);
        if (requestCode == Constants.LOCATION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "onRequestPermissionsResultDone: " + Constants.LOCATION_REQUEST);
                onResume();
                allowGPS();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE) {
            // Make sure the request was successful
            try{
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    if (goHomeViewModel.mMutableLiveData.getValue().equals(Constants.DEST_SUBMIT)) {
                        goHomeViewModel.setDest(place);
                    }
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        Log.e(TAG, "onLocationChanged: " + location.getLatitude() + "," + location.getLongitude());
        if (!async && location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            goHomeViewModel.mapConfig.saveLastLocation(latLng);
            goHomeViewModel.mapConfig.moveCamera(latLng);
            addCircleLocation(latLng);
            async = true;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (fragmentGoHomeBinding != null && fragmentGoHomeBinding.mapView != null)
            fragmentGoHomeBinding.mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fragmentGoHomeBinding != null && fragmentGoHomeBinding.mapView != null)
            fragmentGoHomeBinding.mapView.onPause();
    }

    @Override
    public void onDestroy() {

        if (fragmentGoHomeBinding != null && fragmentGoHomeBinding.mapView != null)
            if (goHomeViewModel != null) {
                goHomeViewModel.reset();
                fragmentGoHomeBinding.mapView.onDestroy();
            }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (fragmentGoHomeBinding.mapView != null) fragmentGoHomeBinding.mapView.onLowMemory();
    }
}
