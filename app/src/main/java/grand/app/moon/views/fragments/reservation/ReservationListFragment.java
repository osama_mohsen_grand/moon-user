package grand.app.moon.views.fragments.reservation;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ReservationAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentReservationListBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.reservation.ReservationOrder;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.reservation.ReservationListViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationListFragment extends BaseFragment {


    private FragmentReservationListBinding fragmentReservationListBinding;
    private ReservationListViewModel reservationListViewModel;
    public ReservationAdapter reservationAdapter;
    private ReservationMainFragment reservationMainFragment;
    String status = "";
    int reserved_type = 0;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReservationListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reservation_list, container, false);
        getData();
        bind();
        setEvent();
        return fragmentReservationListBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.STATUS)) {
            status = getArguments().getString(Constants.STATUS);
            reservationMainFragment = ((ReservationMainFragment) this.getParentFragment());
        }
        if (getArguments() != null && getArguments().containsKey(Constants.RESERVED_TYPE)) {
            reserved_type = getArguments().getInt(Constants.RESERVED_TYPE);
        }
        Timber.e("reserved_type:" + reserved_type);
    }


    //if  you want to get order recent or new  send status 0 else if you want to get previous order  send status 1
    private void bind() {
        reservationListViewModel = new ReservationListViewModel(status, reserved_type);
        AppUtils.initVerticalRV(fragmentReservationListBinding.rvReservation, fragmentReservationListBinding.rvReservation.getContext(), 1);
        fragmentReservationListBinding.setReservationListViewModel(reservationListViewModel);
    }

    private void setEvent() {
        reservationListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, reservationListViewModel.getReservationRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    reservationListViewModel.showPage(true);
                    if(reservationListViewModel.getReservationRepository().getReservationResponse().reservationOrders.size() > 0){
                        reservationAdapter = new ReservationAdapter(reservationListViewModel.getReservationRepository().getReservationResponse().reservationOrders, status);
                        fragmentReservationListBinding.rvReservation.setAdapter(reservationAdapter);
                        setEventAdapter();
                        reservationListViewModel.haveData();
                    }else{
                        reservationListViewModel.noData();
                    }
                } else if (action.equals(Constants.CONFIRM)) {
//                    ReservationOrder reservationOrder = reservationListViewModel.getReservationRepository().getReservationResponse().reservationOrders.get(reservationListViewModel.category_delete_position);
//                    if (reservationMainFragment != null)
//                        reservationMainFragment.moveToHistory(reservationOrder);
                    toastMessage(reservationListViewModel.getReservationRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    reservationAdapter.remove(reservationListViewModel.category_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        reservationAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.DELETE)) {
                    ReservationOrder reservationOrder = reservationListViewModel.getReservationRepository().getReservationResponse().reservationOrders.get(mutable.position);
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.oops))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_reservation))
                            .setActionText(ResourceManager.getString(R.string.remove))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    reservationListViewModel.category_delete_position = mutable.position;
                                    reservationListViewModel.reservationOrderActionRequest.order_id = reservationOrder.id;
                                    reservationListViewModel.reservationOrderActionRequest.status = 0;
                                    reservationListViewModel.getReservationRepository().reservationOrderAction(reservationListViewModel.reservationOrderActionRequest);
                                }
                            });
                } else if (mutable.type.equals(Constants.SUBMIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.RESERVATION_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.reservation_details));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, reservationListViewModel.getReservationRepository().getReservationResponse().reservationOrders.get(mutable.position).id);
                    bundle.putInt(Constants.TYPE, reserved_type);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }

}
