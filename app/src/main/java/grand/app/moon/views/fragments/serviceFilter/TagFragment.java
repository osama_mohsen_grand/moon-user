package grand.app.moon.views.fragments.serviceFilter;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.TagsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentTagBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.tags.TagsViewModel;

public class TagFragment extends BaseFragment {
    View rootView;
    private FragmentTagBinding fragmentTagBinding;
    private TagsViewModel tagsViewModel;
    private TagsAdapter tagsAdapter;
    public String type= "",service_id = "";
    private Service service;
    private ArrayList<Integer> selected = new ArrayList<>();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTagBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tag, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }


    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            this.service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.ID)) {
            service_id = getArguments().getString(Constants.ID);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.SELECTED)) {
            this.selected = getArguments().getIntegerArrayList(Constants.SELECTED);
        }
    }
    
    private void bind() {
        tagsViewModel = new TagsViewModel(service_id,service);
        AppUtils.initVerticalRV(fragmentTagBinding.rvTags, fragmentTagBinding.rvTags.getContext(), 1);
        fragmentTagBinding.setTagsViewModel(tagsViewModel);
        rootView = fragmentTagBinding.getRoot();
    }


    private void setEvent() {
        tagsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, tagsViewModel.getTagsRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.TAGS)) {
                    tagsAdapter = new TagsAdapter(tagsViewModel.getTagsRepository().getTagsResponse().data,selected);
                    fragmentTagBinding.rvTags.setAdapter(tagsAdapter);
                    setEventAdapter();
                }else if(action.equals(Constants.SUBMIT)){
                    Intent intent=new Intent();
                    intent.putExtra(Constants.SELECTED,selected);
                    ((ParentActivity)context).setResult(Constants.TAGS_RESULT,intent);
                    ((ParentActivity)context).finish();//finishing activity
                }
            }
        });
    }
    /*

     */

    private void setEventAdapter() {
        tagsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                IdNameImage tag = tagsViewModel.getTagsRepository().getTagsResponse().data.get(pos);
                int id = Integer.parseInt(tag.id);
                if(selected.contains(id)){
                    selected.remove(selected.indexOf(id));
                }else
                    selected.add(id);
//                Timber.e("selected:"+selected.toString());
                tagsAdapter.update(selected);
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        tagsViewModel.reset();

    }


}
