package grand.app.moon.views.fragments.cart;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.CartAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentCartBinding;
import grand.app.moon.databinding.FragmentNotificationsBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.cart.DeleteCartRequest;
import grand.app.moon.models.cart.Item;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.animation.MyBounceInterpolator;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.cart.CartViewModel;
import grand.app.moon.viewmodels.notification.NotificationViewModel;
import grand.app.moon.views.activities.BaseActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends BaseFragment {

    View rootView;
    private FragmentCartBinding fragmentCartBinding;
    private CartViewModel cartViewModel;
    CartAdapter cartAdapter;
    int action_id = 0;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentCartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        cartViewModel = new CartViewModel();
        fragmentCartBinding.setCartViewModel(cartViewModel);
        AppUtils.initVerticalRV(fragmentCartBinding.rvCart, fragmentCartBinding.rvCart.getContext(), 1);
        rootView = fragmentCartBinding.getRoot();
    }

    private static final String TAG = "CartFragment";
    private void setEvent() {
        cartViewModel.noData();
        cartViewModel.cartMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, cartViewModel.getCartRepository().getMessage());
                Log.d(TAG,"handleActions");
                if (action.equals(Constants.CART)) {
                    Log.d(TAG,"cart");
                    cartViewModel.showPage(true);
                    if (cartViewModel.getCartRepository().getCartResponse() != null
                            && cartViewModel.getCartRepository().getCartResponse().data != null
                            && cartViewModel.getCartRepository().getCartResponse().data.items != null) {
                        Log.d(TAG,"start here");

                        cartAdapter = new CartAdapter(cartViewModel.getCartRepository().getCartResponse().data.items);
                        fragmentCartBinding.rvCart.setAdapter(cartAdapter);
                        if(cartViewModel.getCartRepository().getCartResponse().data.items.size() > 0)
                            cartViewModel.haveData();
                        else
                            cartViewModel.noData();
                        setEventAdapter();
                    }else
                        cartViewModel.noData();
                    Log.d(TAG,"DONE");

                } else if (action.equals(Constants.DELETED)) {
                    cartViewModel.updateDeleteTotal(cartViewModel.getCartRepository().getCartResponse().data.items.get(action_id));
                    cartViewModel.getCartRepository().getCartResponse().data.items.remove(action_id);
                    cartAdapter.update(cartViewModel.getCartRepository().getCartResponse().data.items);
                    if (cartViewModel.getCartRepository().getCartResponse().data.items.size() == 0) {
//                        cartViewModel.showPage(false);
                        cartViewModel.noData();
                    }
                } else if (action.equals(Constants.SUCCESS)) {
                    toastMessage(cartViewModel.getCartRepository().getMessage());
                    Item item = cartViewModel.getCartRepository().getCartResponse().data.items.get(action_id);
                    cartViewModel.updateTotal(item);
                    item.qty = item.qty_tmp;
                    cartAdapter.update(cartViewModel.getCartRepository().getCartResponse().data.items);


                    final Animation myAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bounce);

                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);

                    fragmentCartBinding.tvDelivery.startAnimation(myAnim);
                    fragmentCartBinding.tvSubTotal.startAnimation(myAnim);
                    fragmentCartBinding.tvTotal.startAnimation(myAnim);

                } else if (action.equals(Constants.SUBMIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.CHECKOUT);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.HAS_DELEGATE, cartViewModel.getCartRepository().getCartResponse().data.hasDelegate);
                    bundle.putString(Constants.TOTAL, "" + cartViewModel.getCartRepository().getCartResponse().data.totalPrice);
                    bundle.putString(Constants.SUB_TOTAL, "" + cartViewModel.getCartRepository().getCartResponse().data.subTotal);
                    bundle.putString(Constants.DELIVERY, "" + cartViewModel.getCartRepository().getCartResponse().data.delivery);
                    bundle.putInt(Constants.PAYMENT_TYPE,cartViewModel.getCartRepository().getCartResponse().data.payment_type);

                    if (cartViewModel.getCartRepository().getCartResponse().data.items.size() > 0) {
                        bundle.putInt(Constants.TYPE, cartViewModel.getCartRepository().getCartResponse().data.items.get(0).type);
                        bundle.putInt(Constants.FLAG, cartViewModel.getCartRepository().getCartResponse().data.items.get(0).flag);
                    }
                    bundle.putSerializable(Constants.CART,cartViewModel.getCartRepository().getCartResponse().data);

                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.checkout));
                    startActivity(intent);
                }
            }
        });
    }

    private void setEventAdapter() {
        cartAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                Item item = cartViewModel.getCartRepository().getCartResponse().data.items.get(mutable.position);
                action_id = mutable.position;
                if (mutable.type.equals(Constants.ADD)) {
                    cartViewModel.update(item);
                } else if (mutable.type.equals(Constants.MINUS)) {
                    cartViewModel.update(item);
                } else if (mutable.type.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.delete_cart))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_product))
                            .setActionText(ResourceManager.getString(R.string.continue_))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    cartViewModel.getCartRepository().deleteCart(new DeleteCartRequest(Integer.parseInt(item.id), 2));
                                }
                            });


                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cartViewModel != null) cartViewModel.reset();
    }

}
