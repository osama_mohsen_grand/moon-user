package grand.app.moon.views.fragments.service;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ServiceCategoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentServiceDetailsBinding;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.app.AppMoonServices;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ServiceListResponse;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.location.LocationLatLng;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.viewmodels.service.ServiceDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.cityRegion.CityRegionFragment;
import grand.app.moon.views.fragments.gallery.DiscoverMainFragment;
import grand.app.moon.views.fragments.institution.InstitutionShopFragment;
import grand.app.moon.views.fragments.reservation.ClinicListFragment;
import timber.log.Timber;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceDetailsFragment extends BaseFragment {

    private FragmentServiceDetailsBinding fragmentServiceDetailsBinding;
    private ServiceDetailsViewModel viewmodel;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    public ServiceCategoryAdapter serviceCategoryAdapter;
    public ArrayList<Integer> tags = new ArrayList<>();
    public int filter = 1;
    boolean setServiceCategory = false;
    SwapAdapter adapter = null;
    public String service_id = "";
    List<Service> services = null;
    Service service;
    public int type = -1;
    LocationLatLng locationLatLng;
    double lat, lng;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentServiceDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_details, container, false);
        new Handler().postDelayed(() -> {
            locationLatLng = new LocationLatLng(getActivityBase(), ServiceDetailsFragment.this);
            lat = UserHelper.getLat();
            lng = UserHelper.getLng();
            Timber.e("lat:" + lat);
            Timber.e("lng:" + lng);
            getData();
            bind();
            setEvent();
            Log.d(TAG, service.mType + "");
        }, 100);
        return fragmentServiceDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
            ((BaseActivity) getActivityBase()).backActionBarView.flag = service.mFlag;
            ((BaseActivity) getActivityBase()).backActionBarView.type = service.mType;
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private void bind() {
        viewmodel = new ServiceDetailsViewModel(service);
        serviceCategoryAdapter = new ServiceCategoryAdapter(new ArrayList<>());
        setTabs();
        AppUtils.initHorizontalRV(fragmentServiceDetailsBinding.rvServiceCategories, fragmentServiceDetailsBinding.rvServiceCategories.getContext(), 1);
        fragmentServiceDetailsBinding.setServiceDetailsViewModel(viewmodel);
        setSearchAction();
    }

    private void setSearchAction() {
        ((BaseActivity) getActivityBase()).backActionBarView.layoutActionBarBackBinding.imgBaseBarSearch.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.SEARCH);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.FLAG, service.mFlag);
            bundle.putInt(Constants.TYPE, service.mType);
            bundle.putSerializable(Constants.SERVICE, service);
            bundle.putInt(Constants.SERVICE_ID, Integer.parseInt(service_id));
            intent.putExtra(Constants.BUNDLE, bundle);
            startActivity(intent);
        });
    }


    public void setTabs() {
//        fragmentServiceDetailsBinding.viewpager.setAdapter(null);
//        fragmentServiceDetailsBinding.slidingTabs.setupWithViewPager(null);
        if (service.mType == Integer.parseInt(Constants.TYPE_COMPANIES) || service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING)) {
            tabModels = AppMoonServices.getTabShopModelsWithoutVoucher(getArguments());
            viewmodel.callService();
        } else if (service.mType == Integer.parseInt(Constants.TYPE_INSTITUTIONS)) {
            tabModels = AppMoonServices.getTabShopModelsWithoutVoucher(getArguments());
            viewmodel.callService();
        } else if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC)) {
            tabModels = AppMoonServices.getTabShopModelsWithoutVoucher(getArguments());
            viewmodel.callService();
        } else if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
            tabModels = AppMoonServices.getTabShopModelsWithoutVoucher(getArguments());
            viewmodel.callService();
//            intent.putExtra(Constants.PAGE, Constants.COMPANIES);
//            bundle.putSerializable(Constants.SERVICE, service);
        } else {
            tabModels = AppMoonServices.getTabShopModels(getArguments());
            viewmodel.callService();
        }
        adapter = new SwapAdapter(getChildFragmentManager(), tabModels);
        fragmentServiceDetailsBinding.viewpager.setAdapter(adapter);
        fragmentServiceDetailsBinding.viewpager.setOffscreenPageLimit(tabModels.size());
        fragmentServiceDetailsBinding.slidingTabs.setupWithViewPager(fragmentServiceDetailsBinding.viewpager);
        adapter.notifyDataSetChanged();
    }

    private void setEvent() {
        viewmodel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, viewmodel.homeRepository.getMessage());
//                handleActions(action, serviceDetailsViewModel.getDiscoverRepository().getMessage());
                if (action.equals(Constants.SUCCESS)) {
                    viewmodel.response = viewmodel.homeRepository.getServiceDetailsResponse().mData;
                    updateUI();
                    setService(viewmodel.response.mServices);
                }
                if (action.equals(Constants.TAGS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.TAGS);
                    Bundle bundle = getArguments();
                    assert bundle != null;
                    bundle.putString(Constants.ID, service_id);
                    bundle.putIntegerArrayList(Constants.SELECTED, tags);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.tags));
                    startActivityForResult(intent, Constants.TAGS_RESULT);
                }
                if (action.equals(Constants.CITY)) {
                    Intent intent = new Intent(requireContext(), BaseActivity.class);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.city));
                    intent.putExtra(Constants.PAGE, CityRegionFragment.class.getName());
                    startActivityForResult(intent, Constants.CITY_REGION_REQUEST);
                }
                if (action.equals(Constants.DISCOVER)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, DiscoverMainFragment.class.getName());
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.discover));
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.SERVICES, viewmodel.serviceListResponse);
                    bundle.putBoolean(Constants.OFFLINE, true);
                    bundle.putSerializable(Constants.SERVICE, service);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
                if (action.equals(Constants.FILTER)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.FILTER);
                    Bundle bundle = getArguments();
                    assert bundle != null;
                    bundle.putInt(Constants.SELECTED, filter);
                    bundle.putSerializable(Constants.SERVICE, service);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.filter));
                    startActivityForResult(intent, Constants.FILTER_RESULT);
                }
            }
        });

    }

    private void updateUI() {
        if (viewmodel.isOneService) {
            if (AppMoon.haveVoucher(service.mType)) {
                for (int i = 0; i < 4; i++) {
                    if (i < 3) {
                        ServiceShopFragment serviceShopFragment = (ServiceShopFragment) adapter.getItem(i);
                        if (serviceShopFragment != null) {
                            serviceShopFragment.viewModel.filter = filter;
                            serviceShopFragment.viewModel.tags = tags;
                            serviceShopFragment.viewModel.service_id = service_id;
                            serviceShopFragment.viewModel.updateView(viewmodel.response.mShops);
                        }
                    } else {
                        ServiceMapFragment serviceMapFragment = (ServiceMapFragment) adapter.getItem(i);
                        serviceMapFragment.mapViewModel.filter = filter;
                        serviceMapFragment.mapViewModel.tags = tags;
                        serviceMapFragment.mapViewModel.service_id = service_id;
                        serviceMapFragment.mapViewModel.updateView(viewmodel.response.mShops);
                    }
                }
            } else {
                for (int i = 0; i < 3; i++) {
                    if (i < 2) {
                        ServiceShopFragment serviceShopFragment = (ServiceShopFragment) adapter.getItem(i);
                        if (serviceShopFragment != null) {
                            serviceShopFragment.viewModel.filter = filter;
                            serviceShopFragment.viewModel.tags = tags;
                            serviceShopFragment.viewModel.service_id = service_id;
                            serviceShopFragment.viewModel.updateView(viewmodel.response.mShops);
                        }
                    } else {
                        ServiceMapFragment serviceMapFragment = (ServiceMapFragment) adapter.getItem(i);
                        serviceMapFragment.mapViewModel.filter = filter;
                        serviceMapFragment.mapViewModel.tags = tags;
                        serviceMapFragment.mapViewModel.service_id = service_id;
                        serviceMapFragment.mapViewModel.updateView(viewmodel.response.mShops);
                    }
                }
            }
        }
    }

    public void setServiceId(int sv_id) {
        service_id = sv_id + "";
        viewmodel.service_id = sv_id;
        ((BaseActivity) getActivityBase()).backActionBarView.service_id = Integer.parseInt(service_id);
    }

    public void setService(List<Service> services) {
        this.services = services;
        if (!setServiceCategory) {
            serviceCategoryAdapter = new ServiceCategoryAdapter(services);
            viewmodel.serviceListResponse = new ServiceListResponse(services);
            fragmentServiceDetailsBinding.rvServiceCategories.setAdapter(serviceCategoryAdapter);
            setServiceCategory = true;
            if (services.size() > 0) {
                setServiceId(services.get(0).mId);
            }
            setEventAdapter();
        } else {
            serviceCategoryAdapter.update(services);
        }
    }

    private void setEventAdapter() {
        serviceCategoryAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                serviceCategoryAdapter.selected = position;
                setServiceId(services.get(position).mId);
                serviceCategoryAdapter.notifyDataSetChanged();
                viewmodel.callService();
                resetAdapter();
                viewmodel.reset();

            }

        });
    }

    private static final String TAG = "ServiceDetailsFragment";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Log.d(TAG, "here");
        if (requestCode == Constants.TAGS_RESULT && resultCode == Constants.TAGS_RESULT) {
            this.tags = data.getIntegerArrayListExtra(Constants.SELECTED);
            viewmodel.tags = this.tags;
            resetAdapter();
            viewmodel.callService();
        }
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            this.filter = data.getIntExtra(Constants.FILTER, 1);
            viewmodel.filter = this.filter;
            Log.e(TAG, "onActivityResult:FILTER_RESULT ");
            resetAdapter();
            viewmodel.callService();
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            Log.e(TAG, "onActivityResult: ");
            UserHelper.saveKey(Constants.LAT, "" + data.getDoubleExtra("lat", 0));
            UserHelper.saveKey(Constants.LNG, "" + data.getDoubleExtra("lng", 0));
            Timber.e("lat:" + data.getDoubleExtra("lat", 0));
            Timber.e("lng:" + data.getDoubleExtra("lng", 0));
            ((BaseActivity) context).backActionBarView.showAddress();
            resetAdapter();
            viewmodel.callService();
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.CITY_REGION_REQUEST) {
            if (data.hasExtra(Constants.BUNDLE)) {
                Bundle bundle = data.getBundleExtra(Constants.BUNDLE);
                viewmodel.city_id = -1;
                viewmodel.region_id = -1;
                if (bundle.containsKey(Constants.CITY_ID))
                    viewmodel.city_id = bundle.getInt(Constants.CITY_ID);
                if (bundle.containsKey(Constants.REGION_ID))
                    viewmodel.region_id = bundle.getInt(Constants.REGION_ID);
                viewmodel.callService();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void resetAdapter() {
//        Log.e(TAG, "resetAdapter: ");
//        adapter = (SwapAdapter) fragmentServiceDetailsBinding.viewpager.getAdapter();
//        try {
//            if (service.mType == Integer.parseInt(Constants.TYPE_INSTITUTIONS)) {
//                for (int i = 0; i < 3; i++) {
//                    if (i < 2) {
//                        InstitutionShopFragment institutionShopFragment = (InstitutionShopFragment) adapter.getItem(i);
//                        if (institutionShopFragment != null) {
//                            institutionShopFragment.institutionViewModel.filter = filter;
//                            institutionShopFragment.institutionViewModel.tags = tags;
//                            institutionShopFragment.institutionViewModel.service_id = service_id;
//                            institutionShopFragment.institutionViewModel.callService();
//                        }
//                    } else {
//                        ServiceMapFragment serviceMapFragment = (ServiceMapFragment) adapter.getItem(i);
//                        serviceMapFragment.mapViewModel.filter = filter;
//                        serviceMapFragment.mapViewModel.tags = tags;
//                        serviceMapFragment.mapViewModel.service_id = service_id;
//                        serviceMapFragment.mapViewModel.callService();
//                    }
//                }
//            } else if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) || service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
//                for (int i = 0; i < 3; i++) {
//                    if (i < 2) {
//                        ClinicListFragment clinicListFragment = (ClinicListFragment) adapter.getItem(i);
//                        if (clinicListFragment != null) {
//                            clinicListFragment.clinicListViewModel.filter = filter;
//                            clinicListFragment.clinicListViewModel.tags = tags;
//                            clinicListFragment.clinicListViewModel.service_id = service_id;
//                            clinicListFragment.clinicListViewModel.callService();
//                        }
//                    } else {
//                        ServiceMapFragment serviceMapFragment = (ServiceMapFragment) adapter.getItem(i);
//                        serviceMapFragment.mapViewModel.filter = filter;
//                        serviceMapFragment.mapViewModel.tags = tags;
//                        serviceMapFragment.mapViewModel.service_id = service_id;
//                        serviceMapFragment.mapViewModel.callService();
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            Timber.e("Error:" + ex.getMessage());
//        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewmodel != null) {
            viewmodel.reset();
            UserHelper.saveKey(Constants.LAT, lat + "");
            UserHelper.saveKey(Constants.LNG, lng + "");
        }
    }

}
