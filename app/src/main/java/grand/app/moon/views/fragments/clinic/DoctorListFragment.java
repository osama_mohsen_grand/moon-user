package grand.app.moon.views.fragments.clinic;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.DoctorsAdapter;
import grand.app.moon.adapter.FilterAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentDoctorListBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.BottomSheetDialogHelper;
import grand.app.moon.viewmodels.clinic.DoctorListViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.reservation.ClinicDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
//TODO DELETE OSAMA DOCTOR
public class DoctorListFragment extends BaseFragment {

    private FragmentDoctorListBinding fragmentDoctorListBinding;
    private DoctorListViewModel doctorListViewModel;
    private DoctorsAdapter doctorsAdapter = new DoctorsAdapter(new ArrayList<>());
    ClinicDetailsResponse clinicDetailsResponse;
    BottomSheetDialogHelper bottomSheetDialogHelper;
    ClinicDetailsFragment clinicDetailsFragment;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentDoctorListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_list, container, false);
        Log.d(TAG,"DoctorListFragment");
        doctorListViewModel = new DoctorListViewModel();
        clinicDetailsFragment = ((ClinicDetailsFragment) this.getParentFragment());
        getData();
        bind();
        setEvent();
        return fragmentDoctorListBinding.getRoot();
    }

    private static final String TAG = "DoctorListFragment";

    private void getData() {
        AppUtils.initVerticalRV(fragmentDoctorListBinding.rvDoctors, fragmentDoctorListBinding.rvDoctors.getContext(), 1);
        fragmentDoctorListBinding.rvDoctors.setAdapter(doctorsAdapter);
        setEventAdapter();
        if (getArguments() != null && getArguments().containsKey(Constants.CLINIC_DETAILS)) {
            clinicDetailsResponse = (ClinicDetailsResponse) getArguments().getSerializable(Constants.CLINIC_DETAILS);
            Log.e(TAG, "getData: "+clinicDetailsResponse.data.doctorsList.size() );
            if (clinicDetailsResponse != null) {
                doctorsAdapter.update(clinicDetailsFragment.clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.doctorsList);
            }
        }
    }

    private void setEvent() {
        doctorListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.FILTER)) {
                    if (bottomSheetDialogHelper.filterDialog == null) {
                        bottomSheetDialogHelper.filterFamousShopAds(clinicDetailsResponse.data.specialist, false, (dialog, type, object) -> {
                            if (type.equals(Constants.ARRAY)) {
                                ArrayList<Integer> list = (ArrayList<Integer>) object;
                                if (list != null && list.size() > 0) {
                                    clinicDetailsFragment.clinicDetailsViewModel.specialist_id = list.get(0);
                                    clinicDetailsFragment.clinicDetailsViewModel.callService();
                                }
                            }
                        });
                    } else {
                        bottomSheetDialogHelper.filterDialog.show();
                    }
                }
            }
        });
    }

    private void bind() {
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivityBase());
        fragmentDoctorListBinding.setDoctorListViewModel(doctorListViewModel);
    }

    private void setEventAdapter() {
        doctorsAdapter.mMutableLiveDataAdapter.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Log.d(TAG,"here change");
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.BOOK_CLINIC)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.BOOK_CLINIC);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.book_details));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, clinicDetailsResponse.data.doctorsList.get(mutable.position).id);
                    bundle.putSerializable(Constants.CLINIC_DETAILS,clinicDetailsResponse.data);
                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC));
                    bundle.putInt(Constants.PAYMENT_TYPE,clinicDetailsFragment.clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.payment_type);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        doctorListViewModel.reset();
    }
}
