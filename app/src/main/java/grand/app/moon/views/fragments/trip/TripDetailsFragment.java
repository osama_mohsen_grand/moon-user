package grand.app.moon.views.fragments.trip;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentTripDetailsBinding;
import grand.app.moon.databinding.FragmentTruckPickupFormBinding;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.models.triphistory.TripHistoryResponse;
import grand.app.moon.models.truck.TruckRequest;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.viewmodels.trip.TripDetailsViewModel;
import grand.app.moon.viewmodels.trucks.TrucksPickupFormViewModel;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.TrackActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class TripDetailsFragment extends BaseFragment {
    View rootView;
    private FragmentTripDetailsBinding fragmentTripDetailsBinding;
    private TripDetailsViewModel tripDetailsViewModel;
    private TripHistoryResponse.Datum trip;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentTripDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trip_details, container, false);
        getData();
        bind();
        setEvents();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.TRIP)){
            trip = (TripHistoryResponse.Datum) getArguments().getSerializable(Constants.TRIP);
        }
    }

    private void bind() {
        tripDetailsViewModel = new TripDetailsViewModel(trip);
        fragmentTripDetailsBinding.setTripDetailsViewModel(tripDetailsViewModel);
        rootView = fragmentTripDetailsBinding.getRoot();
    }


    private void setEvents() {
        tripDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            assert action != null;
            if (action.equals(Constants.TRIP_DETAILS_MAP)) {
                Intent intent = new Intent(context,TrackActivity.class);
                intent.putExtra(Constants.TRIP_ID,trip.id);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tripDetailsViewModel != null) tripDetailsViewModel.reset();
    }
}
