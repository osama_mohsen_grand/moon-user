package grand.app.moon.views.fragments.intro;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import grand.app.moon.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Slide2Fragment extends Fragment {


    public Slide2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_slide2, container, false);
    }

}
