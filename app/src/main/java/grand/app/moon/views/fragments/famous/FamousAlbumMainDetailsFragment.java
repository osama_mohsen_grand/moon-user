package grand.app.moon.views.fragments.famous;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AlbumAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFamousAlbumMainDetailsBinding;
import grand.app.moon.models.ads.companyAdsCategory.Datum;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.viewmodels.famous.FamousAlbumMainDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousAlbumMainDetailsFragment extends BaseFragment {


    private FragmentFamousAlbumMainDetailsBinding fragmentFamousAlbumDetailsBinding;
    private FamousAlbumMainDetailsViewModel famousAlbumMainDetailsViewModel;
    public int id = -1,flag=1,shop_id=-1;
    public String type = "", tab = "";
    public boolean ads = false;
//    private AlbumImagesAdapter albumImagesAdapter;
    private AlbumAdapter albumAdapter;
    private ArrayList<ImageVideo> data = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFamousAlbumDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_album_main_details, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousAlbumDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.FLAG))
            flag = getArguments().getInt(Constants.FLAG);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getString(Constants.TYPE);
        if (getArguments() != null && getArguments().containsKey(Constants.ADS))
            ads = getArguments().getBoolean(Constants.ADS);
        if (getArguments() != null && getArguments().containsKey(Constants.TAB))
            tab = getArguments().getString(Constants.TAB);
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_ID))
            shop_id = getArguments().getInt(Constants.SHOP_ID);
        if (getArguments() != null && getArguments().containsKey(Constants.ALBUM_ADS)) {
            Datum datum = (Datum) getArguments().getSerializable(Constants.ALBUM_ADS);
            data = new ArrayList<>(datum.ads);
        }
    }

    private void bind() {
        AppUtils.initVerticalRV(fragmentFamousAlbumDetailsBinding.rvFamousAlbumDetails, fragmentFamousAlbumDetailsBinding.rvFamousAlbumDetails.getContext(), 3);
        fragmentFamousAlbumDetailsBinding.setFamousAlbumMainDetailsViewModel(famousAlbumMainDetailsViewModel);
        if(data == null) {
            famousAlbumMainDetailsViewModel = new FamousAlbumMainDetailsViewModel(id,shop_id, type, tab,ads);
        } else {
            famousAlbumMainDetailsViewModel = new FamousAlbumMainDetailsViewModel();
            setAdapter();
        }
    }

    private void setEvent() {
        famousAlbumMainDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAlbumMainDetailsViewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    data = new ArrayList<>(famousAlbumMainDetailsViewModel.getFamousRepository().getFamousAlbumImagesResponse().data);
                    setAdapter();
                }
            }
        });
    }

    public void setAdapter(){
        albumAdapter = new AlbumAdapter(data,true);
        if (getArguments() != null && getArguments().containsKey(Constants.CONTENT))
            albumAdapter.content = getArguments().getBoolean(Constants.CONTENT);
        albumAdapter.flag = flag;
        fragmentFamousAlbumDetailsBinding.rvFamousAlbumDetails.setAdapter(albumAdapter);
//        setEventAdapter();
    }


//    private void setEventAdapter() {
//        albumAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
//            @Override
//            public void onChanged(@Nullable Object o) {
//                Mutable mutable = (Mutable) o;
//                if (mutable.type.equals(Constants.IMAGE)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
//                    intent.putExtra(Constants.NAME_BAR, data.get(mutable.position).name);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Constants.IMAGE, data.get(mutable.position).image);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivityForResult(intent, Constants.RELOAD_RESULT);
//                } else if (mutable.type.equals(Constants.VIDEO)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
//                    intent.putExtra(Constants.NAME_BAR, data.get(mutable.position).name);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Constants.VIDEO, data.get(mutable.position).image);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivity(intent);
//                }
//            }
//        });
//    }
}
