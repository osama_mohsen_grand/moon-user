package grand.app.moon.views.fragments.shop;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentShopDetailsInfoBinding;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.viewmodels.shop.ShopDetailsInfoViewModel;
import grand.app.moon.views.fragments.branch.BranchesAccountInfoFragment;
import grand.app.moon.views.fragments.famous.FamousAlbumsUserDetails;
import grand.app.moon.views.fragments.profile.InfoServiceFragment;
import grand.app.moon.views.fragments.profile.ProfileSocialFragment;
import grand.app.moon.views.fragments.profile.TransferGuideFragment;

//ShopDetailsInfoViewModel
public class ShopDetailsInfoFragment extends BaseFragment {
    private FragmentShopDetailsInfoBinding binding;
    public ShopDetailsInfoViewModel viewModel;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    public int id = -1;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_details_info, container, false);
        Log.d(TAG,"doner here");
        getData();
        bind();
        setEvent();
        return binding.getRoot();
    }

    private static final String TAG = "ShopDetailsInfoFragment";

    private void getData() {
        Log.d(TAG,"here");
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            id = getArguments().getInt(Constants.ID);
            viewModel = new ShopDetailsInfoViewModel(id);
            Log.d(TAG, "done");
        }
    }


    private void bind() {
        binding.setViewModel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.ACCOUNT_INFO)) {
                    viewModel.response = viewModel.getRepository().accountInfoResponse;
                    viewModel.showPage(true);
                    viewModel.notifyChange();
                    setTabs();
                }
            }
        });
    }

    public void setTabs() {

       if (viewModel.response.gallery.size() > 0) {
            FamousAlbumsUserDetails galleryFragment = new FamousAlbumsUserDetails();
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable(Constants.SHOP_DETAILS, viewModel.response);
           bundle1.putString(Constants.KIND, Constants.GALLERY);
           bundle1.putInt(Constants.FLAG, 1);
           galleryFragment.setArguments(bundle1);
            tabModels.add(new TabModel(getString(R.string.album), galleryFragment));
        }

        if (viewModel.response.famous.size() > 0) {
            FamousAlbumsUserDetails famousAlbumsUserDetails = new FamousAlbumsUserDetails();
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable(Constants.SHOP_DETAILS, viewModel.response);
            bundle1.putString(Constants.KIND, Constants.FAMOUS);
            bundle1.putInt(Constants.ID, id);
            bundle1.putInt(Constants.FLAG, 1);
            bundle1.putBoolean(Constants.ADS,true);
            famousAlbumsUserDetails.setArguments(bundle1);
            tabModels.add(new TabModel(getString(R.string.famous), famousAlbumsUserDetails));
        }

        if (viewModel.response.photographer.size() > 0) {
            FamousAlbumsUserDetails photographersUserDetails = new FamousAlbumsUserDetails();
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable(Constants.SHOP_DETAILS, viewModel.response);
            bundle1.putString(Constants.KIND, Constants.PHOTOGRAPHER);
            bundle1.putInt(Constants.FLAG, 1);
            bundle1.putInt(Constants.ID, id);
            bundle1.putBoolean(Constants.ADS,true);
            photographersUserDetails.setArguments(bundle1);
            tabModels.add(new TabModel(getString(R.string.photographer), photographersUserDetails));
        }


        ShopDetailsMapFragment shopDetailsMapFragment = new ShopDetailsMapFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.ACCOUNT_INFO, viewModel.response);//mean album
        shopDetailsMapFragment.setArguments(bundle);
        tabModels.add(new TabModel(getString(R.string.map), shopDetailsMapFragment));
//
//        Log.e(TAG,"BranchesAccountInfoFragment");
//
        ProfileSocialFragment profileSocialFragment = new ProfileSocialFragment();
        profileSocialFragment.setArguments(bundle);
        tabModels.add(new TabModel(getString(R.string.profile), profileSocialFragment));

        InfoServiceFragment infoServiceFragment = new InfoServiceFragment();
        infoServiceFragment.setArguments(bundle);
        tabModels.add(new TabModel(getString(R.string.info_and_service), infoServiceFragment));

        //
        TransferGuideFragment transferGuideFragment = new TransferGuideFragment();
        transferGuideFragment.setArguments(bundle);
        tabModels.add(new TabModel(getString(R.string.transfer_guide), transferGuideFragment));

        BranchesAccountInfoFragment branchesAccountInfoFragment = new BranchesAccountInfoFragment();
        branchesAccountInfoFragment.setArguments(bundle);
        tabModels.add(new TabModel(getString(R.string.branches), branchesAccountInfoFragment));




        SwapAdapter adapter = new SwapAdapter(getChildFragmentManager(), tabModels);
        binding.viewpager.setAdapter(adapter);
        binding.viewpager.setOffscreenPageLimit(tabModels.size());
        binding.slidingTabs.setupWithViewPager(binding.viewpager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.reset();

    }
}