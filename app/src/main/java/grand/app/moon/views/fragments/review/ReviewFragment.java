package grand.app.moon.views.fragments.review;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ReviewAdapter;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFilterBinding;
import grand.app.moon.databinding.FragmentReviewBinding;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.review.ReviewViewModel;
import grand.app.moon.viewmodels.settings.FilterViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

public class ReviewFragment extends BaseFragment {
    View rootView;
    private FragmentReviewBinding fragmentReviewBinding;
    private ReviewViewModel reviewViewModel;
    int shop_id = -1;
    public int type = 0;
    ReviewAdapter reviewAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReviewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_review, container, false);
        UserHelper.saveKey(Constants.RELOAD,Constants.FALSE);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            shop_id = getArguments().getInt(Constants.ID);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)) { // shop_id
            type = getArguments().getInt(Constants.TYPE);
        }
    }

    private void bind() {
        reviewViewModel = new ReviewViewModel(shop_id,type);
        AppUtils.initVerticalRV(fragmentReviewBinding.rvReview, fragmentReviewBinding.rvReview.getContext(), 1);
        fragmentReviewBinding.setReviewViewModel(reviewViewModel);
        rootView = fragmentReviewBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(UserHelper.retrieveKey(Constants.RELOAD).equals(Constants.TRUE) && reviewViewModel != null)
            reviewViewModel.callService();
    }

    private void setEvent() {
        reviewViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, reviewViewModel.getReviewRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.REVIEW)) {
                    reviewViewModel.showPage(true);
                    if(reviewViewModel.getReviewRepository().getReviewResponse().mData.size() == 0){
                        reviewViewModel.noData();
                    }else {
                        reviewAdapter = new ReviewAdapter(reviewViewModel.getReviewRepository().getReviewResponse().mData);
                        fragmentReviewBinding.rvReview.setAdapter(reviewAdapter);
                    }
                }else if(action.equals(Constants.ADD_REVIEW)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.ADD_REVIEW);
                    intent.putExtra(Constants.BUNDLE,getArguments());
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_review));
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        reviewViewModel.reset();

    }


}
