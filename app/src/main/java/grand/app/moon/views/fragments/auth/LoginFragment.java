package grand.app.moon.views.fragments.auth;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.facebook.FacebookModel;
import grand.app.moon.customviews.facebook.FacebookResponseInterface;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.repository.FirebaseRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.user.LoginViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.R;
import grand.app.moon.databinding.FragmentLoginBinding;
import grand.app.moon.vollyutils.AppHelper;
import grand.app.moon.vollyutils.MyApplication;
import grand.app.moon.vollyutils.URLS;

import static grand.app.moon.utils.Constants.RC_SIGN_IN;

public class LoginFragment extends BaseFragment {
    View rootView;
    private FragmentLoginBinding fragmentLoginBinding;
    private LoginViewModel loginViewModel;
    GoogleSignInClient mGoogleSignInClient;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        bind();
        setEvent();
        setupGoogleLogin();
        setupFacebookLogin();
        return rootView;
    }

    private void bind() {
        loginViewModel = new LoginViewModel();
        loginViewModel.callbackManager = CallbackManager.Factory.create();
        fragmentLoginBinding.setLoginViewModel(loginViewModel);
        rootView = fragmentLoginBinding.getRoot();
    }

    FirebaseRepository firebaseRepository = new FirebaseRepository();
    private void setupFacebookLogin() {

        try {
            PackageInfo info = requireActivity().getPackageManager().getPackageInfo(requireActivity().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                firebaseRepository.logCrash(hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "printHashKey()", e);
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }

        LoginManager.getInstance().logOut();
        fragmentLoginBinding.btnFacebook.setPermissions(Arrays.asList("email", "public_profile"));
        loginViewModel.btnFacebook = fragmentLoginBinding.btnFacebook;
        loginViewModel.setupFacebookSignIn();
    }

    private void setupGoogleLogin() {
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();


        mGoogleSignInClient = GoogleSignIn.getClient(getActivityBase(), signInOptions);
    }


    private void setEvent() {
        loginViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Intent intent = new Intent(getActivity(), BaseActivity.class);
                Bundle bundle = new Bundle();
                String action = (String) o;
                handleActions(action, loginViewModel.getLoginRepository().getMessage());
                assert action != null;
                Log.e("action", action);
                if (action.equals(Constants.HOME)) {
                    ((AppCompatActivity) context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                } else if (action.equals(Constants.TRIP)) {
                    ((AppCompatActivity) context).finishAffinity();
//                    intent = new Intent(getActivity(), TrackActivity.class);
                    intent.putExtra(Constants.TRIP_ID, String.valueOf(UserHelper.getTripDetails().tripId));
                    startActivity(intent);
                } else if (action.equals(Constants.FORGET_PASSWORD)) {
                    intent.putExtra(Constants.PAGE, Constants.PHONE_VERIFICATION);
                    bundle.putString(Constants.NAME, ResourceManager.getString(R.string.label_forget_password));
                    bundle.putString(Constants.TYPE, Constants.FORGET_PASSWORD);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (action.equals(Constants.REGISTRATION)) {
                    intent.putExtra(Constants.PAGE, Constants.PHONE_VERIFICATION);
                    bundle.putString(Constants.TYPE,Constants.REGISTRATION);
                    startActivity(intent);
                } else if (action.equals(Constants.UNAUTHORIZED)) {
                    bundle.putSerializable(Constants.FACEBOOK, loginViewModel.facebookModel);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.PAGE, Constants.PHONE_VERIFICATION);
                    startActivity(intent);
                } else if (action.equals(Constants.FACEBOOK)) {
                    fragmentLoginBinding.btnFacebook.performClick();
                } else if (action.equals(Constants.GOOGLE)) {
                    intent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(intent, RC_SIGN_IN);
                }
            }
        });
    }

    private static final String TAG = "LoginFragment";

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        loginViewModel.callbackManager.onActivityResult(requestCode, resultCode, data);//facebook
        Log.d(TAG,"resultCode:"+resultCode);
        if (fragmentLoginBinding != null)
            loginViewModel.setupFacebookSignIn();
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                loginViewModel.googleSignInAccount(task.getResult(ApiException.class));
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.d(TAG,ex.getMessage());
                showError(getString(R.string.please_try_again));
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loginViewModel != null) loginViewModel.reset();
    }

}
