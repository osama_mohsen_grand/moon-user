package grand.app.moon.views.fragments.story;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.bolaware.viewstimerstory.Momentz;
import com.bolaware.viewstimerstory.MomentzCallback;
import com.bolaware.viewstimerstory.MomentzView;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentStoryBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.moon.MoonHelper;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.story.StoryViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoryFragment extends BaseFragment implements MomentzCallback {

    FragmentStoryBinding fragmentStoryBinding;
    ArrayList<MomentzView> momentzViews = new ArrayList<>();

    public void loadImage(ImageView imageView, String url) {
        momentz.pause(true);
        Picasso.with(getContext())
                .load(url)
                .into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        try {
                            if (imageView != null && momentz != null) momentz.resume();
                            Timber.e("success:LoadImage");
                        } catch (Exception Ex) {
                            Ex.printStackTrace();
                        }

//                        startStories();
                    }

                    @Override
                    public void onError() {
                        Timber.e("failed:LoadImage");
                    }
                });
    }

    public SimpleExoPlayer player = null;
    MediaSource mediaSource;

    public class VideoPlayerConfig {
        //Minimum Video you want to buffer while Playing
        public static final int MIN_BUFFER_DURATION = 2000;
        //Max Video you want to buffer during PlayBack
        public static final int MAX_BUFFER_DURATION = 5000;
        //Min Video you want to buffer before start Playing it
        public static final int MIN_PLAYBACK_START_BUFFER = 1500;
        //Min video You want to buffer when user resumes video
        public static final int MIN_PLAYBACK_RESUME_BUFFER = 2000;
    }


//    public void initVideo(String url) {
//        momentz.pause(true);
//        TrackSelector trackSelector = new DefaultTrackSelector();
//        LoadControl loadControl = new DefaultLoadControl.Builder()
//                .setAllocator(new DefaultAllocator(true, 16))
//                .setBufferDurationsMs(VideoPlayerConfig.MIN_BUFFER_DURATION,
//                        VideoPlayerConfig.MAX_BUFFER_DURATION,
//                        VideoPlayerConfig.MIN_PLAYBACK_START_BUFFER,
//                        VideoPlayerConfig.MIN_PLAYBACK_RESUME_BUFFER)
//                .setTargetBufferBytes(-1)
//                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();
//
//        player = ExoPlayerFactory.newSimpleInstance(getActivityBase(), trackSelector, loadControl);
//        mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
//                .createMediaSource(Uri.parse(url));
//
//        PlayerView playerView = new PlayerView(context);
//        playerView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT));
//        playerView.setBackgroundColor(getResources().getColor(R.color.colorBlack));
//        playerView.setShutterBackgroundColor(getResources().getColor(R.color.colorBlack));
//        playerView.setPlayer(player);
//
//        player.setPlayWhenReady(true);
//        player.prepare(mediaSource, true, false);
//
//
//        player.addListener(new Player.EventListener() {
//            @Override
//            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//                Timber.e("state:" + playbackState);
//                if (playbackState == ExoPlayer.STATE_READY) {
//                    momentz.resume();
//                } else if (playbackState == ExoPlayer.STATE_ENDED) {
////                    return;=
////                    player.stop();
////                    loadObject();
//                    player.stop();
//                }
//            }
//
//
//        });
//
//    }

    public VideoView videoView = null;
    WebView webView = null;

    public void loadVideo(VideoView videoView, String url, int index) {
        momentz.pause(false);
        this.videoView = videoView;
//        Glide.with(this).load(url).into(fragmentStoryBinding.imgStory);
//        fragmentStoryBinding.imgStory.setVisibility(View.VISIBLE);
//        fragmentStoryBinding.progress.setVisibility(View.VISIBLE);
//        Timber.e("urlLoadVideo:"+url);
        Uri uri = Uri.parse(url);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();
        videoView.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mediaPlayer, int what, int i1) {
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START && momentz != null) {
//                    fragmentStoryBinding.imgStory.setVisibility(View.GONE);
//                    fragmentStoryBinding.progress.setVisibility(View.GONE);
                    // Here the video starts
//                    momentz.editDurationAndResume(index, story.mStories.get(index).duration/ 1000);
//                    Toast.makeText(context, "Video loaded from the internet", Toast.LENGTH_LONG).show();
                    momentz.resume();
                    return true;
                }
                Timber.e("what:" + what);
                return false;
            }
        });
    }

    public void initVideo(PlayerView playerView, String url) {
        if (getActivityBase() != null) {
            TrackSelector trackSelector = new DefaultTrackSelector();
            LoadControl loadControl = new DefaultLoadControl.Builder()
                    .setAllocator(new DefaultAllocator(true, 16))
                    .setBufferDurationsMs(VideoPlayerConfig.MIN_BUFFER_DURATION,
                            VideoPlayerConfig.MAX_BUFFER_DURATION,
                            VideoPlayerConfig.MIN_PLAYBACK_START_BUFFER,
                            VideoPlayerConfig.MIN_PLAYBACK_RESUME_BUFFER)
                    .setTargetBufferBytes(-1)
                    .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();

            player = ExoPlayerFactory.newSimpleInstance(getActivityBase(), trackSelector, loadControl);
            mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                    .createMediaSource(Uri.parse(url));
            playerView.setUseController(false);
            playerView.setPlayer(player);
            player.setPlayWhenReady(true);
            player.prepare(mediaSource, true, false);
        }
    }

    public PlayerView playerView;

    public void loadVideoWebView(View view, String url) {
//        momentz.pause(false);
        momentz.pause(true);
//        Glide.with(this).load(url).into(fragmentStoryBinding.imgStory);
//        fragmentStoryBinding.imgStory.setVisibility(View.VISIBLE);
//        fragmentStoryBinding.progress.setVisibility(View.VISIBLE);
        playerView = view.findViewById(R.id.player_view);
        initVideo(playerView, url);
        player.addListener(new Player.EventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                Timber.e("state:" + playbackState);
                if (playbackState == ExoPlayer.STATE_READY && momentz != null) {
                    try {
                        momentz.resume();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    public ImageView getImage() {
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }

    public VideoView getVideo() {
        VideoView videoView = new VideoView(context);
        videoView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        return videoView;
    }

    public View getWebView() {
//        WebViewFragmentBinding webViewFragmentBinding = DataBindingUtil.inflate(LayoutInflater.from(context),R.layout.web_view_fragment,null, true);
        return LayoutInflater.from(getActivity()).inflate(R.layout.web_view_fragment, null);
    }


    public Momentz momentz = null;
    StoryViewModel viewModel = new StoryViewModel();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentStoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_story, container, false);
        getData();
        viewModel.getMutableLiveData().observe((LifecycleOwner) context, o -> {
                    String action = (String) o;
                    if (action.equals(Constants.SUBMIT)) {
                        Log.d(TAG, "SUBMIT with pause");
                        Log.d(TAG, "CURRENT " + current);
                        Story story = viewModel.story;
                        if (current.equals(Constants.VIDEO) && this.player != null) {
                            Log.d(TAG, "VIDEO was pause");
                            player.setPlayWhenReady(false);
//                            this.videoView.pause();
                        }
                        try {
                            momentz.pause(false);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        if (story.type < Constants.TYPE_ADMIN_STORY) {
                            if (story.workDays != 0)
                                MoonHelper.shopDetails(context, story.type, story.flag, story.mId, story.mName);
                            else
                                Toasty.info(requireContext(), ResourceManager.getString(R.string.this_shop_is_close_now), Toasty.LENGTH_SHORT).show();
                        }
                    }
                }
        );
        prepareViews();
        momentz = new Momentz(context, momentzViews, fragmentStoryBinding.llStoryContainer, this, R.drawable.green_lightgrey_drawable);
        momentz.start();
        fragmentStoryBinding.setViewmodel(viewModel);
        return fragmentStoryBinding.getRoot();
    }

    private void prepareViews() {
        momentzViews.clear();
        MomentzView momentzView = null;
        for (IdNameImage story : viewModel.story.mStories) {
            Timber.e("story:" + story.image);
            if (story.type == 1) {
                ImageView imageView = getImage();
                momentzView = new MomentzView(imageView, 5);
            } else {
//                VideoView videoView = getVideo();
                View webView = getWebView();
                momentzView = new MomentzView(webView, story.duration);
            }
            momentzViews.add(momentzView);
        }
    }


    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.STORY))
            viewModel.story = (Story) getArguments().getSerializable(Constants.STORY);
    }


    @Override
    public void done() {
        if (getActivityBase() != null) {
            getActivityBase().finish();
        }
    }

    private static final String TAG = "StoryFragment";


    @Override
    public void onDestroy() {

        if (player != null) {
            player.stop();
        }
        if (current.equals(Constants.VIDEO) && playerView != null) {
            playerView.onPause();
        } else if (current.equals(Constants.VIDEO) && playerView != null && player != null) {
            Log.d(Constants.VIDEO, "stop");
            playerView.onPause();
            player.stop();
        } else if (current.equals(Constants.VIDEO) && videoView != null && player != null) {
            Log.d(Constants.VIDEO, "stop");
            videoView.pause();
            player.stop();
        }
        try {
            if (position < (viewModel.story.mStories.size() - 1) && momentz != null)
                momentz.pause(false);
            momentz = null;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        viewModel.viewStory(viewed);
        Intent intent = new Intent();
        intent.putExtra("storyCount", viewed.size());
        getActivityBase().setResult(Activity.RESULT_OK, intent);
        super.onDestroy();

    }

    public String current = "";

    public int position = 0;
    public ArrayList<String> viewed = new ArrayList<>();

    @Override
    public void onNextCalled(@NotNull View view, @NotNull Momentz momentz, int i) {
        position = i;
        addView(position);
        destroyPlayer();
        if (view instanceof ImageView) {
            current = Constants.IMAGE;
            if (i < viewModel.story.mStories.size())
                loadImage((ImageView) view, viewModel.story.mStories.get(i).image);
            if (player != null) player.stop();
        } else if (view instanceof VideoView) {
            current = Constants.VIDEO;
//            initVideo(story.mStories.get(i).image);
            if (i < viewModel.story.mStories.size())
                loadVideo((VideoView) view, viewModel.story.mStories.get(i).image, i);
        } else {
            current = Constants.VIDEO;
            if (i < viewModel.story.mStories.size())
                loadVideoWebView((View) view, viewModel.story.mStories.get(i).image);
        }
        if (position == viewModel.story.mStories.size()) {
            momentz.pause(false);
            onDestroy();
        }
    }

    public void addView(int position) {
        if (position < viewModel.story.mStories.size()) {
            String id = viewModel.story.mStories.get(position).id;
            if (!viewed.contains(id))
                viewed.add(id);
        }
    }

    public void destroyPlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (momentz != null) momentz.resume();
        if (current.equals(Constants.VIDEO) && player != null) {
//            int resumeWindow = player.getCurrentWindowIndex();
//            int resumePosition = Math.toIntExact(player.isCurrentWindowSeekable() ? Math.max(0, player.getCurrentPosition())
//                    : C.TIME_UNSET);
//
//            Log.d(TAG,"resumeWindow "+resumeWindow);
//            Log.d(TAG,"resumePosition "+resumePosition);

//            Log.d(TAG,"position:"+player.getCurrentPosition()+"");
//            player.seekTo(player.getCurrentPosition());
            player.setPlayWhenReady(true);

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause Story");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop Story");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
