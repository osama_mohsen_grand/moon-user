package grand.app.moon.views.fragments.map;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentMapBookingBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.BottomSheetDialogHelper;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.maputils.direction.DirectionDistance;
import grand.app.moon.utils.maputils.direction.DirectionHelper;
import grand.app.moon.utils.maputils.direction.DrawRoute;
import grand.app.moon.utils.maputils.location.GPSAllowListener;
import grand.app.moon.utils.maputils.location.GPSLocation;
import grand.app.moon.utils.maputils.location.LocationListenerEvent;
import grand.app.moon.utils.maputils.location.MapActivityHelper;
import grand.app.moon.utils.maputils.permission.MapPermissionFragment;
import grand.app.moon.utils.maputils.tracking.FireTracking;
import grand.app.moon.utils.storage.location.LocationHelper;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.map.MapBookingViewModel;
import grand.app.moon.viewmodels.schedule.ScheduleViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.vollyutils.MyApplication;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapBookingFragment extends BaseFragment implements LocationListener {

    private View rootView;
    private FragmentMapBookingBinding fragmentMapBookingBinding;
    private MapBookingViewModel mapBookingViewModel;
    public static double lat = 0, lng = 0;
    private static final String TAG = "HomeFragment";
    private boolean async = false;
    public FusedLocationProviderClient fusedLocationClient;
    MapPermissionFragment mapPermission;
    MapActivityHelper mapActivityHelper;
    BottomSheetDialogHelper bottomSheetDialogHelper;
    FireTracking fireTracking;
    DrawRoute drawRoute;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMapBookingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_map_booking, container, false);
        LocationHelper.clear();
        if (fragmentMapBookingBinding != null) {
            bind(savedInstanceState);
            setEvent();
        }
        getData();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.DRIVER_ID)) {
            mapBookingViewModel.truckRequest.driver_id = String.valueOf(getArguments().getInt(Constants.DRIVER_ID));
        }else {
            mapBookingViewModel.truckRequest.driver_id = null;
            mapBookingViewModel.truckRequest.goods_weight = null;
            mapBookingViewModel.truckRequest.name = null;
            mapBookingViewModel.truckRequest.phone = null;
            mapBookingViewModel.truckRequest.goods_type = null;
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            mapBookingViewModel.truckRequest.trip_type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PRICE)) {
            mapBookingViewModel.priceKM_per_kilo = getArguments().getInt(Constants.PRICE); // truck price
        }else
            mapBookingViewModel.priceKM_per_kilo = UserHelper.getUserDetails().taxi_price_per_kilo; // Taxi price

        if (getArguments() != null && getArguments().containsKey(Constants.START_COUNTER)) {
            mapBookingViewModel.start_counter = getArguments().getInt(Constants.START_COUNTER); // truck open door
        } else
            mapBookingViewModel.start_counter = UserHelper.getUserDetails().start_counter;//Taxi open door
    }

    private void bind(Bundle savedInstanceState) {
        mapBookingViewModel = new MapBookingViewModel();
        mapPermission = new MapPermissionFragment(this);
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivity());
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

        fragmentMapBookingBinding.mapView.onCreate(savedInstanceState);

        fragmentMapBookingBinding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.e(TAG, "bind: ");
        fragmentMapBookingBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                mapBookingViewModel.mapConfig = new MapConfig(getContext(), mMap);
                mapActivityHelper = new MapActivityHelper(getActivity(), mapBookingViewModel.mapConfig);
                mapBookingViewModel.mapConfig.setMapStyle();//set style google map
                mapBookingViewModel.mapConfig.setSettings();//add setting google map
                mapBookingViewModel.mapConfig.checkLastLocation();
                mapBookingViewModel.mapConfig.changeMyLocationButtonLocation(fragmentMapBookingBinding.mapView);//change location button
//                mapBookingViewModel.mapConfig.setLocationButtonListeners();//enable button location listeners
                mapBookingViewModel.mapConfig.getGoogleMap().setPadding(0, 220, 0, 220);
                mapBookingViewModel.mapConfig.getGoogleMap().setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        try {
                            if (!mapBookingViewModel.pickupSelected.get() && mapBookingViewModel.mapConfig.getGoogleMap().getCameraPosition().target.latitude != 0 && mapBookingViewModel.mapConfig.getGoogleMap().getCameraPosition().target.longitude != 0) {
                                lat = mapBookingViewModel.mapConfig.getGoogleMap().getCameraPosition().target.latitude;
                                lng = mapBookingViewModel.mapConfig.getGoogleMap().getCameraPosition().target.longitude;
                                if (!mapBookingViewModel.pickupSelected.get()) {//check not select pickup location before
                                    mapBookingViewModel.setPickupLocation(new LatLng(lat, lng));
                                }
                            }
                        }catch (Exception ex){
                            ex.printStackTrace();
                            Log.d(TAG,"ex:"+ex.getMessage());
                        }

                    }
                });//addMarkerOnMap_

                Log.e(TAG, "onMapReady: start");
                if (!mapPermission.validLocationPermission()) {
                    mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST);
                } else {
                    mapBookingViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                    mapActivityHelper.startUpdateLocation(new LocationListenerEvent() {
                        @Override
                        public void update(LatLng latLng, Object object) {
                            async = false;
                            mapBookingViewModel.mapConfig.saveLastLocation(latLng);
                        }
                    });
                    onResume();
                    enableMyLocationIfPermitted();
                }
            }
        });
        fragmentMapBookingBinding.setMapBookingViewModel(mapBookingViewModel);
        rootView = fragmentMapBookingBinding.getRoot();
    }

    private void showAutoComplete() {
        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            Places.initialize(getContext(), getString(R.string.google_direction_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(getContext());
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    private void enableMyLocationIfPermitted() {
        if (!mapPermission.validLocationPermission()) {
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        } else if (mapBookingViewModel.mapConfig != null && mapBookingViewModel.mapConfig.getGoogleMap() != null) {
            allowGPS();
            onResume();
        }
    }


    @SuppressLint("MissingPermission")
    private void addCircleLocation(LatLng latLng) {
        if (!mapPermission.validLocationPermission()) {
            return;
        }
        mapBookingViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
        mapBookingViewModel.mapConfig.moveCamera(latLng);
        onResume();
    }

    private void setEvent() {
        mapBookingViewModel.mMutableLiveData.observe((LifecycleOwner) getActivity(), new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, mapBookingViewModel.getTrucksRepository().getMessage());
                if (action.equals(Constants.PICKUP_SUBMIT) || action.equals(Constants.DEST_SUBMIT)) {
                    showAutoComplete();
                } else if (action.equals(Constants.DONE)) {
                    toastMessage(mapBookingViewModel.getTrucksRepository().getMessage());
                    ((AppCompatActivity) context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                } else if (action.equals(Constants.SCHEDULE)) {
                    bottomSheetDialogHelper.showScheduleCalendar(new DialogHelperInterface() {
                        @Override
                        public void OnClickListenerContinue(Dialog dialog, View view) {
                            mapBookingViewModel.isSchedule.set(true);
                            mapBookingViewModel.truckRequest.date = ScheduleViewModel.date_server;
                            mapBookingViewModel.truckRequest.time = ScheduleViewModel.time_server;
                            mapBookingViewModel.truckRequest.type = Constants.SCHEDULED;
                            mapBookingViewModel.setType(ScheduleViewModel.date_select + ", " + ScheduleViewModel.time_server);
                        }
                    });
                } else if (action.equals(Constants.ORDER)) {
                    DirectionHelper directionHelper = new DirectionHelper(getActivityBase(), mapBookingViewModel.mapConfig);
                    //TODO
//                    directionHelper.json_route = directionHelper.loadJSONFromAsset();
//                    directionHelper.drawRoute();
                    //TODO END

                    directionHelper.json_route = directionHelper.getRouteUrl(mapBookingViewModel.markers_locations);
                    Log.e(TAG, "onChanged: " + directionHelper.json_route);
                    new DrawRoute(getActivityBase(), directionHelper.json_route, mapBookingViewModel.mapConfig, new DirectionDistance() {
                        @Override
                        public void receive(int distanceM, int timeSec) {
                            Timber.e("Distance:" + distanceM);
                            Timber.e("pricePerHour:" + mapBookingViewModel.priceKM_per_kilo);
                            double distanceK = distanceM / 1000;
                            double res = distanceK * mapBookingViewModel.priceKM_per_kilo;
                            Timber.e("res:" + res + mapBookingViewModel.start_counter);
                            mapBookingViewModel.setPrice(res + mapBookingViewModel.start_counter);
                            mapBookingViewModel.truckRequest.distance = String.valueOf(distanceK);
                            double timeMin = timeSec / 60;
                            mapBookingViewModel.truckRequest.trip_time = String.valueOf(timeMin);
                        }
                    }).execute();
                }

//                    fireTracking = new FireTracking(getActivity(),mapBookingViewModel.mapConfig,mapActivityHelper);
//                } else if (action.equals(Constants.SUBMIT_ORDER)) {
//                    toastMessage(mapBookingViewModel.getOrderRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
//                    mapBookingViewModel.clearMap();
//                }
                else if (action.equals(Constants.CONFIRM)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.pickup_form));
                    intent.putExtra(Constants.PAGE, Constants.PICKUP_FORM);
                    mapBookingViewModel.prepareLocationRequest();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.REQUEST, mapBookingViewModel.truckRequest);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }

    private LocationCallback locationCallback;

    @SuppressLint("MissingPermission")
    public void allowGPS() {
        if (!GPSLocation.checkGps(context)) {
            GPSLocation.EnableGPSAutoMatically((ParentActivity) context, new GPSAllowListener() {
                @Override
                public void GPSStatus(boolean isOpen) {
                    if (isOpen) {
                        mapBookingViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                        updateLocation();
                    }
                }
            });

        } else {
            if (mapPermission.validLocationPermission()) {
                Location location = GPSLocation.getLocation((ParentActivity) context);
                if (location != null && !async) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mapBookingViewModel.setPickupLocation(latLng);
                    mapBookingViewModel.mapConfig.saveLastLocation(latLng);
                    mapBookingViewModel.mapConfig.moveCamera(latLng);
                    async = true;
                }
                updateLocation();
            }
        }
    }


    private LocationManager mLocationManager;


    @SuppressLint("MissingPermission")
    public void updateLocation() {
        Log.e(TAG, "updateLocation: ");
        if (!mapPermission.validLocationPermission()) {
            return;
        }
        mapBookingViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        Task<Location> task = fusedLocationClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                Log.e(TAG, "onSuccess: ");
                if (location != null && !async) {
                    //Write your implemenation here
                    Log.e(TAG, location.getLatitude() + " " + location.getLongitude());
                }
            }
        });
        onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mLocationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: Before super");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e(TAG, "onRequestPermissionsResult: " + Constants.LOCATION_REQUEST);
        if (requestCode == Constants.LOCATION_REQUEST && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "onRequestPermissionsResultDone: " + Constants.LOCATION_REQUEST);
                onResume();
                allowGPS();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE) {
            // Make sure the request was successful
            try {
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    if(place != null && place.getLatLng() != null) {
                        if (mapBookingViewModel.mMutableLiveData.getValue().equals(Constants.PICKUP_SUBMIT)) {
                            mapBookingViewModel.setPickUp(place);
                        } else {
                            mapBookingViewModel.setDest(place);
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        Log.e(TAG, "onLocationChanged: " + location.getLatitude() + "," + location.getLongitude());
        if (!async && location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mapBookingViewModel.setPickupLocation(latLng);
            mapBookingViewModel.mapConfig.saveLastLocation(latLng);
            mapBookingViewModel.mapConfig.moveCamera(latLng);
            addCircleLocation(latLng);
            async = true;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (context != null && fragmentMapBookingBinding != null && fragmentMapBookingBinding.mapView != null)
                fragmentMapBookingBinding.mapView.onResume();
        }catch (Exception ex){
            Timber.e("ex:"+ex.getMessage());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fragmentMapBookingBinding != null && fragmentMapBookingBinding.mapView != null)
            fragmentMapBookingBinding.mapView.onPause();
    }

    @Override
    public void onDestroy() {

        if (fragmentMapBookingBinding != null && fragmentMapBookingBinding.mapView != null)
            if (mapBookingViewModel != null) {
                mapBookingViewModel.reset();
                fragmentMapBookingBinding.mapView.onDestroy();
            }
        fragmentMapBookingBinding = null;
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (fragmentMapBookingBinding.mapView != null)
            fragmentMapBookingBinding.mapView.onLowMemory();
    }

}
