package grand.app.moon.views.fragments.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentLanguageBinding;
import grand.app.moon.databinding.FragmentNoConnectionBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.app.NoConnectionViewModel;
import grand.app.moon.viewmodels.language.LanguageViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;


public class NoConnectionFragment extends BaseFragment {
    View rootView;
    private FragmentNoConnectionBinding fragmentNoConnectionBinding;
    private NoConnectionViewModel noConnectionViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentNoConnectionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_connection, container, false);
        bind();
        rootView = fragmentNoConnectionBinding.getRoot();
        return rootView;
    }

    private void bind() {
        noConnectionViewModel = new NoConnectionViewModel();
        setEvents();
        fragmentNoConnectionBinding.setNoConnectionViewModel(noConnectionViewModel);
    }

    private void setEvents() {
        noConnectionViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
//                handleActions(action,noConnectionViewModel.getPrivacyRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.FAILURE_CONNECTION)){
                    Toast.makeText(getActivity(), ""+ ResourceManager.getString(R.string.please_check_connection), Toast.LENGTH_SHORT).show();
                }else if(action.equals(Constants.RELOAD)){
                    getActivityBase().finishAffinity();
                    if(UserHelper.getUserId() == -1)
                        startActivity(new Intent(getActivity(),BaseActivity.class));
                    else
                        startActivity(new Intent(getActivity(), MainActivity.class));
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        noConnectionViewModel.reset();
    }
}
