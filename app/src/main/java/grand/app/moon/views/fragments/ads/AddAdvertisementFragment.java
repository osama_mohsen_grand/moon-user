package grand.app.moon.views.fragments.ads;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moon.MultipleImageSelect.models.Image;
import grand.app.moon.R;
import grand.app.moon.adapter.AlbumEditImagesAdapter;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentAddAdvertisementBinding;
import grand.app.moon.databinding.FragmentRegisterCarBinding;
import grand.app.moon.models.adsCompanyFilter.AdsCompanyFilterResponse;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.Datum;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.viewmodels.ads.AddAdvertisementViewModel;
import grand.app.moon.viewmodels.user.RegisterCarViewModel;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddAdvertisementFragment extends BaseFragment {
    View rootView;
    private FragmentAddAdvertisementBinding fragmentAddAdvertisementBinding;
    private AddAdvertisementViewModel addAdvertisementViewModel;
    private int service_id = -1, category_id = -1, sub_category = -1,id= -1,type=-1;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    boolean isEdit = false;
    public AlbumEditImagesAdapter albumEditImagesAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAddAdvertisementBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_advertisement, container, false);
        getData();
        bind();
        setEvent();
        setImagesAdapter();

        return rootView;
    }

    private void setImagesAdapter() {
        albumEditImagesAdapter = new AlbumEditImagesAdapter("1",addAdvertisementViewModel.productImages);
        AppUtils.initVerticalRV(fragmentAddAdvertisementBinding.rvImages, fragmentAddAdvertisementBinding.rvImages.getContext(), 3);
        fragmentAddAdvertisementBinding.rvImages.setAdapter(albumEditImagesAdapter);
//        albumEditImagesAdapter.addNewImageView();
        setEventAdapter();
    }


    private static final String TAG = "AddAdvertisementFragment";

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.EDIT))
            isEdit = getArguments().getBoolean(Constants.EDIT);
        if(isEdit) {
            if (getArguments() != null && getArguments().containsKey(Constants.ID))
                id = getArguments().getInt(Constants.ID);

            if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
                type = getArguments().getInt(Constants.TYPE);
            addAdvertisementViewModel = new AddAdvertisementViewModel(id,type);
        }else{
            if (getArguments() != null && getArguments().containsKey(Constants.ID))
                service_id = getArguments().getInt(Constants.ID);
            if (getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID))
                category_id = getArguments().getInt(Constants.CATEGORY_ID);
            if (getArguments() != null && getArguments().containsKey(Constants.SUB_CATEGORY_ID))
                sub_category = getArguments().getInt(Constants.SUB_CATEGORY_ID);
            addAdvertisementViewModel = new AddAdvertisementViewModel(service_id, category_id, sub_category);
        }
    }

    private void bind() {
        fragmentAddAdvertisementBinding.setAddAdvertisementViewModel(addAdvertisementViewModel);

        fragmentAddAdvertisementBinding.edtRegisterCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> popUpCities = new ArrayList<>();
                Datum country = AppMoon.getCities(UserHelper.retrieveKey(Constants.COUNTRY_ID), UserHelper.getCountries().data);
                if (country != null) {
                    Timber.e("country:" + country.countryName);
                    for (City city : country.cities)
                        popUpCities.add(city.cityName);
                    popUpMenuHelper.openPopUp(getActivity(), fragmentAddAdvertisementBinding.edtRegisterCity, popUpCities, new PopUpInterface() {
                        @Override
                        public void submitPopUp(int position) {
                            addAdvertisementViewModel.addAdsRequest.setCity(popUpCities.get(position));
                            addAdvertisementViewModel.addAdsRequest.setCity_id(country.cities.get(position).id + "");
                            addAdvertisementViewModel.notifyChange();
                        }
                    });
                }
            }
        });


        rootView = fragmentAddAdvertisementBinding.getRoot();
    }

    private void setEvent() {
        addAdvertisementViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addAdvertisementViewModel.getAdsRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(addAdvertisementViewModel.getAdsRepository().getMessage());
                    getActivityBase().finishAffinity();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.MY_ADS);
                    startActivity(intent);
                } else if (action.equals(Constants.IMAGE)) {
                    Intent intent = new Intent(context, AlbumSelectActivity.class);
                    intent.putExtra(grand.app.moon.MultipleImageSelect.helpers.Constants.INTENT_EXTRA_LIMIT, 3);
                    startActivityForResult(intent, Constants.REQUEST_CODE);
                } else if (action.equals(Constants.LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                }else if(action.equals(Constants.ADS_DETAILS)){
                    addAdvertisementViewModel.showPage(true);
                    addAdvertisementViewModel.updateView(addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data);
                    albumEditImagesAdapter.update(addAdvertisementViewModel.productImages);
//                    albumEditImagesAdapter.addNewImageView();
                }else if(action.equals(Constants.DELETED)){
//                    addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.images.remove(addAdvertisementViewModel.category_delete_position);
                    albumEditImagesAdapter.remove(addAdvertisementViewModel.category_delete_position);
                    addAdvertisementViewModel.productImages.set(addAdvertisementViewModel.category_delete_position,null);
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(addAdvertisementViewModel.baseError);
                }
            }
        });
    }

    private void setEventAdapter() {
        albumEditImagesAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.SUBMIT)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if(idNameImage == null || (idNameImage.image.equals("") && idNameImage.volleyFileObject == null)) {
                        addAdvertisementViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddAdvertisementFragment.this, Constants.FILE_TYPE_IMAGE);
                    }else if(idNameImage.volleyFileObject != null){
                        addAdvertisementViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddAdvertisementFragment.this, Constants.FILE_TYPE_IMAGE);
                    }
                } else if (mutable.type.equals(Constants.DELETE)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if(idNameImage.volleyFileObject == null) {
                        new DialogConfirm(getActivity())
                                .setTitle(ResourceManager.getString(R.string.remove_item))
                                .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                                .setActionText(ResourceManager.getString(R.string.remove_item))
                                .setActionCancel(ResourceManager.getString(R.string.cancel))
                                .show(new DialogHelperInterface() {
                                    @Override
                                    public void OnClickListenerContinue(Dialog dialog, View view) {
                                        addAdvertisementViewModel.category_delete_position = mutable.position;
                                        addAdvertisementViewModel.delete(Integer.parseInt(addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.productImages.get(mutable.position).id));
                                    }
                                });
                    }else{
                        albumEditImagesAdapter.remove(mutable.position);
                    }
                }
            }
        });
    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            addAdvertisementViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddAdvertisementFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, ""+getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }else if (requestCode == Constants.FILE_TYPE_IMAGE ) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, "url", Constants.FILE_TYPE_IMAGE);
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(volleyFileObject1.getFile())).start(context, AddAdvertisementFragment.this);
        }else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            Log.d(TAG,"doner");
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                Log.d(TAG,addAdvertisementViewModel.new_image_position+"");
                Log.d(TAG,addAdvertisementViewModel.productImages.size()+"");
                int pos = addAdvertisementViewModel.new_image_position + 1 - addAdvertisementViewModel.productImages.size();

//                Timber.e("pos_new:"+addAdvertisementViewModel.new_image_position );
//                Timber.e("pos_before:"+addAdvertisementViewModel.images.size());
//                Timber.e("pos_final:"+pos);
                VolleyFileObject volleyFileObject = new VolleyFileObject("product_images["+ pos +"]", resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                albumEditImagesAdapter.update(addAdvertisementViewModel.new_image_position,volleyFileObject);
                addAdvertisementViewModel.productImages.set(addAdvertisementViewModel.new_image_position,albumEditImagesAdapter.idNameImages.get(addAdvertisementViewModel.new_image_position));

                if(volleyFileObject.getFile().exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(volleyFileObject.getFile().getAbsolutePath());
//                    fragmentAddAdvertisementBinding.imgSource.setImageBitmap(myBitmap);
                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
