package grand.app.moon.views.fragments.be;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.hbb20.CountryCodePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import grand.app.moon.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moon.MultipleImageSelect.models.Image;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentBeDelegateBinding;
import grand.app.moon.models.app.AccountTypeModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.Datum;
import grand.app.moon.models.country.Region;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.utils.maputils.location.LocationLatLng;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.viewmodels.be.BeDelegateViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.views.fragments.auth.ProfileFragment;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

public class BeDelegateFragment extends BaseFragment {

    private View rootView;

    private FragmentBeDelegateBinding fragmentBeDelegateBinding;
    private BeDelegateViewModel beDelegateViewModel;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBeDelegateBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_be_delegate, container, false);
        Timber.e("register page");
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        beDelegateViewModel = new BeDelegateViewModel();
//        beDelegateViewModel.beDelegateRequest.cpp = fragmentBeDelegateBinding.ccp.getDefaultCountryCode();
//        fragmentBeDelegateBinding.ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
//            @Override
//            public void onCountrySelected() {
//                beDelegateViewModel.beDelegateRequest.cpp = fragmentBeDelegateBinding.ccp.getSelectedCountryCode();
//            }
//        });
        fragmentBeDelegateBinding.setBeDelegateViewModel(beDelegateViewModel);
        initPopUp();
        rootView = fragmentBeDelegateBinding.getRoot();
    }

    private void initPopUp() {
        fragmentBeDelegateBinding.edtRegisterAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AccountTypeModel> accountTypeModels = AppMoon.getAccountType();
                ArrayList<String> popUpModels = new ArrayList<>();
                for (AccountTypeModel accountTypeModel : accountTypeModels)
                    popUpModels.add(accountTypeModel.type);
                popUpMenuHelper.openPopUp(getActivity(), fragmentBeDelegateBinding.edtRegisterAccountType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        fragmentBeDelegateBinding.edtRegisterAccountType.setText(accountTypeModels.get(position).type);
                        beDelegateViewModel.beDelegateRequest.setTypeText(accountTypeModels.get(position).type);
                        beDelegateViewModel.beDelegateRequest.setType(String.valueOf(accountTypeModels.get(position).id));
                        beDelegateViewModel.updateType();
                    }
                });
            }
        });
    }

    //
    public List<Region> regions = new ArrayList<>();

    private void setEvent() {
        beDelegateViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, beDelegateViewModel.getBeDelegateRepository().getMessage());

                Timber.e(action);
                if (action.equals(Constants.REGISTRATION)) {//submitSearch register
                    toastMessage(beDelegateViewModel.getBeDelegateRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    ((MainActivity) context).navigationDrawerView.homePage();
                } else if (action.equals(Constants.SERVICE_SUCCESS)) {//get service type
                    //Log services
                } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                    pickImageDialogSelect();
                } else if (action.equals(Constants.SELECT_IMAGE_MULTIPLE)) {//select image profile
                    Intent intent = new Intent(context, AlbumSelectActivity.class);
                    intent.putExtra(grand.app.moon.MultipleImageSelect.helpers.Constants.INTENT_EXTRA_LIMIT, 2);
                    startActivityForResult(intent, Constants.REQUEST_CODE);
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(beDelegateViewModel.baseError);
                } else if (action.equals(Constants.LOCATIONS)) {//select location
                    Timber.e("Locations");
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                }else if (action.equals(Constants.COLOR)) {
                    ColorPickerDialogBuilder
                            .with(context)
                            .setTitle("Choose color")
                            .initialColor(R.color.colorAccent)
                            .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                            .density(12)
                            .setOnColorSelectedListener(new OnColorSelectedListener() {
                                @Override
                                public void onColorSelected(int selectedColor) {
                                    Timber.e("color:"+selectedColor);
                                }
                            })
                            .setPositiveButton("ok", new ColorPickerClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
//                                    Timber.e("color:"+selectedColor);
                                    String hexColor = String.format("#%06X", (0xFFFFFF & selectedColor));
//                                    Timber.e("color_update:"+hexColor);
                                    beDelegateViewModel.beDelegateRequest.setCar_color(hexColor);
                                    beDelegateViewModel.notifyChange();
                                }
                            })
                            .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .build()
                            .show();
                }
            }
        });
        beDelegateViewModel.mMutableLiveDataCountry.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, beDelegateViewModel.getCountryRepository().getMessage());
                if (action.equals(Constants.COUNTRIES)) {//load countries
                    beDelegateViewModel.showPage(true);
                    fragmentBeDelegateBinding.edtRegisterCity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ArrayList<String> popUpCities = new ArrayList<>();
                            country = AppMoon.getCities(UserHelper.retrieveKey(Constants.COUNTRY_ID), beDelegateViewModel.getCountryRepository().getCountriesResponse().data);
                            beDelegateViewModel.showPage(true);
                            getTrucks();
                            if (country != null) {
                                Timber.e("country:" + country.countryName);
                                for (City city : country.cities)
                                    popUpCities.add(city.cityName);
                                popUpMenuHelper.openPopUp(getActivity(), fragmentBeDelegateBinding.edtRegisterCity, popUpCities, new PopUpInterface() {
                                    @Override
                                    public void submitPopUp(int position) {
                                        beDelegateViewModel.beDelegateRequest.setCity_id(String.valueOf(country.cities.get(position).id));
                                        fragmentBeDelegateBinding.edtRegisterCity.setText(country.cities.get(position).cityName);
                                        fragmentBeDelegateBinding.edtRegisterRegion.setText("");
                                        regions = new ArrayList<>(country.cities.get(position).regions);
                                        Timber.e("regions" + regions.size());
                                        getRegions();
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    public List<IdName> trucks = new ArrayList<>();
    Datum country = null;

    private void getTrucks() {
        Timber.e("getTrucks:Start");
        trucks = new ArrayList<>(country.trucks);
        fragmentBeDelegateBinding.edtRegisterTruck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> popUpTrucks = new ArrayList<>();
                for (IdName truck : trucks)
                    popUpTrucks.add(truck.name);
                Timber.e("size:" + trucks.size());
                popUpMenuHelper.openPopUp(getActivity(), fragmentBeDelegateBinding.edtRegisterTruck, popUpTrucks, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        beDelegateViewModel.beDelegateRequest.setCarId(String.valueOf(trucks.get(position).id));
                        fragmentBeDelegateBinding.edtRegisterTruck.setText(trucks.get(position).name);
                    }
                });
            }
        });
    }
    

    private void getRegions() {
        ArrayList<String> popUpRegions = new ArrayList<>();
        for (Region region : regions)
            popUpRegions.add(region.regionName);
        Timber.e("size:" + popUpRegions.size());
        popUpMenuHelper.openPopUp(getActivity(), fragmentBeDelegateBinding.edtRegisterRegion, popUpRegions, new PopUpInterface() {
            @Override
            public void submitPopUp(int position) {
                beDelegateViewModel.beDelegateRequest.setRegion_id(String.valueOf(regions.get(position).id));
                fragmentBeDelegateBinding.edtRegisterRegion.setText(regions.get(position).regionName);
            }
        });

    }

    int counter = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Timber.e("onActivityResult:" + requestCode);
        Timber.e("imageName:" + beDelegateViewModel.image_select);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, beDelegateViewModel.image_select, Constants.FILE_TYPE_IMAGE);
            beDelegateViewModel.setImage(volleyFileObject);
            if (beDelegateViewModel.image_select.equals("image")) {
                fragmentBeDelegateBinding.imgRegisterUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
            } else if (beDelegateViewModel.image_select.equals("car_image")) {
                beDelegateViewModel.beDelegateRequest.setCarImage(getString(R.string.car_image_had_been_selected));
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, BeDelegateFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, ""+getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            Log.d("onActivityResult","request done");
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                beDelegateViewModel.setImage(volleyFileObject);
                if (beDelegateViewModel.image_select.equals("image")) {
                    fragmentBeDelegateBinding.imgRegisterUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
                } else if (beDelegateViewModel.image_select.equals("car_image")) {
                    beDelegateViewModel.beDelegateRequest.setCarImage(getString(R.string.car_image_had_been_selected));
                }
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            beDelegateViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
            beDelegateViewModel.beDelegateRequest.setAddress(new LocationLatLng(getActivityBase(),BeDelegateFragment.this).getAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0)));
        }
        if (requestCode == Constants.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            try {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                counter = images.size();
                if (beDelegateViewModel.image_select.equals(Constants.ID_IMAGE)) {
                    if (counter == 2) {
                        beDelegateViewModel.beDelegateRequest.setIdImage(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    } else
                        toastInfo(fragmentBeDelegateBinding.tilRegisterIdImage.getHelperText().toString());
                } else if (beDelegateViewModel.image_select.equals("license_image")) {
                    if (counter == 2)
                        beDelegateViewModel.beDelegateRequest.setDrivingLicence(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    else
                        toastInfo(fragmentBeDelegateBinding.tilRegisterLicence.getHelperText().toString());
                }
                if (counter == 2) {
                    for (int i = 0; i < images.size(); i++) {
                        beDelegateViewModel.setImage(new VolleyFileObject(beDelegateViewModel.image_select + "[" + i + "]", images.get(i).path, Constants.FILE_TYPE_IMAGE));
                    }
                }
                beDelegateViewModel.notifyChange();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        beDelegateViewModel.reset();

    }

}

