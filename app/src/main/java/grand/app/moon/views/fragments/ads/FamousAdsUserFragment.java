package grand.app.moon.views.fragments.ads;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.CompanyAdsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFamousAdsUserBinding;
import grand.app.moon.models.famous.details.AlbumModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.MovementHelper;
import grand.app.moon.utils.dialog.BottomSheetDialogHelper;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.famous.FamousAlbumsUserDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.famous.FamousAlbumsUserDetails;

public class FamousAdsUserFragment extends BaseFragment {

    private FragmentFamousAdsUserBinding fragmentFamousAdsUserBinding;
    private FamousAlbumsUserDetailsViewModel famousAlbumsUserDetailsViewModel;
    private CompanyAdsAdapter adapter;
    BottomSheetDialogHelper bottomSheetDialogHelper;

    public int id = -1;
    int tab = -1;
    String type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UserHelper.saveKey(Constants.RELOAD, Constants.FALSE);
        fragmentFamousAdsUserBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_ads_user, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousAdsUserBinding.getRoot();

    }

    private void getData() {

        adapter = new CompanyAdsAdapter(new ArrayList<>());
        fragmentFamousAdsUserBinding.rvFamousAlbumDetails.setAdapter(adapter);
        setEventAdapter();
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            id = getArguments().getInt(Constants.ID);
            adapter.famous_id = id;
        }if (getArguments() != null && getArguments().containsKey(Constants.TAB))
            tab = getArguments().getInt(Constants.TAB);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getString(Constants.TYPE);
    }


    private void bind() {
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivityBase());
        famousAlbumsUserDetailsViewModel = new FamousAlbumsUserDetailsViewModel();
        famousAlbumsUserDetailsViewModel.setData(id, tab,type);
        famousAlbumsUserDetailsViewModel.getFamousDetails();
        AppUtils.initVerticalRV(fragmentFamousAdsUserBinding.rvFamousAlbumDetails, fragmentFamousAdsUserBinding.rvFamousAlbumDetails.getContext(), 3);
        fragmentFamousAdsUserBinding.setFamousAlbumUserDetailsViewModel(famousAlbumsUserDetailsViewModel);
    }

    private static final String TAG = "FamousAdsUserFragment";

    List<AlbumModel> companyAds = null;

    private void setEvent() {
        famousAlbumsUserDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAlbumsUserDetailsViewModel.getFamousRepository().getMessage());
                if (famousAlbumsUserDetailsViewModel.getAdsRepository() != null)
                    handleActions(action, famousAlbumsUserDetailsViewModel.getAdsRepository().getMessage());
                assert action != null;
                Log.d(TAG, "action: "+action);
                if (action.equals(Constants.SUCCESS)) {
                    Log.d(TAG, "onChanged: "+tab);
                    Log.d(TAG, "type: "+type);
                    Log.d(TAG, "id: "+id);
                    Log.d(TAG,""+famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().ads.size());
                    if(famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().ads.size() > 0)
                        adapter.update(famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().ads);
                    else
                        famousAlbumsUserDetailsViewModel.noData();
//                    companyAds = famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().ads;
////                    Timber.e("size:"+imageVideos.size());
//                    if (companyAds != null && companyAds.size() > 0) {
//                            famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data = famousAlbumsUserDetailsViewModel.getAdsRepository().getAdsResponse().data;
//                            adapter.update( famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().ads);
//                    } else {
//                        famousAlbumsUserDetailsViewModel.noData();
//                    }
                } else if (action.equals(Constants.FILTER)) {
                    if (bottomSheetDialogHelper.filterDialog == null) {
                        bottomSheetDialogHelper.filterFamousShopAds(famousAlbumsUserDetailsViewModel.getAdsRepository().getFilterShopAdsResponse().data, false, (dialog, type, object) -> {
                            if (type.equals(Constants.ARRAY)) {
                                ArrayList<Integer> list = (ArrayList<Integer>) object;
                                Log.d(TAG,"list:"+list.size());
                                if (list != null && list.size() > 0) {
                                    Log.d(TAG,list.get(0).toString());
                                    Intent intent = new Intent(context, BaseActivity.class);
                                    intent.putExtra(Constants.NAME_BAR, getString(R.string.ads));
                                    intent.putExtra(Constants.PAGE, AdvertisementFragment.class.getName());
                                    Bundle bundle = new Bundle();
                                    bundle.putInt(Constants.SHOP_ID,list.get(0));
                                    bundle.putInt(Constants.FAMOUS_ID,id);
                                    intent.putExtra(Constants.BUNDLE,bundle);
                                    startActivity(intent);
//                                    famousAlbumsUserDetailsViewModel.submitFilter(list);
                                }
                            }
                        });
                    } else {
                        bottomSheetDialogHelper.filterDialog.show();
                    }
                }
            }
        });
    }




    private void setEventAdapter() {
        adapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;

            }
        });
    }

}
