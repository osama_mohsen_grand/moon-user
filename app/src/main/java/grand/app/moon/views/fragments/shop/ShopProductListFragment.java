package grand.app.moon.views.fragments.shop;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ProductAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentShopProductListBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.product.ShopProductListDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.ads.AdvertisementDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopProductListFragment extends BaseFragment {


    private FragmentShopProductListBinding fragmentShopProductBinding;
    private ShopProductListDetailsViewModel shopProductListDetailsViewModel;
    private ProductAdapter productAdapter;
    private String category_id,shop_id;
    private Service service;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentShopProductBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_product_list, container, false);
        getData();
        return fragmentShopProductBinding.getRoot();
    }

    private void getData() {
        Log.d(TAG,"getData:");
        if(getArguments() != null && getArguments().containsKey(Constants.ID))
            shop_id = getArguments().getString(Constants.ID);
        if(getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID))
            category_id = getArguments().getString(Constants.CATEGORY_ID);
        if(getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        Log.d(TAG,"serviceType:"+service.mType);
    }

    private void bind() {
        shopProductListDetailsViewModel = new ShopProductListDetailsViewModel(category_id,shop_id,service.mType,service.mFlag);
        AppUtils.initVerticalRV(fragmentShopProductBinding.rvShopProduct, fragmentShopProductBinding.rvShopProduct.getContext(), 1);
        fragmentShopProductBinding.setShopProductViewModel(shopProductListDetailsViewModel);
    }

    private void setEvent() {
        shopProductListDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, shopProductListDetailsViewModel.getProductRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.PRODUCTS)) {
                    if(shopProductListDetailsViewModel.getProductRepository().getData().products.size() > 0) {
                        productAdapter = new ProductAdapter(shopProductListDetailsViewModel.getProductRepository().getData().products);
                        fragmentShopProductBinding.rvShopProduct.setAdapter(productAdapter);
                        shopProductListDetailsViewModel.notifyChange();
                        setEventAdapter();
                    }else
                        shopProductListDetailsViewModel.noDataTextDisplay.set(true);

                }
            }
        });
    }

    private void setEventAdapter() {
        productAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Intent intent = new Intent(context, BaseActivity.class);
                ProductList productList = shopProductListDetailsViewModel.getProductRepository().getData().products.get(pos);
                intent.putExtra(Constants.NAME_BAR, productList.name);
                Bundle bundle = new Bundle();

                if(service.mType == Integer.parseInt(Constants.TYPE_COMPANIES) || service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING)){
                    intent.putExtra(Constants.PAGE, AdvertisementDetailsFragment.class.getName());
                }else {
                    intent.putExtra(Constants.PAGE, ShopProductDetailsFragment.class.getName());
                    bundle.putSerializable(Constants.PRODUCT, productList);
                }
                bundle.putInt(Constants.TYPE,service.mType);
                bundle.putInt(Constants.ID,Integer.parseInt(productList.id));
                bundle.putSerializable(Constants.SERVICE, service);
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }
        });
    }

    private static final String TAG = "ShopProductListFragment";
    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: " );
        Log.e(TAG, "working here: " );
        bind();
        setEvent();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(shopProductListDetailsViewModel != null)
            shopProductListDetailsViewModel.reset();
    }
}
