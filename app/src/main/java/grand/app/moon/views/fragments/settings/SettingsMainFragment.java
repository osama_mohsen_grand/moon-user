package grand.app.moon.views.fragments.settings;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentSettingsMainBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.app.SettingsMainViewModel;
import grand.app.moon.viewmodels.app.SettingsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsMainFragment extends BaseFragment {

    private FragmentSettingsMainBinding binding;
    private SettingsMainViewModel viewModel;
    private int type = 1;
    private String nameBar;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings_main, container, false);
        getData();
        bind();
        setEvent();
        return binding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)){
            type = getArguments().getInt(Constants.TYPE);
        }
    }

    private void bind() {
        viewModel = new SettingsMainViewModel(type);
        binding.setViewModel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getSettingsRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){ // success add image
                    viewModel.adapter.update(viewModel.getSettingsRepository().settingsResponse.data);
                }
            }
        });
    }



}
