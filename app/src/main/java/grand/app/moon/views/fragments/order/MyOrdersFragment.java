package grand.app.moon.views.fragments.order;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.OrderAdapter;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentHomeBinding;
import grand.app.moon.databinding.FragmentMyOrdersBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.models.order.Order;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.order.OrderListViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrdersFragment extends BaseFragment {


    private FragmentMyOrdersBinding fragmentMyOrdersBinding;
    private OrderListViewModel orderListViewModel;
    private OrderAdapter orderAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentMyOrdersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);
        bind();
        setEvent();
        return fragmentMyOrdersBinding.getRoot();
    }

    private void bind() {
        orderListViewModel = new OrderListViewModel();
        AppUtils.initVerticalRV(fragmentMyOrdersBinding.rvMyOrders, fragmentMyOrdersBinding.rvMyOrders.getContext(), 1);
        fragmentMyOrdersBinding.setOrderListViewModel(orderListViewModel);
    }

    private static final String TAG = "MyOrdersFragment";
    private void setEvent() {
        orderListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, orderListViewModel.getOrderRepository().getMessage());
                Log.d(TAG,action);
                assert action != null;
                if (action.equals(Constants.ORDERS)) {
                    if(orderListViewModel.getOrderRepository().getOrderListResponse().orders.size() > 0) {
                        orderAdapter = new OrderAdapter(orderListViewModel.getOrderRepository().getOrderListResponse().orders);
                        fragmentMyOrdersBinding.rvMyOrders.setAdapter(orderAdapter);
                        setEventAdapter();
                    }else{
                        orderListViewModel.noData();
                    }
                }else if(action.equals(Constants.DELETED)){
                    orderAdapter.remove(orderListViewModel.delete_position);
                }
            }
        });
    }

    private void setEventAdapter() {

        orderAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                int pos = (int) mutable.position;
                if(mutable.type.equals(Constants.SUBMIT)) {
                    Timber.e("position:" + pos);
                    Order order = orderListViewModel.getOrderRepository().getOrderListResponse().orders.get(pos);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ORDER_DETAILS);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.ID, order.id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.order_details));
                    startActivity(intent);
                }else if(mutable.type.equals(Constants.DELETE)){
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.delete))
                            .setMessage(getString(R.string.do_you_want_delete_order))
                            .setActionText(ResourceManager.getString(R.string.continue_))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    orderListViewModel.deleteOrder(pos);
                                }
                            });
                }

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        orderListViewModel.reset();
    }

}
