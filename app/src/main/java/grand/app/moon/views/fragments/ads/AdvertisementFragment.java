package grand.app.moon.views.fragments.ads;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AlbumAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentAdvertisementBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.ads.AdvertisementDetailsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementFragment extends BaseFragment {

    private Context context;
    private FragmentAdvertisementBinding binding;
    private AdvertisementDetailsViewModel viewModel;
    public int famous_id = -1,shop_id=-1;
    public String type = "", tab = "";
    //    private AlbumImagesAdapter albumImagesAdapter;
    private AlbumAdapter albumAdapter = new AlbumAdapter(new ArrayList<>(),true);
    private ArrayList<ImageVideo> data = null;

    private static final String TAG = "AdvertisementFragment";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_advertisement, container, false);
        Log.d(TAG,"binding");
        getData();
        bind();
        setEvent();
        return binding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.FAMOUS_ID))
            famous_id = getArguments().getInt(Constants.FAMOUS_ID);
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_ID))
            shop_id = getArguments().getInt(Constants.SHOP_ID);

    }

    private void bind() {
        AppUtils.initVerticalRV(binding.rvFamousAlbumDetails, binding.rvFamousAlbumDetails.getContext(), 3);
        setAdapter();
        viewModel = new AdvertisementDetailsViewModel();
        if(famous_id != -1 && shop_id != -1) viewModel.getFamousShopAds(famous_id,shop_id);
        binding.setViewmodel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getAdvertisementCompanyRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.FAMOUS_SHOP_ADS)) {
                    data = new ArrayList<>(viewModel.getAdvertisementCompanyRepository().getAdsResponse().data);
                    albumAdapter.update(data);
                }
            }
        });
    }

    public void setAdapter(){
        albumAdapter = new AlbumAdapter(data,true);
        binding.rvFamousAlbumDetails.setAdapter(albumAdapter);
//        setEventAdapter();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
