package grand.app.moon.views.fragments.reservation;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ClinicListAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentClinicListBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.clinic.ClinicModel;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.reservation.ClinicListViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClinicListFragment extends BaseFragment {


    private FragmentClinicListBinding fragmentClinicListBinding;
    public ClinicListViewModel clinicListViewModel;
    private ClinicListAdapter clinicListAdapter;
    private String category_id, shop_id, type;
    private Service service;
    private int filter = 1;
    ArrayList<Integer> tags;
    ServiceDetailsFragment serviceDetailsFragment;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentClinicListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_list, container, false);
        serviceDetailsFragment = ((ServiceDetailsFragment) this.getParentFragment());
        getData();
        bind();
        setEvent();
        return fragmentClinicListBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            shop_id = getArguments().getString(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID))
            category_id = getArguments().getString(Constants.CATEGORY_ID);
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
    }

    private void bind() {
        clinicListViewModel = new ClinicListViewModel(type, service, serviceDetailsFragment.service_id, serviceDetailsFragment.tags, filter);
        AppUtils.initVerticalRV(fragmentClinicListBinding.rvClinic, fragmentClinicListBinding.rvClinic.getContext(), 1);
        fragmentClinicListBinding.setClinicListViewModel(clinicListViewModel);
    }

    private static final String TAG = "ClinicListFragment";
    private void setEvent() {
        clinicListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, clinicListViewModel.getClinicRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    if (clinicListViewModel.getClinicRepository().getClinicServiceResponse().mData.clinics.size() > 0) {
                        clinicListAdapter = new ClinicListAdapter(clinicListViewModel.getClinicRepository().getClinicServiceResponse().mData.clinics,String.valueOf(service.mType));
                        fragmentClinicListBinding.rvClinic.setAdapter(clinicListAdapter);
                        setEventAdapter();
                        clinicListViewModel.haveData();
                    } else
                        clinicListViewModel.noData();
                    setData();
                    clinicListViewModel.showPage(true);
                } else if (action.equals(Constants.FOLLOW)) {
                    List<ClinicModel> clinicModels = clinicListViewModel.getClinicRepository().getClinicServiceResponse().mData.clinics;
                    clinicModels.get(clinicListViewModel.followPosition).isFollow = !clinicModels.get(clinicListViewModel.followPosition).isFollow;
                    clinicListViewModel.getClinicRepository().getClinicServiceResponse().mData.clinics = clinicModels;
                    clinicListAdapter.update(clinicModels);
                }
            }
        });
    }

    private void setEventAdapter() {
        clinicListAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                ClinicModel clinicModel = clinicListViewModel.getClinicRepository().getClinicServiceResponse().mData.clinics.get(mutable.position);
                if (mutable.type.equals(Constants.FOLLOW)) {
                    clinicListViewModel.followPosition = mutable.position;
                    clinicListViewModel.getClinicRepository().follow(new FollowRequest(clinicModel.id, service.mType));
                } else if (mutable.type.equals(Constants.DETAILS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.CLINIC_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, clinicModel.name);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, clinicModel.id);
                    bundle.putSerializable(Constants.SERVICE,service);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }

    private void setData() {
        serviceDetailsFragment.setService(clinicListViewModel.getClinicRepository().getClinicServiceResponse().mData.mServices);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (clinicListViewModel != null)
            clinicListViewModel.callService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (clinicListViewModel != null)
            clinicListViewModel.reset();
    }

}
