package grand.app.moon.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.FamousAdapter;
import grand.app.moon.adapter.SearchAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFamousSearchBinding;
import grand.app.moon.models.famous.list.Famous;
import grand.app.moon.models.search.SearchModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.search.FamousSearchViewModel;
import grand.app.moon.views.activities.BaseActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class FamousSearchFragment extends BaseFragment {

    private FragmentFamousSearchBinding fragmentFamousSearchBinding;
    public FamousSearchViewModel famousSearchViewModel;
    SearchAdapter searchAdapter;
    int type = 4;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UserHelper.saveKey(Constants.RELOAD,Constants.FALSE);
        fragmentFamousSearchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_search, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousSearchBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
    }


    private void bind() {
        famousSearchViewModel = new FamousSearchViewModel(type);
        AppUtils.initVerticalRV(fragmentFamousSearchBinding.rvFamous, fragmentFamousSearchBinding.rvFamous.getContext(), 1);
        fragmentFamousSearchBinding.setFamousSearchViewModel(famousSearchViewModel);
        fragmentFamousSearchBinding.search.setActivated(true);
        fragmentFamousSearchBinding.search.setQueryHint(ResourceManager.getString(R.string.search_hint));
        fragmentFamousSearchBinding.search.onActionViewExpanded();
        fragmentFamousSearchBinding.search.setIconified(false);
        fragmentFamousSearchBinding.search.clearFocus();
        fragmentFamousSearchBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String s) {
                famousSearchViewModel.text =s;
                famousSearchViewModel.submitSearch();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        
    }

    @Override
    public void onResume() {
        super.onResume();
        if(UserHelper.retrieveKey(Constants.RELOAD).equals(Constants.TRUE) && famousSearchViewModel != null) {
            famousSearchViewModel.submitSearch();
            UserHelper.saveKey(Constants.RELOAD,Constants.TRUE);
        }
    }



    private void setEvent() {
        famousSearchViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousSearchViewModel.getSearchRepository().getMessage());
                assert action != null;

                if (action.equals(Constants.SUCCESS)) {
                    if(famousSearchViewModel.getSearchRepository().getSearchResponse().data.size() > 0) {
                        famousSearchViewModel.showPage(true);
                        famousSearchViewModel.noDataTextDisplay.set(false);
                        searchAdapter = new SearchAdapter();
                        fragmentFamousSearchBinding.rvFamous.setAdapter(searchAdapter);
                        searchAdapter.setList(famousSearchViewModel.getSearchRepository().getSearchResponse().data);
                        setEventAdapter();
                    }else{
                        famousSearchViewModel.noDataTextDisplay.set(true);
                    }
                }
            }
        });
    }


    private void setEventAdapter() {
        searchAdapter.getMutableLiveData().observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
//                int position = (int) o;
                SearchModel searchModel = (SearchModel) o;
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
                intent.putExtra(Constants.NAME_BAR, searchModel.name);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ID, searchModel.id);
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onDestroy() {
        famousSearchViewModel.reset();
        super.onDestroy();
    }

}
