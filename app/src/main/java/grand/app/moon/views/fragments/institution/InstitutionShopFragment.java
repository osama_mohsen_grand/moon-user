package grand.app.moon.views.fragments.institution;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.InstitutionAdapter;
import grand.app.moon.adapter.ShopAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentInstitutionShopBinding;
import grand.app.moon.databinding.FragmentServiceShopBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.institution.InstitutionViewModel;
import grand.app.moon.viewmodels.service.ServiceShopViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstitutionShopFragment extends BaseFragment {

    private FragmentInstitutionShopBinding fragmentInstitutionShopBinding;
    public InstitutionViewModel institutionViewModel;
    private String type;
    private int filter = 1;
    ArrayList<Integer> tags;
    private Service service;
    InstitutionAdapter institutionAdapter;
    ServiceDetailsFragment serviceDetailsFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        fragmentInstitutionShopBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_institution_shop, container, false);
        serviceDetailsFragment = ((ServiceDetailsFragment) this.getParentFragment());
        getData();
        bind();
        setEvent();
        return fragmentInstitutionShopBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
    }


    private void bind() {
        institutionViewModel = new InstitutionViewModel(type, service, serviceDetailsFragment.service_id,serviceDetailsFragment.tags,filter);
        AppUtils.initVerticalRV(fragmentInstitutionShopBinding.rvServiceInstitution, fragmentInstitutionShopBinding.rvServiceInstitution.getContext(), 1);
        fragmentInstitutionShopBinding.setInstitutionViewModel(institutionViewModel);
    }

    private void setEvent() {
        institutionViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, institutionViewModel.getHomeRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    institutionViewModel.showPage(true);
                    institutionAdapter = new InstitutionAdapter(institutionViewModel.getHomeRepository().getServiceDetailsResponse().mData.mShops);
                    fragmentInstitutionShopBinding.rvServiceInstitution.setAdapter(institutionAdapter);
                    setData();
                    setEventAdapter();
                    if(institutionAdapter.getItemCount() > 0){
                        institutionViewModel.haveData();
                    }else{
                        institutionViewModel.noData();
                    }
                }
            }
        });
    }



    private void setEventAdapter() {
        institutionAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                ShopDetails shop = institutionViewModel.getHomeRepository().getServiceDetailsResponse().mData.mShops.get(pos);
                Bundle bundle = getArguments();
                ShopDetailsFragment shopDetailsFragment = new ShopDetailsFragment();
                bundle.putInt(Constants.ID, shop.mId);
                shopDetailsFragment.setArguments(bundle);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
                intent.putExtra(Constants.BUNDLE,bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(institutionViewModel != null)
            institutionViewModel.callService();
    }

    private void setData() {
        serviceDetailsFragment.setService(institutionViewModel.getHomeRepository().getServiceDetailsResponse().mData.mServices);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(institutionViewModel != null)
            institutionViewModel.reset();
    }
}
