package grand.app.moon.views.fragments.order;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.CartAdapter;
import grand.app.moon.adapter.OrderDetailsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentCartBinding;
import grand.app.moon.databinding.FragmentOrderDetailsBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.cart.DeleteCartRequest;
import grand.app.moon.models.cart.Item;
import grand.app.moon.models.order.details.OrderDetails;
import grand.app.moon.models.order.details.OrderProductDetails;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.order.details.OrderDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MapAddressActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailsFragment extends BaseFragment {

    View rootView;
    private FragmentOrderDetailsBinding fragmentOrderDetailsBinding;
    private OrderDetailsViewModel orderDetailsViewModel;
    OrderDetailsAdapter orderDetailsAdapter;
    int order_id = 0;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentOrderDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            order_id = getArguments().getInt(Constants.ID);
    }

    private void bind() {
        orderDetailsViewModel = new OrderDetailsViewModel(order_id);
        fragmentOrderDetailsBinding.setOrderDetailsViewModel(orderDetailsViewModel);
        AppUtils.initVerticalRV(fragmentOrderDetailsBinding.rvOrderDetails, fragmentOrderDetailsBinding.rvOrderDetails.getContext(), 1);
        rootView = fragmentOrderDetailsBinding.getRoot();
    }

    private void setEvent() {
        orderDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, orderDetailsViewModel.getOrderRepository().getMessage());
                if (action.equals(Constants.ORDER_DETAILS)) {
                    orderDetailsAdapter = new OrderDetailsAdapter(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.products);
                    fragmentOrderDetailsBinding.rvOrderDetails.setAdapter(orderDetailsAdapter);
                    if (orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate != null) {
                        orderDetailsViewModel.haveDelegate = true;
                    }
                    if (orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.shop != null) {
                        orderDetailsViewModel.haveShop = true;
                    }
                    orderDetailsViewModel.setStatusChat();
                    orderDetailsViewModel.showPage(true);
                } else if (action.equals(Constants.DELEGATE_LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lat));
                    intent.putExtra(Constants.LNG, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lng));
                    startActivity(intent);
                } else if (action.equals(Constants.SHOP_LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.shop.lat));
                    intent.putExtra(Constants.LNG, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.shop.lng));
                    startActivity(intent);
                } else if (action.equals(Constants.CHAT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    OrderDetails orderDetails = orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data;
                    bundle.putInt(Constants.ID,orderDetails.orderId);
                    bundle.putInt(Constants.TYPE,orderDetails.delegate.type);
                    bundle.putString(Constants.NAME,orderDetails.delegate.name);
                    bundle.putString(Constants.IMAGE,orderDetails.delegate.image);
                    boolean allowChat = (orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.statusId==4 ? false : true);
                    bundle.putBoolean(Constants.ALLOW_CHAT,allowChat);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);

                } else if (action.equals(Constants.CHAT_SHOP)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    OrderDetails orderDetails = orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data;
                    bundle.putInt(Constants.ID,orderDetails.orderId);
                    bundle.putInt(Constants.TYPE,orderDetails.shop.type);
                    bundle.putString(Constants.NAME,orderDetails.shop.name);
                    bundle.putString(Constants.IMAGE,orderDetails.shop.image);
                    bundle.putBoolean(Constants.ALLOW_CHAT,true);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);

                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (orderDetailsViewModel != null) orderDetailsViewModel.reset();
    }


}
