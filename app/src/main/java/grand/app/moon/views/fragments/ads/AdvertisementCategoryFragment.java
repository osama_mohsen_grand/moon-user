package grand.app.moon.views.fragments.ads;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ServiceCategoryFilterAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentAdvertisementCategoryBinding;
import grand.app.moon.models.ads.AdsDetails;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementCategoryFragment extends BaseFragment {
    public ServiceCategoryFilterAdapter serviceCategoryFilterAdapter;
    public FragmentAdvertisementCategoryBinding fragmentAdsCategoryBinding;
    AdsDetails adsDetails;
    int service_id = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentAdsCategoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_advertisement_category, container, false);
        AppUtils.initVerticalRV(fragmentAdsCategoryBinding.rvAdsCategory, fragmentAdsCategoryBinding.rvAdsCategory.getContext(), 2);
        getData();
        return fragmentAdsCategoryBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ADS_MAIN)){
            adsDetails = (AdsDetails) getArguments().getSerializable(Constants.ADS_MAIN);
            assert adsDetails != null;
            serviceCategoryFilterAdapter = new ServiceCategoryFilterAdapter(adsDetails.mServices);
            serviceCategoryFilterAdapter.selected = -1;
            fragmentAdsCategoryBinding.rvAdsCategory.setAdapter(serviceCategoryFilterAdapter);

            serviceCategoryFilterAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                @Override
                public void onChanged(@Nullable Object o) {
                    int position = (int) o;
                    Timber.e("pos:"+position);
                    serviceCategoryFilterAdapter.selected = -1;
                    service_id = adsDetails.mServices.get(position).mId;

                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADS_COMPANY_FILTER);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FROM,Constants.ADD_ADS);
                    bundle.putInt(Constants.ID,service_id);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivityForResult(intent, Constants.FILTER_RESULT);

                }
            });


        }
    }


}
