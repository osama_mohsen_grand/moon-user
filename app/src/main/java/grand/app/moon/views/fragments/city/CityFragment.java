package grand.app.moon.views.fragments.city;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.CountryAdapter;
import grand.app.moon.adapter.TagsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentCityBinding;
import grand.app.moon.databinding.FragmentCountryBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.CountriesResponse;
import grand.app.moon.models.country.Datum;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.city.CityViewModel;
import grand.app.moon.viewmodels.country.CountryViewModel;
import grand.app.moon.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityFragment extends BaseFragment {


    View rootView;
    private FragmentCityBinding fragmentCityBinding;
    private CityViewModel cityViewModel;
    private TagsAdapter tagsAdapter;

    private ArrayList<Integer> selected = new ArrayList<>();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentCityBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_city, container, false);
        getData();
        bind();
        rootView = fragmentCityBinding.getRoot();
        return rootView;
    }
    List<IdNameImage> cities = new ArrayList<>();

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            selected.add(getArguments().getInt(Constants.ID));
        }
    }



    private void bind() {
        CountriesResponse countriesResponse = UserHelper.getCountries();
        for(Datum datum : countriesResponse.data){
            if(datum.id == Integer.parseInt(UserHelper.retrieveKey(Constants.COUNTRY_ID))){
                for(City city : datum.cities){
                    IdNameImage idNameImage = new IdNameImage();
                    idNameImage.id = city.id+"";
                    idNameImage.name = city.cityName;
                    cities.add(idNameImage);
                }
                break;
            }
        }
        cityViewModel = new CityViewModel();
        setEvents();
        AppUtils.initVerticalRV(fragmentCityBinding.rvCity, fragmentCityBinding.rvCity.getContext(), 1);
        tagsAdapter = new TagsAdapter(cities,selected);
        fragmentCityBinding.rvCity.setAdapter(tagsAdapter);
        fragmentCityBinding.setCityViewModel(cityViewModel);
        setEventAdapter();
    }

    private void setEvents() {
        cityViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if(action.equals(Constants.SUBMIT)){
                    Intent intent=new Intent();
                    if(selected.size() > 0){
                        intent.putExtra(Constants.SELECTED,selected.get(0));
                    }
                    ((ParentActivity)context).setResult(Constants.CITY_RESULT,intent);
                    ((ParentActivity)context).finish();//finishing activity
                }
            }
        });
    }

    private void setEventAdapter() {
        tagsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                selected.clear();
                IdNameImage tag = cities.get(pos);
                int id = Integer.parseInt(tag.id);
                selected.add(id);
                tagsAdapter.update(selected);

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cityViewModel.reset();
    }

}
