package grand.app.moon.views.activities;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import androidx.fragment.app.Fragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.base.ParentViewModel;
import grand.app.moon.customviews.dialog.rate.DialogRateModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.product.Product;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.notification.NotificationGCMModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.LanguagesHelper;
import grand.app.moon.utils.MovementHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.views.fragments.LanguageFragment;
import grand.app.moon.views.fragments.NotificationFragment;
import grand.app.moon.views.fragments.OpenLocationFragment;
import grand.app.moon.views.fragments.PrivacyFragment;
import grand.app.moon.views.fragments.ads.AddAdvertisementFragment;
import grand.app.moon.views.fragments.ads.AdvertisementCategoryFragment;
import grand.app.moon.views.fragments.ads.AdvertisementDetailsFragment;
import grand.app.moon.views.fragments.ads.FamousCompanyAdsCategoriesFragment;
import grand.app.moon.views.fragments.adsCompanyFilter.AdsCompanyFilterFragment;
import grand.app.moon.views.fragments.ads.AdvertisementMainFragment;
import grand.app.moon.views.fragments.auth.ProfileFragment;
import grand.app.moon.views.fragments.famous.FamousSearchFragment;
import grand.app.moon.views.fragments.map.MapBookingFragment;
import grand.app.moon.views.fragments.rate.RateFragment;
import grand.app.moon.views.fragments.auth.UpdateInfoFragment;
import grand.app.moon.views.fragments.base.NoConnectionFragment;
import grand.app.moon.views.fragments.beauty.BookBeautyFragment;
import grand.app.moon.views.fragments.cart.CheckoutFragment;
import grand.app.moon.views.fragments.chat.ChatFragment;
import grand.app.moon.views.fragments.city.CityFragment;
import grand.app.moon.views.fragments.clinic.BookClinicFragment;
import grand.app.moon.views.fragments.company.CompaniesFragment;
import grand.app.moon.views.fragments.famous.FamousAlbumMainDetailsFragment;
import grand.app.moon.views.fragments.famous.FamousDetailsFragment;
import grand.app.moon.views.fragments.famous.FamousDiscover;
import grand.app.moon.views.fragments.famous.FamousListFragment;
import grand.app.moon.views.fragments.order.OrderDetailsFragment;
import grand.app.moon.views.fragments.rate.RateNotificationFragment;
import grand.app.moon.views.fragments.reservation.ClinicDetailsFragment;
import grand.app.moon.views.fragments.reservation.ReservationDetailsFragment;
import grand.app.moon.views.fragments.reservation.ReservationMainFragment;
import grand.app.moon.views.fragments.review.AddReviewFragment;
import grand.app.moon.views.fragments.review.ReviewFragment;
import grand.app.moon.views.fragments.search.SearchFragment;
import grand.app.moon.views.fragments.shop.ShopAlbumFragment;
import grand.app.moon.views.fragments.trip.TripDetailsFragment;
import grand.app.moon.views.fragments.trip.TripHistoryFragment;
import grand.app.moon.R;
import grand.app.moon.customviews.actionbar.BackActionBarView;
import grand.app.moon.databinding.ActivityBaseBinding;
import grand.app.moon.viewmodels.common.BaseViewModel;
import grand.app.moon.views.fragments.GoHomeFragment;
import grand.app.moon.views.fragments.auth.LoginFragment;
import grand.app.moon.views.fragments.SplashFragment;
import grand.app.moon.views.fragments.auth.VerificationCodeFragment;
import grand.app.moon.views.fragments.auth.ChangePasswordFragment;
import grand.app.moon.views.fragments.auth.PhoneVerificationFragment;
import grand.app.moon.views.fragments.auth.RegisterFragment;
import grand.app.moon.views.fragments.base.VideoFragment;
import grand.app.moon.views.fragments.base.ZoomFragment;
import grand.app.moon.views.fragments.filter.FilterFragment;
import grand.app.moon.views.fragments.intro.IntroFragment;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;
import grand.app.moon.views.fragments.gallery.GalleryFragment;
import grand.app.moon.views.fragments.serviceFilter.TagFragment;
import grand.app.moon.views.fragments.settings.CountryFragment;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;
import grand.app.moon.views.fragments.shop.ShopProductDetailsFragment;
import grand.app.moon.views.fragments.story.StoryFragment;
import grand.app.moon.views.fragments.trucks.TruckPickupFormFragment;
import grand.app.moon.views.fragments.trucks.TrucksFragment;
import grand.app.moon.vollyutils.MyApplication;
import timber.log.Timber;

public class BaseActivity extends ParentActivity {

    ActivityBaseBinding activityBaseBinding;
    ParentViewModel baseViewModel;

    public BackActionBarView backActionBarView;
    boolean notification_checked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        LanguagesHelper.setLanguage(this,"ar");
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        getNotification();
        initPages();
//        initDynamicLink();
        baseViewModel = new BaseViewModel();
        activityBaseBinding.setBaseViewModel(baseViewModel);


//        FirebaseDynamicLinks.getInstance().createDynamicLink().buildDynamicLink().getUri()
        Log.d(TAG, "notification_checked:" + notification_checked);
        // ATTENTION: This was auto-generated to handle app links.
//        Intent appLinkIntent = getIntent();
//        String appLinkAction = appLinkIntent.getAction();
//        Uri appLinkData = appLinkIntent.getData();
//        handleIntent(getIntent());

    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
//        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        Log.d(TAG,"here");
        Log.d(TAG,"worked");
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            Log.d(TAG,"appLinkData:"+appLinkData.toString());
            Timber.d("appLinkAction:" + appLinkAction.toString());
            Uri appData = Uri.parse("content://com.moon4online").buildUpon().build();

        }
    }


    void initPages(){
        if (!notification_checked) {
            Log.d(TAG,"notification_checked: is false");

            if (getIntent().getExtras() != null) {
                Log.e("page", "getIntent()");
                if (getIntent().getExtras() != null) {
                    if (getIntent().hasExtra(Constants.PAGE)) {
                        String fragmentName = getIntent().getStringExtra(Constants.PAGE);
//                        liveData.setValue(new Mutable());
                        Timber.e("Fragment New Instance");
                        Fragment fragment = null;
                        try {
                            fragment = (Fragment) Class.forName(fragmentName).newInstance();
                            MovementHelper.addFragmentTag(this, getBundle(fragment), fragmentName, "");
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LOGIN)) {
                            MovementHelper.replaceFragment(this, new LoginFragment(), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAILURE_CONNECTION)) {
                            MovementHelper.replaceFragment(this, getBundle(new NoConnectionFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CHANGE_PASSWORD)) {
                            MovementHelper.replaceFragment(this, getBundle(new ChangePasswordFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LANGUAGE)) {
                            MovementHelper.replaceFragment(this, getBundle(new LanguageFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.REGISTRATION)) {
                            MovementHelper.replaceFragment(this, getBundle(new RegisterFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PHONE_VERIFICATION)) {
                            MovementHelper.replaceFragment(this, getBundle(new PhoneVerificationFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.VERIFICATION)) {
                            MovementHelper.replaceFragment(this, getBundle(new VerificationCodeFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SPLASH)) {
                            MovementHelper.replaceFragment(this, getBundle(new SplashFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.INTRO)) {
                            MovementHelper.replaceFragment(this, getBundle(new IntroFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.COUNTRIES)) {
                            MovementHelper.replaceFragment(this, getBundle(new CountryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LOCATION)) {
                            MovementHelper.replaceFragment(this, getBundle(new OpenLocationFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.WRITE_CODE)) {
                            MovementHelper.replaceFragment(this, getBundle(new VerificationCodeFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CHAT_DETAILS)) {
                            MovementHelper.replaceFragment(this, getBundle(new ChatFragment()), "");
                        }  else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.VERIFICATION)) {
                            MovementHelper.replaceFragment(this, getBundle(new VerificationCodeFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PRIVACY_POLICY)) {
                            MovementHelper.replaceFragment(this, getBundle(new PrivacyFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.HISTORY)) {
                            MovementHelper.replaceFragment(this, getBundle(new TripHistoryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.GO_HOME)) {
                            MovementHelper.replaceFragment(this, getBundle(new GoHomeFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PROFILE)) {
                            MovementHelper.replaceFragment(this, getBundle(new ProfileFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.UPDATE_PROFILE)) {
                            MovementHelper.replaceFragment(this, getBundle(new UpdateInfoFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.RATE)) {
                            MovementHelper.replaceFragment(this, getBundle(new RateFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.RATE_NOTIFICATION)) {
                            MovementHelper.replaceFragment(this, getBundle(new RateNotificationFragment()), "");
//                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PAYMENT)) {
//                            MovementHelper.replaceFragment(this, getBundle(new PaymentFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ZOOM)) {
                            MovementHelper.replaceFragment(this, getBundle(new ZoomFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.VIDEO)) {
                            MovementHelper.replaceFragment(this, getBundle(new VideoFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.TAGS)) {
                            MovementHelper.replaceFragment(this, getBundle(new TagFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SERVICE_DETAILS)) {
                            MovementHelper.replaceFragment(this, getBundle(new ServiceDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.INSTITUTIONS)) {
                            MovementHelper.replaceFragment(this, getBundle(new ServiceDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.GALLERY)) {
                            MovementHelper.replaceFragment(this, getBundle(new GalleryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.STORY)) {
                            MovementHelper.replaceFragment(this, getBundle(new StoryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SHOP_DETAILS)) {
                            MovementHelper.replaceFragment(this, getBundle(new ShopDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PRODUCT_DETAILS)) {
                            MovementHelper.replaceFragment(this, getBundle(new ShopProductDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FILTER)) {
                            MovementHelper.replaceFragment(this, getBundle(new FilterFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.REVIEW)) {
                            MovementHelper.replaceFragment(this, getBundle(new ReviewFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_REVIEW)) {
                            MovementHelper.replaceFragment(this, getBundle(new AddReviewFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CHECKOUT)) {
                            MovementHelper.replaceFragment(this, getBundle(new CheckoutFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ORDER_DETAILS)) {
                            MovementHelper.replaceFragment(this, getBundle(new OrderDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAMOUS)) {
                            MovementHelper.addFragmentTag(this, getBundle(new FamousListFragment()), Constants.FAMOUS_MAIN_DETAILS, "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAMOUS_DETAILS)) {
                            MovementHelper.addFragmentTag(this, getBundle(new FamousDetailsFragment()), Constants.FAMOUS_DETAILS, "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ALBUM_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new FamousAlbumMainDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.DISCOVER)) {
                            MovementHelper.addFragment(this, getBundle(new FamousDiscover()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADS_MAIN)) {
                            MovementHelper.addFragment(this, getBundle(new AdvertisementMainFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CITY)) {
                            MovementHelper.addFragment(this, getBundle(new CityFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADS_COMPANY_FILTER)) {
                            MovementHelper.addFragment(this, getBundle(new AdsCompanyFilterFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADS_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new AdvertisementDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADS_CATEGORY)) {
                            MovementHelper.addFragment(this, getBundle(new AdvertisementCategoryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CATEGORY_COMPANY_ADS)) {
                            MovementHelper.addFragment(this, getBundle(new FamousCompanyAdsCategoriesFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SHOP_ALBUM)) {
                            MovementHelper.addFragment(this, getBundle(new ShopAlbumFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_ADS)) {
                            MovementHelper.addFragment(this, getBundle(new AddAdvertisementFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.COMPANIES)) {
                            MovementHelper.addFragment(this, getBundle(new CompaniesFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CLINIC_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new ClinicDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.BOOK_CLINIC)) {
                            MovementHelper.addFragment(this, getBundle(new BookClinicFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.BOOK_BEAUTY)) {
                            MovementHelper.addFragment(this, getBundle(new BookBeautyFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.RESERVATION)) {
                            MovementHelper.addFragment(this, getBundle(new ReservationMainFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.RESERVATION_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new ReservationDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.TRUCKS)) {
                            MovementHelper.addFragment(this, getBundle(new TrucksFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.MAP_BOOKING)) {
                            MovementHelper.addFragment(this, getBundle(new MapBookingFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PICKUP_FORM)) {
                            MovementHelper.addFragment(this, getBundle(new TruckPickupFormFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.TRIP_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new TripDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SEARCH)) {
                            MovementHelper.addFragment(this, getBundle(new SearchFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAMOUS_SEARCH)) {
                            MovementHelper.addFragment(this, getBundle(new FamousSearchFragment()), "");
                        }
                    } else
                        MovementHelper.replaceFragment(this, new SplashFragment(), "");
                } else {
                    MovementHelper.replaceFragment(this, new SplashFragment(), "");
                }
            } else {
                MovementHelper.replaceFragment(this, new SplashFragment(), "");
            }
        }

    }

    private void initDynamicLink() {
        FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
            @Override
            public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                Log.d(TAG,"we have a dynamic link");
                Uri deepLink = null;
                if(pendingDynamicLinkData != null){
                    deepLink = pendingDynamicLinkData.getLink();
                    if(deepLink != null){
                        notification_checked = true;
                        int product_id = Integer.parseInt(deepLink.getQueryParameter("product_id"));
                        int flag = Integer.parseInt(deepLink.getQueryParameter("flag"));
                        Service service = new Service();
                        service.mFlag = flag;
                        ProductList product = new ProductList();
                        product.id = product_id+"";
                        ShopProductDetailsFragment shopProductDetailsFragment = new ShopProductDetailsFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Constants.PRODUCT,product);
                        bundle.putSerializable(Constants.SERVICE, service);
                        setTitleName(deepLink.getQueryParameter("name"));
                        shopProductDetailsFragment.setArguments(bundle);
                        MovementHelper.replaceFragment(BaseActivity.this, shopProductDetailsFragment, "");
//                        finishAffinity();
//                        Intent intent = new Intent(BaseActivity.this,BaseActivity.class);
//                        intent.putExtra(Constants.NAME_BAR,ResourceManager.getString(R.string.details));
//                        intent.putExtra(Constants.BUNDLE,bundle);
//                        intent.putExtra(Constants.PAGE,ShopProductDetailsFragment.class.getName());
//                        startActivity(intent);
                        return;
                    }
                }else{
                    initPages();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG,"we onFailure a dynamic link");
                Toast.makeText(BaseActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setTitleName() {
        Timber.e("Title Name");
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            Timber.e("Title Name Done");
            backActionBarView = new BackActionBarView(this);
            backActionBarView.setTitle(getIntent().getStringExtra(Constants.NAME_BAR));
            if (getIntent() != null && getIntent().hasExtra(Constants.PAGE)) {
                backActionBarView.setSearchVisibility(getIntent().getStringExtra(Constants.PAGE), getIntent().getBundleExtra(Constants.BUNDLE));
                String page = getIntent().getStringExtra(Constants.PAGE);
                if (page.equals(Constants.SERVICE_DETAILS) || page.equals(Constants.ADS_MAIN) || page.equals(Constants.COMPANIES)) {
                    backActionBarView.showAddress();
                }
            }
            activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
//        initDynamicLink();
    }

    private void setTitleName(String name) {
        backActionBarView = new BackActionBarView(this);
        backActionBarView.setTitle(name);
        activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);
    }

    private Fragment getBundle(Fragment fragment) {
        Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE);
        fragment.setArguments(bundle);
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            setTitleName();
        }
        return fragment;
    }

    private static final String TAG = "BaseActivity";

    private void getNotification() {
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constants.BUNDLE_NOTIFICATION)) {
            Intent intent = new Intent(this, BaseActivity.class);
            Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE_NOTIFICATION);
            NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
            bundle.putInt(Constants.ID, notificationGCMModel.order_id);
            if (notificationGCMModel.notification_type == 1) {
                setTitleName(ResourceManager.getString(R.string.order_details));
                OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
                bundle.putInt(Constants.STATUS, 1);
                orderDetailsFragment.setArguments(bundle);
                notification_checked = true;
                MovementHelper.replaceFragment(this, orderDetailsFragment, "");
                return;
            } else if (notificationGCMModel.notification_type == 2) {
//                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                setTitleName(ResourceManager.getString(R.string.chat));
//                intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
                bundle.putInt(Constants.ID, notificationGCMModel.order_id);
                bundle.putInt(Constants.TYPE, notificationGCMModel.sender_type);
//                intent.putExtra(Constants.BUNDLE, bundle);
                notification_checked = true;
//                startActivity(intent);
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setArguments(bundle);
                MovementHelper.replaceFragment(this, chatFragment, "");
                return;
            } else if (notificationGCMModel.notification_type == 5) {//chat normal
                Timber.e("notification Base");
                notification_checked = true;
                ChatFragment chatFragment = new ChatFragment();
                bundle.putInt(Constants.ID_CHAT, notificationGCMModel.id);
                bundle.putInt(Constants.TYPE, notificationGCMModel.sender_type);
                bundle.putBoolean(Constants.ALLOW_CHAT, true);
                bundle.putBoolean(Constants.CHAT_FIRST, true);
                intent.putExtra(Constants.BUNDLE, bundle);
                chatFragment.setArguments(bundle);
                MovementHelper.replaceFragment(this, chatFragment, "");
                return;
            } else if (notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) ||
                    notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY) ||
                    notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_TAXI) ||
                    notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_TRUCKS)) {

                if (notificationGCMModel.finish == 1) {
                    setTitleName(ResourceManager.getString(R.string.rate));
                    notification_checked = true;
                    RateNotificationFragment rateNotificationFragment = new RateNotificationFragment();
                    DialogRateModel dialogRateModel = AppMoon.getDialogRateModel(notificationGCMModel);
                    bundle.putSerializable(Constants.RATE_NOTIFICATION, dialogRateModel);
                    rateNotificationFragment.setArguments(bundle);
                    MovementHelper.replaceFragment(this, rateNotificationFragment, "");
                    return;
                } else {
                    Timber.e("notification_type:" + notificationGCMModel.notification_type);
                    setTitleName(ResourceManager.getString(R.string.reservation));
                    notification_checked = true;
                    ReservationDetailsFragment reservationDetailsFragment = new ReservationDetailsFragment();
                    bundle.putInt(Constants.TYPE, notificationGCMModel.notification_type);
                    reservationDetailsFragment.setArguments(bundle);
                    MovementHelper.replaceFragment(this, reservationDetailsFragment, "");
                    return;
                }
            }else if (notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_ADMIN_NOTIFICATION)) {//notification from admin
                notification_checked = true;
                setTitleName(ResourceManager.getString(R.string.label_notification));
                MovementHelper.replaceFragment(this, new NotificationFragment(), "");
                return;
            }
        }
    }

    //
    @Override
    public void onBackPressed() {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
            if (fragment instanceof StoryFragment) {
                if (((StoryFragment) fragment).momentz != null) {
                    ((StoryFragment) fragment).momentz.pause(false);
                }
                if (((StoryFragment) fragment).player != null) {
                    ((StoryFragment) fragment).player.stop();
                    ((StoryFragment) fragment).player = null;
                }

                if (((StoryFragment) fragment).playerView != null) {
                    ((StoryFragment) fragment).playerView.onPause();
                    ((StoryFragment) fragment).playerView = null;
                }

                if (((StoryFragment) fragment).videoView != null) {
                    ((StoryFragment) fragment).videoView.pause();
                    ((StoryFragment) fragment).videoView = null;
                }
                fragment.onDestroy();

            }
        } catch (Exception ex) {
            Log.e("exceptionBase", ex.getMessage());
        }
        super.onBackPressed();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"result_ok:"+requestCode);
        //((LoginFragment) BaseActivity.this).onActivityResult(requestCode,resultTYPE_RESERVATION_CLINICCode,data);
    }
}