package grand.app.moon.views.fragments.filter;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentFilterBinding;
import grand.app.moon.databinding.FragmentVerificationCodeBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.settings.FilterViewModel;
import grand.app.moon.viewmodels.user.VerificationCodeViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

public class FilterFragment extends BaseFragment {
    View rootView;
    private FragmentFilterBinding fragmentFilterBinding;
    private FilterViewModel filterViewModel;
    public String type= "";
    public int filter=1;
    public Service service;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.SELECTED)){
            filter = getArguments().getInt(Constants.SELECTED);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.SERVICE)){
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if(service != null && service.mType == Integer.parseInt( Constants.TYPE_INSTITUTIONS)){
            fragmentFilterBinding.minOrderAmount.setVisibility(View.GONE);
        }
    }

    private void bind() {
        Timber.e("filter:"+filter);
        filterViewModel = new FilterViewModel(filter,service);
        fragmentFilterBinding.setFilterViewModel(filterViewModel);
        rootView = fragmentFilterBinding.getRoot();
    }

    private void setEvent() {
        filterViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.FILTER)) {
                    Intent intent=new Intent();
                    intent.putExtra(Constants.FILTER,filterViewModel.filter);
                    ((ParentActivity)context).setResult(Constants.FILTER_RESULT,intent);
                    ((ParentActivity)context).finish();//finishing activity
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        filterViewModel.reset();
        rootView = null;
        filterViewModel = null;
    }


}
