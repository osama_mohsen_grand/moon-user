package grand.app.moon.views.fragments.reservation;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moon.R;
import grand.app.moon.adapter.ReservationAdapter;
import grand.app.moon.databinding.FragmentReservationMainBinding;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.models.reservation.ReservationOrder;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationMainFragment extends Fragment {

    private FragmentReservationMainBinding fragmentReservationMainBinding;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    ReservationListFragment reservationFragment = null;
// bundle.putString(Constants.TYPE, Constants.TYPE_RESERVATION_CLINIC);
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReservationMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reservation_main, container, false);
        bind();
        return fragmentReservationMainBinding.getRoot();
    }


    //if  you want to get order recent or new  send status 0 else if you want to get previous order  send status 1
    private void bind() {
        ReservationListFragment reservationRecentFragment = new ReservationListFragment();
        Bundle bundle = new Bundle();
        Timber.e("type:start");
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            bundle.putInt(Constants.RESERVED_TYPE, getArguments().getInt(Constants.TYPE));
            Timber.e("type:"+getArguments().getInt(Constants.TYPE));
        }
        bundle.putString(Constants.STATUS,"0");
        reservationRecentFragment.setArguments(bundle);

        reservationFragment = new ReservationListFragment();
        bundle = new Bundle();
        Timber.e("type:start");
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            Timber.e("type:"+getArguments().getInt(Constants.TYPE));
            bundle.putInt(Constants.RESERVED_TYPE, getArguments().getInt(Constants.TYPE));
        }
        bundle.putString(Constants.STATUS,"1");
        reservationFragment.setArguments(bundle);

        tabModels.add(new TabModel(getString(R.string.recent_appointments),reservationRecentFragment));
        tabModels.add(new TabModel(getString(R.string.appointments_history),reservationFragment));

        SwapAdapter adapter = new SwapAdapter(getChildFragmentManager(),tabModels);
        fragmentReservationMainBinding.viewpager.setAdapter(adapter);
        fragmentReservationMainBinding.viewpager.setOffscreenPageLimit(tabModels.size());
        fragmentReservationMainBinding.slidingTabs.setupWithViewPager(fragmentReservationMainBinding.viewpager);

    }

    public void moveToHistory(ReservationOrder reservationOrder){
        Timber.e("welcome here");
        ArrayList<ReservationOrder> reservationOrders = new ArrayList<>();
        if(reservationFragment.reservationAdapter == null) {
            reservationFragment.reservationAdapter = new ReservationAdapter(reservationOrders, "1");
        }
        reservationOrders.add(reservationOrder);
        reservationFragment.reservationAdapter.update(reservationOrders);
    }

}
