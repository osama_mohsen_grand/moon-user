package grand.app.moon.views.fragments.moon;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentMoonMapBinding;
import grand.app.moon.databinding.FragmentServiceMapBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.models.service.ShopMap;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.viewmodels.map.MapViewModel;
import grand.app.moon.viewmodels.map.MoonMapViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;
import grand.app.moon.vollyutils.MyApplication;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoonMapFragment extends BaseFragment {

    FragmentMoonMapBinding fragmentMoonMapBinding;
    ServiceDetailsFragment serviceDetailsFragment;
    private int filter = 1;
    public MoonMapViewModel moonMapViewModel = null;
    Bundle savedInstanceState;
    private Service service;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMoonMapBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_moon_map, container, false);
        bind();
        return fragmentMoonMapBinding.getRoot();
    }

    private void bind() {

        fragmentMoonMapBinding.mapView.onCreate(savedInstanceState);
        moonMapViewModel = new MoonMapViewModel();
        moonMapViewModel.callService();
        setEvent();
        fragmentMoonMapBinding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }


        fragmentMoonMapBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                moonMapViewModel.mapConfig = new MapConfig(context, mMap);
                moonMapViewModel.mapConfig.setMapStyle();//set style google map
                moonMapViewModel.mapConfig.setSettings();//add setting google map
                moonMapViewModel.mapConfig.checkLastLocation();
            }
        });
        fragmentMoonMapBinding.setMoonMapViewModel(moonMapViewModel);
    }

    Story story = null;
    Observer observer;

    private void setEvent() {
        observer = new Observer<Object>() {

            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, moonMapViewModel.getMoonMapRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    if (fragmentMoonMapBinding.mapView != null && moonMapViewModel.getMoonMapRepository().getMoonMapResponse().stories != null) {
                        moonMapViewModel.mapConfig.clear();
                        for (int i = 0; i < moonMapViewModel.getMoonMapRepository().getMoonMapResponse().stories.size(); i++) {
                            story = moonMapViewModel.getMoonMapRepository().getMoonMapResponse().stories.get(i);
//                            moonMapViewModel.mapConfig.addMarker(new LatLng(story.mLat, story.mLng), i, story.mImage, story.mName, new ImageLoaderHelper.ImageMarkerListeners() {
//                                @Override
//                                public void isLoaded(Marker marker) {
//                                    if (fragmentMoonMapBinding != null) {
//                                        moonMapViewModel.mapConfig.zoomCamera(150);
//                                        fragmentMoonMapBinding.mapView.onResume();
//                                    }
//                                }
//                            });

//                            moonMapViewModel.mapConfig.addMarker(story, i, marker -> {
//                                if (fragmentMoonMapBinding != null) {
//                                    moonMapViewModel.mapConfig.zoomCamera(150);
//                                    fragmentMoonMapBinding.mapView.onResume();
//                                }
//                            });
                            addMarker(story,i);
                        }
                        moonMapViewModel.mapConfig.getGoogleMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                marker.showInfoWindow();
                                return true;
                            }
                        });

                        moonMapViewModel.mapConfig.getGoogleMap().setOnInfoWindowClickListener(marker -> {
                            // TODO Auto-generated method stub

                            int pos = (int) moonMapViewModel.mapConfig.getId(marker);
                            Log.d(TAG,pos+"");
                            if(pos < moonMapViewModel.getMoonMapRepository().getMoonMapResponse().stories.size()) {
                                Story story = moonMapViewModel.getMoonMapRepository().getMoonMapResponse().stories.get(pos);
                                Intent intent = new Intent(context, BaseActivity.class);
                                intent.putExtra(Constants.PAGE, Constants.STORY);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(Constants.STORY, story);
                                intent.putExtra(Constants.BUNDLE, bundle);
                                intent.putExtra(Constants.NAME_BAR, moonMapViewModel.getMoonMapRepository().getMoonMapResponse().stories.get(pos).mName);

                                story.storyView = 1;
                                moonMapViewModel.mapConfig.addMarker(story, moonMapViewModel.mapConfig.markers.size(), markerNew -> {
                                    if (fragmentMoonMapBinding != null) {
                                        moonMapViewModel.mapConfig.zoomCamera(150);
                                        fragmentMoonMapBinding.mapView.onResume();
                                    }
                                });
                                marker.hideInfoWindow();
                                startActivity(intent);
                            }
                        });

                    }
                }
            }
        };
        moonMapViewModel.mMutableLiveData.observe((LifecycleOwner) context, observer);
    }

    public void addMarker(Story story , int pos){
        Log.d(TAG,"pos:"+pos);
        moonMapViewModel.mapConfig.addMarker(story, pos, marker -> {
            if (fragmentMoonMapBinding != null) {
                moonMapViewModel.mapConfig.zoomCamera(150);
                fragmentMoonMapBinding.mapView.onResume();
            }
        });
    }

    private static final String TAG = "MoonMapFragment";

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onResume");
        if (fragmentMoonMapBinding != null)
            fragmentMoonMapBinding.mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (fragmentMoonMapBinding != null) { //prevent crashing if the map doesn't exist yet (eg. on starting activity)
            fragmentMoonMapBinding.mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onStop");
        if (fragmentMoonMapBinding != null)
            fragmentMoonMapBinding.mapView.onPause();
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        if (fragmentMoonMapBinding != null)
            fragmentMoonMapBinding.mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(TAG, "onLowMemory");
        fragmentMoonMapBinding.mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        if (moonMapViewModel != null) {
            moonMapViewModel.reset();
            fragmentMoonMapBinding.mapView.onDestroy();
            moonMapViewModel.mMutableLiveData.removeObserver(observer);
            moonMapViewModel.mapConfig = null;
            moonMapViewModel = null;
        }
        fragmentMoonMapBinding = null;
        super.onDestroy();
    }

}
