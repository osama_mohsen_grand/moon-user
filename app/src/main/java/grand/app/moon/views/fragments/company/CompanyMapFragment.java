package grand.app.moon.views.fragments.company;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentServiceMapBinding;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopMap;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.viewmodels.map.MapViewModel;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;
import grand.app.moon.vollyutils.MyApplication;

/**
 * A simple {@link Fragment} subclass.
 * TODO DELETE OSAMA
 */
public class CompanyMapFragment extends BaseFragment {


    FragmentServiceMapBinding fragmentServiceMapBinding;
    CompaniesFragment companiesFragment;
    private int filter = 1;
    public MapViewModel mapViewModel = null;
    Bundle savedInstanceState;
    private Service service;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentServiceMapBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_map, container, false);
        companiesFragment = ((CompaniesFragment) this.getParentFragment());
        getData();
        this.savedInstanceState = savedInstanceState;
        bind(savedInstanceState);
        return fragmentServiceMapBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
    }

    private void bind(Bundle savedInstanceState) {

        fragmentServiceMapBinding.mapView.onCreate(savedInstanceState);
        mapViewModel =  new MapViewModel(service,filter);
        setEvent();
        fragmentServiceMapBinding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }


        fragmentServiceMapBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                mapViewModel.mapConfig = new MapConfig(context, mMap);
                mapViewModel.mapConfig.setMapStyle();//set style google map
                mapViewModel.mapConfig.setSettings();//add setting google map
                mapViewModel.mapConfig.checkLastLocation();

            }
        });
        fragmentServiceMapBinding.setMapViewModel(mapViewModel);
    }

    AdsCompanyModel adsCompanyModel = null;

    private void setEvent() {
        mapViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, mapViewModel.getCompanyRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    if(fragmentServiceMapBinding.mapView != null && mapViewModel.getCompanyRepository().getCompanyMainResponse().mData.companies != null){
                        mapViewModel.mapConfig.clear();
                        for(int i=0; i< mapViewModel.getCompanyRepository().getCompanyMainResponse().mData.companies.size(); i++){
                            adsCompanyModel =  mapViewModel.getCompanyRepository().getCompanyMainResponse().mData.companies.get(i);
                            mapViewModel.mapConfig.addMarker(new LatLng(adsCompanyModel.lat, adsCompanyModel.lng),adsCompanyModel.id, adsCompanyModel.image, adsCompanyModel.name, marker -> {
                                mapViewModel.mapConfig.zoomCamera(150);
                                fragmentServiceMapBinding.mapView.onResume();
                            });
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fragmentServiceMapBinding != null && fragmentServiceMapBinding.mapView != null) {
            fragmentServiceMapBinding.mapView.onResume();
            if(mapViewModel != null)
                mapViewModel.callService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fragmentServiceMapBinding != null && fragmentServiceMapBinding.mapView != null)
            fragmentServiceMapBinding.mapView.onPause();
    }

    @Override
    public void onDestroy() {

        if (fragmentServiceMapBinding != null && fragmentServiceMapBinding.mapView != null)
            if (mapViewModel != null) {
                mapViewModel.reset();
                fragmentServiceMapBinding.mapView.onDestroy();
            }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (fragmentServiceMapBinding.mapView != null) fragmentServiceMapBinding.mapView.onLowMemory();
    }


}
