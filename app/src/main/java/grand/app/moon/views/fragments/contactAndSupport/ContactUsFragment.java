package grand.app.moon.views.fragments.contactAndSupport;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentContactUsBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.contact.ContactUsViewModel;

public class ContactUsFragment extends BaseFragment {

    private FragmentContactUsBinding binding;
    private ContactUsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false);
        bind();
        setEvent();
        return binding.getRoot();
    }

    private void bind() {
        viewModel = new ContactUsViewModel();
        binding.setContactUsViewModel(viewModel);

    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getRepository().getMessage());
                if(action.equals(Constants.SUCCESS)){
                    toastMessage(viewModel.getRepository().getMessage());
                    viewModel.request.setEmail("");
                    viewModel.request.setMessage("");
                    viewModel.request.setName("");
                    viewModel.request.setSubject("");
                    viewModel.notifyChange();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }
}
