package grand.app.moon.views.fragments.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.CountryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentCountryBinding;
import grand.app.moon.models.country.Datum;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.country.CountryViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;


public class CountryFragment extends BaseFragment {
    View rootView;
    private FragmentCountryBinding fragmentCountryBinding;
    private CountryViewModel countryViewModel;
    private CountryAdapter countryAdapter;
    private static final String TAG = "CountryFragment :";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentCountryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_country, container, false);
        bind();
        rootView = fragmentCountryBinding.getRoot();
        return rootView;
    }

    private void bind() {
        countryViewModel = new CountryViewModel();
        setEvents();
        AppUtils.initVerticalRV(fragmentCountryBinding.rvCountry, fragmentCountryBinding.rvCountry.getContext(), 1);
        fragmentCountryBinding.setCountryViewModel(countryViewModel);
    }

    private void setEvents() {
        countryViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,countryViewModel.getCountryRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.COUNTRIES)){
                    countryAdapter = new CountryAdapter(countryViewModel.getCountryRepository().getCountriesResponse().data);
                    if(!UserHelper.retrieveKey(Constants.COUNTRY_ID).equals("")){
                        int country_id = Integer.parseInt(UserHelper.retrieveKey(Constants.COUNTRY_ID));
                        int pos = -1;
                        for(Datum country : countryViewModel.getCountryRepository().getCountriesResponse().data) {
                            pos++;
                            if (country.id == country_id)
                                countryAdapter.setPosition(pos);
                        }
                    }
                    countryViewModel.showPage(true);
                    fragmentCountryBinding.rvCountry.setAdapter(countryAdapter);
                    setEventAdapter();
                }else if(action.equals(Constants.SUBMIT)){
                    if(countryAdapter.getPosition() != -1){
                        UserHelper.saveKey(Constants.COUNTRY_ID,String.valueOf(countryViewModel.getCountryRepository().getCountriesResponse().data.get(countryAdapter.getPosition()).id));
                        UserHelper.saveKey(Constants.COUNTRY_CODE,countryViewModel.getCountryRepository().getCountriesResponse().data.get(countryAdapter.getPosition()).code);
                        UserHelper.saveCurrency(countryViewModel.getCountryRepository().getCountriesResponse().data.get(countryAdapter.getPosition()).currency);
                        Intent intent = null;
                        if(getActivity() instanceof MainActivity){
                            intent = new Intent(context,MainActivity.class);
                        }else if(getActivity() instanceof BaseActivity){
                            if(UserHelper.getUserId() == -1) {
                                intent = new Intent(context, BaseActivity.class);
                                intent.putExtra(Constants.PAGE,Constants.INTRO);
                            }else{
                                intent = new Intent(context, MainActivity.class);
                            }
                        }
                        getActivityBase().finishAffinity();
                        startActivity(intent);

                    }else{
                        showError(ResourceManager.getString(R.string.please_choose_your_country));
                    }
                }
            }
        });
    }

    /*

     */

    private void setEventAdapter() {
        countryAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                countryAdapter.setPosition(pos);
                UserHelper.saveKey(Constants.COUNTRY_ID,String.valueOf(countryViewModel.getCountryRepository().getCountriesResponse().data.get(pos).id));
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        countryViewModel.reset();
    }
}
