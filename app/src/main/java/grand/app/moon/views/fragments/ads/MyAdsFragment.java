package grand.app.moon.views.fragments.ads;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.MyAdsAdapter;
import grand.app.moon.adapter.NotificationAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentMyAdsBinding;
import grand.app.moon.databinding.FragmentNotificationsBinding;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.ads.MyAdsModel;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.ads.MyAdsViewModel;
import grand.app.moon.viewmodels.notification.NotificationViewModel;
import grand.app.moon.views.activities.BaseActivity;

public class MyAdsFragment extends BaseFragment {

    View rootView;
    private FragmentMyAdsBinding fragmentMyAdsBinding;
    private MyAdsViewModel myAdsViewModel;
    MyAdsAdapter myAdsAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMyAdsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_ads, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        myAdsViewModel = new MyAdsViewModel();
        fragmentMyAdsBinding.setMyAdsViewModel(myAdsViewModel);
        rootView = fragmentMyAdsBinding.getRoot();
    }

    private void setEvent() {
        myAdsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, myAdsViewModel.getAdsRepository().getMessage());
                if(action.equals(Constants.MY_ADS)){
                    if(myAdsViewModel.getAdsRepository().getMyAdsResponse().data.size() != 0) {
                        AppUtils.initVerticalRV(fragmentMyAdsBinding.rvMyAds, fragmentMyAdsBinding.rvMyAds.getContext(), 1);
                        myAdsAdapter = new MyAdsAdapter(myAdsViewModel.getAdsRepository().getMyAdsResponse().data);
                        fragmentMyAdsBinding.rvMyAds.setAdapter(myAdsAdapter);
                        setEventAdapter();
                    }else
                        myAdsViewModel.noData();
                }else if(action.equals(Constants.DELETED)){
                    myAdsAdapter.deleteItem();
                }
            }
        });
    }

    private void setEventAdapter() {
        myAdsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if(mutable.type.equals(Constants.SUBMIT)){
                    MyAdsModel myAdsModel = myAdsViewModel.getAdsRepository().getMyAdsResponse().data.get(mutable.position);

                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.ADS_DETAILS);
                    intent.putExtra(Constants.NAME_BAR,myAdsModel.name);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE,myAdsModel.type);
                    bundle.putInt(Constants.ID,myAdsModel.id);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);

                }else if(mutable.type.equals(Constants.DELETE)){
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.delete))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_ads))
                            .setActionText(ResourceManager.getString(R.string.continue_))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    myAdsAdapter.delete_position = mutable.position;
                                    myAdsViewModel.deleteAds( myAdsViewModel.getAdsRepository().getMyAdsResponse().data.get(mutable.position).id);
                                }
                            });
                }else if(mutable.type.equals(Constants.EDIT)){
                    MyAdsModel myAdsModel = myAdsViewModel.getAdsRepository().getMyAdsResponse().data.get(mutable.position);

                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.ADD_ADS);
                    intent.putExtra(Constants.NAME_BAR,myAdsModel.name);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE,myAdsModel.type);
                    bundle.putInt(Constants.ID,myAdsModel.id);
                    bundle.putBoolean(Constants.EDIT,true);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (myAdsViewModel != null) myAdsViewModel.reset();
    }

}
