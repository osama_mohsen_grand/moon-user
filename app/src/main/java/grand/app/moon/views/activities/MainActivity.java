package grand.app.moon.views.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;

import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.customviews.actionbar.HomeActionBarView;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.customviews.menu.NavigationDrawerView;
import grand.app.moon.databinding.ActivityMainBinding;
import grand.app.moon.models.user.profile.User;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.ImmediateUpdateActivity;
import grand.app.moon.utils.LanguagesHelper;
import grand.app.moon.utils.MovementHelper;
import grand.app.moon.utils.dialog.DialogHelper;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.fragments.HomeFragment;
import grand.app.moon.views.fragments.HomeFragment;
import grand.app.moon.views.fragments.cart.CartFragment;
import grand.app.moon.vollyutils.MyApplication;
import timber.log.Timber;

import static grand.app.moon.utils.ImmediateUpdateActivity.UPDATE_REQUEST_CODE;

public class MainActivity extends ParentActivity {
    public HomeActionBarView homeActionBarView = null;
    public NavigationDrawerView navigationDrawerView;
    public String message = "";
    ImmediateUpdateActivity immediateUpdateActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        immediateUpdateActivity = new ImmediateUpdateActivity(this);
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(),LanguagesHelper.getCurrentLanguage());
        initializeToken();
        initFacebook();
        bind();
        getData();
        setEvents();
    }

    private void getData() {
        if (getIntent() != null && getIntent().hasExtra(Constants.MESSAGE)) {
            message = getIntent().getStringExtra(Constants.MESSAGE);
//            new OrderCodeViewModel(this,message);
        }
    }


    ActivityMainBinding activityMainBinding;

    private View bind() {
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        homeActionBarView = new HomeActionBarView(this);
        navigationDrawerView = new NavigationDrawerView(this);
        activityMainBinding.llBaseContainer.addView(navigationDrawerView);
        navigationDrawerView.layoutNavigationDrawerBinding.llBaseActionBarContainer.addView(homeActionBarView, 0);
        homeActionBarView.setNavigation(navigationDrawerView);
        homeActionBarView.connectDrawer(navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu, true);
        navigationDrawerView.setActionBar(homeActionBarView);


        if (getIntent() != null && getIntent().hasExtra(Constants.PAGE) &&
                getIntent().getStringExtra(Constants.PAGE).equals(Constants.LOGIN)) {
            navigationDrawerView.loginPage();
        } else if (getIntent() != null && getIntent().hasExtra(Constants.PAGE) &&
                getIntent().getStringExtra(Constants.PAGE).equals(Constants.CART)) {
            navigationDrawerView.cartPage();
            activityMainBinding.bnvMainBottom.setSelectedItemId(R.id.bottomNavigationCartId);
        } else if (getIntent() != null && getIntent().hasExtra(Constants.PAGE) &&
                getIntent().getStringExtra(Constants.PAGE).equals(Constants.MY_ADS)) {
            navigationDrawerView.myAdsPage();
        } else
            navigationDrawerView.homePage();
        setTabListeners();
        return activityMainBinding.getRoot();
    }

    public void pleaseLoginFirst() {
        new DialogConfirm(this)
                .setTitle(ResourceManager.getString(R.string.app_name))
                .setTitleTextSize(ResourceManager.getDimens(R.dimen.sp9))
                .setMessage(ResourceManager.getString(R.string.please_login_to_complete_this_action))
                .setMessageTextSize(ResourceManager.getDimens(R.dimen.sp7))
                .setActionText(ResourceManager.getString(R.string.label_login))
                .setActionTextSize(ResourceManager.getDimens(R.dimen.sp5))
                .setActionCancel(ResourceManager.getString(R.string.cancel))
                .setImage(R.mipmap.ic_launcher, ResourceManager.getDimens(R.dimen.dp70w), ResourceManager.getDimens(R.dimen.dp70h))
                .show(new DialogHelperInterface() {
                    @Override
                    public void OnClickListenerContinue(Dialog dialog, View view) {
                        navigationDrawerView.loginPage();
                    }
                });
    }

    private void setTabListeners() {
        activityMainBinding.bnvMainBottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.bottomNavigationHomeId) {
                    navigationDrawerView.homePage();
//                    activityMainBinding.bnvMainBottom.setSelected(true);
                }
                if (item.getItemId() == R.id.bottomChatId) {
                    if (UserHelper.getUserId() == -1) {
                        pleaseLoginFirst();
                        return false;
                    } else {
                        User user = UserHelper.getUserDetails();
                        user.chats_count = 0;
                        UserHelper.saveUserDetails(user);
                        activityMainBinding.bnvMainBottom.removeBadge(R.id.bottomChatId);
                        activityMainBinding.bnvMainBottom.setSelected(true);
                        navigationDrawerView.chatList();
                    }
                }
                if (item.getItemId() == R.id.bottomNavigationCartId) {
                    if (UserHelper.getUserId() == -1) {
                        pleaseLoginFirst();
                        return false;
                    } else
                        navigationDrawerView.cartPage();
//                    MovementHelper.replaceFragment(MainActivity.this, new CartFragment(),"");
//                    activityMainBinding.bnvMainBottom.setSelected(true);
                }
                if (item.getItemId() == R.id.bottomNavigationMoonMap) {
//                    if (UserHelper.getUserId() == -1) {
//                        pleaseLoginFirst();
//                        return false;
//                    } else
                        navigationDrawerView.moonMap();
//                    activityMainBinding.bnvMainBottom.setSelected(true);
                }
                if (item.getItemId() == R.id.bottomNavigationDiscoverId) {
                    navigationDrawerView.discoverPage();
//                    activityMainBinding.bnvMainBottom.setSelected(true);
                }
                return true;
            }
        });
        chatCount();
    }

    private void chatCount() {
        Log.e(TAG, "chatCount: "+UserHelper.getUserDetails().chats_count );
        if(UserHelper.getUserId() != -1 && UserHelper.getUserDetails().chats_count != 0){
            Log.e(TAG, "imagesCount: "+UserHelper.getUserDetails().chats_count );
            activityMainBinding.bnvMainBottom.getOrCreateBadge(R.id.bottomChatId)
                    .setNumber(UserHelper.getUserDetails().chats_count);
        }else {
            Log.e(TAG, "imagesCount: else");

//            activityMainBinding.bnvMainBottom.getOrCreateBadge(R.id.bottomChatId)
//                    .clearNumber();
            activityMainBinding.bnvMainBottom.removeBadge(R.id.bottomChatId);
        }
        if(UserHelper.getUserId() != -1)
            navigationDrawerView.updateChatCount(UserHelper.getUserDetails().chats_count);
    }


    private void setEvents() {
        navigationDrawerView.mutableLiveData.observe(this, new Observer<Object>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onChanged(@Nullable Object object) {
                if (object instanceof String) {
                    String text = (String) object;
                    if (text.equals(Constants.LOGOUT)) {
                        logoutApp();
                    } else {
                        homeActionBarView.setTitle(text);
                        navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawer(Gravity.START);
                    }
                } else if (object instanceof Boolean) {
                    //profile
                    Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.PROFILE);
                    startActivityForResult(intent, Constants.RESULT_PROFILE_RESPONSE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_PROFILE_RESPONSE) {
            if (resultCode == Activity.RESULT_OK) {
                navigationDrawerView.setHeader();
            }
        }
    }//onActivityResult


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onBackPressed() {

        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {

            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                super.onBackPressed();
                return;
            }
            HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().findFragmentByTag(Constants.HOME);
            if (homeFragment != null) {
                int[] ids = {R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no};
                DialogHelper.showDialogHelper(MainActivity.this, R.layout.dialog_close_app, ids, (dialog, view) -> {
                    switch (view.getId()) {
                        case R.id.tv_dialog_close_app_yes:
                            dialog.dismiss();
                            finish();
                            break;
                        case R.id.tv_dialog_close_app_no:
                            dialog.dismiss();
                            break;

                    }
                });
            } else {
                if (navigationDrawerView != null) {
                    navigationDrawerView.homePage();
                    activityMainBinding.bnvMainBottom.setSelectedItemId(R.id.bottomNavigationHomeId);
                }
            }
            return;

        } else
            finish();

    }

    public void logoutApp() {
        int[] ids = {R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no};
        DialogHelper.showDialogHelper(MainActivity.this, R.layout.dialog_logout_app, ids, (dialog, view) -> {
            switch (view.getId()) {
                case R.id.tv_dialog_close_app_yes:
                    dialog.dismiss();
                    finishAffinity();
                    UserHelper.clearUserDetails();
                    Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                    startActivity(intent);
                    break;
                case R.id.tv_dialog_close_app_no:
                    dialog.dismiss();
                    break;

            }
        });
    }

    private static final String TAG = "MainActivity";

    private BroadcastReceiver notify = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, "onReceive: " );
            chatCount();
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: " );
        registerReceiver(notify, new IntentFilter(Constants.NOTIFICATION));
        chatCount();
        updateAuto();
   }

    private void updateAuto() {
        immediateUpdateActivity.getAppUpdateManager().getAppUpdateInfo().addOnSuccessListener(it -> {
            if (it.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                try {
                    immediateUpdateActivity.getAppUpdateManager().startUpdateFlowForResult(it, AppUpdateType.IMMEDIATE, this, UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notify);
    }
}
