package grand.app.moon.views.fragments.be;


import android.os.Bundle;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moon.MultipleImageSelect.models.Image;
import grand.app.moon.R;
import grand.app.moon.adapter.SuggestAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentBeShopBinding;
import grand.app.moon.databinding.FragmentRegisterBinding;
import grand.app.moon.models.app.AccountTypeModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.Datum;
import grand.app.moon.models.country.Region;
import grand.app.moon.models.service.shop.Service;
import grand.app.moon.models.shop.AddShopResponse;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.utils.dialog.BottomSheetDialogHelper;
import grand.app.moon.utils.maputils.location.LocationLatLng;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.viewmodels.be.BeShopViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.views.fragments.auth.ProfileFragment;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

public class BeShopFragment extends BaseFragment {
    View rootView;

    private FragmentBeShopBinding fragmentBeShopBinding;
    private BeShopViewModel beShopViewModel;
    public String type = "", phone = "";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBeShopBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_be_shop, container, false);
        getData();
        bind();
        setEvent();

        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
    }

    private void bind() {
        beShopViewModel = new BeShopViewModel();
        AppUtils.initHorizontalRV(fragmentBeShopBinding.rvSuggestions, fragmentBeShopBinding.rvSuggestions.getContext(), 1);

        fragmentBeShopBinding.setBeShopViewModel(beShopViewModel);
        initPopUp();
        rootView = fragmentBeShopBinding.getRoot();
    }

    private void pickImageDialog() {
        FileOperations.pickImage(context, BeShopFragment.this, Constants.FILE_TYPE_IMAGE);
    }

    private void initPopUp() {
        fragmentBeShopBinding.edtRegisterAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AccountTypeModel> accountTypeModels = AppMoon.getShopAccountType();
                ArrayList<String> popUpModels = new ArrayList<>();
                for (AccountTypeModel accountTypeModel : accountTypeModels)
                    popUpModels.add(accountTypeModel.type);
                popUpMenuHelper.openPopUp(getActivity(), fragmentBeShopBinding.edtRegisterAccountType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        fragmentBeShopBinding.edtRegisterAccountType.setText(accountTypeModels.get(position).type);
                        beShopViewModel.beShopRequest.setType(String.valueOf(accountTypeModels.get(position).id));
                        beShopViewModel.changeVisibility();
                        if (!accountTypeModels.get(position).id.equals(Constants.TYPE_FAMOUS_PEOPLE) &&
                                !accountTypeModels.get(position).id.equals(Constants.TYPE_PHOTOGRAPHER))
                            beShopViewModel.getServices(Integer.parseInt(accountTypeModels.get(position).id));
                        fragmentBeShopBinding.edtRegisterType.setText("");
                        beShopViewModel.beShopRequest.setService_id("");
                    }
                });

            }
        });
        fragmentBeShopBinding.edtRegisterType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getServiceType();
            }
        });

        fragmentBeShopBinding.edtRegisterService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beShopViewModel.getServiceRepository() != null && beShopViewModel.getServiceRepository().getServiceResponse() != null) {
                    Timber.e("Click");
                    ArrayList<String> popUpModelsService = new ArrayList<>();
                    for (Service service : beShopViewModel.getServiceRepository().getServiceResponse().services) {
                        popUpModelsService.add(service.name);
                    }
                    popUpMenuHelper.openPopUp(getActivity(), fragmentBeShopBinding.edtRegisterService, popUpModelsService, new PopUpInterface() {
                        @Override
                        public void submitPopUp(int position) {
                            fragmentBeShopBinding.edtRegisterService.setText(popUpModelsService.get(position));
                            beShopViewModel.beShopRequest.setShops_services(String.valueOf(beShopViewModel.getServiceRepository().getServiceResponse().services.get(position).id));
                        }
                    });
                }
            }
        });
    }

    public void getServiceType() {
        if (!beShopViewModel.beShopRequest.getType().equals("")) {
            ArrayList<String> popUpModelsType = AppMoon.getServiceType(Integer.parseInt(beShopViewModel.beShopRequest.getType()));
            if (popUpModelsType.size() > 0) {
                fragmentBeShopBinding.tiRegisterType.setVisibility(View.VISIBLE);
                fragmentBeShopBinding.edtRegisterType.setVisibility(View.VISIBLE);
                popUpMenuHelper.openPopUp(getActivity(), fragmentBeShopBinding.edtRegisterType, popUpModelsType, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        String serviceType = AppMoon.getServiceId(popUpModelsType.get(position));
                        beShopViewModel.beShopRequest.setService_id(serviceType);
                        fragmentBeShopBinding.edtRegisterType.setText(popUpModelsType.get(position));
                    }
                });
            } else {
                fragmentBeShopBinding.tiRegisterType.setVisibility(View.GONE);
                fragmentBeShopBinding.tiRegisterType.setVisibility(View.GONE);
            }
        }
    }

    public List<Region> regions = new ArrayList<>();
    SuggestAdapter suggestAdapter = null;
    private void setEvent() {
        beShopViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, beShopViewModel.getCountryRepository().getMessage());
                handleActions(action, beShopViewModel.getBeShopRepository().getMessage());
                handleActions(action, beShopViewModel.getServiceRepository().getMessage());
                Timber.e(action);
                if (action.equals(Constants.REGISTRATION)) {//submitSearch register
                    toastMessage(beShopViewModel.getBeShopRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, MainActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
//                    ((ParentActivity) context).finishAffinity();
                    startActivity(intent);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Constants.TYPE, Constants.REGISTRATION);
//                    bundle.putString(Constants.NAME_BAR, "");
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivity(intent);
                } else if (action.equals(Constants.SERVICE_SUCCESS)) {//get service type
                    //Log services
                } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                    if (beShopViewModel.image_select.equals(Constants.COMMERCIAL_IMAGE) || beShopViewModel.image_select.equals(Constants.LICENCE_IMAGE)) {
                        Intent intent = new Intent(context, AlbumSelectActivity.class);
                        intent.putExtra(grand.app.moon.MultipleImageSelect.helpers.Constants.INTENT_EXTRA_LIMIT, 2);
                        startActivityForResult(intent, Constants.REQUEST_CODE);
                    } else
                        pickImageDialog();
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(beShopViewModel.baseError);
                } else if (action.equals(Constants.LOCATIONS)) {//select location
                    Timber.e("Locations");
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                } else if (action.equals(Constants.COUNTRIES)) {//load countries
                    beShopViewModel.setShowPage(true);
                    fragmentBeShopBinding.edtRegisterCity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ArrayList<String> popUpCities = new ArrayList<>();
                            Datum country = AppMoon.getCities(UserHelper.retrieveKey(Constants.COUNTRY_ID), beShopViewModel.getCountryRepository().getCountriesResponse().data);
                            if (country != null) {
                                Timber.e("country:" + country.countryName);
                                for (City city : country.cities)
                                    popUpCities.add(city.cityName);
                                popUpMenuHelper.openPopUp(getActivity(), fragmentBeShopBinding.edtRegisterCity, popUpCities, new PopUpInterface() {
                                    @Override
                                    public void submitPopUp(int position) {
                                        beShopViewModel.beShopRequest.setCity_id(String.valueOf(country.cities.get(position).id));
                                        fragmentBeShopBinding.edtRegisterCity.setText(country.cities.get(position).cityName);
                                        fragmentBeShopBinding.edtRegisterRegion.setText("");
                                        regions = new ArrayList<>(country.cities.get(position).regions);
                                        Timber.e("regions" + regions.size());
                                        getRegions();
                                    }
                                });
                            }
                        }
                    });

                    fragmentBeShopBinding.edtRegisterRegion.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getRegions();
                        }
                    });
                }else if(action.equals(Constants.SUGGESTIONS)){
                    fragmentBeShopBinding.nscrollCheckout.scrollTo(0,(int)fragmentBeShopBinding.tplBeShopNickname.getY());
                    AddShopResponse addShopResponse = beShopViewModel.getBeShopRepository().getAddShopResponse();
                    fragmentBeShopBinding.tplBeShopNickname.setError(addShopResponse.msg);
                    if(suggestAdapter == null) {
                        suggestAdapter = new SuggestAdapter(addShopResponse.suggestions);
                        fragmentBeShopBinding.rvSuggestions.setAdapter(suggestAdapter);
                        suggestAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                            @Override
                            public void onChanged(@Nullable Object o) {
                                int pos = (int) o;
                                beShopViewModel.beShopRequest.setNickName(addShopResponse.suggestions.get(pos).name);
                                beShopViewModel.notifyChange();
                            }
                        });
                    }else{
                        suggestAdapter.updateAll(addShopResponse.suggestions);
                    }
                }
            }
        });
    }

    private void getRegions() {
        ArrayList<String> popUpRegions = new ArrayList<>();
        for (Region region : regions)
            popUpRegions.add(region.regionName);
        Timber.e("size:" + popUpRegions.size());
        popUpMenuHelper.openPopUp(getActivity(), fragmentBeShopBinding.edtRegisterRegion, popUpRegions, new PopUpInterface() {
            @Override
            public void submitPopUp(int position) {
                beShopViewModel.beShopRequest.setRegion_id(String.valueOf(regions.get(position).id));
                fragmentBeShopBinding.edtRegisterRegion.setText(regions.get(position).regionName);
            }
        });

    }

    int counter = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, beShopViewModel.image_select, Constants.FILE_TYPE_IMAGE);
                beShopViewModel.setImage(beShopViewModel.image_select, volleyFileObject);

            if (beShopViewModel.image_select.equals("shop_image"))
//                fragmentBeShopBinding.imgRegisterUser.setImageBitmap(Objects.requireNonNull(volleyFileObject).getCompressObject().getImage());
                fragmentBeShopBinding.imgRegisterUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, BeShopFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, ""+getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            beShopViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
            beShopViewModel.beShopRequest.setAddress(new LocationLatLng(getActivityBase(),BeShopFragment.this).getAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0)));
        }
        if (requestCode == Constants.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            try {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

                counter = images.size();
                if (beShopViewModel.image_select.equals(Constants.COMMERCIAL_IMAGE)) {
                    if (counter == 2)
                        beShopViewModel.beShopRequest.setCommercial(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    else
                        toastInfo(fragmentBeShopBinding.tplBeShopCommerical.getHelperText().toString());
                } else {
                    if (counter == 2)
                        beShopViewModel.beShopRequest.setLicense(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    else
                        toastInfo(fragmentBeShopBinding.tplBeShopLicence.getHelperText().toString());
                }
                if (counter == 2) {
                    for (int i = 0; i < images.size(); i++) {
                        String paramName = beShopViewModel.image_select + "[" + i + "]";
                        beShopViewModel.setImage(paramName, new VolleyFileObject(paramName, images.get(i).path, Constants.FILE_TYPE_IMAGE));
                    }
                }
                beShopViewModel.notifyChange();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            Timber.e("CallService");
            int category_id = data.getIntExtra(Constants.CATEGORY_ID, -1);
            beShopViewModel.beShopRequest.setCategory_id(category_id);
            ArrayList<Integer> subCategories = data.getIntegerArrayListExtra(Constants.SUB_CATEGORY_ID);
            Timber.e("categories:" + category_id);
            Timber.e("subCategories:" + subCategories.size());
            beShopViewModel.beShopRequest.setSubCategory_id(subCategories);
            beShopViewModel.notifyChange();
        }
        if (requestCode == Constants.SERVICES_RESULT && resultCode == Constants.SERVICES_RESULT) {
            this.beShopViewModel.beShopRequest.setServiceIdList(data.getIntegerArrayListExtra(Constants.SELECTED));
            fragmentBeShopBinding.edtRegisterService.setText(ResourceManager.getString(R.string.done_selected_service));
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        beShopViewModel.reset();

    }


}
