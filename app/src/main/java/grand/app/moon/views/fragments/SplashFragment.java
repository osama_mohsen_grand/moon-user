package grand.app.moon.views.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.utils.L;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.LanguagesHelper;
import grand.app.moon.utils.maputils.background.LocationBackground;
import grand.app.moon.utils.maputils.location.GPSLocation;
import grand.app.moon.utils.maputils.location.LocationChangeListener;
import grand.app.moon.utils.maputils.location.LocationLatLng;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.common.SplashViewModel;
import grand.app.moon.R;
import grand.app.moon.databinding.FragmentSplashBinding;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashFragment extends BaseFragment {
    private FragmentSplashBinding fragmentSplashBinding;
    private SplashViewModel splashViewModel;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    boolean locationIsOpen = false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentSplashBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false);
        thread();
//        splashStart();
//        ((ParentActivity)context).startBackgroundService();
//        setEvent();
        return fragmentSplashBinding.getRoot();
    }


    Thread splashThread;
    Disposable disposable;

    private void thread() {
        openLocation();
        disposable = Observable.interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> L.e(TAG, "Throwable " + throwable.getMessage()))
                .subscribe(i -> setEvent());
    }




    private void openLocation() {

        LocationLatLng locationLatLng = new LocationLatLng(getActivityBase(), SplashFragment.this);

        if (GPSLocation.checkGps(context) && locationLatLng.mapPermission.validLocationPermission()) {
            locationLatLng.getLocation(new LocationChangeListener() {
                @Override
                public void select(String address, LatLng latLng) {
                    locationIsOpen = true;
                    UserHelper.saveKey(Constants.LAT, "" + latLng.latitude);
                    UserHelper.saveKey(Constants.LNG, "" + latLng.longitude);
//                    UserHelper.saveKey(Constants.LAT, "" + "30.0829");
//                    UserHelper.saveKey(Constants.LNG, "" + "31.2813");
                    UserHelper.saveKey(Constants.USER_ADDRESS, address);
                    Log.d("lat",latLng.latitude+"");
                    Log.d("lng",latLng.longitude+"");
//                    setEvent();
                }

                @Override
                public void error() {

                }
            });
        }
    }

    private static final String TAG = "SplashFragment";

    private void setEvent() {
        try {
            Intent intent = null;

            Timber.e("language" + UserHelper.retrieveKey(Constants.LANGUAGE_HAVE));
            if (UserHelper.retrieveKey(Constants.LANGUAGE_HAVE).equals("")) {
                Timber.e("language");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LANGUAGE);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.label_language));
            } else if (UserHelper.retrieveKey(Constants.COUNTRY_ID).equals("")) {
                Timber.e("country");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.country));
            } else if (locationIsOpen) {
                Timber.e("country");
                ((ParentActivity) context).finishAffinity();
                intent = new Intent(context, MainActivity.class);
            } else {
                Timber.e("not fetch location");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LOCATION);
            }
            if(disposable!=null)
            disposable.dispose();
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
