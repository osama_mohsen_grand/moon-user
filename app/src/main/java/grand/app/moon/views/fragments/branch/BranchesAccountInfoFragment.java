package grand.app.moon.views.fragments.branch;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.BranchAdapter;
import grand.app.moon.adapter.NotificationAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentBranchesAccountInfoBinding;
import grand.app.moon.databinding.FragmentBranchesAccountInfoBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.models.shop.ShopData;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.branch.BranchesAccountInfoViewModel;
import grand.app.moon.views.activities.MapAddressActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class BranchesAccountInfoFragment extends BaseFragment {

    private static final String TAG = "BranchesAccountInfoFrag";
    AccountInfoResponse accountInfoResponse;
    View rootView;
    private FragmentBranchesAccountInfoBinding binding;
    private BranchesAccountInfoViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_branches_account_info, container, false);
        bind();
        getData();
        return rootView;
    }

    private void bind() {
        AppUtils.initVerticalRV(binding.rvBranches, binding.rvBranches.getContext(), 1);
        viewModel = new BranchesAccountInfoViewModel();
        binding.rvBranches.setAdapter(viewModel.adapter);
        binding.setViewModel(viewModel);
        rootView = binding.getRoot();
    }

    private void getData() {
        Log.d(TAG,"getData");
        if (getArguments() != null && getArguments().containsKey(Constants.ACCOUNT_INFO)) {
            Log.d(TAG,"getData ACCOUNT_INFO");
            accountInfoResponse = (AccountInfoResponse) getArguments().getSerializable(Constants.ACCOUNT_INFO);
            viewModel.adapter.update(accountInfoResponse.branches);
            viewModel.notifyChange();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }
}
