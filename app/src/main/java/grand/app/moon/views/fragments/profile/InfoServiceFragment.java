package grand.app.moon.views.fragments.profile;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ServiceInfoAdapter;
import grand.app.moon.adapter.SocialAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentInfoServiceBinding;
import grand.app.moon.databinding.FragmentProfileSocialBinding;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.profile.ProfileSocialViewModel;
import grand.app.moon.views.fragments.famous.FamousDetailsFragment;

public class InfoServiceFragment extends BaseFragment {
    View rootView;
    private FragmentInfoServiceBinding binding;
    public ProfileSocialViewModel viewModel;
    private static final String TAG = "InfoServiceFragment";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_info_service, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        viewModel = new ProfileSocialViewModel();
//        if(getArguments() != null && getArguments().containsKey(Constants.ID)){
//            viewModel.setId(getArguments().getInt(Constants.ID));
//            viewModel.getData();
//        }
        if (getArguments() != null && getArguments().containsKey(Constants.ACCOUNT_INFO)) {
            viewModel.response = (AccountInfoResponse) getArguments().getSerializable(Constants.ACCOUNT_INFO);
            if(viewModel.response != null && viewModel.response.infoServices != null)
            viewModel.adapter.update(viewModel.response.infoServices);
        }

        binding.setViewModel(viewModel);
    }

    private void bind() {
        AppUtils.initVerticalRV(binding.rvServices, binding.rvServices.getContext(), 1);
        binding.rvServices.setAdapter(viewModel.adapter);
        rootView = binding.getRoot();
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.repository.getMessage());
                Log.d(TAG,action);
                if(action.equals(Constants.ACCOUNT_INFO)){
                    Log.d(TAG,"has ACCOUNT_INFO;");
                    viewModel.response = viewModel.repository.accountInfoResponse;
                    setParentData();
                    viewModel.notifyChange();
                }
            }
        });
    }

    private void setParentData() {
        if(this.getParentFragment() instanceof  FamousDetailsFragment) {
            Log.d(TAG,"has setParentData;");
            FamousDetailsFragment frag = ((FamousDetailsFragment) this.getParentFragment());
            frag.famousDetailsViewModel.accountInfoResponse = viewModel.response;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }

}
