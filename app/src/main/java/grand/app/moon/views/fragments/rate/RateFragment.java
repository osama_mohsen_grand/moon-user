package grand.app.moon.views.fragments.rate;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentRateBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.rate.RateViewModel;
import grand.app.moon.views.activities.MainActivity;

public class RateFragment extends BaseFragment {
    View rootView;
    private FragmentRateBinding fragmentRateBinding;
    private RateViewModel rateViewModel;
    TripDetailsResponse tripDetailsResponse;
    String name="",image="",tripId="";
    public String total = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentRateBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_rate, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ORDER))
            tripDetailsResponse = (TripDetailsResponse) getArguments().getSerializable(Constants.ORDER);
        if(getArguments() != null && getArguments().containsKey(Constants.TRIP_ID))
            tripId = getArguments().getString(Constants.TRIP_ID);
        if(getArguments() != null && getArguments().containsKey(Constants.TOTAL))
            total = getArguments().getString(Constants.TOTAL);
        if(getArguments() != null && getArguments().containsKey(Constants.NAME))
            name = getArguments().getString(Constants.NAME);
        if(getArguments() != null && getArguments().containsKey(Constants.IMAGE))
            image = getArguments().getString(Constants.IMAGE);
    }

    private void bind() {
        rateViewModel = new RateViewModel(tripId,name,image,total);
        fragmentRateBinding.setRateViewModel(rateViewModel);
        rootView = fragmentRateBinding.getRoot();
    }

    private void setEvent() {
        rateViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, rateViewModel.getTripActionRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    ((AppCompatActivity)context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(rateViewModel != null) rateViewModel.reset();
    }

}
