package grand.app.moon.views.fragments.auth;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hbb20.CountryCodePicker;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentPhoneVerificationBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.user.PhoneVerificationViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneVerificationFragment extends BaseFragment {
    private FragmentPhoneVerificationBinding fragmentPhoneVerificationBinding;
    private PhoneVerificationViewModel phoneVerificationViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentPhoneVerificationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_phone_verification, container, false);
        phoneVerificationViewModel = new PhoneVerificationViewModel();
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            phoneVerificationViewModel.request.setRequestKind(getArguments().getString(Constants.TYPE));
        }
        bind();
        setEvent();
        return fragmentPhoneVerificationBinding.getRoot();
    }

    private void bind() {
        phoneVerificationViewModel.cpp = "+" + fragmentPhoneVerificationBinding.ccp.getDefaultCountryCode();
        phoneVerificationViewModel.request.cpp = phoneVerificationViewModel.cpp;
        fragmentPhoneVerificationBinding.ccp.setCountryForNameCode(fragmentPhoneVerificationBinding.ccp.getDefaultCountryNameCode());
        fragmentPhoneVerificationBinding.ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                phoneVerificationViewModel.cpp = "+" + fragmentPhoneVerificationBinding.ccp.getSelectedCountryCode();
                phoneVerificationViewModel.request.cpp = fragmentPhoneVerificationBinding.ccp.getSelectedCountryCode();
                Timber.e("cpp:" + phoneVerificationViewModel.cpp);
            }
        });
        fragmentPhoneVerificationBinding.setPhoneVerificationViewModel(phoneVerificationViewModel);
    }


    private static final String TAG = "PhoneVerificationFragme";

    private void setEvent() {
        phoneVerificationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Timber.e(phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage());
                handleActions(action, phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.WRITE_CODE)) {
                    toastMessage(phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VERIFICATION);
                    Bundle bundle = getArguments();
                    if (bundle == null)
                        bundle = new Bundle();
                    bundle.putString(Constants.TYPE, phoneVerificationViewModel.request.kind);
                    bundle.putString(Constants.PHONE, phoneVerificationViewModel.request.phone);
                    bundle.putString(Constants.VERIFY_ID, phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                    bundle.putString(Constants.NAME_BAR, "");
                    intent.putExtra(Constants.BUNDLE, bundle);
                    ((ParentActivity) context).finish();
                    Log.d(TAG, "type:" + phoneVerificationViewModel.request.kind);
                    startActivity(intent);
                }

            }
        });
        phoneVerificationViewModel.mMutableLiveDataLogin.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Timber.e(phoneVerificationViewModel.getLoginRepository().getMessage());
                handleActions(action, phoneVerificationViewModel.getLoginRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    phoneVerificationViewModel.sendCode(requireActivity());
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (phoneVerificationViewModel != null) phoneVerificationViewModel.reset();
    }

}
