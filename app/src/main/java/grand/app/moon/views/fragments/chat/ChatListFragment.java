package grand.app.moon.views.fragments.chat;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ChatListAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentChatListBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.chat.list.ChatList;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.chat.ChatListViewModel;
import grand.app.moon.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatListFragment extends BaseFragment {


    private View rootView;
    private FragmentChatListBinding fragmentChatBinding;
    private ChatListViewModel chatListViewModel;
    private ChatListAdapter chatListAdapter;



    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentChatBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_list, container, false);
        bind();
        setEvent();
        return rootView;
    }




    private void bind() {
        chatListViewModel = new ChatListViewModel();
        AppUtils.initVerticalRV(fragmentChatBinding.rvChatList, fragmentChatBinding.rvChatList.getContext(), 1);
        fragmentChatBinding.setChatListViewModel(chatListViewModel);
        rootView = fragmentChatBinding.getRoot();
    }

    private void setEvent() {
        chatListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, chatListViewModel.getChatRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.CHAT_LIST)) {

                    if(chatListViewModel.getChatRepository().getChatListResponse().data.size() > 0) {
                        chatListAdapter = new ChatListAdapter(chatListViewModel.getChatRepository().getChatListResponse().data);
                        fragmentChatBinding.rvChatList.setAdapter(chatListAdapter);
                        setEventAdapter();
                    }else {
                        chatListViewModel.noData();
                    }

                }
//                else if (action.equals(Constants.ADD)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.ADD_CATEGORY);
//                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_category));
////                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////                        startActivityForResult(intent, Constants.RELOAD_RESULT,ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
////                    }else
//                    startActivityForResult(intent, Constants.RELOAD_RESULT);
//
//
//                } else if (action.equals(Constants.DELETE)) {
//                    toastMessage(chatListViewModel.getCategoryRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
//                    chatListAdapter.remove(chatListViewModel.category_delete_position);
//                }
            }
        });
    }


    private void setEventAdapter() {
        chatListAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.CHAT_DETAILS)) {
                    ChatList chatList = chatListViewModel.getChatRepository().getChatListResponse().data.get(mutable.position);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID_CHAT,chatList.id);
                    bundle.putInt(Constants.TYPE,chatList.type);
                    bundle.putString(Constants.NAME,chatList.name);
                    bundle.putString(Constants.IMAGE,chatList.image);
                    bundle.putBoolean(Constants.ALLOW_CHAT,true);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
                
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        chatListViewModel.reset();

    }

}
