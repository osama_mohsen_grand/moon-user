package grand.app.moon.views.fragments.cart;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import java.io.File;
import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.RadioAdapter;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.ShippingAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentCheckoutBinding;
import grand.app.moon.databinding.FragmentHomeBinding;
import grand.app.moon.models.cart.CartData;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.models.shipping.Shipping;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.checkout.CheckoutViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckoutFragment extends BaseFragment {

    private FragmentCheckoutBinding fragmentCheckoutBinding;
    private CheckoutViewModel checkoutViewModel;
    private ShippingAdapter shippingAdapter;
    public boolean hasDelegate = false;
    public String delivery = "", total = "", subTotal = "";
    public int flag , type;
    ArrayList<Shipping> shippings = new ArrayList<>();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentCheckoutBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_checkout, container, false);
        getData();
        bind();
        setEvent();
        return fragmentCheckoutBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.HAS_DELEGATE))
            hasDelegate = getArguments().getBoolean(Constants.HAS_DELEGATE);
        if (getArguments() != null && getArguments().containsKey(Constants.TOTAL))
            total = getArguments().getString(Constants.TOTAL);
        if (getArguments() != null && getArguments().containsKey(Constants.SUB_TOTAL))
            subTotal = getArguments().getString(Constants.SUB_TOTAL);
        if (getArguments() != null && getArguments().containsKey(Constants.DELIVERY))
            delivery = getArguments().getString(Constants.DELIVERY);
        if (getArguments() != null && getArguments().containsKey(Constants.FLAG))
            flag = getArguments().getInt(Constants.FLAG);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
    }

    private void bind() {
        checkoutViewModel = new CheckoutViewModel(hasDelegate, total, subTotal, delivery);
        if (getArguments() != null && getArguments().containsKey(Constants.CART))
            checkoutViewModel.cartData = (CartData) getArguments().getSerializable(Constants.CART);
        checkoutViewModel.init();
        if(!AppUtils.isVisibleShipping(type,flag)) {
            fragmentCheckoutBinding.tvCheckoutShipping.setVisibility(View.GONE);
            fragmentCheckoutBinding.rvShipping.setVisibility(View.GONE);
        }
        AppUtils.initVerticalRV(fragmentCheckoutBinding.rvShipping, fragmentCheckoutBinding.rvShipping.getContext(), 1);
        shippings.add(new Shipping(1, ResourceManager.getString(R.string.from_shop), "0"));
        shippings.add(new Shipping(2, ResourceManager.getString(R.string.home_delivery), delivery));
        if (hasDelegate) {
            shippingAdapter = new ShippingAdapter(shippings);
            fragmentCheckoutBinding.rvShipping.setAdapter(shippingAdapter);
            checkoutViewModel.delivery = "0";
            checkoutViewModel.total = subTotal;
            setEventAdapter();
            checkoutViewModel.showPage(true);
        }
        Timber.e("delivery" + delivery);
        fragmentCheckoutBinding.setCheckoutViewModel(checkoutViewModel);
    }

    private void setEvent() {
        checkoutViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, checkoutViewModel.getOrderRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SHIPPING)) {
                    checkoutViewModel.showPage(true);
                    shippings.addAll(checkoutViewModel.getOrderRepository().getShippingResponse().shippings);
                    shippingAdapter = new ShippingAdapter(shippings);
                    fragmentCheckoutBinding.rvShipping.setAdapter(shippingAdapter);
                    setEventAdapter();
//                    checkoutViewModel.notifyChange();
                } else if (action.equals(Constants.LOCATIONS)) {//select location
                    Timber.e("Locations");
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                } else if (action.equals(Constants.SUCCESS)) {
                    toastMessage(checkoutViewModel.getOrderRepository().getMessage());
                    ((ParentActivity)context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }else if(action.equals(Constants.SCROLL_TOP)){
                    fragmentCheckoutBinding.nscrollCheckout.fullScroll(ScrollView.FOCUS_UP);
                }
            }
        });
    }

    private static final String TAG = "CheckoutFragment";
    private void setEventAdapter() {
        Log.d(TAG,"setEventAdapter");
        if(shippingAdapter != null && shippingAdapter.mMutableLiveData != null) {
            Log.d(TAG,"setEventAdapter is Here");

            shippingAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                @Override
                public void onChanged(@Nullable Object o) {
                    Log.d(TAG,"setEventAdapter is Here onChange");

                    int pos = (int) o;
                    Shipping shipping = shippings.get(pos);
                    if (pos < 2) {
                        //delivery , 0 -> take order from shop , 1 -> deliver order to home
                        checkoutViewModel.orderRequest.delivery_method = String.valueOf(shipping.id);
                        if(pos == 0){
                            checkoutViewModel.delivery = "0";
                            checkoutViewModel.total = subTotal;
                        }else{
                            checkoutViewModel.delivery = delivery;
                            checkoutViewModel.total = total;
                        }
                        checkoutViewModel.orderRequest.company_id = null;
                    } else {
                        checkoutViewModel.orderRequest.delivery_method = "3";
                        checkoutViewModel.orderRequest.company_id = String.valueOf(shipping.id);

                    }
                    checkoutViewModel.updateDelivery(shipping.price);
                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            checkoutViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        checkoutViewModel.reset();
    }


}
