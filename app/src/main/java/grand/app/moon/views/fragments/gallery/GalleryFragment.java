package grand.app.moon.views.fragments.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AlbumAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.actionbar.SearchAction;
import grand.app.moon.databinding.FragmentDiscoverBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.clinic.ClinicDetails;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.ShopData;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.BottomSheetDialogHelper;
import grand.app.moon.utils.dialog.BottomSheetFlexDialogHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.discover.DiscoverViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import timber.log.Timber;

public class GalleryFragment extends BaseFragment implements SearchAction {
    View rootView;
    private static final String TAG = "GalleryFragment";
    private FragmentDiscoverBinding fragmentDiscoverBinding;
    private DiscoverViewModel discoverViewModel;
    private Service service;
    String type = Constants.DISCOVER, service_id = "";
    AlbumAdapter albumAdapter;

    ClinicDetailsResponse clinicDetailsResponse;
    List<ImageVideo> data;
    List<ImageVideo> AllResult;
    List<ImageVideo> dataTmp = new ArrayList<>();
    public int id = -1;
    BottomSheetFlexDialogHelper bottomSheetFlexDialogHelper;

    private boolean isOffline = false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,TAG);
        fragmentDiscoverBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_discover, container, false);
        fragmentDiscoverBinding.rvDiscover.setVisibility(View.INVISIBLE);
        getData();
        bind();
        searchSubmit();
        return rootView;
    }


    private void getData() {
        Timber.e("getData:Done Gallery");
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            this.service = (Service) getArguments().getSerializable(Constants.SERVICE);
            Log.d(TAG,"service Serializable");
        }

        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            id = getArguments().getInt(Constants.ID);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE_ID)) {
            service_id = getArguments().getString(Constants.SERVICE_ID);
            Log.d(TAG,"service_id:"+service_id);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.OFFLINE)) {
            isOffline = getArguments().getBoolean(Constants.OFFLINE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.CLINIC_DETAILS)) {
            clinicDetailsResponse = (ClinicDetailsResponse) getArguments().getSerializable(Constants.CLINIC_DETAILS);
            if (type.equals(Constants.ADS))
                data = clinicDetailsResponse.data.adds;
            else if (type.equals(Constants.GALLERY))
                data = clinicDetailsResponse.data.gallery;
        }
        Timber.e("type:" + type);
    }

    private void bind() {
        bottomSheetFlexDialogHelper = new BottomSheetFlexDialogHelper(getActivityBase());

        AppUtils.initVerticalRV(fragmentDiscoverBinding.rvDiscover, fragmentDiscoverBinding.rvDiscover.getContext(), 3);
        discoverViewModel = new DiscoverViewModel();
        if (data != null) {
            setAdapterData(data);
        } else {
            Log.d(TAG,"service:");
            if (service != null) {
                Log.d(TAG,"service not null");
                discoverViewModel = new DiscoverViewModel(service, service_id);
            }else {
                discoverViewModel.getDiscover();
            }
        }
        discoverViewModel.setType(id,type);
        fragmentDiscoverBinding.setDiscoverViewModel(discoverViewModel);
        setEvent();

        if (isOffline) {
            MainActivity mainActivity = (MainActivity) getActivity();
            if (mainActivity != null) {
                mainActivity.homeActionBarView.layoutActionBarHomeBinding.imgHomeBarSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (fragmentDiscoverBinding.search.getVisibility() == View.VISIBLE)
                            fragmentDiscoverBinding.search.setVisibility(View.GONE);
                        else
                            fragmentDiscoverBinding.search.setVisibility(View.VISIBLE);

                    }
                });
                fragmentDiscoverBinding.search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String s = editable.toString();
                        if (AllResult != null) {
                            if (!s.equals("")) {
                                data = new ArrayList<>();
                                for (ImageVideo shopSearch : AllResult) {
                                    if (shopSearch.shopName.contains(s))
                                        data.add(shopSearch);
                                }
                            } else {
                                data = new ArrayList<>(AllResult);
                            }
                            albumAdapter.update(data);
                        }
                    }
                });
            }
        }

        if(type.equals(Constants.ADS))
            fragmentDiscoverBinding.llDiscoverFilter.setVisibility(View.VISIBLE);
        else
            fragmentDiscoverBinding.llDiscoverFilter.setVisibility(View.GONE);

        rootView = fragmentDiscoverBinding.getRoot();
    }

    private void setEvent() {
        discoverViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Log.d(TAG,action);
                handleActions(action, discoverViewModel.getDiscoverRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.DISCOVER)) {
                    Log.d(TAG,"discover Arrived");
                    Log.d(TAG,"discover Arrived : "+discoverViewModel.getDiscoverRepository().getAlbumResponse().data.size());
                    if (discoverViewModel.getDiscoverRepository().getAlbumResponse().data.size() > 0)
                        setAdapterData(discoverViewModel.getDiscoverRepository().getAlbumResponse().data);
                    else {
                        discoverViewModel.noData();
                        fragmentDiscoverBinding.rvDiscover.setVisibility(View.GONE);
                    }

                }else if(action.equals(Constants.FILTER)){
                    if (bottomSheetFlexDialogHelper.filterDialog == null) {
                        bottomSheetFlexDialogHelper.filterFamousShopAds(discoverViewModel.getDiscoverRepository().getFilterShopAdsResponse().data, false, (dialog, type, object) -> {
                            if (type.equals(Constants.ARRAY)) {
                                ArrayList<Integer> list = (ArrayList<Integer>) object;
                                Log.d("list",""+list.size());
                                albumAdapter.submitFilter(list);
                            }
                        });
                    } else {
                        bottomSheetFlexDialogHelper.filterDialog.show();
                    }
                }
            }
        });
    }

    private void setAdapterData(List<ImageVideo> data) {
        Timber.e("size:" + data.size());
        this.data = data;
        AllResult = data;
        albumAdapter = new AlbumAdapter(data,true);
        fragmentDiscoverBinding.rvDiscover.setAdapter(albumAdapter);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                fragmentDiscoverBinding.rvDiscover.setVisibility(View.VISIBLE);
            }
        }, 500);


        setEventAdapter();
    }


    private void setEventAdapter() {
//        albumAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
//            @Override
//            public void onChanged(@Nullable Object o) {
//                Mutable mutable = (Mutable) o;
//                if (mutable.type.equals(Constants.IMAGE)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
//                    intent.putExtra(Constants.NAME_BAR, data.get(mutable.position).name);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Constants.IMAGE, data.get(mutable.position).image);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivityForResult(intent, Constants.RELOAD_RESULT);
//                } else if (mutable.type.equals(Constants.VIDEO)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
//                    intent.putExtra(Constants.NAME_BAR, data.get(mutable.position).name);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(Constants.VIDEO, data.get(mutable.position).image);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivity(intent);
//                }
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (discoverViewModel != null)
            discoverViewModel.reset();

    }

    @Override
    public void searchSubmit() {
        Timber.e("Submit:SearchGalleryFragment");
    }
}
