package grand.app.moon.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentLanguageBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.LanguagesHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.language.LanguageViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.vollyutils.MyApplication;


public class LanguageFragment extends BaseFragment {
    View rootView;
    private FragmentLanguageBinding fragmentLanguageBinding;
    private LanguageViewModel languageViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLanguageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_language, container, false);
        bind();
        rootView = fragmentLanguageBinding.getRoot();
        return rootView;
    }

    private void bind() {
        languageViewModel = new LanguageViewModel();
        setEvents();
        fragmentLanguageBinding.setLanguageViewModel(languageViewModel);
    }

    private void setEvents() {
        languageViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
//                handleActions(action,languageViewModel.getPrivacyRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.LANGUAGE)){
                    LanguagesHelper.setLanguage(languageViewModel.getLanguage());
                    LanguagesHelper.changeLanguage(context,languageViewModel.getLanguage());
                    LanguagesHelper.changeLanguage(MyApplication.getInstance(),languageViewModel.getLanguage());
                    UserHelper.saveKey(Constants.LANGUAGE_HAVE,Constants.TRUE);
                    UserHelper.saveKey(Constants.LANGUAGE_HAVE, "true");

                    Intent intent = null;
                    if(getActivity() instanceof MainActivity) {
                        intent = new Intent(context, MainActivity.class);
                        getActivityBase().finishAffinity();
                        startActivity(intent);
                    }else {
                        intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                        intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.country));
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        languageViewModel.reset();
    }
}
