package grand.app.moon.views.fragments.ads;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import grand.app.moon.R;
import grand.app.moon.adapter.CompanyAdsCategoriesAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentCompanyAdsCategoriesBinding;
import grand.app.moon.models.famous.details.AlbumModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.ads.CompanyAdsCategoriesViewModel;

public class FamousCompanyAdsCategoriesFragment extends BaseFragment {

    private FragmentCompanyAdsCategoriesBinding binding;
    private CompanyAdsCategoriesViewModel viewModel;
    private CompanyAdsCategoriesAdapter adapter;
    AlbumModel albumModel = null;

    private static final String TAG = "FamousCompanyAdsCategoriesFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UserHelper.saveKey(Constants.RELOAD, Constants.FALSE);
        Log.d(TAG,"done");
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_company_ads_categories, container, false);
        getData();
        bind();
        return binding.getRoot();

    }

    private void getData() {
        viewModel = new CompanyAdsCategoriesViewModel();
        if (getArguments() != null && getArguments().containsKey(Constants.CATEGORIES)){
            Log.d(TAG,"has argument");
            albumModel = (AlbumModel) getArguments().getSerializable(Constants.CATEGORIES);
            adapter = new CompanyAdsCategoriesAdapter(albumModel.categories);
            adapter.famous_id = getArguments().getInt(Constants.FAMOUS_ID);
        }
    }


    private void bind() {
        AppUtils.initVerticalRV(binding.rvCompanyAdsCategories, binding.rvCompanyAdsCategories.getContext(), 3);
        binding.rvCompanyAdsCategories.setAdapter(adapter);
        binding.setViewModel(viewModel);
    }

}
