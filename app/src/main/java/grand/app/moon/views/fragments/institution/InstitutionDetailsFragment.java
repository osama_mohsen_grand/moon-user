package grand.app.moon.views.fragments.institution;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.InstitutionServicesAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentInstituationDetailsBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.order.details.OrderDetails;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.models.shop.ShopData;
import grand.app.moon.models.shop.ShopDetails;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.institution.InstitutionDetailsViewModel;
import grand.app.moon.viewmodels.institution.InstitutionViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstitutionDetailsFragment extends BaseFragment {

    FragmentInstituationDetailsBinding fragmentInstituationDetailsBinding;
    InstitutionDetailsViewModel institutionDetailsViewModel;
    private Service service;
    private int id;
    private ShopData shopData;
    InstitutionServicesAdapter institutionServicesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentInstituationDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_instituation_details, container, false);
        AppUtils.initVerticalRV(fragmentInstituationDetailsBinding.rvServiceInstitution, fragmentInstituationDetailsBinding.rvServiceInstitution.getContext(), 1);
        getData();
        return fragmentInstituationDetailsBinding.getRoot();
    }


    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);

        }
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            id = getArguments().getInt(Constants.ID);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.INSTITUTIONS)){
            shopData = (ShopData) getArguments().getSerializable(Constants.INSTITUTIONS);
            institutionServicesAdapter = new InstitutionServicesAdapter(shopData.categories);
            fragmentInstituationDetailsBinding.rvServiceInstitution.setAdapter(institutionServicesAdapter);
        }
    }

    public void bind(){
        institutionDetailsViewModel = new InstitutionDetailsViewModel(id, service,shopData.shop_details);
        fragmentInstituationDetailsBinding.setInstitutionDetailsViewModel(institutionDetailsViewModel);
    }

    private void setEvent() {
        institutionDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.CHAT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID_CHAT,shopData.shop_details.id);
                    bundle.putInt(Constants.TYPE,shopData.shop_details.type);
                    bundle.putString(Constants.NAME,shopData.shop_details.name);
                    bundle.putString(Constants.IMAGE,shopData.shop_details.image);
                    bundle.putBoolean(Constants.ALLOW_CHAT,true);
                    bundle.putBoolean(Constants.CHAT_FIRST,true);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }else if(action.equals(Constants.LOCATIONS)){
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, shopData.shop_details.lat);
                    intent.putExtra(Constants.LNG, shopData.shop_details.lng);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
        setEvent();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(institutionDetailsViewModel != null)
            institutionDetailsViewModel.reset();
    }
    
    
}
