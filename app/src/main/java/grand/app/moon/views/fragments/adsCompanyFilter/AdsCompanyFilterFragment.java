package grand.app.moon.views.fragments.adsCompanyFilter;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AdsCompanyFilterAdapter;
import grand.app.moon.adapter.CountryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentAdsCompanyFilterBinding;
import grand.app.moon.databinding.FragmentCountryBinding;
import grand.app.moon.models.adsCompanyFilter.AdsCompanyFilter;
import grand.app.moon.models.adsCompanyFilter.AdsCompanyFilterResponse;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.adsCompanyFilter.AdsCompanyFilterViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdsCompanyFilterFragment extends BaseFragment {


    View rootView;
    private static final String TAG = "AdsCompanyFilterFragmen";
    private FragmentAdsCompanyFilterBinding fragmentAdsCompanyFilterBinding;
    private AdsCompanyFilterViewModel adsCompanyFilterViewModel;
    private AdsCompanyFilterAdapter adsCompanyFilterAdapter;
    AdsCompanyFilterResponse adsCompanyFilterResponse = null;
    int service_id = -1, category_id = -1, position = -1;
    public String from = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAdsCompanyFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ads_company_filter, container, false);
        getData();
        bind();
        rootView = fragmentAdsCompanyFilterBinding.getRoot();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            service_id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.FROM))
            from = getArguments().getString(Constants.FROM);
        if (getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID))
            category_id = getArguments().getInt(Constants.CATEGORY_ID);
        if (getArguments() != null && getArguments().containsKey(Constants.POSITION))
            position = getArguments().getInt(Constants.POSITION);
        if (getArguments() != null && getArguments().containsKey(Constants.ADS_COMPANY_FILTER))
            adsCompanyFilterResponse = (AdsCompanyFilterResponse) getArguments().getSerializable(Constants.ADS_COMPANY_FILTER);
    }

    private void bind() {
        AppUtils.initVerticalRV(fragmentAdsCompanyFilterBinding.rvFilter, fragmentAdsCompanyFilterBinding.rvFilter.getContext(), 1);
        if (category_id == -1) {
            adsCompanyFilterViewModel = new AdsCompanyFilterViewModel(service_id);
        } else {
            adsCompanyFilterViewModel = new AdsCompanyFilterViewModel();
            setAdapter(adsCompanyFilterResponse.data.get(position).subCategory);
        }
        setEvents();
        fragmentAdsCompanyFilterBinding.setAdsCompanyFilterViewModel(adsCompanyFilterViewModel);


    }

    private void setEvents() {
        adsCompanyFilterViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, adsCompanyFilterViewModel.getAdvertisementCompanyRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    adsCompanyFilterResponse = adsCompanyFilterViewModel.getAdvertisementCompanyRepository().getAdsCompanyFilterResponse();
                    setAdapter(adsCompanyFilterResponse.data);
                }
            }
        });
    }

    public void setAdapter(List<AdsCompanyFilter> adsCompanyFilters) {
        adsCompanyFilterAdapter = new AdsCompanyFilterAdapter(adsCompanyFilters);
        adsCompanyFilterViewModel.showPage(true);
        fragmentAdsCompanyFilterBinding.rvFilter.setAdapter(adsCompanyFilterAdapter);
        setEventAdapter();
    }

    private void setEventAdapter() {
        adsCompanyFilterAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Timber.e("onChanged:pos"+pos);
                Timber.e("onChanged:position"+position);
                Timber.e("onChanged:category_id"+category_id);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
                Bundle bundle = getArguments() != null ? getArguments() : new Bundle();
                if (category_id == -1) {
                    intent.putExtra(Constants.PAGE, Constants.ADS_COMPANY_FILTER);
                    category_id = adsCompanyFilterResponse.data.get(pos).id;
                    bundle.putInt(Constants.CATEGORY_ID, category_id);
                    bundle.putInt(Constants.POSITION, pos);
                    bundle.putSerializable(Constants.ADS_COMPANY_FILTER, adsCompanyFilterResponse);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    getActivityBase().setResult(Constants.FILTER_RESULT, intent);
//                    ((ParentActivity) context).finish();//finishing activity
                    startActivityForResult(intent, Constants.FILTER_RESULT);
                } else if (position != -1) {
                    if (from.equals(Constants.ADS_COMPANY_FILTER)) {
                        intent = new Intent();
                        intent.putExtra(Constants.CATEGORY_ID, category_id);
                        intent.putExtra(Constants.SUB_CATEGORY_ID, adsCompanyFilterResponse.data.get(position).subCategory.get(pos).id);
                        Timber.e("category_id:"+category_id);
                        Timber.e("subcategory_id:"+adsCompanyFilterResponse.data.get(position).subCategory.get(pos).id);
                        ((ParentActivity) context).setResult(Constants.FILTER_RESULT, intent);
                        ((ParentActivity) context).finish();//finishing activity
                    } else if(from.equals(Constants.ADD_ADS)) {
                        intent.putExtra(Constants.PAGE,Constants.ADD_ADS);
                        bundle.putInt(Constants.ID, service_id);
                        bundle.putInt(Constants.CATEGORY_ID, category_id);
                        bundle.putInt(Constants.SUB_CATEGORY_ID, adsCompanyFilterResponse.data.get(position).subCategory.get(pos).id);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        getActivityBase().finish();
                        startActivity(intent);
                    }
                }
            }
        });
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e( "onActivityResult: result "+requestCode+","+resultCode );
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            Timber.e("CallService");
            Timber.e("cat:"+data.getIntExtra(Constants.CATEGORY_ID,-1));
            Timber.e("sub_cat:"+data.getIntExtra(Constants.SUB_CATEGORY_ID,-1));

            Intent intent = new Intent();
            intent.putExtra(Constants.CATEGORY_ID, data.getIntExtra(Constants.CATEGORY_ID,-1));
            intent.putExtra(Constants.SUB_CATEGORY_ID, data.getIntExtra(Constants.SUB_CATEGORY_ID,-1));
            ((ParentActivity) context).setResult(Constants.FILTER_RESULT, intent);
            ((ParentActivity) context).finish();//finishing activity


//            companiesDetailsViewModel.category_id = data.getIntExtra(Constants.CATEGORY_ID,-1);
//            companiesDetailsViewModel.sub_category_id = data.getIntExtra(Constants.SUB_CATEGORY_ID,-1);
//            resetAdapter();
        }else {
            category_id = -1;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        adsCompanyFilterViewModel.reset();
    }
}
