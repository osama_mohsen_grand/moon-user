package grand.app.moon.views.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropFragment;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.customviews.facebook.FacebookModel;
import grand.app.moon.databinding.FragmentRegisterBinding;
import grand.app.moon.models.app.AccountTypeModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.Datum;
import grand.app.moon.models.country.Region;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.viewmodels.user.RegisterViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

public class RegisterFragment extends BaseFragment {
    View rootView;
    private FragmentRegisterBinding fragmentRegisterBinding;
    private RegisterViewModel registerViewModel;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    public List<Region> regions = new ArrayList<>();
    public String type = "", phone = "";
    public FacebookModel facebookModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);

        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {
            phone = getArguments().getString(Constants.PHONE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.FACEBOOK)) {
            facebookModel = (FacebookModel) getArguments().getSerializable(Constants.FACEBOOK);
        }
    }

    private void bind() {
        registerViewModel = new RegisterViewModel(phone);
        if (facebookModel != null) {
            fragmentRegisterBinding.imgRegisterUser.setImageResource(android.R.color.transparent);
            ImageLoaderHelper.ImageLoaderLoad(context, facebookModel.image, fragmentRegisterBinding.imgRegisterUser);
            registerViewModel.setSocial(facebookModel);
        }
        fragmentRegisterBinding.setRegisterViewModel(registerViewModel);
        rootView = fragmentRegisterBinding.getRoot();
    }

    private void setEvent() {
        registerViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, registerViewModel.getRegisterRepository().getMessage());
                if (action.equals(Constants.LOGIN)) {
                    ((AppCompatActivity) context).finish();
                } else if (action.equals(Constants.REGISTRATION)) {
                    toastMessage(registerViewModel.getRegisterRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, MainActivity.class);
                    ((ParentActivity) context).finishAffinity();
                    startActivity(intent);
                } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                    pickImageDialogSelect();
                } else if (action.equals(Constants.LOCATIONS)) {//select location
                    Timber.e("Locations");
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(registerViewModel.baseError);
                } else if (action.equals(Constants.COUNTRIES)) {//load countries
                    registerViewModel.showPage(true);
                    fragmentRegisterBinding.edtRegisterCity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ArrayList<String> popUpCities = new ArrayList<>();
                            Datum country = AppMoon.getCities(UserHelper.retrieveKey(Constants.COUNTRY_ID), registerViewModel.getCountryRepository().getCountriesResponse().data);
                            if (country != null) {
                                Timber.e("country:" + country.countryName);
                                for (City city : country.cities)
                                    popUpCities.add(city.cityName);
                                popUpMenuHelper.openPopUp(getActivity(), fragmentRegisterBinding.edtRegisterCity, popUpCities, new PopUpInterface() {
                                    @Override
                                    public void submitPopUp(int position) {
                                        registerViewModel.registerUserRequest.setCity_id(String.valueOf(country.cities.get(position).id));
                                        fragmentRegisterBinding.edtRegisterCity.setText(country.cities.get(position).cityName);
                                        fragmentRegisterBinding.edtRegisterRegion.setText("");
                                        regions = new ArrayList<>(country.cities.get(position).regions);
                                        Timber.e("regions" + regions.size());
                                        getRegions();
                                    }
                                });
                            }
                        }
                    });

                    fragmentRegisterBinding.edtRegisterRegion.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getRegions();
                        }
                    });
                }
            }
        });
    }

    private void getRegions() {
        ArrayList<String> popUpRegions = new ArrayList<>();
        for (Region region : regions)
            popUpRegions.add(region.regionName);
        Timber.e("size:" + popUpRegions.size());
        popUpMenuHelper.openPopUp(getActivity(), fragmentRegisterBinding.edtRegisterRegion, popUpRegions, new PopUpInterface() {
            @Override
            public void submitPopUp(int position) {
                registerViewModel.registerUserRequest.setRegion_id(String.valueOf(regions.get(position).id));
                fragmentRegisterBinding.edtRegisterRegion.setText(regions.get(position).regionName);
            }
        });
    }

    private static final String TAG = "RegisterFragment";
    
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityRequest:" + requestCode);
        Timber.e("onActivityResult:" + resultCode);
        if(requestCode == Constants.FILE_TYPE_IMAGE && resultCode == RESULT_OK){
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            registerViewModel.volleyFileObject = volleyFileObject;
            fragmentRegisterBinding.imgRegisterUser.setImageURI(null);
            fragmentRegisterBinding.imgRegisterUser.setImageURI(Uri.parse(volleyFileObject.getFile().getAbsolutePath()));
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, RegisterFragment.this);
            } else if (data != null && data.getData() != null) {
                String path = FileOperations.getPath(requireContext(), data.getData());
                Log.d(TAG,"path:"+path);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            Log.d("onActivityResult", "request done");
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
//                VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), resultUri.getPath(), Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
                fragmentRegisterBinding.imgRegisterUser.setImageBitmap(volleyFileObject.getBitmap());
                registerViewModel.volleyFileObject = volleyFileObject;
                fragmentRegisterBinding.imgRegisterUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            registerViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        registerViewModel.reset();

    }

}
