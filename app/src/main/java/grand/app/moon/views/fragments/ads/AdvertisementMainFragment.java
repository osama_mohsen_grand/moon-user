package grand.app.moon.views.fragments.ads;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AdsCompanyAdapter;
import grand.app.moon.adapter.ServiceCategoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentAdvertisementMainBinding;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.ads.AdvertisementMainViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementMainFragment extends BaseFragment {
    private FragmentAdvertisementMainBinding fragmentAdvertisementMainBinding;
    private AdvertisementMainViewModel advertisementMainViewModel;
    public ServiceCategoryAdapter serviceCategoryAdapter;
    public AdsCompanyAdapter adsCompanyAdapter;
    public int filter = 1;
    List<Service> services = null;
    List<AdsCompanyModel> ads = null;
    Service service;
    boolean setServiceCategory = false;
    public String service_id = "";
    double lat ,lng;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAdvertisementMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_advertisement_main, container, false);
        getData();
        lat = UserHelper.getLat();
        lng = UserHelper.getLng();
        bind();
        setEvent();
        setSearchAction();
        return fragmentAdvertisementMainBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.SERVICE))
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
    }



    private void setSearchAction() {
        ((BaseActivity)getActivityBase()).backActionBarView.layoutActionBarBackBinding.imgBaseBarSearch.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), BaseActivity.class);
            intent.putExtra(Constants.PAGE,Constants.SEARCH);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.FLAG,service.mFlag);
            bundle.putInt(Constants.TYPE,service.mType);
            bundle.putSerializable(Constants.SERVICE,service);
            bundle.putInt(Constants.SERVICE_ID,Integer.parseInt(service_id));
            intent.putExtra(Constants.BUNDLE,bundle);
            startActivity(intent);
        });
    }


    private void bind() {
        advertisementMainViewModel = new AdvertisementMainViewModel(Constants.ADS);
        serviceCategoryAdapter = new ServiceCategoryAdapter(new ArrayList<>());
        AppUtils.initHorizontalRV(fragmentAdvertisementMainBinding.rvAdsCategory, fragmentAdvertisementMainBinding.rvAdsCategory.getContext(), 1);
        AppUtils.initVerticalRV(fragmentAdvertisementMainBinding.rvAdsList, fragmentAdvertisementMainBinding.rvAdsList.getContext(), 2);
        fragmentAdvertisementMainBinding.setAdvertisementMainViewModel(advertisementMainViewModel);
    }

    private void setEvent() {

        advertisementMainViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, advertisementMainViewModel.getAdvertisementCompanyRepository().getMessage());
                if(action.equals(Constants.SUCCESS)){
                    services = advertisementMainViewModel.getAdvertisementCompanyRepository().getAdsMainResponse().mData.mServices;
                    ads = advertisementMainViewModel.getAdvertisementCompanyRepository().getAdsMainResponse().mData.ads;
                    //set service
                    if (!setServiceCategory) {
                        serviceCategoryAdapter = new ServiceCategoryAdapter(advertisementMainViewModel.getAdvertisementCompanyRepository().getAdsMainResponse().mData.mServices);
                        fragmentAdvertisementMainBinding.rvAdsCategory.setAdapter(serviceCategoryAdapter);
                        setServiceCategory = true;
                        if (services.size() > 0) {
                            advertisementMainViewModel.service_id = services.get(0).mId;
                            service_id = advertisementMainViewModel.service_id+"";
                        }
                        //set shops
                        adsCompanyAdapter = new AdsCompanyAdapter(advertisementMainViewModel.getAdvertisementCompanyRepository().getAdsMainResponse().mData.ads);

                        fragmentAdvertisementMainBinding.rvAdsList.setAdapter(adsCompanyAdapter);
                        setEventAdapter();

                    } else {
                        serviceCategoryAdapter.update(services);
                        adsCompanyAdapter.update(ads);
                    }
                }else if(action.equals(Constants.CITY)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.CITY);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.city));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID,advertisementMainViewModel.city_id);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivityForResult(intent, Constants.CITY_RESULT);
                }else if(action.equals(Constants.FILTER)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADS_COMPANY_FILTER);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FROM,Constants.ADS_COMPANY_FILTER);
                    bundle.putInt(Constants.ID,advertisementMainViewModel.service_id);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivityForResult(intent, Constants.FILTER_RESULT);
                }else if(action.equals(Constants.ADD)){
                    if(UserHelper.getUserId() != -1) {
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.ADS_CATEGORY);
                        intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.FROM, Constants.ADD_ADS);
                        bundle.putSerializable(Constants.ADS_MAIN, advertisementMainViewModel.getAdvertisementCompanyRepository().getAdsMainResponse().mData);
                        bundle.putInt(Constants.ID, advertisementMainViewModel.service_id);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        startActivity(intent);
                    }else {
                        AppUtils.pleaseLoginFirst(getActivityBase(), new DialogHelperInterface() {
                            @Override
                            public void OnClickListenerContinue(Dialog dialog, View view) {
                                Intent intent = new Intent(context,BaseActivity.class);
                                intent.putExtra(Constants.PAGE,Constants.LOGIN);
                                getActivityBase().finishAffinity();
                                startActivity(intent);
                            }
                        });
                    }
                }
            }
        });
    }



    private void setEventAdapter() {
        serviceCategoryAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                Timber.e("pos:"+position);
                serviceCategoryAdapter.selected = position;
                advertisementMainViewModel.service_id = services.get(position).mId;
                service_id = services.get(position).mId+"";
                serviceCategoryAdapter.notifyDataSetChanged();
                advertisementMainViewModel.callService();
            }
        });

        Timber.e("shop_id"+service.mId);
        Timber.e("shop_id_service"+service_id);

        adsCompanyAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                AdsCompanyModel adsCompanyModel = advertisementMainViewModel.getAdvertisementCompanyRepository().getAdsMainResponse().mData.ads.get(position);

                Intent intent = new Intent(context,BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.ADS_DETAILS);
                intent.putExtra(Constants.NAME_BAR,adsCompanyModel.name);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.TYPE,service.mType);
                bundle.putInt(Constants.ID,adsCompanyModel.id);
                bundle.putInt(Constants.SHOP_ID,service.mId);
                Timber.e("shop_id:"+service.mId);
                Timber.e("shop_id_service"+service_id);
                intent.putExtra(Constants.BUNDLE,bundle);
                startActivity(intent);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("requestCode"+requestCode);
        Timber.e("resultCode"+resultCode);
        if (requestCode == Constants.CITY_RESULT && resultCode == Constants.CITY_RESULT) {
            advertisementMainViewModel.city_id = data.getIntExtra(Constants.SELECTED,-1);
            advertisementMainViewModel.callService();
        }
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            Timber.e("CallService");
            advertisementMainViewModel.category_id = data.getIntExtra(Constants.CATEGORY_ID,-1);
            advertisementMainViewModel.sub_category_id = data.getIntExtra(Constants.SUB_CATEGORY_ID,-1);
            advertisementMainViewModel.callService();
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            UserHelper.saveKey(Constants.LAT,""+data.getDoubleExtra("lat", 0));
            UserHelper.saveKey(Constants.LNG,""+data.getDoubleExtra("lng", 0));
            Timber.e("lat:"+data.getDoubleExtra("lat", 0));
            Timber.e("lng:"+data.getDoubleExtra("lng", 0));
            ((BaseActivity)context).backActionBarView.showAddress();
            advertisementMainViewModel.callService();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    public void onResume() {
        super.onResume();
        if(isReloadPage())
            advertisementMainViewModel.callService();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (advertisementMainViewModel != null) {
            advertisementMainViewModel.reset();
            UserHelper.saveKey(Constants.LAT,lat+"");
            UserHelper.saveKey(Constants.LNG,lng+"");
        }
    }

}
