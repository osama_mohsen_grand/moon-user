package grand.app.moon.views.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentProfileBinding;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.Datum;
import grand.app.moon.models.country.Region;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.utils.dialog.DialogChoosePhoto;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.utils.upload.FilesUtilsFragment;
import grand.app.moon.viewmodels.user.ProfileViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.views.fragments.ads.AddAdvertisementFragment;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends BaseFragment {
    View rootView;
    private FragmentProfileBinding fragmentProfileBinding;
    private ProfileViewModel profileViewModel;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    public String type = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
    }

    private void bind() {
        profileViewModel = new ProfileViewModel();
        fragmentProfileBinding.setProfileViewModel(profileViewModel);
        rootView = fragmentProfileBinding.getRoot();
    }

    ArrayList<String> countries = new ArrayList<>();
    ArrayList<String> regions = new ArrayList<>();
    int city_position = -1;
    private static final String TAG = "ProfileFragment";
    private void setEvent() {
        profileViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, profileViewModel.getLoginRepository().getMessage());
            Timber.e(action);
            if (action.equals(Constants.SUCCESS)) {//submitSearch register
                toastMessage(profileViewModel.getLoginRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                ((MainActivity) context).navigationDrawerView.setHeader();
            } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                pickImageDialogSelect();
            } else if (action.equals(Constants.LOCATIONS)) {//select location
                Timber.e("Locations");
                Intent intent = new Intent(context, MapAddressActivity.class);
                startActivityForResult(intent, Constants.ADDRESS_RESULT);
            } else if (action.equals(Constants.CHANGE_PASSWORD)) {
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.CHANGE_PASSWORD);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.change_password));
                startActivity(intent);
            }
//            else if(action.equals(Constants.LANGUAGE)){
//                Intent intent = new Intent(context,BaseActivity.class);
//                intent.putExtra(Constants.PAGE, Constants.LANGUAGE);
//                intent.putExtra(Constants.NAME_BAR, getString(R.string.label_language));
//                startActivity(intent);
//            }else if(action.equals(Constants.COUNTRIES)){
//                Intent intent = new Intent(context,BaseActivity.class);
//                intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
//                intent.putExtra(Constants.NAME_BAR, getString(R.string.country));
//                startActivity(intent);
//            }
            else if (action.equals(Constants.COUNTRIES)) {
                profileViewModel.showPage(true);
                for (Datum country : profileViewModel.getLoginRepository().getCountriesResponse().data)
                    countries.add(country.countryName);
                profileViewModel.setData();
            } else if (action.equals(Constants.CHOOSE)) {
                popUpMenuHelper.openPopUp(getActivity(), fragmentProfileBinding.edtRegisterCountry, countries, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        profileViewModel.country_position = position;
                        profileViewModel.profileRequest.country_id = profileViewModel.getLoginRepository().getCountriesResponse().data.get(position).id + "";
                        profileViewModel.profileRequest.country_name = profileViewModel.getLoginRepository().getCountriesResponse().data.get(position).countryName;
                        profileViewModel.setHelper(profileViewModel.profileRequest.country_id);
                        profileViewModel.cities.clear();
                        regions.clear();

                        for(City city : profileViewModel.getLoginRepository().getCountriesResponse().data.get(position).cities){
                            profileViewModel.cities.add(city.cityName);
                        }

                        profileViewModel.profileRequest.city_name = "";
                        profileViewModel.profileRequest.region_name = "";

                        profileViewModel.profileRequest.city_id = "";
                        profileViewModel.profileRequest.region_id = "";

                        profileViewModel.notifyChange();
                        fragmentProfileBinding.edtRegisterCity.performClick();
                    }
                });
            }else if(action.equals(Constants.CITY)){
                Log.d(TAG,"city");
                popUpMenuHelper.openPopUp(getActivity(), fragmentProfileBinding.edtRegisterCity, profileViewModel.cities, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        Log.d(TAG,"position");
                        if(profileViewModel.country_position != -1) {
                            City city = profileViewModel.getLoginRepository().getCountriesResponse().data
                                    .get(profileViewModel.country_position).cities.get(position);

                            city_position  = position;
                            profileViewModel.profileRequest.city_id = city.id+"";
                            profileViewModel.profileRequest.city_name = city.cityName;


                            profileViewModel.profileRequest.region_name = "";
                            profileViewModel.profileRequest.region_id = "";
                            regions.clear();
                            for (Region region : profileViewModel.getLoginRepository().getCountriesResponse().data
                                    .get(profileViewModel.country_position).cities.get(position).regions) {
                                regions.add(region.regionName);
                            }
                            profileViewModel.notifyChange();
                            fragmentProfileBinding.edtRegisterRegion.performClick();
                        }
                    }
                });
            }else if(action.equals(Constants.REGION)){
                Log.d(TAG,"region");
                popUpMenuHelper.openPopUp(getActivity(), fragmentProfileBinding.edtRegisterRegion, regions, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        Log.d(TAG,"region:"+position);
                        if(profileViewModel.country_position != -1 && city_position != -1) {
                            Region region = profileViewModel.getLoginRepository()
                                    .getCountriesResponse().data.get(profileViewModel.country_position)
                                    .cities.get(city_position).regions.get(position);
                            profileViewModel.profileRequest.region_id = region.id+"";

                            profileViewModel.profileRequest.region_name = region.regionName;
                            profileViewModel.notifyChange();
                        }
                    }
                });
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            profileViewModel.setImage(volleyFileObject);
            fragmentProfileBinding.imgProfileUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
//            fragmentProfileBinding.imgProfileUser.setImageBitmap(volleyFileObject.getBitmap());
            UCrop.of(Uri.fromFile(volleyFileObject.getFile()), Uri.fromFile(volleyFileObject.getFile())).start(context, ProfileFragment.this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, ProfileFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            Log.d("onActivityResult","request done");
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
//                VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), resultUri.getPath(), Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
                fragmentProfileBinding.imgProfileUser.setImageBitmap(volleyFileObject.getBitmap());
                profileViewModel.profileRequest.volleyFileObject = volleyFileObject;
                fragmentProfileBinding.imgProfileUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
            }
        }
        if (resultCode == RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            profileViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        profileViewModel.reset();

    }


}
