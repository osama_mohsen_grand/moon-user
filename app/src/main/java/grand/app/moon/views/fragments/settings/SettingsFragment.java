package grand.app.moon.views.fragments.settings;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentSettingsBinding;
import grand.app.moon.models.settings.Settings;
import grand.app.moon.models.settings.SettingsModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.app.SettingsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BaseFragment {

    private FragmentSettingsBinding fragmentSettingsBinding;
    private SettingsViewModel viewModel;
    private int type = 1;
    private Settings settings;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        bind();
        getData();
        return fragmentSettingsBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.SETTINGS)){
            settings = (Settings) getArguments().getSerializable(Constants.SETTINGS);
            if(settings != null && settings.settings.size() > 0) {
                viewModel.adapter.update(settings.settings);
            }else{
                viewModel.noData();
            }
        }
    }

    private void bind() {
        viewModel = new SettingsViewModel(type);
        fragmentSettingsBinding.setViewModel(viewModel);
    }




}
