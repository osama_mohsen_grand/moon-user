package grand.app.moon.views.fragments.review;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ReviewAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentAddReviewBinding;
import grand.app.moon.databinding.FragmentReviewBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.review.AddReviewViewModel;
import grand.app.moon.viewmodels.review.ReviewViewModel;
import grand.app.moon.views.activities.BaseActivity;

public class AddReviewFragment extends BaseFragment {
    View rootView;
    private FragmentAddReviewBinding fragmentAddReviewBinding;
    private AddReviewViewModel addReviewViewModel;
    int shop_id = -1 , type= -1;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAddReviewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_review, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            shop_id = getArguments().getInt(Constants.ID);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)) { // type
            type = getArguments().getInt(Constants.TYPE);
        }
    }

    private void bind() {
        addReviewViewModel = new AddReviewViewModel(shop_id,type);
        fragmentAddReviewBinding.setAddReviewViewModel(addReviewViewModel);
        rootView = fragmentAddReviewBinding.getRoot();
    }


    private void setEvent() {
        addReviewViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addReviewViewModel.getReviewRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    UserHelper.saveKey(Constants.RELOAD,Constants.TRUE);
                    ((ParentActivity)context).finish();
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(addReviewViewModel != null)
            addReviewViewModel.reset();

    }


}
