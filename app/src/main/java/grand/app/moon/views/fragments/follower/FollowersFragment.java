package grand.app.moon.views.fragments.follower;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.FollowerAdapter;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFollowersBinding;
import grand.app.moon.databinding.FragmentHomeBinding;
import grand.app.moon.models.followers.Follower;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.followers.FollowersViewModel;
import grand.app.moon.viewmodels.home.HomeViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowersFragment extends BaseFragment {

    private FragmentFollowersBinding fragmentFollowersBinding;
    private FollowersViewModel followersViewModel;
    private StoryAdapter storyAdapter;
    private FollowerAdapter followerAdapter;



    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentFollowersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_followers, container, false);
        bind();
        setEvent();
        return fragmentFollowersBinding.getRoot();
    }

    private void bind() {
        followersViewModel = new FollowersViewModel();
        AppUtils.initHorizontalRV(fragmentFollowersBinding.rvStories, fragmentFollowersBinding.rvStories.getContext(), 1);
        AppUtils.initVerticalRV(fragmentFollowersBinding.rvFollowers, fragmentFollowersBinding.rvFollowers.getContext(), 1);
        fragmentFollowersBinding.setFollowersViewModel(followersViewModel);
    }


    private void setEvent() {
        followersViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, followersViewModel.getMoonMapRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    storyAdapter = new StoryAdapter(followersViewModel.getMoonMapRepository().getMoonMapResponse().stories,FollowersFragment.this);
                    fragmentFollowersBinding.rvStories.setAdapter(storyAdapter);

                    followerAdapter = new FollowerAdapter(followersViewModel.getMoonMapRepository().getMoonMapResponse().followers);
                    fragmentFollowersBinding.rvFollowers.setAdapter(followerAdapter);
                    setEventAdapter();
                }else if(action.equals(Constants.FOLLOW)){
                    followerAdapter.updateItem();
                }
            }
        });
    }

    private void setEventAdapter() {

        followerAdapter.getMutableLiveData().observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Follower follower = (Follower) o;
                if(followerAdapter.type.equals(Constants.FOLLOW)) {
                    followersViewModel.getMoonMapRepository().follow(new FollowRequest(follower.id, follower.type));
                }else{
                    Intent intent = new Intent(context, BaseActivity.class);
                    Bundle bundle = new Bundle();
                    Service service = new Service();
                    service.mType = follower.type;
                    service.mFlag = follower.flag;
                    Timber.e("type:"+service.mType);
                    if (service.mType == Integer.parseInt(Constants.TYPE_FAMOUS_PEOPLE)) {
                        intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
                        bundle.putInt(Constants.ID, follower.id);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_FAMOUS_PEOPLE));
                    } else if (service.mType == Integer.parseInt(Constants.TYPE_PHOTOGRAPHER)) {
                        intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
                        bundle.putInt(Constants.ID, follower.id);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_PHOTOGRAPHER));
                    } else if (service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING)) {
                        intent.putExtra(Constants.PAGE, Constants.ADS_MAIN);
                        bundle.putSerializable(Constants.SERVICE, service);
                    } else if (service.mType == Integer.parseInt(Constants.TYPE_TRUCKS)) {
                        if(UserHelper.getUserId() != -1) {
                            intent.putExtra(Constants.PAGE, Constants.TRUCKS);
                            bundle.putSerializable(Constants.SERVICE, service);
                        }else {
                            toastInfo(getString(R.string.please_login_first));
                            return;
                        }
                    } else if (service.mType == Integer.parseInt(Constants.TYPE_TAXI)) {
                        if(UserHelper.getUserId() != -1) {
                            intent.putExtra(Constants.PAGE, Constants.MAP_BOOKING);
                            bundle.putString(Constants.TYPE,Constants.TYPE_TAXI);
                            bundle.putSerializable(Constants.SERVICE, service);
                        }else {
                            toastInfo(getString(R.string.please_login_first));
                            return;
                        }
                    } else if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) || service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
                        if(UserHelper.getUserId() != -1) {
                            intent.putExtra(Constants.PAGE, Constants.CLINIC_DETAILS);
                            bundle.putSerializable(Constants.SERVICE, service);
                            bundle.putInt(Constants.ID, follower.id);
                            intent.putExtra(Constants.BUNDLE, bundle);
                            bundle.putSerializable(Constants.SERVICE, service);
                        }else {
                            toastInfo(getString(R.string.please_login_first));
                            return;
                        }
                    }else {
                        bundle.putInt(Constants.ID, follower.id);
                        intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
                        bundle.putSerializable(Constants.SERVICE, service);
                    }
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, follower.name);
                    startActivity(intent);


                }
            }
        });
    }
    /*
                int position = (int) o;
                Famous famous = famousListViewModel.getFamousRepository().getFamousListResponse().data.allFamous.get(position);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
                intent.putExtra(Constants.NAME_BAR, famous.getName());
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ID, famous.getmId());
                bundle.putInt(Constants.TYPE, type);
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
     */
}
