package grand.app.moon.views.fragments.search;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.adapter.SearchAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentSearchBinding;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.search.SearchModel;
import grand.app.moon.moon.MoonHelper;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.search.SearchViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

public class SearchFragment extends BaseFragment {
    View rootView;
    private FragmentSearchBinding fragmentSearchBinding;
    private SearchViewModel searchViewModel;
    public SearchAdapter searchAdapter;
    public int service_id = 0;
    Service service;
    int type = -1;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentSearchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
        getData();
        bind();
        setEvent();
        setEventAdapter();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.SERVICE)){
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.SERVICE_ID)){
            service_id = getArguments().getInt(Constants.SERVICE_ID);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)){
            type = getArguments().getInt(Constants.TYPE);
        }
    }

    private void bind() {
        AppUtils.initVerticalRV(fragmentSearchBinding.rvSearch, fragmentSearchBinding.rvSearch.getContext(), 1);
        if(service != null)
            searchViewModel = new SearchViewModel(service_id,service.mFlag,service.mType);
        else
            searchViewModel = new SearchViewModel();

        searchAdapter = new SearchAdapter();
        fragmentSearchBinding.rvSearch.setAdapter(searchAdapter);
        fragmentSearchBinding.setSearchViewModel(searchViewModel);
        fragmentSearchBinding.search.setActivated(true);
        fragmentSearchBinding.search.setQueryHint(ResourceManager.getString(R.string.search_hint));
        fragmentSearchBinding.search.onActionViewExpanded();
        fragmentSearchBinding.search.setIconified(false);
        fragmentSearchBinding.search.clearFocus();
        fragmentSearchBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String s) {
                searchViewModel.submitSearch(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        rootView = fragmentSearchBinding.getRoot();
    }

    private static final String TAG = "SearchFragment";

    private void setEvent() {
        searchViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,searchViewModel.searchRepository.getMessage());
                if(action.equals(Constants.SEARCH)){
                    if(searchViewModel.searchRepository.getSearchResponse().data.size() != 0){
                        Log.d(TAG,"data size > 0");
                        Timber.e("update:"+searchViewModel.searchRepository.getSearchResponse().data.size());
                        fragmentSearchBinding.rvSearch.setVisibility(View.VISIBLE);
                        searchAdapter.setList(searchViewModel.searchRepository.getSearchResponse().data);
                        searchViewModel.haveData();
                    }else{
                        Log.d(TAG,"no data");
                        //no result
                        fragmentSearchBinding.rvSearch.setVisibility(View.GONE);
                        searchAdapter.clear();
                        searchViewModel.noData();
                    }
                }
                //        fragmentSearchBinding.rvSearch.setAdapter(new NotificationAdapter(notificationViewModel.getNotificationRepository().getData().data));
            }
        });
    }

    public void setEventAdapter(){
        searchAdapter.getMutableLiveData().observe((LifecycleOwner) context, new Observer<SearchModel>() {
            @Override
            public void onChanged(SearchModel searchModel) {
//                Timber.e("search:"+searchModel.id);
                Log.d(TAG,"here");
                if(searchModel.workDays != 0) MoonHelper.shopDetails(context, searchModel.type, searchModel.flag, searchModel.id, searchModel.name);
                else
                    Toasty.info(requireContext(), ResourceManager.getString(R.string.this_shop_is_close_now),Toasty.LENGTH_SHORT).show();
//                Toast.makeText(context, "welcome here erere:"+ResourceManager.getString(R.string.this_shop_is_close_now), Toast.LENGTH_SHORT).show();
//                    Toasty.normal(requireContext(), ResourceManager.getString(R.string.this_shop_is_close_now));
//                toastInfo(ResourceManager.getString(R.string.this_shop_is_close_now));

//                if(service == null){
//                    service = new Service();
//                    service.mType = searchModel.type;
//                    service.mFlag = searchModel.flag;
//                }
//                if(service.mType == Integer.parseInt(Constants.TYPE_INSTITUTIONS)) {
//                    //SHOP_DETAILS
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putInt(Constants.ID, searchModel.id);
//                    bundle.putSerializable(Constants.SERVICE,service);
//                    intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
//                    intent.putExtra(Constants.NAME_BAR, searchModel.name);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivity(intent);
//                }else if (service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) ||
//                        service.mType == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)){
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.CLINIC_DETAILS);
//                    intent.putExtra(Constants.NAME_BAR, searchModel.name);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable(Constants.SERVICE, service);
//                    bundle.putInt(Constants.ID, searchModel.id);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivity(intent);
//                }else if (service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING) || service.mType == Integer.parseInt(Constants.TYPE_COMPANIES)){
//                    Intent intent = new Intent(context,BaseActivity.class);
//                    intent.putExtra(Constants.PAGE,Constants.ADS_DETAILS);
//                    intent.putExtra(Constants.NAME_BAR,searchModel.name);
//                    Bundle bundle = new Bundle();
//                    bundle.putInt(Constants.TYPE,service.mType);
//                    bundle.putInt(Constants.ID,searchModel.id);
//                    intent.putExtra(Constants.BUNDLE,bundle);
//                    startActivity(intent);
//                }else {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putInt(Constants.ID, searchModel.id);
//                    bundle.putSerializable(Constants.SERVICE,service);
//                    intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
//                    intent.putExtra(Constants.NAME_BAR, searchModel.name);
//                    intent.putExtra(Constants.BUNDLE,bundle);
//                    startActivity(intent);
//                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (searchViewModel != null) searchViewModel.reset();
    }

}
