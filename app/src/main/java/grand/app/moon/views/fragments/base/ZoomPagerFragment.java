package grand.app.moon.views.fragments.base;


import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import grand.app.moon.R;
import grand.app.moon.databinding.FragmentZoomBinding;
import grand.app.moon.databinding.FragmentZoomPagerBinding;
import grand.app.moon.models.base.IdName;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.viewmodels.app.ZoomViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZoomPagerFragment extends Fragment {


    FragmentZoomPagerBinding binding;
    ArrayList<IdName> images;
    public ZoomViewModel zoomViewModel;
    int position = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =   DataBindingUtil.inflate(inflater,R.layout.fragment_zoom_pager, container, false);
        zoomViewModel = new ZoomViewModel();
        getData();
        binding.setViewModel(zoomViewModel);
        return binding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.IMAGES)){
            images = new ArrayList<>((ArrayList<IdName>) getArguments().getSerializable(Constants.IMAGES));
            zoomViewModel.setImages(images);
            SwapAdapter adapter = new SwapAdapter(getFragmentManager(),zoomViewModel.tabModels);
            binding.viewPager.setAdapter(adapter);
            binding.viewPager.setEnabled(false);
            final ViewPager.LayoutParams layoutParams = new ViewPager.LayoutParams();
            layoutParams.width = ViewPager.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewPager.LayoutParams.WRAP_CONTENT;
            layoutParams.gravity = Gravity.BOTTOM;
            if(getArguments().containsKey(Constants.POSITION)) {
                position = getArguments().getInt(Constants.POSITION);
            }
            binding.viewPager.setCurrentItem(position);
        }
    }

}
