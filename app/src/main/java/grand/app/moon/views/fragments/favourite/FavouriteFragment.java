package grand.app.moon.views.fragments.favourite;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.FavouriteAdapter;
import grand.app.moon.adapter.NotificationAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFavouriteBinding;
import grand.app.moon.databinding.FragmentNotificationsBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.famous.details.Product;
import grand.app.moon.models.favourite.FavouriteModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.favourite.FavouriteViewModel;
import grand.app.moon.viewmodels.notification.NotificationViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

public class FavouriteFragment extends BaseFragment {
    View rootView;
    FavouriteAdapter favouriteAdapter;
    private FragmentFavouriteBinding binding;
    private FavouriteViewModel viewModel;

    private static final String TAG = "FavouriteFragment";
    
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favourite, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        viewModel = new FavouriteViewModel();
        favouriteAdapter = new FavouriteAdapter();
        binding.rvList.setAdapter(favouriteAdapter);
        setEventAdapter();
        binding.setFavouriteViewModel(viewModel);
        rootView = binding.getRoot();
    }

    private void setEventAdapter() {

        favouriteAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Mutable mutable = (Mutable) aVoid;
                int pos = mutable.position;
                if(mutable.type.equals(Constants.DELETE)) {
                    FavouriteModel model = favouriteAdapter.getList().get(pos);
                    viewModel.remove_pos = pos;
                    viewModel.remove();
                }else if(mutable.type.equals(Constants.SUBMIT)){
                    FavouriteModel model = favouriteAdapter.getList().get(pos);
                    if(model.type == Integer.parseInt(Constants.TYPE_COMPANIES) || model.type == Integer.parseInt(Constants.TYPE_ADVERTISING)) {
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.ADS_DETAILS);
                        intent.putExtra(Constants.NAME_BAR, model.name);
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.TYPE, model.type);
                        bundle.putInt(Constants.ID, model.id);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        startActivity(intent);
                    }else if(model.type < 4){
                        ProductList product = new ProductList();
                        product.id = model.id+"";
                        Service service = new Service();
                        service.mType = model.type;
                        service.mFlag = model.flag;
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.NAME_BAR,model.name);
                        intent.putExtra(Constants.PAGE,Constants.PRODUCT_DETAILS);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Constants.PRODUCT,product);
                        bundle.putSerializable(Constants.SERVICE,service);
                        intent.putExtra(Constants.BUNDLE,bundle);
                        startActivity(intent);
                    }else if(model.type == Integer.parseInt(Constants.TYPE_ADVERTISING)){
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE,Constants.ADS_DETAILS);
                        intent.putExtra(Constants.NAME_BAR,model.name);
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.TYPE,model.type);
                        bundle.putInt(Constants.ID,model.id);
                        intent.putExtra(Constants.BUNDLE,bundle);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getProductRepository().getMessage());
                if(action.equals(Constants.SUCCESS)){
                    if(viewModel.getProductRepository().getFavouriteResponse().data.size() != 0) {
                        viewModel.haveData();
                        AppUtils.initVerticalRV(binding.rvList, binding.rvList.getContext(), 1);
                        favouriteAdapter.setList(viewModel.getProductRepository().getFavouriteResponse().data);
                    }else {
                        favouriteAdapter.clear();
                        viewModel.noData();
                    }

                }else if(action.equals(Constants.FAVOURITE)){
                    toastMessage( viewModel.getProductRepository().getMessage());
                    favouriteAdapter.remove(viewModel.remove_pos);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }

}
