package grand.app.moon.views.fragments.service;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ShopAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentServiceShopBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.service.ServiceShopViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceShopFragment extends BaseFragment {

    private FragmentServiceShopBinding fragmentServiceShopBinding;
    public ServiceShopViewModel viewModel;
    private String type;
    private int filter = 1;
    ArrayList<Integer> tags;
    private Service service;
    ServiceDetailsFragment serviceDetailsFragment;

    private static final String TAG = "ServiceShopFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        fragmentServiceShopBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_shop, container, false);
        serviceDetailsFragment = ((ServiceDetailsFragment) this.getParentFragment());
        Log.d(TAG,"init");
        getData();
        bind();
        setEvent();
        return fragmentServiceShopBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
    }

    private void bind() {
        viewModel = new ServiceShopViewModel(type);
        viewModel.adapter  = new ShopAdapter(this);
        viewModel.adapter.service = service;
//        viewModel.callService();
        fragmentServiceShopBinding.setViewModel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getHomeRepository().getMessage());
                assert action != null;
//                if (action.equals(Constants.SUCCESS)) {
//                    viewModel.setData();
//                    setData();
//                    setEventAdapter();
//                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        if(serviceShopViewModel != null)
//            serviceShopViewModel.callService();
    }

    private void setData() {
        Log.d(TAG, "setData: ");
        serviceDetailsFragment.setService(viewModel.getHomeRepository().getServiceDetailsResponse().mData.mServices);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"result_ok:"+requestCode);
        if(requestCode == Constants.VIEW_REQUEST){
            Log.d(TAG,"result_ok done");
            if(data != null && data.hasExtra("isViewed")) {
                Log.d(TAG,"result_ok done isViewed");
                boolean viewed = data.getBooleanExtra("isViewed",false);
                if(viewed){
                    Log.d(TAG,"result_ok done viewed");
                    viewModel.adapter.storyViewed();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(viewModel != null)
            viewModel.reset();
    }
}
