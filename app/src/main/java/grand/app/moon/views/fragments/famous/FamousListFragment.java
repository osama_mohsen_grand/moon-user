package grand.app.moon.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.FamousAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFamousListBinding;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.famous.list.Famous;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.famous.FamousListViewModel;
import grand.app.moon.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousListFragment extends BaseFragment {

    private FragmentFamousListBinding fragmentFamousListBinding;
    private FamousListViewModel famousListViewModel;
    private FamousAdapter famousAdapter;
    int type = 4;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentFamousListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_list, container, false);
        getData();
        setSearchAction();
        bind();
        setEvent();
        return fragmentFamousListBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
    }

    private void setSearchAction() {
        ((BaseActivity)getActivityBase()).backActionBarView.layoutActionBarBackBinding.imgBaseBarSearch.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), BaseActivity.class);
            intent.putExtra(Constants.PAGE,Constants.FAMOUS_SEARCH);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.TYPE,type);
            intent.putExtra(Constants.NAME_BAR,getString(R.string.search));
            intent.putExtra(Constants.BUNDLE,bundle);
            startActivity(intent);
        });
    }

    private void bind() {
        famousListViewModel = new FamousListViewModel(type);
        AppUtils.initVerticalRV(fragmentFamousListBinding.rvFamous, fragmentFamousListBinding.rvFamous.getContext(), 3);
        fragmentFamousListBinding.setFamousListViewModel(famousListViewModel);
        initPopUp();
    }

    private void initPopUp() {
        fragmentFamousListBinding.rlFamousListGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> popUpModels = AppMoon.genderList();
                popUpMenuHelper.openPopUp(getActivity(), fragmentFamousListBinding.rlFamousListGender, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        famousListViewModel.setGender(AppMoon.getGenderId(popUpModels.get(position)), popUpModels.get(position));
                    }
                });
            }
        });

        fragmentFamousListBinding.rlPhotographerType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> popUpModels = AppMoon.photographerList();
                popUpMenuHelper.openPopUp(getActivity(), fragmentFamousListBinding.rlPhotographerType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        famousListViewModel.setPhotographer(AppMoon.getPhotograhperTypeId(popUpModels.get(position)), popUpModels.get(position));
                    }
                });
            }
        });
    }

    private void setEvent() {
        famousListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousListViewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    famousListViewModel.showPage(true);
                    List<Famous> famousList = famousListViewModel.getFamousRepository().getFamousListResponse().data.allFamous;
                    famousAdapter = new FamousAdapter(famousList);
                    fragmentFamousListBinding.rvFamous.setAdapter(famousAdapter);
                    famousListViewModel.haveData(famousList);
                    setEventAdapter();
                }else if(action.equals(Constants.DISCOVER)){
                    Intent intent = new Intent(context,BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.DISCOVER);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.discover));
                    intent.putExtra(Constants.BUNDLE,getArguments());
                    startActivity(intent);
                }
            }
        });
    }

    private void setEventAdapter() {
        famousAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                Famous famous = famousListViewModel.getFamousRepository().getFamousListResponse().data.allFamous.get(position);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
                intent.putExtra(Constants.NAME_BAR, famous.getName());
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ID, famous.getmId());
                bundle.putInt(Constants.TYPE, type);
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }
        });
    }


}
