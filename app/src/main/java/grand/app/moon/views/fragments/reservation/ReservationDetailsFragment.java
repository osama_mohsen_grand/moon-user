package grand.app.moon.views.fragments.reservation;


import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.OrderDetailsAdapter;
import grand.app.moon.adapter.TextAdapter;
import grand.app.moon.adapter.TextSimpleAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.DialogReservationConfirmBinding;
import grand.app.moon.databinding.FragmentOrderDetailsBinding;
import grand.app.moon.databinding.FragmentReservationDetatilsBinding;
import grand.app.moon.models.order.details.OrderDetails;
import grand.app.moon.models.reservation.detatils.ReservationDetailsModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.order.details.OrderDetailsViewModel;
import grand.app.moon.viewmodels.reservation.ReservationDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MapAddressActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationDetailsFragment extends BaseFragment {
    View rootView;
    private FragmentReservationDetatilsBinding fragmentReservationDetatilsBinding;
    private ReservationDetailsViewModel reservationDetailsViewModel;
    int order_id = 0,reserved_type = 0;
    TextSimpleAdapter textSimpleAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentReservationDetatilsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reservation_detatils, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            order_id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            reserved_type = getArguments().getInt(Constants.TYPE);
    }

    private void bind() {
        reservationDetailsViewModel = new ReservationDetailsViewModel(order_id,reserved_type);
        fragmentReservationDetatilsBinding.setReservationDetailsViewModel(reservationDetailsViewModel);
        AppUtils.initVerticalRV(fragmentReservationDetatilsBinding.rvServices, fragmentReservationDetatilsBinding.rvServices.getContext(), 1);
        rootView = fragmentReservationDetatilsBinding.getRoot();
    }

    private static final String TAG = "ReservationDetailsFragm";
    private void setEvent() {
        reservationDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, reservationDetailsViewModel.getClinicRepository().getMessage());
                if(action.equals(Constants.RESERVATION_DETAILS)){
                    reservationDetailsViewModel.model= reservationDetailsViewModel.getClinicRepository().getReservationDetailsResponse().reservationDetailsModel;
                    reservationDetailsViewModel.setPrice(reservationDetailsViewModel.model.fees+"");
                    reservationDetailsViewModel.setSpecialist(reservationDetailsViewModel.model.doctorSpecialties+" ("+
                            reservationDetailsViewModel. model.yearsExperience+" "+getString(R.string.years)+")");
                    if(reservationDetailsViewModel.model.services != null){
                        textSimpleAdapter = new TextSimpleAdapter(reservationDetailsViewModel.model.services);
                        fragmentReservationDetatilsBinding.rvServices.setAdapter(textSimpleAdapter);
                    }
                    if(reservationDetailsViewModel.model.orderFlag == 4){
                        Log.d(TAG, "onChanged: ");
                        showDialogEditAppointment();
                    }
                    reservationDetailsViewModel.showPage(true);
                }else if(action.equals(Constants.CONFIRM)){
                    toastMessage(reservationDetailsViewModel.getClinicRepository().getMessage());
                    if(showDialog!=null && showDialog.isShowing()) showDialog.dismiss();
                    getActivityBase().finish();
                }
            }
        });
    }

    AlertDialog showDialog;
    private void showDialogEditAppointment() {
        Rect displayRectangle = new Rect();
        Window window = getActivityBase().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        DialogReservationConfirmBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_reservation_confirm, null, false);
        binding.setViewmodel(reservationDetailsViewModel);
        binding.getRoot().setMinimumWidth((int)(displayRectangle.width() * 0.9f));
        View view =binding.getRoot();
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.customDialog);
        builder.setView(view);
        showDialog = builder.create();
        showDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (reservationDetailsViewModel != null) reservationDetailsViewModel.reset();
    }

}
