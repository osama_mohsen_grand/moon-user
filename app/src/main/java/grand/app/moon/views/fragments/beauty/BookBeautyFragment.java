package grand.app.moon.views.fragments.beauty;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.common.base.Joiner;
import com.vivekkaushik.datepicker.DatePickerTimeline;
import com.vivekkaushik.datepicker.OnDateSelectedListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.BeautyServiceAdapter;
import grand.app.moon.adapter.TimeAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentBookBeautyBinding;
import grand.app.moon.databinding.FragmentBookClinicBinding;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.doctor.Schedule;
import grand.app.moon.models.reservation.DateTimeModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.beauty.BookBeautyViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookBeautyFragment extends BaseFragment {

    private static final String TAG = "BookBeautyFragment";
    FragmentBookBeautyBinding fragmentBookBeautyBinding;
    BookBeautyViewModel bookBeautyViewModel;
    int id;
    TimeAdapter timeAdapter;
    int type = Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC),payment_type;
    ArrayList<Integer> ids = new ArrayList<>();
    ClinicDetailsResponse clinicDetailsResponse;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentBookBeautyBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_book_beauty, container, false);
        getData();
        bind();
        initDate();
        setEvent();
        return fragmentBookBeautyBinding.getRoot();

    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            id = getArguments().getInt(Constants.ID);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PAYMENT_TYPE)) {
            payment_type = getArguments().getInt(Constants.PAYMENT_TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.CLINIC_DETAILS)) {
            clinicDetailsResponse = (ClinicDetailsResponse) getArguments().getSerializable(Constants.CLINIC_DETAILS);
        }

    }

    private void bind() {
        bookBeautyViewModel = new BookBeautyViewModel(clinicDetailsResponse);
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICES)) {
            ids = getArguments().getIntegerArrayList(Constants.SERVICES);;
            bookBeautyViewModel.reservationRequest.service = Joiner.on(",").join(ids);
        }
        bookBeautyViewModel.updateUI(payment_type);
        fragmentBookBeautyBinding.setBookBeautyViewModel(bookBeautyViewModel);
    }

    private void initDate() {
        DatePickerTimeline datePickerTimeline = fragmentBookBeautyBinding.datePickerTimeline;
        datePickerTimeline.setDateTextColor(ResourceManager.getColor(R.color.colorBlack));
        datePickerTimeline.setDayTextColor(ResourceManager.getColor(R.color.colorPrimary));
        datePickerTimeline.setMonthTextColor(ResourceManager.getColor(R.color.colorPrimary));

        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new Date());
        String[] datesData = currentDate.split("-");

        datePickerTimeline.setInitialDate(Integer.parseInt(datesData[2]), Integer.parseInt(datesData[1]) - 1, Integer.parseInt(datesData[0]));
        datePickerTimeline.setDisabledDateColor(ResourceManager.getColor(R.color.colorPrimaryAc));
// Set a date Selected Listener
        datePickerTimeline.setOnDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int dayOfWeek) {
                if (bookBeautyViewModel != null) {
                    bookBeautyViewModel.reservationRequest.date = year + "-" + AppUtils.getNumberForDate(month + 1) + "-" + AppUtils.getNumberForDate(day);
                    bookBeautyViewModel.reservationRequest.day_id = AppMoon.getDay(dayOfWeek);
                }
            }

            @Override
            public void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled) {
                // Do Something
            }
        });
        datePickerTimeline.setActiveDate(Calendar.getInstance());
    }

    private void setEvent() {
        bookBeautyViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, bookBeautyViewModel.getClinicRepository().getMessage());
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(bookBeautyViewModel.getClinicRepository().getMessage());
                    getActivityBase().finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                } else if (action.equals(Constants.ERROR)) {
                    showError(bookBeautyViewModel.baseError);
                } else if (action.equals(Constants.REVIEW)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.REVIEW);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (action.equals(Constants.CHAT)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.DISCOVER);
//                    intent.putExtra(Constants.BUNDLE, getArguments());
//                    startActivity(intent);

                    Intent intent = new Intent(context,BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID_CHAT, clinicDetailsResponse.data.details.id);
                    bundle.putInt(Constants.TYPE, type);
                    bundle.putBoolean(Constants.ALLOW_CHAT, true);
                    bundle.putBoolean(Constants.CHAT_FIRST, true);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);

                }
            }
        });
    }

}
