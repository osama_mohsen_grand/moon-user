package grand.app.moon.views.fragments.beauty;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.BeautyServiceAdapter;
import grand.app.moon.adapter.ClinicListAdapter;
import grand.app.moon.adapter.DoctorsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentBeautyServiceListBinding;
import grand.app.moon.databinding.FragmentDoctorListBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.clinic.ClinicModel;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.BottomSheetDialogHelper;
import grand.app.moon.viewmodels.beauty.BeautyViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.reservation.ClinicDetailsFragment;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeautyServiceListFragment extends BaseFragment {

    private FragmentBeautyServiceListBinding fragmentBeautyServiceListBinding;
    private BeautyViewModel beautyViewModel;
    private BeautyServiceAdapter beautyServiceAdapter;
    ClinicDetailsResponse clinicDetailsResponse;
    BottomSheetDialogHelper bottomSheetDialogHelper;
    ClinicDetailsFragment clinicDetailsFragment;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentBeautyServiceListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_beauty_service_list, container, false);
        beautyViewModel = new BeautyViewModel();
        clinicDetailsFragment = ((ClinicDetailsFragment) this.getParentFragment());

        getData();
        bind();
        setEvent();
        return fragmentBeautyServiceListBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.CLINIC_DETAILS)) {
            AppUtils.initVerticalRV(fragmentBeautyServiceListBinding.rvBeauty, fragmentBeautyServiceListBinding.rvBeauty.getContext(), 1);
            clinicDetailsResponse = (ClinicDetailsResponse) getArguments().getSerializable(Constants.CLINIC_DETAILS);

            if(clinicDetailsResponse != null) {
                beautyServiceAdapter = new BeautyServiceAdapter(clinicDetailsResponse.data.services);
                beautyViewModel.setIdNamePrices(clinicDetailsResponse.data.services);
                fragmentBeautyServiceListBinding.rvBeauty.setAdapter(beautyServiceAdapter);
                setEventAdapter();
            }
        }
    }

    private void setEvent() {
        beautyViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.SUBMIT)) {
                    if(beautyViewModel.ids.size() == 0){
                        showError(getString(R.string.please_select_at_least_one_service));
                    }else{
                        Intent intent = new Intent(context,BaseActivity.class);
                        intent.putExtra(Constants.PAGE,Constants.BOOK_BEAUTY);
                        intent.putExtra(Constants.NAME_BAR,getString(R.string.book_details));
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.ID,clinicDetailsResponse.data.details.id);
                        bundle.putSerializable(Constants.CLINIC_DETAILS,clinicDetailsResponse);
                        bundle.putInt(Constants.PAYMENT_TYPE,clinicDetailsFragment.clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.payment_type);
                        bundle.putIntegerArrayList(Constants.SERVICES,beautyViewModel.ids);
                        intent.putExtra(Constants.BUNDLE,bundle);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private void bind() {
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivityBase());
        fragmentBeautyServiceListBinding.setBeautyViewModel(beautyViewModel);
    }


    private void setEventAdapter() {
        beautyServiceAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Mutable mutable = (Mutable) aVoid;
                if(mutable!=null) {
                    if (mutable.type.equals(Constants.SUBMIT)) {
                        int pos = mutable.position;
                        if (beautyViewModel.ids.contains(Integer.parseInt(clinicDetailsResponse.data.services.get(pos).id))) {
                            beautyViewModel.ids.remove((Integer) Integer.parseInt(clinicDetailsResponse.data.services.get(pos).id));
                        } else
                            beautyViewModel.ids.add(Integer.parseInt(clinicDetailsResponse.data.services.get(pos).id));
                        Timber.e("ids:"+beautyViewModel.ids.toString());
                        beautyViewModel.calculatePrice();
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        beautyViewModel.reset();
    }

}
