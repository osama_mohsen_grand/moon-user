package grand.app.moon.views.fragments.intro;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentSlide3Binding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.intro.IntroViewModel;
import grand.app.moon.viewmodels.intro.Slide3ViewModel;
import grand.app.moon.views.activities.BaseActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class Slide3Fragment extends BaseFragment {

    FragmentSlide3Binding fragmentSlide3Binding;
    Slide3ViewModel slide3ViewModel ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentSlide3Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_slide3, container, false);
        slide3ViewModel = new Slide3ViewModel();
        fragmentSlide3Binding.setSlide3ViewModel(slide3ViewModel);
        setEvent();
        return fragmentSlide3Binding.getRoot();
    }

    private void setEvent() {
        slide3ViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,slide3ViewModel.baseError);
                assert action != null;
                if(action.equals(Constants.LOCATIONS)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.LOCATION);
                    ((AppCompatActivity) context).finishAffinity();
                    startActivity(intent);
                }
            }
        });

    }
    

}
