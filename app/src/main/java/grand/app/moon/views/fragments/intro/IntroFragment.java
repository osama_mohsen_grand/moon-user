package grand.app.moon.views.fragments.intro;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentIntroBinding;
import grand.app.moon.viewmodels.intro.IntroViewModel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class IntroFragment extends BaseFragment {

    FragmentIntroBinding fragmentIntroBinding;
    IntroViewModel introViewModel;
    ArrayList<String> text = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentIntroBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_intro, container, false);
        introViewModel = new IntroViewModel(getFragmentManager());
        fragmentIntroBinding.setIntroViewModel(introViewModel);
        text.add(getString(R.string.intro1));
        text.add(getString(R.string.intro2));
        text.add(getString(R.string.intro3));
        fragmentIntroBinding.tvTitle.setText(text.get(0));
        fragmentIntroBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                fragmentIntroBinding.tvTitle.setText(text.get(position));
            }
        });

        return fragmentIntroBinding.getRoot();

    }

}
