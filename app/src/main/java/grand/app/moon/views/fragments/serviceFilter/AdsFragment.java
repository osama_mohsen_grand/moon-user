package grand.app.moon.views.fragments.serviceFilter;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AdsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.actionbar.SearchAction;
import grand.app.moon.databinding.FragmentAdsBinding;
import grand.app.moon.databinding.FragmentDiscoverBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.ShopData;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.ads.AdsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import timber.log.Timber;

public class AdsFragment extends BaseFragment implements SearchAction {
    View rootView;
    private static final String TAG = "AdsFragment";
    private FragmentAdsBinding binding;
    private AdsViewModel viewmodel;
    private Service service;
    String type = Constants.DISCOVER, service_id = "";
    AdsAdapter adapter;
    ShopData shopData;
    ClinicDetailsResponse clinicDetailsResponse;
    List<ImageVideo> data;
    List<ImageVideo> AllResult;
    List<ImageVideo> dataTmp = new ArrayList<>();
    private boolean isOffline = false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,TAG);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ads, container, false);
        getData();
        bind();
        searchSubmit();
        return rootView;
    }


    private void getData() {
        Timber.e("getData:Done Gallery");
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            this.service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE_ID)) {
            service_id = getArguments().getString(Constants.SERVICE_ID);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.OFFLINE)) {
            isOffline = getArguments().getBoolean(Constants.OFFLINE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_DETAILS)) {
            shopData = (ShopData) getArguments().getSerializable(Constants.SHOP_DETAILS);
            if (type.equals(Constants.ADS))
                data = shopData.photographer;
            else if (type.equals(Constants.GALLERY))
                data = shopData.gallery;
        } else if (getArguments() != null && getArguments().containsKey(Constants.CLINIC_DETAILS)) {
            clinicDetailsResponse = (ClinicDetailsResponse) getArguments().getSerializable(Constants.CLINIC_DETAILS);
            if (type.equals(Constants.ADS))
                data = clinicDetailsResponse.data.adds;
            else if (type.equals(Constants.GALLERY))
                data = clinicDetailsResponse.data.gallery;
        }
        Timber.e("type:" + type);
    }

    private void bind() {
        AppUtils.initVerticalRV(binding.rvDiscover, binding.rvDiscover.getContext(), 3);
        viewmodel = new AdsViewModel();
        if (data != null) {
            setAdapterData(data);
        } else {
            if (service != null)
                viewmodel = new AdsViewModel(service, service_id);
            else {
                viewmodel.getDiscover();
            }
            binding.setViewmodel(viewmodel);
            setEvent();
        }

        if (isOffline) {
            MainActivity mainActivity = (MainActivity) getActivity();
            if (mainActivity != null) {
                mainActivity.homeActionBarView.layoutActionBarHomeBinding.imgHomeBarSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (binding.search.getVisibility() == View.VISIBLE)
                            binding.search.setVisibility(View.GONE);
                        else
                            binding.search.setVisibility(View.VISIBLE);

                    }
                });
                binding.search.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        String s = editable.toString();
                        if (AllResult != null) {
                            if (!s.equals("")) {
                                data = new ArrayList<>();
                                for (ImageVideo shopSearch : AllResult) {
                                    if (shopSearch.shopName.contains(s))
                                        data.add(shopSearch);
                                }
                            } else {
                                data = new ArrayList<>(AllResult);
                            }
                            adapter.update(data);
                        }
                    }
                });
            }
        }

        rootView = binding.getRoot();
    }

    private void setEvent() {
        viewmodel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewmodel.getDiscoverRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.DISCOVER)) {
                    if (viewmodel.getDiscoverRepository().getAlbumResponse().data.size() > 0)
                        setAdapterData(viewmodel.getDiscoverRepository().getAlbumResponse().data);
                    else
                        viewmodel.noData();
                }
            }
        });
    }

    private void setAdapterData(List<ImageVideo> data) {
        Timber.e("size:" + data.size());
        this.data = data;
        AllResult = data;
        adapter = new AdsAdapter(data);
        binding.rvDiscover.setAdapter(adapter);
        setEventAdapter();
        setSearchData();
    }

    private void setSearchData() {

    }

    private void setEventAdapter() {
        adapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.IMAGE)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    intent.putExtra(Constants.NAME_BAR, data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, data.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.VIDEO)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                    intent.putExtra(Constants.NAME_BAR, data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.VIDEO, data.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewmodel != null)
            viewmodel.reset();

    }

    @Override
    public void searchSubmit() {
        Timber.e("Submit:SearchGalleryFragment");
    }
}
