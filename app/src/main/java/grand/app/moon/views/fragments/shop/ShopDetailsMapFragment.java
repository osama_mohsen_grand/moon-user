package grand.app.moon.views.fragments.shop;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentServiceMapBinding;
import grand.app.moon.databinding.FragmentShopDetailsInfoBinding;
import grand.app.moon.databinding.FragmentShopDetailsMapBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.models.service.ShopMap;
import grand.app.moon.models.shop.Branch;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.maputils.base.CustomMarker;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.viewmodels.map.MapViewModel;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;
import grand.app.moon.vollyutils.MyApplication;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopDetailsMapFragment extends BaseFragment {


    FragmentShopDetailsMapBinding binding;
    ServiceDetailsFragment serviceDetailsFragment;
    public MapViewModel mapViewModel = null;
    Bundle savedInstanceState;
    private AccountInfoResponse response;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_details_map, container, false);
//        serviceDetailsFragment = ((ServiceDetailsFragment) this.getParentFragment());
        getData();
        this.savedInstanceState = savedInstanceState;
        return binding.getRoot();
    }

    private static final String TAG = "ShopDetailsMapFragment";

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ACCOUNT_INFO)) {
            response = (AccountInfoResponse) getArguments().getSerializable(Constants.ACCOUNT_INFO);
//            ArrayList<CustomMarker> customMarkers = new ArrayList<>();
//            for (int i = 0; i < response.branches.size(); i++) {
//                customMarkers.add(new CustomMarker(-1,
//                        response.branches.get(i).name,
//                        new LatLng(response.branches.get(i).lat, response.branches.get(i).lng), response.branches.get(i).image));
//            }
//            mapViewModel.mapConfig = new MapConfig();
//            mapViewModel.mapConfig.addMarkerMarkerOptions(customMarkers, new ImageLoaderHelper.MarkersLoaded() {
//                @Override
//                public void isLoadedAllMarkers(ArrayList<MarkerOptions> markers) {
//                    bind(savedInstanceState, markers);
//                }
//            });
////            bind(savedInstanceState);
        }
        bind(savedInstanceState, new ArrayList<>());
    }

    private void bind(Bundle savedInstanceState, ArrayList<MarkerOptions> markerOptions) {
        Log.d(TAG,markerOptions.size()+"");
        mapViewModel = new MapViewModel();
        binding.mapView.onCreate(savedInstanceState);
        binding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }


        binding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                mapViewModel.mapConfig = new MapConfig(context, mMap);
                mapViewModel.mapConfig.setMapStyle();//set style google map
                mapViewModel.mapConfig.setSettings();//add setting google map
                if (response != null) {
                    if (markerOptions.size() > 0) {
                        for(MarkerOptions markerOptions1 : markerOptions){
                            mapViewModel.mapConfig.getGoogleMap().addMarker(markerOptions1);
                        }
                        binding.mapView.onResume();
                    } else {
                        for(Branch branch : response.branches){
                            mapViewModel.mapConfig.addMarker(new LatLng(branch.lat,branch.lng),R.drawable.ic_marker_dest,response.name,branch.address);
                        }
                        mapViewModel.mapConfig.addMarker(new LatLng(response.lat, response.lng), -1, response.image, response.name, marker -> {
                            mapViewModel.mapConfig.zoomCamera(150);
                            binding.mapView.onResume();
                        });
                    }
                }
            }
        });
        binding.setMapViewModel(mapViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (binding != null && binding.mapView != null) {
            binding.mapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (binding != null && binding.mapView != null)
            binding.mapView.onPause();
    }

    @Override
    public void onDestroy() {

        if (binding != null && binding.mapView != null && mapViewModel != null) {
            mapViewModel.reset();
            binding.mapView.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (binding != null && binding.mapView != null)
            binding.mapView.onLowMemory();
    }


}
