package grand.app.moon.views.fragments.base;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import grand.app.moon.R;
import grand.app.moon.databinding.FragmentZoomBinding;
import grand.app.moon.repository.FirebaseRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.app.ZoomViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZoomFragment extends Fragment {


    FragmentZoomBinding fragmentZoomBinding;
    public String image = "";
    FirebaseRepository repository = new FirebaseRepository();
    int flag = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentZoomBinding =   DataBindingUtil.inflate(inflater,R.layout.fragment_zoom, container, false);
        getData();
        Log.d("image2:",image);
        fragmentZoomBinding.setZoomViewModel(new ZoomViewModel(image));
        return fragmentZoomBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.IMAGE)){
            image = getArguments().getString(Constants.IMAGE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.FLAG)) {
            flag = getArguments().getInt(Constants.FLAG);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            int id = getArguments().getInt(Constants.ID);
            repository.mediaView(new MediaViewRequest(id,flag));
        }
    }

}
