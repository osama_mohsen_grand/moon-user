package grand.app.moon.views.fragments;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.adapter.NotificationAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentNotificationsBinding;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.notification.NotificationViewModel;

public class NotificationFragment extends BaseFragment {
    View rootView;
    private FragmentNotificationsBinding fragmentNotificationsBinding;
    private NotificationViewModel notificationViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentNotificationsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        notificationViewModel = new NotificationViewModel();
        AppUtils.initVerticalRV(fragmentNotificationsBinding.rvNotifications, fragmentNotificationsBinding.rvNotifications.getContext(), 1);
        fragmentNotificationsBinding.rvNotifications.setAdapter(notificationViewModel.notificationAdapter);
        fragmentNotificationsBinding.setNotificationViewModel(notificationViewModel);
        rootView = fragmentNotificationsBinding.getRoot();
    }

    private void setEvent() {
        notificationViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, notificationViewModel.getNotificationRepository().getMessage());
            if(action.equals(Constants.NOTIFICATION)){
                if(notificationViewModel.getNotificationRepository().getData().data.size() != 0) {
                    notificationViewModel.notificationAdapter.update(notificationViewModel.getNotificationRepository().getData().data);
                }else
                    notificationViewModel.noData();
            }
        });
        notificationViewModel.notificationAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                int pos = (int) o;
                if(pos == -1){
                    notificationViewModel.noData();
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (notificationViewModel != null) notificationViewModel.reset();
    }

}
