package grand.app.moon.views.fragments.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.app.AppMoon;
import grand.app.moon.utils.Constants;


public class MediaViewRequest {
    @SerializedName("media_id")
    @Expose
    private int id = 0;

    @SerializedName("flag")
    @Expose
    private int flag = 1;

    @SerializedName("type")
    @Expose
    private String type = Constants.DEFAULT_USER;

    public MediaViewRequest(int id, int flag) {
        this.id = id;
        this.flag = flag;
    }
}
