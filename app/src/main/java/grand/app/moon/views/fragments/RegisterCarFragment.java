package grand.app.moon.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentRegisterBinding;
import grand.app.moon.databinding.FragmentRegisterCarBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.user.RegisterCarViewModel;
import grand.app.moon.views.activities.BaseActivity;

public class RegisterCarFragment extends BaseFragment {
    View rootView;
    private FragmentRegisterCarBinding fragmentRegisterCarBinding;
    private RegisterCarViewModel registerCarViewModel;
    public String type= "";
    boolean isUpdate = false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegisterCarBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register_car, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private static final String TAG = "RegisterCarFragment";
    private void getData() {
        if(getArguments()!= null && getArguments().containsKey(Constants.TYPE)){
            type = getArguments().getString(Constants.TYPE);
        }
        if(getArguments()!= null && getArguments().containsKey(Constants.UPDATE_CAR)){
            isUpdate = getArguments().getBoolean(Constants.UPDATE_CAR);
        }
    }

    private void bind() {
        registerCarViewModel = new RegisterCarViewModel(isUpdate);
        fragmentRegisterCarBinding.setRegisterCarViewModel(registerCarViewModel);
        rootView = fragmentRegisterCarBinding.getRoot();
    }

    private void setEvent() {
        registerCarViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,registerCarViewModel.getRegisterRepository().getMessage());

                if(action.equals(Constants.REGISTRATION_CAR)) {
                    toastMessage(registerCarViewModel.getRegisterRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    ((AppCompatActivity)context).finishAffinity();
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.LOGIN);
                    startActivity(intent);
                }else if(action.equals(Constants.UPDATE_PROFILE)){
                    toastMessage(registerCarViewModel.getRegisterRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    ((AppCompatActivity)context).finish();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        registerCarViewModel.reset();

    }


}
