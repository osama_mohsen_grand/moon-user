package grand.app.moon.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentPaymentBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.payment.PaymentViewModel;


public class PaymentFragment extends BaseFragment {
//    View rootView;
//    private FragmentPaymentBinding fragmentPaymentBinding;
//    private PaymentViewModel paymentViewModel;
//
//    @Nullable
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        fragmentPaymentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false);
//        bind();
//        rootView = fragmentPaymentBinding.getRoot();
//        return rootView;
//    }
//
//    private void bind() {
//        paymentViewModel = new PaymentViewModel();
//        setEvents();
//        fragmentPaymentBinding.setPaymentViewModel(paymentViewModel);
//    }
//
//    private void setEvents() {
//        paymentViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
//            @Override
//            public void onChanged(@Nullable Object o) {
//                String action = (String) o;
//                handleActions(action,paymentViewModel.getPaymentRepository().getMessage());
//                assert action != null;
//                if(action.equals(Constants.SUCCESS)){
//                    paymentViewModel.show.set(true);
//                    paymentViewModel.notifyChange();
//                } else if(action.equals(Constants.PAYMENT)){
//                    toastMessage(paymentViewModel.getPaymentRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
//                }
//            }
//        });
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        paymentViewModel.reset();
//    }
}
