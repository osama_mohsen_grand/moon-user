package grand.app.moon.views.fragments.reservation;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ReservationAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentReservationBinding;
import grand.app.moon.databinding.FragmentSettingsBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.app.SettingsViewModel;
import grand.app.moon.viewmodels.reservation.ReservationViewModel;
import grand.app.moon.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationFragment extends BaseFragment {

    private FragmentReservationBinding fragmentReservationBinding;
    private ReservationViewModel reservationViewModel;
    public ReservationAdapter reservationAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentReservationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reservation, container, false);
        bind();
        setEvent();
        return fragmentReservationBinding.getRoot();
    }

    private void bind() {
        reservationViewModel = new ReservationViewModel();
        fragmentReservationBinding.setReservationViewModel(reservationViewModel);
    }

    private void setEvent() {
        reservationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.RESERVATION);
                Bundle bundle = new Bundle();
                if (action.equals(Constants.HEALTH)) {
                    intent.putExtra(Constants.NAME_BAR,getString(R.string.health_reservation));
                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC));
                } else if (action.equals(Constants.BEAUTY)) {
                    intent.putExtra(Constants.NAME_BAR,getString(R.string.beauty_reservation));
                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY));
                }
                intent.putExtra(Constants.BUNDLE,bundle);
                getActivityBase().startActivity(intent);
            }
        });
    }
}
