package grand.app.moon.views.fragments.shop;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.DataBindingUtil;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentShopDetailsBinding;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.viewmodels.shop.ShopDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.company.CompanyProductListFragment;
import grand.app.moon.views.fragments.institution.InstitutionBranchesListFragment;
import grand.app.moon.views.fragments.institution.InstitutionDetailsFragment;
import grand.app.moon.vollyutils.AppHelper;

public class ShopDetailsFragment extends BaseFragment {
    private FragmentShopDetailsBinding fragmentShopDetailsBinding;
    private ShopDetailsViewModel shopDetailsViewModel;
    private Service service;
    private int id;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    SwapAdapter adapter = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentShopDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_details, container, false);
        getData();
        bind();
        setEvent();
        return fragmentShopDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            id = getArguments().getInt(Constants.ID);
        }
    }


    private void bind() {
        AppUtils.initHorizontalRV(fragmentShopDetailsBinding.rvShopDetailsDepartments, fragmentShopDetailsBinding.rvShopDetailsDepartments.getContext(), 1);
        shopDetailsViewModel = new ShopDetailsViewModel(id, service.mType, service.mFlag);
        fragmentShopDetailsBinding.setShopDetailsViewModel(shopDetailsViewModel);
    }

    public void setTabs() {
        Bundle bundle = getArguments();
        bundle.putSerializable(Constants.SHOP_DETAILS, shopDetailsViewModel.getShopRepository().getData().shopData);


        if (service.mType == Integer.parseInt(Constants.TYPE_INSTITUTIONS)) {
            fragmentShopDetailsBinding.slidingTabs.setTabMode(TabLayout.MODE_FIXED);
            bundle = new Bundle();
            bundle.putSerializable(Constants.INSTITUTIONS, shopDetailsViewModel.getShopRepository().getData().shopData);
            bundle.putSerializable(Constants.SERVICE, service);
            bundle.putInt(Constants.ID, id);

            InstitutionDetailsFragment institutionDetailsFragment = new InstitutionDetailsFragment();
            institutionDetailsFragment.setArguments(bundle);
            tabModels.add(0, new TabModel(getString(R.string.details), institutionDetailsFragment));


            InstitutionBranchesListFragment institutionBranchesListFragment = new InstitutionBranchesListFragment();
            institutionBranchesListFragment.setArguments(bundle);
            tabModels.add(1, new TabModel(getString(R.string.branches), institutionBranchesListFragment));


        } else {
            for (int i = 0; i < shopDetailsViewModel.getShopRepository().getData().shopData.categories.size(); i++) {
                Bundle NewBundle = new Bundle();
                NewBundle.putString(Constants.ID, "" + shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.id);
                NewBundle.putString(Constants.CATEGORY_ID, shopDetailsViewModel.getShopRepository().getData().shopData.categories.get(i).id);
                NewBundle.putSerializable(Constants.SERVICE, service);

//                if (service.mType == Integer.parseInt(Constants.TYPE_COMPANIES) || service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING)) {
//                    CompanyProductListFragment companyProductListFragment = new CompanyProductListFragment();
//                    companyProductListFragment.setArguments(NewBundle);
//                    tabModels.add(new TabModel(shopDetailsViewModel.getShopRepository().getData().shopData.categories.get(i).name, companyProductListFragment));
//                } else {
                    ShopProductListFragment shopProductFragment = new ShopProductListFragment();
                    shopProductFragment.setArguments(NewBundle);
                    tabModels.add(new TabModel(shopDetailsViewModel.getShopRepository().getData().shopData.categories.get(i).name, shopProductFragment));
//                }
            }
        }
        adapter = new SwapAdapter(getChildFragmentManager(), tabModels);
        fragmentShopDetailsBinding.viewpager.setAdapter(adapter);
        fragmentShopDetailsBinding.viewpager.setOffscreenPageLimit(0);
        fragmentShopDetailsBinding.slidingTabs.setupWithViewPager(fragmentShopDetailsBinding.viewpager);
    }

    private static final String TAG = "ShopDetailsFragment";

    private void setEvent() {
        shopDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, shopDetailsViewModel.getShopRepository().getMessage());
                if (action.equals(Constants.SHOP_DETAILS)) {
                    shopDetailsViewModel.showPage(true);
                    shopDetailsViewModel.setTabBar();
                    shopDetailsViewModel.setData();
                    setTabs();
                    shopDetailsViewModel.notifyChange();
                } else if (action.equals(Constants.REVIEW)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.REVIEW);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, id);
                    bundle.putInt(Constants.TYPE, service.mType);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (action.equals(Constants.DISCOVER)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.DISCOVER);
                    intent.putExtra(Constants.BUNDLE, getArguments());
                    startActivity(intent);
                } else if (action.equals(Constants.BACK)) {
                    requireActivity().finish();
                } else if (action.equals(Constants.DETAILS)) {
                    Log.d(TAG, "details");
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, ShopDetailsInfoFragment.class.getName());
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }else if (action.equals(Constants.FAMOUS_DETAILS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.name);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.id);
                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_FAMOUS_PEOPLE));
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }else if(action.equals(Constants.FOLLOW)){
                    int x = Integer.parseInt(shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.followersCount);
                    shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.isFollow = !shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.isFollow;
                    if (shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.isFollow) {
                        x++;
                        shopDetailsViewModel.shopDepartmentAdapter.updateFollow(true);
                    } else {
                        x--;
                        shopDetailsViewModel.shopDepartmentAdapter.updateFollow(false);
                    }
                    shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.followersCount = "" + x;
                    shopDetailsViewModel.notifyChange();
                }
            }
        });

        shopDetailsViewModel.shopDepartmentAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                    @Override
                    public void onChanged(@Nullable Object o) {
                        String type = (String) o;
                        if(type.equals(Constants.SHARE)){
                            AppHelper.shareCustom(requireActivity(),
                                    shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.name,
                                    shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.description
                                            +"\n"+
                                            shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.share,fragmentShopDetailsBinding.profileImage
                                    );
                        }else if(type.equals(Constants.ACCOUNT_INFO)){
                            Intent intent = new Intent(context,BaseActivity.class);
                            intent.putExtra(Constants.PAGE,ShopDetailsInfoFragment.class.getName());
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.ID, shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.id);
                            intent.putExtra(Constants.BUNDLE, bundle);
                            startActivity(intent);
                        }else if(type.equals(Constants.FOLLOW)){
                            shopDetailsViewModel.getShopRepository().follow(new FollowRequest(shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.id, service.mType));
                        }else if(type.equals(Constants.CHAT)){
                            if (UserHelper.getUserId() != -1) {
                                Intent intent = new Intent(context, BaseActivity.class);
                                intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
                                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                                Bundle bundle = new Bundle();
                                bundle.putInt(Constants.ID_CHAT, shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.id);
                                bundle.putInt(Constants.TYPE, service.mType);
                                bundle.putString(Constants.NAME, shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.name);
                                bundle.putString(Constants.IMAGE, shopDetailsViewModel.getShopRepository().getData().shopData.shop_details.image);
                                bundle.putBoolean(Constants.ALLOW_CHAT, true);
                                bundle.putBoolean(Constants.CHAT_FIRST, true);
                                intent.putExtra(Constants.BUNDLE, bundle);
                                startActivity(intent);
                            } else
                                toastInfo(getString(R.string.please_login_first));
                        }
                    }
                }
            );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"result_ok:"+requestCode);
        if(requestCode == Constants.VIEW_REQUEST){
            Log.d(TAG,"result_ok done");
            if(data != null && data.hasExtra("isViewed")) {
                Log.d(TAG,"result_ok done isViewed");
                boolean viewed = data.getBooleanExtra("isViewed",false);
                if(viewed){
                    Log.d(TAG,"result_ok done viewed");
                    fragmentShopDetailsBinding.profileImage.setBorderWidth(0);
                }
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        shopDetailsViewModel.reset();
    }
}
