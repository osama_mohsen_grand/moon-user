package grand.app.moon.views.fragments.company;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ProductAdapter;
import grand.app.moon.adapter.ProductCompanyAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentCompanyProductListBinding;
import grand.app.moon.databinding.FragmentShopProductListBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.product.ShopProductListDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.ads.AdvertisementDetailsFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyProductListFragment extends BaseFragment {


    private FragmentCompanyProductListBinding fragmentCompanyProductListBinding;
    private ShopProductListDetailsViewModel shopProductListDetailsViewModel;
    private ProductCompanyAdapter productCompanyAdapter;
    private String category_id,shop_id;
    private Service service;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentCompanyProductListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_company_product_list, container, false);
        getData();
        return fragmentCompanyProductListBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ID))
            shop_id = getArguments().getString(Constants.ID);
        if(getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID))
            category_id = getArguments().getString(Constants.CATEGORY_ID);
        if(getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
    }

    private void bind() {
        shopProductListDetailsViewModel = new ShopProductListDetailsViewModel(category_id,shop_id,service.mType,service.mFlag);
        AppUtils.initVerticalRV(fragmentCompanyProductListBinding.rvCompanyProduct, fragmentCompanyProductListBinding.rvCompanyProduct.getContext(), 1);
        fragmentCompanyProductListBinding.setShopProductViewModel(shopProductListDetailsViewModel);
    }

    private void setEvent() {
        shopProductListDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, shopProductListDetailsViewModel.getProductRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.PRODUCTS)) {
                    if(shopProductListDetailsViewModel.getProductRepository().getData().products.size() > 0) {
                        productCompanyAdapter = new ProductCompanyAdapter(shopProductListDetailsViewModel.getProductRepository().getData().products);
                        fragmentCompanyProductListBinding.rvCompanyProduct.setAdapter(productCompanyAdapter);
                        shopProductListDetailsViewModel.notifyChange();
                        setEventAdapter();
                    }else
                        shopProductListDetailsViewModel.noDataTextDisplay.set(true);

                }
            }
        });
    }

    private void setEventAdapter() {
        productCompanyAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                ProductList productList = shopProductListDetailsViewModel.getProductRepository().getData().products.get(pos);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, AdvertisementDetailsFragment.class.getName());
                intent.putExtra(Constants.NAME_BAR,productList.name);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.TYPE,service.mType);
                bundle.putInt(Constants.ID,Integer.parseInt(productList.id));
                intent.putExtra(Constants.BUNDLE,bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
        setEvent();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(shopProductListDetailsViewModel != null)
            shopProductListDetailsViewModel.reset();
    }
}
