package grand.app.moon.views.fragments.settings;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.settings.SettingsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentPrivacyListBinding;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.app.PrivacyListViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyListFragment extends BaseFragment {

    private FragmentPrivacyListBinding binding;
    private PrivacyListViewModel viewModel;
    private int type = 1;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_privacy_list, container, false);
        bind();
        setEvent();
        return binding.getRoot();
    }


    private void bind() {
        viewModel = new PrivacyListViewModel(13);
        binding.setViewModel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getSettingsRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
//                    AppUtils.initVerticalRV(binding.rvNotifications, binding.rvNotifications.getContext(), 1);
//                    binding.rvNotifications.setAdapter(new SettingsAdapter(viewModel.getSettingsRepository().settingsResponse.data));
                }
            }
        });
    }
}
