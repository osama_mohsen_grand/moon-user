package grand.app.moon.views.fragments.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AlbumAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.actionbar.SearchAction;
import grand.app.moon.databinding.FragmentDiscoverBinding;
import grand.app.moon.databinding.FragmentDiscoverMainBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.discover.DiscoverService;
import grand.app.moon.models.discover.MainDiscoverResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.ShopData;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.BottomSheetFlexDialogHelper;
import grand.app.moon.viewmodels.discover.DiscoverMainViewModel;
import grand.app.moon.viewmodels.discover.DiscoverViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.fragments.ExploreFragment;
import grand.app.moon.views.fragments.famous.FamousDetailsFragment;
import grand.app.moon.views.fragments.shop.ShopDetailsInfoFragment;
import timber.log.Timber;

public class DiscoverMainFragment extends BaseFragment {
    View rootView;
    FragmentDiscoverMainBinding binding;
    DiscoverMainViewModel viewModel;
    View baseSearch;
    boolean isSetEventAdapter = false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Log.d(TAG,TAG);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_discover_main, container, false);
        bind();
        return rootView;
    }


    private void bind() {
        AppUtils.initVerticalRV(binding.rvDiscover, binding.rvDiscover.getContext(), 3);
        viewModel = new DiscoverMainViewModel(getArguments());
        binding.setViewmodel(viewModel);
        setEvent();

        if (getActivity() instanceof MainActivity) {
            //from home page
            MainActivity mainActivity = (MainActivity) getActivity();
            baseSearch = mainActivity.homeActionBarView.layoutActionBarHomeBinding.imgHomeBarSearch;
        }else if (getActivity() instanceof BaseActivity){
            //from service details
            BaseActivity baseActivity = (BaseActivity) getActivity();
            baseActivity.backActionBarView.layoutActionBarBackBinding.imgBaseBarSearch.setVisibility(View.VISIBLE);
            baseSearch = baseActivity.backActionBarView.layoutActionBarBackBinding.imgBaseBarSearch;
        }

        baseSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.search.getVisibility() == View.VISIBLE)
                    binding.search.setVisibility(View.GONE);
                else
                    binding.search.setVisibility(View.VISIBLE);
            }
        });

        binding.search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                Log.d(TAG,"searchAfterTextChange:"+s);
                s = s.trim();
                if (!s.isEmpty()) {
                    Log.d(TAG,"find:"+s);
                    Log.d(TAG,"searchAfterTextChange_condition:"+s);
                    Log.d(TAG,"sizeOriginal:"+viewModel.originalList.size());
                    viewModel.albumAdapter.find(s,viewModel.originalList);
                }
                if (s.equals("")) {
                    viewModel.albumAdapter.update(viewModel.originalList);
                }
            }
        });
        rootView = binding.getRoot();
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getDiscoverRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.DISCOVER)) {
                    //from main home fetch all data (categories , services)
                    viewModel.setData();
                    setEventAdapter();
                } else if (action.equals(Constants.ALBUM)) {
                    viewModel.setAlbum();
                    setEventAdapter();
                }
            }
        });
    }

    private static final String TAG = "MainDiscoverFragment";

    private void setEventAdapter() {
        if(!isSetEventAdapter) {
            isSetEventAdapter = true;
            viewModel.discoverServiceAdapter.mMutableLiveData.observe((LifecycleOwner) context, o -> {
                List<IdName> subCategories = viewModel.getDiscoverRepository().mainDiscoverResponse.data.get(viewModel.discoverServiceAdapter.selected).categories;
                DiscoverService discoverService = viewModel.getDiscoverRepository().mainDiscoverResponse.data.get(viewModel.discoverServiceAdapter.selected);
                viewModel.discoverCategoryAdapter.update(subCategories);
                viewModel.albumAdapter.clear();
                if(subCategories.size() > 0) {
                    Log.d(TAG, "category_selected:" + viewModel.discoverServiceAdapter.selected);
                    Log.d(TAG, "category:" + discoverService.id);
                    if (discoverService.categories.size() > 0) {
                        IdName model = discoverService.categories.get(0);
                        Log.d(TAG, "service:" + model.id);
                        viewModel.getDiscoverAll(Integer.parseInt(model.id), discoverService.type, discoverService.flag);
                    }
                }else{
                    viewModel.getDiscoverAll(discoverService.id, discoverService.type, discoverService.flag);
                }
            });
            viewModel.discoverCategoryAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                @Override
                public void onChanged(Object o) {
                    DiscoverService discoverService = viewModel.getDiscoverRepository().mainDiscoverResponse.data.get(viewModel.discoverServiceAdapter.selected);
                    IdName model = discoverService.categories.get((Integer) o);
                    Log.d(TAG, "selected:" + viewModel.discoverCategoryAdapter.selected);
                    Log.d(TAG, "->" + model.id + "," + model.name);
                    viewModel.getDiscoverAll(Integer.parseInt(model.id), discoverService.type, discoverService.flag);
                }
            });
            viewModel.albumAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                @Override
                public void onChanged(Object o) {

                    //ShopDetailsInfoFragment

                    Mutable mutable = (Mutable) o;
                    Log.d(TAG,viewModel.discoverServiceAdapter.selected+"");
                    Log.d(TAG,viewModel.discoverServiceAdapter.discoverService.size()+"");
                    ImageVideo imageVideo = viewModel.albumAdapter.imageVideos.get(mutable.position);
                    Intent intent = new Intent(context, BaseActivity.class);
                    Bundle bundle = new Bundle();
                    DiscoverService discoverService = viewModel.getDiscoverRepository().mainDiscoverResponse.data.get(viewModel.discoverServiceAdapter.selected);

//                    bundle.putInt(Constants.SHOP_ID, imageVideo.shopId);
//                    bundle.putInt(Constants.TYPE, imageVideo.type);
//
//                    if(discoverService.categories.size() > 0) {
//                        bundle.putInt(Constants.CATEGORY_ID, Integer.parseInt(viewModel.discoverCategoryAdapter.discoverService.get(viewModel.discoverCategoryAdapter.selected).id));
//                        bundle.putInt(Constants.FLAG, viewModel.discoverServiceAdapter.discoverService.get(viewModel.discoverServiceAdapter.selected).flag);
//                    }else {
//                        bundle.putInt(Constants.CATEGORY_ID, discoverService.id);
//                        bundle.putInt(Constants.FLAG,discoverService.flag);
//                    }
//                    bundle.putInt(Constants.CATEGORY_ID, Integer.parseInt(viewModel.discoverCategoryAdapter.discoverService.get(viewModel.discoverCategoryAdapter.selected).id));
//                    bundle.putInt(Constants.FLAG, viewModel.discoverServiceAdapter.discoverService.get(viewModel.discoverServiceAdapter.selected).id);
//                    intent.putExtra(Constants.PAGE, ExploreFragment.class.getName());

                    bundle.putInt(Constants.ID, imageVideo.shopId);
                    if(discoverService.type.equals(Integer.parseInt(Constants.TYPE_FAMOUS_PEOPLE)) || discoverService.type.equals(Integer.parseInt(Constants.TYPE_PHOTOGRAPHER))){
                        //TYPE_FAMOUS_PEOPLE
                        bundle.putInt(Constants.TYPE, discoverService.type);
                        intent.putExtra(Constants.PAGE, FamousDetailsFragment.class.getName());
                        intent.putExtra(Constants.NAME_BAR,imageVideo.shopName);
                    }else
                        intent.putExtra(Constants.PAGE, ShopDetailsInfoFragment.class.getName());
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null)
            viewModel.reset();
    }
}
