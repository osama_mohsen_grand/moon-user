package grand.app.moon.views.fragments.company;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.CompanyAdapter;
import grand.app.moon.adapter.InstitutionAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentCompaniesListBinding;
import grand.app.moon.databinding.FragmentInstitutionShopBinding;
import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.company.CompaniesListViewModel;
import grand.app.moon.viewmodels.institution.InstitutionViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.service.ServiceDetailsFragment;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompaniesListFragment extends BaseFragment {

    private FragmentCompaniesListBinding fragmentCompaniesListBinding;
    public CompaniesListViewModel companiesListViewModel;
    private String type;
    public ArrayList<Integer> tags;
    private Service service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        fragmentCompaniesListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_companies_list, container, false);
        getData();
        bind();
//        setEvent();
        fragmentCompaniesListBinding.setViewModel(companiesListViewModel);
        return fragmentCompaniesListBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        Timber.e("type:"+type);
        Timber.e("type:"+service.mType);
    }


    private void bind() {
        companiesListViewModel = new CompaniesListViewModel(type);
        AppUtils.initVerticalRV(fragmentCompaniesListBinding.rvCompanies, fragmentCompaniesListBinding.rvCompanies.getContext(), 1);
        fragmentCompaniesListBinding.rvCompanies.setAdapter(companiesListViewModel.adapter);
        fragmentCompaniesListBinding.setViewModel(companiesListViewModel);
        setEventAdapter();
    }

//    private void setEvent() {
//        companiesListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
//            @Override
//            public void onChanged(@Nullable Object o) {
//                String action = (String) o;
//                handleActions(action, companiesListViewModel.getCompanyRepository().getMessage());
//                assert action != null;
////                if (action.equals(Constants.SUCCESS)) {
////                    companiesListViewModel.showPage(true);
////                    companyAdapter = new CompanyAdapter(companiesListViewModel.getCompanyRepository().getCompanyMainResponse().mData.companies);
////                    fragmentCompaniesListBinding.rvCompanies.setAdapter(companyAdapter);
////                    setData();
////                    if(companyAdapter.getItemCount() > 0) {
////                        setEventAdapter();
////                        companiesListViewModel.haveData();
////                    }else{
////                        companiesListViewModel.noData();
////                    }
////                }
//            }
//        });
//    }


    private static final String TAG = "CompaniesListFragment";
    private void setEventAdapter() {
        companiesListViewModel.adapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                AdsCompanyModel adsCompanyModel = companiesListViewModel.ads.get(pos);
                Log.d(TAG,adsCompanyModel.id+"");
//                Bundle bundle = new Bundle();
//                bundle.putInt(Constants.ID, adsCompanyModel.id);
//                bundle.putSerializable(Constants.SERVICE,service);
//                Intent intent = new Intent(context, BaseActivity.class);
//                intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
//                intent.putExtra(Constants.BUNDLE,bundle);
//                startActivity(intent);

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(companiesListViewModel != null)
            companiesListViewModel.reset();
    }
}
