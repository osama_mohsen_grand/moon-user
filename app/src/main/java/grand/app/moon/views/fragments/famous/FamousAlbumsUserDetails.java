package grand.app.moon.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AlbumAdapter;
import grand.app.moon.adapter.FamousAlbumAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFamousAlbumsUserDetailsBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.models.shop.ShopData;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.famous.FamousAlbumsUserDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousAlbumsUserDetails extends BaseFragment {


    private FragmentFamousAlbumsUserDetailsBinding fragmentFamousAlbumUserDetailsBinding;
    private FamousAlbumsUserDetailsViewModel famousAlbumsUserDetailsViewModel;
    public int id = -1;
    int tab = -1;
    String type;
    String kind;
    private FamousAlbumAdapter albumAdapter = new FamousAlbumAdapter();
    AccountInfoResponse accountInfoResponse;
    List<ImageVideo> data = null;

    private static final String TAG = "FamousAlbumsUserDetails";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView FamousAlbumsUserDetails");
        fragmentFamousAlbumUserDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_albums_user_details, container, false);
        famousAlbumsUserDetailsViewModel = new FamousAlbumsUserDetailsViewModel();
        getData();
        bind();
        setEvent();
        return fragmentFamousAlbumUserDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.FLAG))
            albumAdapter.flag = getArguments().getInt(Constants.FLAG);
        if (getArguments() != null && getArguments().containsKey(Constants.TAB))
            tab = getArguments().getInt(Constants.TAB);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getString(Constants.TYPE);
        if (getArguments() != null && getArguments().containsKey(Constants.ADS))
            albumAdapter.isAds = getArguments().getBoolean(Constants.ADS,false);
        if (getArguments() != null && getArguments().containsKey(Constants.KIND)) {
            kind = getArguments().getString(Constants.KIND);
            if (getArguments().containsKey(Constants.SHOP_DETAILS)) {
                accountInfoResponse = (AccountInfoResponse) getArguments().getSerializable(Constants.SHOP_DETAILS);
                if (kind.equals(Constants.PHOTOGRAPHER))
                    data = accountInfoResponse.photographer;
                else if (kind.equals(Constants.FAMOUS))
                    data = accountInfoResponse.famous;
                else if (kind.equals(Constants.GALLERY))
                    data = accountInfoResponse.gallery;

                albumAdapter.setData(data);
            }
        }
    }

    private void bind() {
        if (data == null) {
            famousAlbumsUserDetailsViewModel.setData(id, tab, type);
            famousAlbumsUserDetailsViewModel.getFamousDetails();
        }
        AppUtils.initVerticalRV(fragmentFamousAlbumUserDetailsBinding.rvFamousAlbumDetails, fragmentFamousAlbumUserDetailsBinding.rvFamousAlbumDetails.getContext(), 3);
        fragmentFamousAlbumUserDetailsBinding.rvFamousAlbumDetails.setAdapter(albumAdapter);
        fragmentFamousAlbumUserDetailsBinding.setFamousAlbumUserDetailsViewModel(famousAlbumsUserDetailsViewModel);
    }


    List<ImageVideo> imageVideos = null;

    private void setEvent() {
        famousAlbumsUserDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAlbumsUserDetailsViewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    imageVideos = famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data;
                    if (imageVideos != null && imageVideos.size() > 0) {
                        albumAdapter.setData(imageVideos);
                    } else {
                        famousAlbumsUserDetailsViewModel.noData();
                    }
                    setData();

                }
            }
        });
    }

    private void setData() {
        FamousDetailsFragment frag = ((FamousDetailsFragment) this.getParentFragment());
        frag.setData(famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse());
    }
}
