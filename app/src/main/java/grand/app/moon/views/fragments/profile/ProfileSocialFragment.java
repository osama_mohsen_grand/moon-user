package grand.app.moon.views.fragments.profile;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.NotificationAdapter;
import grand.app.moon.adapter.ServiceInfoAdapter;
import grand.app.moon.adapter.SocialAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentNotificationsBinding;
import grand.app.moon.databinding.FragmentProfileSocialBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.notification.NotificationViewModel;
import grand.app.moon.viewmodels.profile.ProfileSocialViewModel;
import grand.app.moon.views.fragments.famous.FamousDetailsFragment;
import grand.app.moon.vollyutils.AppHelper;

public class ProfileSocialFragment extends BaseFragment {
    View rootView;
    private FragmentProfileSocialBinding binding;
    public ProfileSocialViewModel viewModel;
    private static final String TAG = "ProfileSocialFragment";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_social, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        Log.d(TAG,"getData");
        viewModel = new ProfileSocialViewModel();
//        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
//            viewModel.setId(getArguments().getInt(Constants.ID));
//            viewModel.getData();
//        }
        if (getArguments() != null && getArguments().containsKey(Constants.ACCOUNT_INFO)) {
            Log.d(TAG,"getARgument");
            viewModel.setResponse( (AccountInfoResponse) getArguments().getSerializable(Constants.ACCOUNT_INFO));
        }
        binding.setViewModel(viewModel);
    }

    private void bind() {
//        AppUtils.initHorizontalRV(binding.rvServices, binding.rvServices.getContext(), 1);
//        binding.rvServices.setAdapter(viewModel.socialAdapter);


        rootView = binding.getRoot();
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.repository.getMessage());
                Log.d(TAG, action);
                if (action.equals(Constants.ACCOUNT_INFO)) {
                    Log.d(TAG, "has ACCOUNT_INFO;");
                    viewModel.setResponse(viewModel.repository.accountInfoResponse);
                    setParentData();
                    viewModel.notifyChange();
                } else if (action.equals(Constants.EMAIL)) {
                    AppUtils.openEmail(context, viewModel.response.email, getString(R.string.app_name), "");
                } else if (action.equals(Constants.PHONE)) {
                    AppUtils.openPhone(context, viewModel.response.number);
                } else if (action.equals(Constants.WEB)) {
                    AppUtils.openBrowser(context, viewModel.response.website);
                }
            }
        });
    }

    public boolean hasAccountInfo() {
        if (this.getParentFragment() instanceof FamousDetailsFragment) {
            FamousDetailsFragment frag = ((FamousDetailsFragment) this.getParentFragment());
            if (frag.famousDetailsViewModel.accountInfoResponse != null) {
                Log.d(TAG, "has response;");
                viewModel.response = frag.famousDetailsViewModel.accountInfoResponse;
//                setData();
                return true;
            }
        }
        Log.d(TAG, "no response;");
        return false;
    }


    private void setParentData() {
        if (this.getParentFragment() instanceof FamousDetailsFragment) {
            Log.d(TAG, "has setParentData;");
            FamousDetailsFragment frag = ((FamousDetailsFragment) this.getParentFragment());
            frag.famousDetailsViewModel.accountInfoResponse = viewModel.response;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }

}
