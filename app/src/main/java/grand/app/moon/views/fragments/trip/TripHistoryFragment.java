package grand.app.moon.views.fragments.trip;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.TripAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentTripHistoryBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.trip.TripHistoryViewModel;
import grand.app.moon.views.activities.BaseActivity;
import timber.log.Timber;


public class TripHistoryFragment extends BaseFragment {
    View rootView;
    private FragmentTripHistoryBinding fragmentTripHistoryBinding;
    private TripHistoryViewModel tripHistoryViewModel;
    private String type;
    private TripAdapter tripAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTripHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trip_history, container, false);
        getData();
        bind();
        rootView = fragmentTripHistoryBinding.getRoot();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getString(Constants.TYPE);
    }

    private void bind() {
        tripHistoryViewModel = new TripHistoryViewModel(type);
        setEvents();
        fragmentTripHistoryBinding.setTripHistoryViewModel(tripHistoryViewModel);
    }

    private void setEvents() {
        tripHistoryViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action,tripHistoryViewModel.getTripHistoryRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.HISTORY)){
                    if(tripHistoryViewModel.getTripHistoryRepository().getTripHistory().data.size() == 0)
                        tripHistoryViewModel.noData();
                    else{
                        AppUtils.initVerticalRV(fragmentTripHistoryBinding.rvTrips, fragmentTripHistoryBinding.rvTrips.getContext(), 1);
                        tripAdapter = new TripAdapter(tripHistoryViewModel.getTripHistoryRepository().getTripHistory().data);
                        fragmentTripHistoryBinding.rvTrips.setAdapter(tripAdapter);
                        setEventAdapter();
                    }
                }
            }
        });
    }

    private void setEventAdapter() {
        tripAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.TRIP_DETAILS);
                intent.putExtra(Constants.NAME_BAR,getString(R.string.trip_details));
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.TRIP,tripHistoryViewModel.getTripHistoryRepository().getTripHistory().data.get(pos));
                intent.putExtra(Constants.BUNDLE,bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tripHistoryViewModel.reset();
    }
}
