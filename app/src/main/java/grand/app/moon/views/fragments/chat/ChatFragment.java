package grand.app.moon.views.fragments.chat;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.devlomi.record_view.OnRecordClickListener;
import com.devlomi.record_view.OnRecordListener;
import com.rygelouv.audiosensei.player.AudioSenseiListObserver;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ChatAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentChatBinding;
import grand.app.moon.models.chat.ChatDetailsModel;
import grand.app.moon.notification.NotificationGCMModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.viewmodels.chat.ChatViewModel;
import grand.app.moon.views.fragments.auth.ProfileFragment;
import grand.app.moon.vollyutils.VolleyFileObject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;
import static grand.app.moon.utils.Constants.AUDIO_REQUEST_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment{

    private static final String TAG = "ChatFragment";

    private View rootView;
    private FragmentChatBinding fragmentChatBinding;
    private ChatViewModel chatViewModel;
    private ChatAdapter chatAdapter;
    int id = -1, type = -1,id_chat = -1;
    int receiver_id = -1;
    String senderName = "", senderImage = "";
    public boolean allowChat = true, chatFirst = false;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentChatBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.ID_CHAT))
            id_chat = getArguments().getInt(Constants.ID_CHAT);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
        if (getArguments() != null && getArguments().containsKey(Constants.ALLOW_CHAT))
            allowChat = getArguments().getBoolean(Constants.ALLOW_CHAT, true);
        Log.e(TAG, "getData: "+id );
    }


    private void bind() {
        chatViewModel = new ChatViewModel(id, type, allowChat, chatFirst,id_chat);
        AppUtils.initVerticalRV(fragmentChatBinding.rvChat, fragmentChatBinding.rvChat.getContext(), 1);
        fragmentChatBinding.setChatViewModel(chatViewModel);

        fragmentChatBinding.recordButton.setRecordView(fragmentChatBinding.recordView);
        fragmentChatBinding.recordButton.setSoundEffectsEnabled(false);

//        fragmentChatBinding.recordView.setOnRecordListener(this);
        fragmentChatBinding.recordView.setOnRecordListener(new OnRecordListener() {
            @Override
            public void onStart() {
                Log.e(TAG, "onStart: Done" );
                if(checkPermissions())
                    record();
            }

            @Override
            public void onCancel() {
                stopMedia();
            }

            @Override
            public void onFinish(long recordTime) {
                finishAudio(recordTime);
            }

            @Override
            public void onLessThanSecond() {
                stopMedia();
            }
        });


        fragmentChatBinding.recordButton.setListenForRecord(true);

        //ListenForRecord must be false ,otherwise onClick will not be called
        fragmentChatBinding.recordButton.setOnRecordClickListener(new OnRecordClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: Start Click" );
                if(checkPermissions()){
                    Log.e(TAG, "checkPermissions:done " );
                    record();
                }else {
                    Log.e(TAG, "checkPermissions:notDone " );
                    RequestPermissions();
                }
            }
        });
        AudioSenseiListObserver.getInstance().registerLifecycle(getLifecycle());
        rootView = fragmentChatBinding.getRoot();
    }

    private void setEvent() {
        chatViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, chatViewModel.getChatRepository().getMessage());
                assert action != null;
                Timber.e("action:"+action);
                if (action.equals(Constants.CHAT)) {
                    setData();
                    chatAdapter = new ChatAdapter(senderName, senderImage, chatViewModel.getChatRepository().getChatDetailsResponse().chats);
                    chatViewModel.receiver_id = chatViewModel.getChatRepository().getChatDetailsResponse().delegate.id;
                    fragmentChatBinding.rvChat.setAdapter(chatAdapter);
                    fragmentChatBinding.rvChat.scrollToPosition(chatAdapter.chats.size() - 1);
                    setEventAdapter();
                } else if (action.equals(Constants.SELECT_IMAGE)) {
                    pickImageDialogSelect();
                } else if (action.equals(Constants.CHAT_SEND)) {
                    chatAdapter.add(chatViewModel.getChatRepository().getChatSendResponse().data);
                    chatViewModel.text = "";
                    mRecorder = null;
                    chatViewModel.notifyChange();
                    fragmentChatBinding.rvChat.scrollToPosition(chatAdapter.chats.size() - 1);
                    AppUtils.hideKeyboard(getActivityBase(), rootView);
                }
//
//                } else if (action.equals(Constants.DELETE)) {
//                    toastMessage(chatViewModel.getCategoryRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
//                    homeAdapter.remove(chatViewModel.category_delete_position);
//                }
            }
        });
    }


    private void setData() {
        if (UserHelper.getUserId() != -1) {
            if (chatViewModel.getChatRepository().getChatDetailsResponse().delegate.type != Integer.parseInt(UserHelper.getUserDetails().type)) {
                senderName = chatViewModel.getChatRepository().getChatDetailsResponse().delegate.name;
                senderImage = chatViewModel.getChatRepository().getChatDetailsResponse().delegate.image;
            } else {
                senderName = chatViewModel.getChatRepository().getChatDetailsResponse().user.name;
                senderImage = chatViewModel.getChatRepository().getChatDetailsResponse().user.image;
            }
        }
    }


    private void setEventAdapter() {
        chatAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
//            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
//            chatViewModel.setFile(volleyFileObject);

            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, ChatFragment.this);

        }
        else  if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                chatViewModel.setFile(volleyFileObject);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, ChatFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, ""+getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private BroadcastReceiver chat = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (chatViewModel != null && chatAdapter != null) {
                Bundle bundle = intent.getBundleExtra(Constants.BUNDLE_NOTIFICATION);
                if (bundle != null && bundle.containsKey(Constants.BUNDLE_NOTIFICATION)) {
                    NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
                    if (notificationGCMModel != null  && notificationGCMModel.id == chatViewModel.getChatRepository().getChatDetailsResponse().delegate.id) {
                        ChatDetailsModel chatDetailsModel = new ChatDetailsModel();
                        chatDetailsModel.senderName = senderName;
                        chatDetailsModel.senderImage = senderImage;
                        if (notificationGCMModel.type_data == 1)
                            chatDetailsModel.message = notificationGCMModel.body;
                        else if (notificationGCMModel.type_data == 2)
                            chatDetailsModel.image = notificationGCMModel.image;
                        else
                            chatDetailsModel.audio = notificationGCMModel.audio;
                        chatDetailsModel.type = notificationGCMModel.type + "";
                        chatAdapter.add(chatDetailsModel);
                        fragmentChatBinding.rvChat.scrollToPosition(chatAdapter.chats.size() - 1);
                    }
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(chat, new IntentFilter(Constants.CHAT));
        if(checkPermissions()){
            fragmentChatBinding.recordButton.setVisibility(View.VISIBLE);
            fragmentChatBinding.imgPermission.setVisibility(View.GONE);
        }else{
            fragmentChatBinding.imgPermission.setVisibility(View.VISIBLE);
            fragmentChatBinding.imgPermission.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RequestPermissions();
                }
            });
            fragmentChatBinding.recordButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(chat);
    }


    public boolean checkPermissions() {
        int result = ContextCompat.checkSelfPermission(ChatFragment.this.getActivity(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(ChatFragment.this.getActivity(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }
    private void RequestPermissions() {
        Log.e(TAG, "RequestPermissions: True" );
        ActivityCompat.requestPermissions(ChatFragment.this.getActivity(), new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, AUDIO_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult: before start result" );
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        Log.e(TAG, "onRequestPermissionsResult: start result" );
        switch (requestCode) {
            case AUDIO_REQUEST_CODE:
                if (grantResults.length> 0) {
                    Log.e(TAG, "onRequestPermissionsResult: permission" );
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] ==  PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) {
                        onResume();
                    }
                }
                break;
        }
    }

    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;
    private static String mFileName = null;
    public void record(){
        Log.e(TAG, "record: " );
        mFileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/chat.mp3";
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(mFileName);
        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            Log.e(TAG, "IOException: "+e.getMessage() );
        }
        fragmentChatBinding.rlChatTextContainer.setVisibility(View.GONE);

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        chatViewModel.reset();

    }

    public void finishAudio(long time){
        stopMedia();
        File file = new File(mFileName);
        if(file.exists()){

            VolleyFileObject volleyFileObject = FileOperations.getFileVolleyFileObject(mFileName, "audio", AUDIO_REQUEST_CODE);

            chatViewModel.setFile(volleyFileObject);
        }
    }


    public void stopMedia(){
        Log.e(TAG, "stopMedia: stop" );
        if(mRecorder != null) {
            try {
                mRecorder.stop();
//                mRecorder.reset();
//                mRecorder.release();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
        fragmentChatBinding.rlChatTextContainer.setVisibility(View.VISIBLE);
    }

}
