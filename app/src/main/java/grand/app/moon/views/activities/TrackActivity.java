package grand.app.moon.views.activities;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import es.dmoral.toasty.Toasty;
import grand.app.moon.R;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.ActivityTrackBinding;
import grand.app.moon.notification.NotificationGCMModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.maputils.direction.DirectionDistance;
import grand.app.moon.utils.maputils.direction.DirectionHelper;
import grand.app.moon.utils.maputils.direction.DrawRoute;
import grand.app.moon.utils.maputils.location.GPSAllowListener;
import grand.app.moon.utils.maputils.location.GPSLocation;
import grand.app.moon.utils.maputils.location.LocationListenerEvent;
import grand.app.moon.utils.maputils.location.MapActivityHelper;
import grand.app.moon.utils.maputils.permission.MapPermissionActivity;
import grand.app.moon.utils.maputils.tracking.FireTracking;
import grand.app.moon.utils.maputils.tracking.caranimation.CarAnimation;
import grand.app.moon.utils.maputils.tracking.maprealtime.FireDriverLocationInterface;
import grand.app.moon.viewmodels.track.TrackViewModel;
import grand.app.moon.vollyutils.MyApplication;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class TrackActivity extends ParentActivity {

    ActivityTrackBinding activityTrackBinding;
    TrackViewModel trackViewModel;
    private static final String TAG = "TrackActivity";
    public FusedLocationProviderClient fusedLocationClient;
    public MapActivityHelper mapActivityHelper;
    public MapPermissionActivity mapPermission;
    public FireTracking fireTracking;
    double lat = 0, lng = 0;
    public int trip_id = -1;
    boolean async = false;
    //1=>accept , 2=>reject  , 3=>arrive  , 4=>start  , 5=> cancel after accept ,  6=>finished trip

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
        bind();
    }

    private void setEvent() {
        trackViewModel.mMutableLiveData.observe(this, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, trackViewModel.getTripRepository().getMessage());
                handleActions(action, trackViewModel.getTripActionRepository().getMessage());
                if (action.equals(Constants.TRIP_DETAILS)) {
                    trackViewModel.updateUi();
                    if (trackViewModel.getTripRepository().getTripDetailsResponse().data.status != 2 || trackViewModel.getTripRepository().getTripDetailsResponse().data.status != 5) {
                        trackViewModel.showDialogDriver();
                        animateCar();
                    }
//                    if (trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus == null || (trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus != null && trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.size() == 0)
//                            || (trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.get(trackViewModel.getTripRepository().getTripDetailsResponse().data.tripStatus.size() - 1).status_id == 5) ) {
//                        Log.e(TAG, "onChanged: here1");
//                        trackViewModel.showDialogDriver();
//                    } else{
//                        Log.e(TAG, "onChanged: here2");
//                        trackViewModel.showDistanceDialog();
//                    }
                    getRoute();
                } else if (action.equals(Constants.CALL)) {
                    Uri call = Uri.parse("tel:" + trackViewModel.getTripRepository().getTripDetailsResponse().data.phone);
                    Intent surf = new Intent(Intent.ACTION_DIAL, call);
                    startActivity(surf);
                } else if (action.equals(Constants.CANCELED)) {
                    toastMessage(trackViewModel.getTripRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    finishAffinity();
                    startActivity(new Intent(TrackActivity.this, MainActivity.class));
                }
            }
        });
    }

    boolean check = false;

    private void getRoute() {
        if (trackViewModel != null && trackViewModel.mapConfig != null && check && trackViewModel.getTripRepository() != null && trackViewModel.getTripRepository().getTripDetailsResponse() != null) {
            check = true;
            DirectionHelper directionHelper = new DirectionHelper(this, trackViewModel.mapConfig);
            ArrayList<LatLng> markers_locations = new ArrayList<>();
//        for(TripDetailsResponse.TripPath tripPath: trackViewModel.getTripRepository().getTripDetailsResponse().data.tripPath)
//            List<TripDetailsResponse.TripPath> tripPaths = trackViewModel.getTripRepository().getTripDetailsResponse().data.tripPath;

            markers_locations.add(new LatLng(trackViewModel.getTripRepository().getTripDetailsResponse().data.startLat, trackViewModel.getTripRepository().getTripDetailsResponse().data.startLng));
            trackViewModel.mapConfig.addMarker(new LatLng(trackViewModel.getTripRepository().getTripDetailsResponse().data.startLat, trackViewModel.getTripRepository().getTripDetailsResponse().data.startLng), R.drawable.ic_map_pin, "");

            markers_locations.add(new LatLng(trackViewModel.getTripRepository().getTripDetailsResponse().data.endLat, trackViewModel.getTripRepository().getTripDetailsResponse().data.endLng));
            trackViewModel.mapConfig.addMarker(new LatLng(trackViewModel.getTripRepository().getTripDetailsResponse().data.endLat, trackViewModel.getTripRepository().getTripDetailsResponse().data.endLng), R.drawable.ic_marker_dest, "");

            directionHelper.json_route = directionHelper.getRouteUrl(markers_locations);
            Log.e(TAG, "getRoute: " + directionHelper.json_route);
            new DrawRoute(this, directionHelper.json_route, trackViewModel.mapConfig, new DirectionDistance() {
                @Override
                public void receive(int distanceM, int timePerSec) {

                }
            }).execute();
        }
    }

    CarAnimation carAnimation = null;

    private void animateCar() {
        if (trackViewModel != null && trackViewModel.mapConfig != null && check && trackViewModel.getTripRepository() != null && trackViewModel.getTripRepository().getTripDetailsResponse() != null) {
            if (trackViewModel.getTripRepository().getTripDetailsResponse().data.status != 6 &&
                    trackViewModel.getTripRepository().getTripDetailsResponse().data.status != 2 &&
                    trackViewModel.getTripRepository().getTripDetailsResponse().data.status != 5) {
                fireTracking.getDriver(String.valueOf(trackViewModel.getTripRepository().getTripDetailsResponse().data.driverId), new FireDriverLocationInterface() {
                    @Override
                    public void getDriverLocation(LatLng driverLatLng) {
                        Log.e(TAG, "getDriverLocation: " + driverLatLng.latitude + "," + driverLatLng.longitude);
                        trackViewModel.mapConfig.setDriverMarker(driverLatLng);
                        if (carAnimation == null) {
                            carAnimation = new CarAnimation(TrackActivity.this, trackViewModel.mapConfig);
                            carAnimation.setConfig(trackViewModel.mapConfig.getDriverMarker(), 10000);
                        } else {
                            Marker marker = trackViewModel.mapConfig.getDriverMarker();
                            marker.setPosition(driverLatLng);
                            carAnimation.setMarker(marker);
                        }
                    }
                });
            }
        }
    }


    private void getData() {
        if (getIntent() != null && getIntent().hasExtra(Constants.TRIP_ID))
            trip_id = getIntent().getIntExtra(Constants.TRIP_ID, -1);
    }

    private View bind() {
        activityTrackBinding = DataBindingUtil.setContentView(this, R.layout.activity_track);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mapPermission = new MapPermissionActivity(this);

        activityTrackBinding.mapView.onCreate(new Bundle());

        activityTrackBinding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }

        activityTrackBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                setViewModel();
                trackViewModel.mapConfig = new MapConfig(getApplication(), mMap);
                mapActivityHelper = new MapActivityHelper(TrackActivity.this, trackViewModel.mapConfig);
                fireTracking = new FireTracking(TrackActivity.this, trackViewModel.mapConfig, mapActivityHelper);
                trackViewModel.mapConfig.setMapStyle();//set style google map
                trackViewModel.mapConfig.setSettings();//add setting google map
                if (trip_id == -1)
                    trackViewModel.mapConfig.checkLastLocation();
                trackViewModel.mapConfig.changeMyLocationButtonLocation(activityTrackBinding.mapView);//change location button
//                trackViewModel.mapConfig.setLocationButtonListeners();//enable button location listeners
                trackViewModel.mapConfig.setPadding(0, 0, 20, 10);
                trackViewModel.mapConfig.getGoogleMap().setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        lat = trackViewModel.mapConfig.getGoogleMap().getCameraPosition().target.latitude;
                        lng = trackViewModel.mapConfig.getGoogleMap().getCameraPosition().target.longitude;
                    }
                });//addMarkerOnMap_

                Log.e(TAG, "onMapReady: start");
                if (!mapPermission.validLocationPermission()) {
                    mapPermission.runtimePermissionLocation(Constants.LOCATION_REQUEST);
                } else {
                    trackViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                    mapActivityHelper.startUpdateLocation(new LocationListenerEvent() {
                        @Override
                        public void update(LatLng latLng, Object object) {
                            async = false;
                            trackViewModel.mapConfig.saveLastLocation(latLng);
                        }
                    });
                    onResume();
                    enableMyLocationIfPermitted();
                }
                check = true;
                getRoute();
                animateCar();
            }
        });


        return activityTrackBinding.getRoot();
    }

    private void setViewModel() {
        trackViewModel = new TrackViewModel(trip_id);
        activityTrackBinding.setTrackViewModel(trackViewModel);
        setEvent();

    }

    private void enableMyLocationIfPermitted() {
        if (!mapPermission.validLocationPermission()) {
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        } else if (trackViewModel.mapConfig != null && trackViewModel.mapConfig.getGoogleMap() != null) {
            allowGPS();
            onResume();
        }
    }

    private void allowGPS() {
        if (!GPSLocation.checkGps(TrackActivity.this)) {
            GPSLocation.EnableGPSAutoMatically((ParentActivity) TrackActivity.this, new GPSAllowListener() {
                @SuppressLint("MissingPermission")
                @Override
                public void GPSStatus(boolean isOpen) {
                    if (isOpen) {
                        trackViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
                        updateLocation();
                    }
                }
            });

        } else {
            if (mapPermission.validLocationPermission()) {
                Location location = GPSLocation.getLocation((ParentActivity) TrackActivity.this);
                if (location != null && !async && trip_id == -1) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    trackViewModel.mapConfig.moveCamera(latLng);
                    async = true;
                }
                updateLocation();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void updateLocation() {
        if (!mapPermission.validLocationPermission()) {
            return;
        }
        trackViewModel.mapConfig.getGoogleMap().setMyLocationEnabled(true);
        Task<Location> task = fusedLocationClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                Log.e(TAG, "onSuccess: ");
                if (location != null && !async && trip_id == -1) {
                    //Write your implemenation here
                    Log.e(TAG, location.getLatitude() + " " + location.getLongitude());
                }
            }
        });
        onResume();
    }


    BroadcastReceiver cancel = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent broadIntentExtra) {
            finish();
            Toasty.info(context, getString(R.string.trip_has_been_canceled_by_driver), Toasty.LENGTH_SHORT).show();
        }
    };

    BroadcastReceiver handleUi = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, "onReceive: Intent" );
            Bundle bundle = intent.getBundleExtra(Constants.BUNDLE_NOTIFICATION);
            if (bundle != null && bundle.containsKey(Constants.BUNDLE_NOTIFICATION)) {
                Log.e(TAG, "onReceive: BUNDLE" );
                NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
                if(notificationGCMModel != null){
                    Log.e(TAG, "status: "+notificationGCMModel.status );
                    trackViewModel.getTripRepository().getTripDetailsResponse().data.status = notificationGCMModel.status;
                    trackViewModel.updateUi();
                }
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(cancel, new IntentFilter(Constants.CANCELED));
        registerReceiver(handleUi, new IntentFilter(Constants.HANDLE_UI));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(cancel);
        unregisterReceiver(handleUi);
    }


    @Override
    protected void onDestroy() {
        if (fireTracking != null)
            fireTracking.stopTracking();
        super.onDestroy();
    }
}
