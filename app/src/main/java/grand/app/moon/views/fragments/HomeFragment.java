package grand.app.moon.views.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import grand.app.moon.R;
import grand.app.moon.adapter.RadioAdapter;
import grand.app.moon.adapter.ServiceAdapter;
import grand.app.moon.adapter.StoryAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentHomeBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.home.HomeViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.fragments.follower.FollowersFragment;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    private FragmentHomeBinding fragmentHomeBinding;
    private HomeViewModel homeViewModel;
    private StoryAdapter storyAdapter;
    private ServiceAdapter serviceAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        bind();
        setEvent();
        return fragmentHomeBinding.getRoot();
    }

    private void bind() {
        homeViewModel = new HomeViewModel();
        AppUtils.initVerticalRV(fragmentHomeBinding.rvHomeService, fragmentHomeBinding.rvHomeService.getContext(), 3);
        AppUtils.initHorizontalRV(fragmentHomeBinding.rvHomeStories, fragmentHomeBinding.rvHomeStories.getContext(), 1);
        fragmentHomeBinding.swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                fragmentHomeBinding.swiperefresh.setRefreshing(true);
                storyAdapter.swapUpdate();
            }
        });

        fragmentHomeBinding.setHomeViewModel(homeViewModel);
    }

    private void setEvent() {
        homeViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, homeViewModel.getHomeRepository().getMessage());
            assert action != null;
            if (action.equals(Constants.SUCCESS)) {
                serviceAdapter = new ServiceAdapter(homeViewModel.getHomeRepository().getHomeResponse().mData);
                fragmentHomeBinding.rvHomeService.setAdapter(serviceAdapter);
                List<Story> stories = homeViewModel.getHomeRepository().getHomeResponse().stories;
//                if(stories.size() > 0) {
//                    fragmentHomeBinding.lineHome.setVisibility(View.GONE);
//                    Story story = new Story();
//                    story.mStories = null;
//                    story.mId = -1;
//                    story.mImage = R.drawable.ic_more+"";
//                    story.mName = getString(R.string.more);
//                    stories.add(story);
//                    storyAdapter = new StoryAdapter(stories, HomeFragment.this);
//                    fragmentHomeBinding.rvHomeStories.setAdapter(storyAdapter);
//                }else {
//                    fragmentHomeBinding.lineHome.setVisibility(View.VISIBLE);
//                }


                Story story = new Story();
                story.mStories = null;
                story.mId = -1;
                story.mImage = R.drawable.ic_more+"";
                story.mName = ResourceManager.getString(R.string.more);
                story.nickname = ResourceManager.getString(R.string.more);
                stories.add(story);
                storyAdapter = new StoryAdapter(stories, HomeFragment.this);
                fragmentHomeBinding.rvHomeStories.setAdapter(storyAdapter);
                storyAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                    @Override
                    public void onChanged(Object o) {
                        fragmentHomeBinding.swiperefresh.setRefreshing(false);
                    }
                });
                homeViewModel.notifyChange();
                setEventAdapter();
            }
        });
    }

    private void setEventAdapter() {

        serviceAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Service service = homeViewModel.getHomeRepository().getHomeResponse().mData.get(pos);
                Intent intent = new Intent(context, BaseActivity.class);
                Bundle bundle = new Bundle();
                Timber.e("type:"+service.mType);
                if (service.mType == Integer.parseInt(Constants.TYPE_FAMOUS_PEOPLE)) {
                    intent.putExtra(Constants.PAGE, Constants.FAMOUS);
                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_FAMOUS_PEOPLE));
                } else if (service.mType == Integer.parseInt(Constants.TYPE_PHOTOGRAPHER)) {
                    intent.putExtra(Constants.PAGE, Constants.FAMOUS);
                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_PHOTOGRAPHER));
                }
//                else if (service.mType == Integer.parseInt(Constants.TYPE_COMPANIES)) {
//                    intent.putExtra(Constants.PAGE, Constants.COMPANIES);
//                    bundle.putSerializable(Constants.SERVICE, service);
//                }
//                else if (service.mType == Integer.parseInt(Constants.TYPE_INSTITUTIONS)) {
//                    intent.putExtra(Constants.PAGE, Constants.SERVICE_DETAILS);
//                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_INSTITUTIONS));
//                    bundle.putSerializable(Constants.SERVICE, service);
//                }
//                else if (service.mType == Integer.parseInt(Constants.TYPE_ADVERTISING)) {
//                    intent.putExtra(Constants.PAGE, Constants.ADS_MAIN);
//                    bundle.putSerializable(Constants.SERVICE, service);
//                }
//                else if (service.mType == Integer.parseInt(Constants.TYPE_TRUCKS)) {
//                    if(UserHelper.getUserId() != -1) {
//                        intent.putExtra(Constants.PAGE, Constants.TRUCKS);
//                        bundle.putSerializable(Constants.SERVICE, service);
//                    }else {
//                        toastInfo(ResourceManager.getString(R.string.please_login_first));
//                        return;
//                    }
//                }
                else if (service.mType == Integer.parseInt(Constants.TYPE_TAXI)) {
                    if(UserHelper.getUserId() != -1) {
                        intent.putExtra(Constants.PAGE, Constants.MAP_BOOKING);
                        bundle.putString(Constants.TYPE,Constants.TYPE_TAXI);
                        bundle.putSerializable(Constants.SERVICE, service);
                    }else {
                        toastInfo(ResourceManager.getString(R.string.please_login_first));
                        return;
                    }
                }else {
                    intent.putExtra(Constants.PAGE, Constants.SERVICE_DETAILS);
                    bundle.putSerializable(Constants.SERVICE, service);
                }
                intent.putExtra(Constants.BUNDLE, bundle);
                intent.putExtra(Constants.NAME_BAR, homeViewModel.getHomeRepository().getHomeResponse().mData.get(pos).mName);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (homeViewModel != null)
            homeViewModel.reset();
    }

    private static final String TAG = "HomeFragment";

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,"result_ok:"+requestCode);
        if(requestCode == Constants.VIEW_REQUEST){
            Log.d(TAG,"result_ok done");
            if(data != null) {
                if(data.hasExtra("storyCount")){
                    int storyCount = data.getIntExtra("storyCount", 0);
                    storyAdapter.updateCount(storyCount);
                }
            }
        }
    }
}
