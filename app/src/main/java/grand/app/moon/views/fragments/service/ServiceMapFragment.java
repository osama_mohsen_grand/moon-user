package grand.app.moon.views.fragments.service;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.ShopAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentServiceMapBinding;
import grand.app.moon.map.GPSAllowListener;
import grand.app.moon.map.GPSLocation;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.models.order.details.Shop;
import grand.app.moon.models.service.ShopDetails;
import grand.app.moon.models.service.ShopMap;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.LanguagesHelper;
import grand.app.moon.utils.MyContextWrapper;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.maputils.base.MapConfig;
import grand.app.moon.utils.storage.location.LocationHelper;
import grand.app.moon.viewmodels.map.MapAddressViewModel;
import grand.app.moon.viewmodels.map.MapViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;
import grand.app.moon.vollyutils.MyApplication;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceMapFragment extends BaseFragment {


    FragmentServiceMapBinding fragmentServiceMapBinding;
    //    ServiceDetailsFragment serviceDetailsFragment;
    private int filter = 1;
    public MapViewModel mapViewModel = null;
    Bundle savedInstanceState;
    private Service service;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentServiceMapBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_map, container, false);
//        serviceDetailsFragment = ((ServiceDetailsFragment) this.getParentFragment());
        getData();
        this.savedInstanceState = savedInstanceState;
        bind(savedInstanceState);
        return fragmentServiceMapBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
    }

    private void bind(Bundle savedInstanceState) {
        Log.d(TAG, "bind");

        fragmentServiceMapBinding.mapView.onCreate(savedInstanceState);
        mapViewModel = new MapViewModel(service, filter);
        setEvent();
        fragmentServiceMapBinding.mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(MyApplication.getInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "savedInstanceState bind");


        fragmentServiceMapBinding.mapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                Log.d(TAG, "mMap");

                mapViewModel.mapConfig = new MapConfig(context, mMap);
                mapViewModel.mapConfig.setMapStyle();//set style google map
                mapViewModel.mapConfig.setSettings();//add setting google map
                mapViewModel.mapConfig.checkLastLocation();
                addMarkers();
            }
        });
        fragmentServiceMapBinding.setMapViewModel(mapViewModel);
    }

    private static final String TAG = "ServiceMapFragment";
    Observer observer;

    public void addMarkers() {
        try {
            if (mapViewModel != null && mapViewModel.shops != null && mapViewModel.mapConfig != null && mapViewModel.mapConfig.isVisible()) {
                mapViewModel.mapConfig.clear();
                for (int i = 0; i < mapViewModel.shops.size(); i++) {
                    ShopDetails shop = mapViewModel.shops.get(i);
                    mapViewModel.mapConfig.addMarker(new LatLng(shop.lat, shop.lng), shop.mId, shop.mImage, shop.mName, marker -> {
                        if (mapViewModel != null && mapViewModel.mapConfig.isVisible()) {
                            mapViewModel.mapConfig.zoomCamera(150);
                            fragmentServiceMapBinding.mapView.onResume();
                        }
                    });
                }
            }


            mapViewModel.mapConfig.getGoogleMap().setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    marker.showInfoWindow();
                    return true;
                }
            });

            mapViewModel.mapConfig.getGoogleMap().setOnInfoWindowClickListener(marker -> {
                // TODO Auto-generated method stub
                int shop_id = (int) mapViewModel.mapConfig.getId(marker);
                Bundle bundle = new Bundle();
                ShopDetailsFragment shopDetailsFragment = new ShopDetailsFragment();
                bundle.putInt(Constants.ID, shop_id);
                bundle.putSerializable(Constants.SERVICE, service);
                shopDetailsFragment.setArguments(bundle);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            });
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    private void setEvent() {
        observer = new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Log.d(TAG, "Object o");

                String action = (String) o;
                handleActions(action, mapViewModel.getHomeRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    addMarkers();
//                    if(fragmentServiceMapBinding.mapView != null && mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.mShops != null){
//                        mapViewModel.mapConfig.clear();
//                        for(int i=0; i< mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.mShops.size(); i++){
//                            shopMap =  mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.mShops.get(i);
//                            mapViewModel.mapConfig.addMarker(new LatLng(shopMap.lat, shopMap.lng),shopMap.id, shopMap.image, shopMap.name, marker -> {
//                                mapViewModel.mapConfig.zoomCamera(150);
//                                fragmentServiceMapBinding.mapView.onResume();
//                            });
//                        }
//                        setClickMarker(mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.mShops);
//                    }

                    //TODO HERE
//                    if (fragmentServiceMapBinding.mapView != null && mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.clinics != null) {
//                        for (int i = 0; i < mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.clinics.size(); i++) {
//                            shopMap = mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.clinics.get(i);
//                            mapViewModel.mapConfig.addMarker(new LatLng(shopMap.lat, shopMap.lng), shopMap.id, shopMap.image, shopMap.name, marker -> {
//                                if (mapViewModel != null && mapViewModel.mapConfig.isVisible()) {
//                                    mapViewModel.mapConfig.zoomCamera(150);
//                                    fragmentServiceMapBinding.mapView.onResume();
//                                }
//                            });
//                        }
//                        setClickMarker(mapViewModel.getHomeRepository().getServiceMapDetailsResponse().mData.clinics);
//                    }
                }
            }
        };
        mapViewModel.mMutableLiveData.observe((LifecycleOwner) context, observer);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fragmentServiceMapBinding != null) {
            fragmentServiceMapBinding.mapView.onResume();
//            if (mapViewModel != null)
//                mapViewModel.callService();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (fragmentServiceMapBinding != null)
            fragmentServiceMapBinding.mapView.onPause();
    }

    @Override
    public void onDestroy() {

        if (fragmentServiceMapBinding != null)
            if (mapViewModel != null) {
                mapViewModel.reset();
                fragmentServiceMapBinding.mapView.onDestroy();
                mapViewModel.mMutableLiveData.removeObserver(observer);
                mapViewModel.mapConfig.destroy();
                mapViewModel.mapConfig = null;
                mapViewModel = null;
            }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        fragmentServiceMapBinding.mapView.onLowMemory();
    }


}
