package grand.app.moon.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.NotificationAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentExploreBinding;
import grand.app.moon.databinding.FragmentNotificationsBinding;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.explore.ExploreViewModel;
import grand.app.moon.viewmodels.notification.NotificationViewModel;

public class ExploreFragment extends BaseFragment {
    View rootView;
    private FragmentExploreBinding binding;
    private ExploreViewModel viewModel;
    int shop_id = -1,type=-1,flag=-1,category_id=-1;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_explore, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments().containsKey(Constants.SHOP_ID)){
            shop_id = getArguments().getInt(Constants.SHOP_ID);
        }
        if(getArguments().containsKey(Constants.TYPE)){
            type = getArguments().getInt(Constants.TYPE);
        }
        if(getArguments().containsKey(Constants.FLAG)){
            flag = getArguments().getInt(Constants.FLAG);
        }
        if(getArguments().containsKey(Constants.CATEGORY_ID)){
            category_id = getArguments().getInt(Constants.CATEGORY_ID);
        }
    }

    private void bind() {
        viewModel = new ExploreViewModel(shop_id,category_id,type,flag);
        binding.setViewmodel(viewModel);
        rootView = binding.getRoot();
    }

    private void setEvent() {

        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getDiscoverRepository().getMessage());
                if(action.equals(Constants.EXPLORE)){
                    viewModel.setData(shop_id);
                }
            }
        });

        viewModel.mMutableLiveDataCommon.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.commonRepository.getMessage());
                if(action.equals(Constants.FOLLOW)){
                    toastMessage(viewModel.commonRepository.getMessage());
                    viewModel.adapter.updateFollow(viewModel.adapter.follow_selected);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }

}
