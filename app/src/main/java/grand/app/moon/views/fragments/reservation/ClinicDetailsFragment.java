package grand.app.moon.views.fragments.reservation;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentClinicDetailsBinding;
import grand.app.moon.models.app.Mutable;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.models.clinic.Data;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.BottomSheetDialogHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.viewmodels.clinic.ClinicDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.beauty.BeautyServiceListFragment;
import grand.app.moon.views.fragments.clinic.DoctorListFragment;
import grand.app.moon.views.fragments.company.CompanyProductListFragment;
import grand.app.moon.views.fragments.gallery.GalleryFragment;
import grand.app.moon.views.fragments.shop.ShopDetailsInfoFragment;
import grand.app.moon.views.fragments.shop.ShopProductListFragment;
import grand.app.moon.vollyutils.AppHelper;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClinicDetailsFragment extends BaseFragment {
    private FragmentClinicDetailsBinding fragmentClinicDetailsBinding;
    public ClinicDetailsViewModel clinicDetailsViewModel;
    private int type;
    private int id;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    private SwapAdapter adapter = null;
    Service service;
//    private int[] tabIcons = {
//            R.drawable.ic_doctor_white,
//            R.drawable.ic_gallery_white,
//            R.drawable.ic_ads_white
//    };


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentClinicDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_details, container, false);
        getData();
        bind();
        setEvent();
        return fragmentClinicDetailsBinding.getRoot();
    }

    private void getData() {

        if (getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            id = getArguments().getInt(Constants.ID);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) { // shop_id
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
            type = service.mType;
        }
    }


    private void bind() {
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivityBase());
        clinicDetailsViewModel = new ClinicDetailsViewModel(id, type);
        setEventAdapter();
        fragmentClinicDetailsBinding.setClinicDetailsViewModel(clinicDetailsViewModel);
    }

    private static final String TAG = "ClinicDetailsFragment";

    private void setTabs() {
        tabModels.clear();
        fragmentClinicDetailsBinding.viewpager.setAdapter(null);
        adapter = null;
        tabModels = new ArrayList<>();

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.CLINIC_DETAILS, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse());

        if (type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
            BeautyServiceListFragment beautyServiceListFragment = new BeautyServiceListFragment();
            beautyServiceListFragment.setArguments(bundle);
            tabModels.add(new TabModel(getString(R.string.reservations), beautyServiceListFragment));
        }

        clinicDetailsViewModel.data = clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data;
        for (int i = 0; i < clinicDetailsViewModel.data.categories.size(); i++) {
            Bundle NewBundle = new Bundle();
            NewBundle.putString(Constants.ID, "" + clinicDetailsViewModel.data.details.id);
            NewBundle.putString(Constants.CATEGORY_ID, clinicDetailsViewModel.data.categories.get(i).id);
            NewBundle.putSerializable(Constants.SERVICE, service);
            ShopProductListFragment shopProductFragment = new ShopProductListFragment();
            shopProductFragment.setArguments(NewBundle);
            tabModels.add(new TabModel(clinicDetailsViewModel.data.categories.get(i).name, shopProductFragment));
        }

        adapter = new SwapAdapter(getChildFragmentManager(), tabModels);
        fragmentClinicDetailsBinding.viewpager.setAdapter(adapter);
        fragmentClinicDetailsBinding.viewpager.setOffscreenPageLimit(0);
        fragmentClinicDetailsBinding.slidingTabs.setupWithViewPager(fragmentClinicDetailsBinding.viewpager);
    }

    BottomSheetDialogHelper bottomSheetDialogHelper;


    private void setEvent() {
        clinicDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                Timber.e("action:" + action);
                handleActions(action, clinicDetailsViewModel.getClinicRepository().getMessage());
                if (action.equals(Constants.CLINIC_DETAILS)) {
                    clinicDetailsViewModel.setData();
                    clinicDetailsViewModel.showPage(true);
                    if (!clinicDetailsViewModel.isClinic.get()) setTabs();
                } else if (action.equals(Constants.REVIEW)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.REVIEW);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, id);
                    bundle.putInt(Constants.TYPE, type);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (action.equals(Constants.FILTER)) {
                    if (bottomSheetDialogHelper.filterDialog == null) {
                        bottomSheetDialogHelper.filterFamousShopAds(clinicDetailsViewModel.getClinicRepository()
                                .getClinicDetailsResponse().data.specialist, false, (dialog, type, object) -> {
                            if (type.equals(Constants.ARRAY)) {
                                ArrayList<Integer> list = (ArrayList<Integer>) object;
                                if (list != null && list.size() > 0) {
                                    clinicDetailsViewModel.specialist_id = list.get(0);
                                    clinicDetailsViewModel.callService();
                                }
                            }
                        });
                    } else {
                        bottomSheetDialogHelper.filterDialog.show();
                    }
                } else if (action.equals(Constants.CHAT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID_CHAT, id);
                    bundle.putInt(Constants.TYPE, type);
                    bundle.putBoolean(Constants.ALLOW_CHAT, true);
                    bundle.putBoolean(Constants.CHAT_FIRST, true);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);

                } else if (action.equals(Constants.FILTER)) {
                    if (bottomSheetDialogHelper.filterDialog == null) {
                        bottomSheetDialogHelper.filterFamousShopAds(clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.specialist, false, (dialog, type, object) -> {
                            if (type.equals(Constants.ARRAY)) {
                                ArrayList<Integer> list = (ArrayList<Integer>) object;
                                if (list != null && list.size() > 0) {
                                    clinicDetailsViewModel.specialist_id = list.get(0);
                                    clinicDetailsViewModel.callService();
                                }
                            }
                        });
                    } else {
                        bottomSheetDialogHelper.filterDialog.show();
                    }
                } else if (action.equals(Constants.FOLLOW)) {
                    int x = Integer.parseInt(clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.followersCount);
                    Log.d(TAG,"x_before:"+x);
                    clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.isFollow = !clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.isFollow;
                    if (clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.isFollow) {
                        x++;
                        clinicDetailsViewModel.shopDepartmentAdapter.updateFollow(true);
                    } else {
                        x--;
                        clinicDetailsViewModel.shopDepartmentAdapter.updateFollow(false);
                    }
                    Log.d(TAG,"x:"+x);
                    clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.followersCount = "" + x;
                    clinicDetailsViewModel.notifyChange();
                } else if (action.equals(Constants.BEAUTY_SUBMIT)) {
                    if (clinicDetailsViewModel.ids.size() == 0) {
                        showError(getString(R.string.please_select_at_least_one_service));
                    } else {
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.BOOK_BEAUTY);
                        intent.putExtra(Constants.NAME_BAR, getString(R.string.book_details));
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.ID, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.id);
                        bundle.putSerializable(Constants.CLINIC_DETAILS, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse());
                        bundle.putInt(Constants.PAYMENT_TYPE, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.payment_type);
                        bundle.putIntegerArrayList(Constants.SERVICES, clinicDetailsViewModel.ids);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        startActivity(intent);
                    }
                } else if (action.equals(Constants.ERROR)) {
                    showError(clinicDetailsViewModel.baseError);
                }
            }
        });
    }

    //here
    private void setEventAdapter() {
        clinicDetailsViewModel.shopDepartmentAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                    @Override
                    public void onChanged(@Nullable Object o) {
                        String event = (String) o;
                        if (event.equals(Constants.SHARE)) {
                            AppHelper.shareCustom(requireActivity(),
                                    clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.name,
                                    clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.share,
                                    fragmentClinicDetailsBinding.imageView6);
                        } else if (event.equals(Constants.ACCOUNT_INFO)) {
                            Intent intent = new Intent(context, BaseActivity.class);
                            intent.putExtra(Constants.PAGE, ShopDetailsInfoFragment.class.getName());
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.ID, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.id);
                            intent.putExtra(Constants.BUNDLE, bundle);
                            startActivity(intent);
                        } else if (event.equals(Constants.FOLLOW)) {
                            clinicDetailsViewModel.getClinicRepository().follow(new FollowRequest(clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.id, type));
                        } else if (event.equals(Constants.CHAT)) {
                            if (UserHelper.getUserId() != -1) {
                                Intent intent = new Intent(context, BaseActivity.class);
                                intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
                                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                                Bundle bundle = new Bundle();
                                bundle.putInt(Constants.ID_CHAT, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.id);
                                bundle.putInt(Constants.TYPE, type);
                                bundle.putString(Constants.NAME, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.name);
                                bundle.putString(Constants.IMAGE, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.image);
                                bundle.putBoolean(Constants.ALLOW_CHAT, true);
                                bundle.putBoolean(Constants.CHAT_FIRST, true);
                                intent.putExtra(Constants.BUNDLE, bundle);
                                startActivity(intent);
                            } else
                                toastInfo(getString(R.string.please_login_first));
                        }
                    }
                }
        );
        clinicDetailsViewModel.beautyServiceAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                clinicDetailsViewModel.calculatePrice();
            }
        });
        clinicDetailsViewModel.doctorsAdapter.mMutableLiveDataAdapter.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Log.d(TAG, "here change");
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.BOOK_CLINIC)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.BOOK_CLINIC);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.book_details));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.doctorsList.get(mutable.position).id);
                    bundle.putInt(Constants.TYPE, Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC));
                    bundle.putInt(Constants.PAYMENT_TYPE, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data.details.payment_type);
                    bundle.putSerializable(Constants.CLINIC_DETAILS, clinicDetailsViewModel.getClinicRepository().getClinicDetailsResponse().data);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clinicDetailsViewModel.reset();
    }
}
