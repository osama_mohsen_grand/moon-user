package grand.app.moon.views.fragments.trucks;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.facebook.FacebookModel;
import grand.app.moon.customviews.facebook.FacebookResponseInterface;
import grand.app.moon.databinding.FragmentLoginBinding;
import grand.app.moon.databinding.FragmentTruckPickupFormBinding;
import grand.app.moon.models.app.AccountTypeModel;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.truck.TruckRequest;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.PopUp.PopUpInterface;
import grand.app.moon.utils.PopUp.PopUpMenuHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.trucks.TrucksPickupFormViewModel;
import grand.app.moon.viewmodels.user.LoginViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class TruckPickupFormFragment extends BaseFragment {

    View rootView;
    private FragmentTruckPickupFormBinding fragmentTruckPickupFormBinding;
    private TrucksPickupFormViewModel trucksPickupFormViewModel;
    private TruckRequest truckRequest;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentTruckPickupFormBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_truck_pickup_form, container, false);
        getData();
        initPopUp();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.REQUEST)){
            truckRequest = (TruckRequest) getArguments().getSerializable(Constants.REQUEST);
            Timber.e("driver_id:"+truckRequest.driver_id);
            Timber.e("price:"+truckRequest.total_price);
            Timber.e("trip_time:"+truckRequest.trip_time);
        }
    }

    private void bind() {
        trucksPickupFormViewModel = new TrucksPickupFormViewModel(truckRequest);
        fragmentTruckPickupFormBinding.setTrucksViewModel(trucksPickupFormViewModel);
        rootView = fragmentTruckPickupFormBinding.getRoot();
    }

    private void setEvent() {
        trucksPickupFormViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, trucksPickupFormViewModel.getTrucksRepository().getMessage());
                assert action != null;
                Log.e("action", action);
                if (action.equals(Constants.DONE)) {
                    toastMessage(trucksPickupFormViewModel.getTrucksRepository().getMessage());
                    ((AppCompatActivity) context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });
    }

    private void initPopUp() {
        ArrayList<String> popUpModels = AppMoon.getGoodTypes();
        fragmentTruckPickupFormBinding.edtTruckPickupType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpMenuHelper.openPopUp(getActivity(), fragmentTruckPickupFormBinding.edtTruckPickupType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        fragmentTruckPickupFormBinding.edtTruckPickupType.setText(popUpModels.get(position));
                        truckRequest.goods_type = String.valueOf(position+1);
                    }
                });

            }
        });
        fragmentTruckPickupFormBinding.edtTruckPickupType.setText(popUpModels.get(0));
        truckRequest.goods_type = "1";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (trucksPickupFormViewModel != null) trucksPickupFormViewModel.reset();
    }

}
