package grand.app.moon.views.fragments.ads;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentAdvertisementDetailsBinding;
import grand.app.moon.models.adsDetails.AdsDetailsModel;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.ads.AdvertisementDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementDetailsFragment extends BaseFragment {

    private FragmentAdvertisementDetailsBinding fragmentAdvertisementDetailsBinding;
    private AdvertisementDetailsViewModel advertisementDetailsViewModel;
    private int type;
    private int id,shop_id;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentAdvertisementDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_advertisement_details, container, false);
        getData();
        bind();
        setEvent();
        return fragmentAdvertisementDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getInt(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            id = getArguments().getInt(Constants.ID);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_ID)) { // shop_id
            shop_id = getArguments().getInt(Constants.SHOP_ID);
        }
    }


    private void bind() {
        advertisementDetailsViewModel = new AdvertisementDetailsViewModel(id, type);
        setTabListeners();
        fragmentAdvertisementDetailsBinding.setAdvertisementDetailsViewModel(advertisementDetailsViewModel);
    }

    /*

     */

    private void setTabListeners() {
        fragmentAdvertisementDetailsBinding.bnvAdsDetails.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.bottomNavigationChatId) {
                    if (UserHelper.getUserId() == -1) {
                        pleaseLoginFirst();
                    }else{
                        AdsDetailsModel adsDetailsModel = advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data;
                        Intent intent = new Intent(context,BaseActivity.class);
                        intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                        intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.ID_CHAT, adsDetailsModel.userId);
                        bundle.putInt(Constants.TYPE,type);
                        bundle.putBoolean(Constants.ALLOW_CHAT, true);
                        bundle.putBoolean(Constants.CHAT_FIRST, true);
                        intent.putExtra(Constants.BUNDLE,bundle);
                        startActivity(intent);
                    }
                }
                if (item.getItemId() == R.id.bottomNavigationEmailId) {
                    if (UserHelper.getUserId() == -1) {
                        pleaseLoginFirst();
                    }else{
                        AdsDetailsModel adsDetailsModel = advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data;
                        AppUtils.openEmail(context,adsDetailsModel.email,adsDetailsModel.name,adsDetailsModel.description);
                    }
                }
                if (item.getItemId() == R.id.bottomNavigationCallId) {
                    if (UserHelper.getUserId() == -1) {
                        pleaseLoginFirst();
                    }else{
                        AppUtils.openPhone(context,advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.phone);
                    }
                }
                return true;
            }
        });
    }

    public void pleaseLoginFirst() {
        new DialogConfirm(context)
                .setTitle(ResourceManager.getString(R.string.app_name))
                .setTitleTextSize(ResourceManager.getDimens(R.dimen.sp9))
                .setMessage(ResourceManager.getString(R.string.please_login_to_complete_this_action))
                .setMessageTextSize(ResourceManager.getDimens(R.dimen.sp7))
                .setActionText(ResourceManager.getString(R.string.label_login))
                .setActionTextSize(ResourceManager.getDimens(R.dimen.sp5))
                .setActionCancel(ResourceManager.getString(R.string.cancel))
                .setImage(R.mipmap.ic_launcher, ResourceManager.getDimens(R.dimen.dp70w), ResourceManager.getDimens(R.dimen.dp70h))
                .show(new DialogHelperInterface() {
                    @Override
                    public void OnClickListenerContinue(Dialog dialog, View view) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra(Constants.PAGE,Constants.LOGIN);
                        getActivityBase().finishAffinity();
                        startActivity(intent);
                    }
                });
    }

    private void setEvent() {
        advertisementDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, advertisementDetailsViewModel.getAdvertisementCompanyRepository().getMessage());
                if (action.equals(Constants.ADS_DETAILS)) {
                    advertisementDetailsViewModel.setData();
                    advertisementDetailsViewModel.showPage(true);

                }else if(action.equals(Constants.LOCATION)){
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.lat);
                    intent.putExtra(Constants.LNG, advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.lng);
                    startActivity(intent);
                }else if(action.equals(Constants.FAVOURITE)){
                    toastMessage(advertisementDetailsViewModel.getAdvertisementCompanyRepository().getMessage());
                    advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.isFavourite = !advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.isFavourite;
//                    if(advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.isFavourite.equals("0")){
//                        advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.isFavourite = "1";
//                    }else
//                        advertisementDetailsViewModel.getAdvertisementCompanyRepository().getAdsDetailsResponse().data.isFavourite = "0";
                    advertisementDetailsViewModel.notifyChange();
                    
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        advertisementDetailsViewModel.reset();
    }

}
