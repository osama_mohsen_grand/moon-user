package grand.app.moon.views.fragments.base;


import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentVideoBinding;
import grand.app.moon.repository.FirebaseRepository;
import grand.app.moon.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends BaseFragment {


    FragmentVideoBinding fragmentVideoBinding;
    public String video = "";
    SimpleExoPlayer player;
    FirebaseRepository repository = new FirebaseRepository();
    int flag = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentVideoBinding =   DataBindingUtil.inflate(inflater,R.layout.fragment_video, container, false);
        getData();
//        Timber.e("video:"+video);
//        fragmentVideoBinding.video.setVideoPath(video);
//        fragmentVideoBinding.video.start();

        player = ExoPlayerFactory.newSimpleInstance(getActivityBase());
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                .createMediaSource(Uri.parse(video));
        fragmentVideoBinding.playerView.setPlayer(player);
        player.setPlayWhenReady(true);
        player.prepare(mediaSource, true, false);

        if (getArguments() != null && getArguments().containsKey(Constants.FLAG)) {
            flag = getArguments().getInt(Constants.FLAG);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            int id = getArguments().getInt(Constants.ID);
            repository.mediaView(new MediaViewRequest(id,flag));
        }

        return fragmentVideoBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.VIDEO)){
            video = getArguments().getString(Constants.VIDEO);
        }
    }

    @Override
    public void onPause() {
        if(player != null) {
            player.stop();
            player.release();
            player.setPlayWhenReady(false);
        }
        player = null;
        super.onPause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
