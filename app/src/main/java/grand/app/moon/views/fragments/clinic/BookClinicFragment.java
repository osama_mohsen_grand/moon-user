package grand.app.moon.views.fragments.clinic;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.common.base.Joiner;
import com.vivekkaushik.datepicker.DatePickerTimeline;
import com.vivekkaushik.datepicker.OnDateSelectedListener;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import grand.app.moon.R;
import grand.app.moon.adapter.TimeAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.calender.CustomGridLayoutManager;
import grand.app.moon.databinding.FragmentBookClinicBinding;
import grand.app.moon.models.clinic.Data;
import grand.app.moon.models.doctor.Schedule;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.reservation.DateTimeModel;
import grand.app.moon.models.service.clinic.ClinicDataDetails;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.clinic.BookDoctorViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.activities.MainActivity;
import timber.log.Timber;

public class BookClinicFragment extends BaseFragment {

    private static final String TAG = "BookClinicFragment";
    FragmentBookClinicBinding fragmentBookClinicBinding;
    BookDoctorViewModel bookDoctorViewModel;
    int id;
    TimeAdapter timeAdapter;
    int type = Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC), payment_type = 0;
    ArrayList<Integer> ids = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentBookClinicBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_book_clinic, container, false);
        getData();
        bind();
        initDate();
        setEvent();
        return fragmentBookClinicBinding.getRoot();

    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) {
            id = getArguments().getInt(Constants.ID);
            Timber.e("id:" + id);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PAYMENT_TYPE)) {
            payment_type = getArguments().getInt(Constants.PAYMENT_TYPE);
        }

    }

    private void bind() {
        Log.d(TAG,"bind");
        bookDoctorViewModel = new BookDoctorViewModel(id);
        if (getArguments() != null && getArguments().containsKey(Constants.CLINIC_DETAILS)) {
            Log.d(TAG,"getArguments");
            bookDoctorViewModel.setAdapter((Data) getArguments().getSerializable(Constants.CLINIC_DETAILS));
        }
        bookDoctorViewModel.updateUI(payment_type);
        AppUtils.initVerticalRV(fragmentBookClinicBinding.rvBookingList, fragmentBookClinicBinding.rvBookingList.getContext(), 5);
        fragmentBookClinicBinding.setBookDoctorViewModel(bookDoctorViewModel);
    }

    private void initDate() {
        DatePickerTimeline datePickerTimeline = fragmentBookClinicBinding.datePickerTimeline;
        datePickerTimeline.setDateTextColor(ResourceManager.getColor(R.color.colorBlack));
        datePickerTimeline.setDayTextColor(ResourceManager.getColor(R.color.colorPrimary));
        datePickerTimeline.setMonthTextColor(ResourceManager.getColor(R.color.colorPrimary));

        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new Date());
        String[] datesData = currentDate.split("-");

        datePickerTimeline.setInitialDate(Integer.parseInt(datesData[2]), Integer.parseInt(datesData[1]) - 1, Integer.parseInt(datesData[0]));
        datePickerTimeline.setDisabledDateColor(ResourceManager.getColor(R.color.colorPrimaryAc));
// Set a date Selected Listener
        datePickerTimeline.setOnDateSelectedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(int year, int month, int day, int dayOfWeek) {
                Timber.e("day:" + dayOfWeek);
                if (bookDoctorViewModel != null) {
                    bookDoctorViewModel.reservationRequest.date = year + "-" + AppUtils.getNumberForDate(month + 1) + "-" + AppUtils.getNumberForDate(day);
                    bookDoctorViewModel.reservationRequest.service = "";
                    bookDoctorViewModel.callService();
                }
            }

            @Override
            public void onDisabledDateSelected(int year, int month, int day, int dayOfWeek, boolean isDisabled) {
                // Do Something
            }
        });
        datePickerTimeline.setActiveDate(Calendar.getInstance());
    }

    public ArrayList<DateTimeModel> times = new ArrayList<>();

    public void TimeData() {
        times.clear();
        int minutes = 0;
        for (Schedule schedule : bookDoctorViewModel.getClinicRepository().getDoctorDetailsResponse().data.schedules) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            try {
                Date dateFrom = simpleDateFormat.parse(schedule.from);
                Date dateTo = simpleDateFormat.parse(schedule.to);
                minutes = schedule.period;
                int start = 0;
                if (dateTo.after(dateFrom)) {
                    Date newDate = new Date(dateFrom.getTime() + 60000 * 0);
                    while (dateTo.after(newDate)) {
                        times.add(new DateTimeModel(simpleDateFormat.format(newDate), schedule.day, schedule.period));
                        start += minutes;
                        newDate = new Date(dateFrom.getTime() + (60000 * start));
                    }
                }
            } catch (ParseException e) {
                Timber.e("ex" + e.getMessage());
                e.printStackTrace();
            }
        }
        for (DateTimeModel d : times) {
            Timber.e("data:" + d.time);
        }
        if (timeAdapter == null) {
            timeAdapter = new TimeAdapter(times, bookDoctorViewModel.getClinicRepository().getDoctorDetailsResponse().data.reservedTime);
            fragmentBookClinicBinding.rvBookingList.setAdapter(timeAdapter);
            setEventAdapter();
        } else {
            timeAdapter.updateAll(times);
        }
    }


    private void setEvent() {
        bookDoctorViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, bookDoctorViewModel.getClinicRepository().getMessage());
                if (action.equals(Constants.DOCTOR_DETAILS)) {
                    TimeData();
                    bookDoctorViewModel.setData(bookDoctorViewModel.getClinicRepository().getDoctorDetailsResponse());
                    bookDoctorViewModel.showPage(true);
                } else if (action.equals(Constants.SUCCESS)) {
                    toastMessage(bookDoctorViewModel.getClinicRepository().getMessage());
                    getActivityBase().finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                } else if (action.equals(Constants.ERROR)) {
                    showError(bookDoctorViewModel.baseError);
                } else if (action.equals(Constants.REVIEW)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.REVIEW);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }


    private void setEventAdapter() {
        timeAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                timeAdapter.update(pos);
                bookDoctorViewModel.reservationRequest.service = times.get(pos).time;
                bookDoctorViewModel.reservationRequest.day_id = times.get(pos).day;
                bookDoctorViewModel.reservationRequest.period = times.get(pos).period;
            }
        });
    }
}

