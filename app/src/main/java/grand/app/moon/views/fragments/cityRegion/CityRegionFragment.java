package grand.app.moon.views.fragments.cityRegion;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.TagsAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentCityRegionBinding;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.CountriesResponse;
import grand.app.moon.models.country.Datum;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.city.CityRegionViewModel;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class CityRegionFragment extends BaseFragment {


    View rootView;
    private FragmentCityRegionBinding binding;
    private CityRegionViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_city_region, container, false);
        bind();
        rootView = binding.getRoot();
        return rootView;
    }

    List<IdNameImage> cities = new ArrayList<>();


    private void bind() {
        CountriesResponse countriesResponse = UserHelper.getCountries();
        for (Datum datum : countriesResponse.data) {
            if (datum.id == Integer.parseInt(UserHelper.retrieveKey(Constants.COUNTRY_ID))) {
                for (City city : datum.cities) {
                    IdNameImage idNameImage = new IdNameImage();
                    idNameImage.id = city.id + "";
                    idNameImage.name = city.cityName;
                    cities.add(idNameImage);
                }
                break;
            }
        }
        viewModel = new CityRegionViewModel();
        setEvents();
        AppUtils.initVerticalRV(binding.rvCity, binding.rvCity.getContext(), 1);
        binding.setViewModel(viewModel);
    }

    private void setEvents() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.SUBMIT)) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    if (viewModel.cityAdapter.getPosition() != -1)
                        bundle.putInt(Constants.CITY_ID, viewModel.country.cities.get(viewModel.cityAdapter.getPosition()).id);
                    if (viewModel.regionAdapter.getPosition() != -1)
                        bundle.putInt(Constants.REGION_ID, viewModel.country.cities.get(viewModel.cityAdapter.getPosition()).regions.get(viewModel.regionAdapter.getPosition()).id);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    requireActivity().setResult(RESULT_OK, intent);
                    requireActivity().finish();
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.reset();
    }

}
