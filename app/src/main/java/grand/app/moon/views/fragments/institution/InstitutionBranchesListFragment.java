package grand.app.moon.views.fragments.institution;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.BranchAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentInstitutionBranchesListBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.shop.ShopData;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.branch.BranchesViewModel;
import grand.app.moon.views.activities.MapAddressActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstitutionBranchesListFragment extends BaseFragment {


    private FragmentInstitutionBranchesListBinding fragmentInstitutionBranchesListBinding;
    private BranchesViewModel branchesViewModel;
    private BranchAdapter branchAdapter;
    private Service service;
    private int id;
    private ShopData shopData;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentInstitutionBranchesListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_institution_branches_list, container, false);
        getData();
        bind();
        return fragmentInstitutionBranchesListBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);

        }
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            id = getArguments().getInt(Constants.ID);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.INSTITUTIONS)){
            shopData = (ShopData) getArguments().getSerializable(Constants.INSTITUTIONS);
            branchAdapter = new BranchAdapter(shopData.branches);
            fragmentInstitutionBranchesListBinding.rvBranches.setAdapter(branchAdapter);
            setEventAdapter();
        }
    }

    private void bind() {
        branchesViewModel = new BranchesViewModel();
        AppUtils.initVerticalRV(fragmentInstitutionBranchesListBinding.rvBranches, fragmentInstitutionBranchesListBinding.rvBranches.getContext(), 1);
        fragmentInstitutionBranchesListBinding.setBranchesViewModel(branchesViewModel);
    }

    private void setEventAdapter() {
        branchAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Intent intent = new Intent(context, MapAddressActivity.class);
                intent.putExtra(Constants.LAT, shopData.branches.get(pos).lat);
                intent.putExtra(Constants.LNG, shopData.branches.get(pos).lng);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(branchesViewModel != null)
            branchesViewModel.reset();
    }
}
