package grand.app.moon.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.databinding.FragmentFamousDetailsBinding;
import grand.app.moon.models.app.TabModel;
import grand.app.moon.models.famous.details.FamousDetailsResponse;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.tabLayout.SwapAdapter;
import grand.app.moon.viewmodels.famous.FamousDetailsViewModel;
import grand.app.moon.views.activities.BaseActivity;
import grand.app.moon.views.fragments.ads.FamousAdsUserFragment;
import grand.app.moon.views.fragments.profile.InfoServiceFragment;
import grand.app.moon.views.fragments.profile.ProfileSocialFragment;
import grand.app.moon.views.fragments.profile.TransferGuideFragment;
import grand.app.moon.vollyutils.AppHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousDetailsFragment extends BaseFragment {

    private FragmentFamousDetailsBinding fragmentFamousDetailsBinding;
    public FamousDetailsViewModel famousDetailsViewModel;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    public int id = -1, type = -1,x;
    FamousDetailsResponse famousDetailsResponse;
    AccountInfoResponse accountInfoResponse = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFamousDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_details, container, false);
        getData();

        bind();
        setEvent();
        return fragmentFamousDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
    }


    private void bind() {
        famousDetailsViewModel = new FamousDetailsViewModel(type);
        fragmentFamousDetailsBinding.setFamousDetailsViewModel(famousDetailsViewModel);

        FamousAlbumsUserDetails famousAlbumsUserDetails = new FamousAlbumsUserDetails();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TAB, 1);//mean album
        bundle.putInt(Constants.ID, id);
        bundle.putInt(Constants.FLAG, 2);
        bundle.putString(Constants.TYPE, type + "");
        famousAlbumsUserDetails.setArguments(bundle);


        FamousAdsUserFragment famousAdsUserFragment = new FamousAdsUserFragment();
        bundle = new Bundle();
        bundle.putInt(Constants.TAB, 2);//mean ads
        bundle.putInt(Constants.ID, id);
        bundle.putString(Constants.TYPE, type + "");
        famousAdsUserFragment.setArguments(bundle);

        tabModels.add(new TabModel(getString(R.string.albums), famousAlbumsUserDetails));
        tabModels.add(new TabModel(getString(R.string.companies_ads), famousAdsUserFragment));

        tabModels.add(new TabModel(getString(R.string.profile), setFragmentBundleId(new ProfileSocialFragment())));
        tabModels.add(new TabModel(getString(R.string.info_and_service), setFragmentBundleId(new InfoServiceFragment())));
        tabModels.add(new TabModel(getString(R.string.transfer_guide), setFragmentBundleId(new TransferGuideFragment())));
//        FamousAlbumsUserDetails famousAlbumsUserDetails1 = new FamousAlbumsUserDetails();
//        famousAlbumsUserDetails1.setArguments(bundle);
//        tabModels.add(new TabModel(getString(R.string.adds),famousAlbumsUserDetails1));
//        tabModels.add(new TabModel(getString(R.string.ads),new AdsFragment()));

        SwapAdapter adapter = new SwapAdapter(getChildFragmentManager(), tabModels);
        fragmentFamousDetailsBinding.viewpager.setAdapter(adapter);
        fragmentFamousDetailsBinding.viewpager.setOffscreenPageLimit(tabModels.size());
        fragmentFamousDetailsBinding.slidingTabs.setupWithViewPager(fragmentFamousDetailsBinding.viewpager);
    }

    private Fragment setFragmentBundleId(Fragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.ID, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void setEvent() {
        famousDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousDetailsViewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.FAMOUS_FOLLOW)) {
                    toastMessage(famousDetailsViewModel.getFamousRepository().getMessage());
                    famousDetailsViewModel.isFollow = !famousDetailsViewModel.isFollow;
                    if (famousDetailsViewModel.isFollow) {
                        x = Integer.parseInt(famousDetailsViewModel.followerCount);
                        x++;
                    } else {
                        x = Integer.parseInt(famousDetailsViewModel.followerCount);
                        x--;
                    }
                    famousDetailsViewModel.followerCount = x+"";
                    famousDetailsViewModel.setFollowerCount();
                    famousDetailsViewModel.notifyChange();
                } else if (action.equals(Constants.CHAT_DETAILS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID_CHAT, famousDetailsResponse.accountDetails.id);
                    bundle.putInt(Constants.TYPE, famousDetailsResponse.accountDetails.type);
                    bundle.putString(Constants.NAME, famousDetailsResponse.accountDetails.name);
                    bundle.putString(Constants.IMAGE, famousDetailsResponse.accountDetails.image);
                    bundle.putBoolean(Constants.ALLOW_CHAT, true);
                    bundle.putBoolean(Constants.CHAT_FIRST, true);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (action.equals(Constants.ERROR)) {
                    toastInfo(famousDetailsViewModel.baseError);
                } else if (action.equals(Constants.SHOP_DETAILS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
                    intent.putExtra(Constants.PAGE, Constants.SHOP_DETAILS);
//                    intent.putExtra(Constants.NAME_BAR, famousDetailsResponse.accountDetails.name);

                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, famousDetailsResponse.accountDetails.id);
                    Service service = new Service();
                    service.mType = 1;
                    service.mFlag = 1;
                    bundle.putSerializable(Constants.SERVICE, service);
                    bundle.putInt(Constants.ID, famousDetailsResponse.accountDetails.id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (action.equals(Constants.SHARE)) {
                    AppHelper.shareCustom(requireActivity(),famousDetailsResponse.accountDetails.name,famousDetailsResponse.accountDetails.description,fragmentFamousDetailsBinding.profileImage);
                }
            }
        });
    }


    public void setHeader(String name, String image, String followers, int isFollow,boolean hasShop) {
        famousDetailsViewModel.setData(id, name, image, followers, isFollow,hasShop);
        famousDetailsViewModel.showPage(true);
    }


    public void setData(FamousDetailsResponse famousDetailsResponse) {
        this.famousDetailsResponse = famousDetailsResponse;
        famousDetailsViewModel.famousDetailsResponse = famousDetailsResponse;
        setHeader(famousDetailsResponse.accountDetails.name, famousDetailsResponse.accountDetails.image, famousDetailsResponse.followers, famousDetailsResponse.isFollow,famousDetailsResponse.hasShop);

        if (famousDetailsResponse.profile != null && famousDetailsResponse.infoServices != null){
            ProfileSocialFragment profileSocialFragment = (ProfileSocialFragment) tabModels.get(2).fragment;
            profileSocialFragment.viewModel.setResponse(famousDetailsResponse.getAccountInfo());

            InfoServiceFragment infoServiceFragment = (InfoServiceFragment) tabModels.get(3).fragment;
            infoServiceFragment.viewModel.setResponse(famousDetailsResponse.getAccountInfo());

            TransferGuideFragment transferGuideFragment = (TransferGuideFragment) tabModels.get(4).fragment;
            transferGuideFragment.viewModel.setResponse(famousDetailsResponse.getAccountInfo());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        famousDetailsViewModel.reset();

    }

}
