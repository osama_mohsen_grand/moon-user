package grand.app.moon.views.fragments.trucks;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import grand.app.moon.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrucksIntroFragment extends Fragment {


    public TrucksIntroFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_trucks_intro, container, false);
    }

}
