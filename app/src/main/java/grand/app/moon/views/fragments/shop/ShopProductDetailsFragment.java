package grand.app.moon.views.fragments.shop;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.AdditionAdapter;
import grand.app.moon.adapter.ColorAdapter;
import grand.app.moon.adapter.RadioAdapter;
import grand.app.moon.adapter.TextAdapter;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.databinding.FragmentShopProductDetailsBinding;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.productList.ProductList;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.dialog.DialogHelperActionInterface;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.product.ShopProductDetailsViewModel;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.vollyutils.AppHelper;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopProductDetailsFragment extends BaseFragment {


    private FragmentShopProductDetailsBinding fragmentShopProductDetailsBinding;
    private ShopProductDetailsViewModel shopProductDetailsViewModel;
    private ColorAdapter colorAdapter = null;
    private TextAdapter sizeAdapter = null;
    private RadioAdapter radioAdapter = null;
    private AdditionAdapter additionAdapter = null;
    private ProductList product;
    private Service service;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentShopProductDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_product_details, container, false);
        getData();
        bind();
        setEventShop();
        setEventCart();
        return fragmentShopProductDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.PRODUCT))
            product = (ProductList) getArguments().getSerializable(Constants.PRODUCT);
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            service = (Service) getArguments().getSerializable(Constants.SERVICE);
        }
    }

    private void bind() {
        shopProductDetailsViewModel = new ShopProductDetailsViewModel(product, service);

        AppUtils.initHorizontalRV(fragmentShopProductDetailsBinding.rvProductColors, fragmentShopProductDetailsBinding.rvProductColors.getContext(), 1);
        AppUtils.initVerticalRV(fragmentShopProductDetailsBinding.rvProductCategoryCheck, fragmentShopProductDetailsBinding.rvProductCategoryCheck.getContext(), 1);
        //rv_product_category_check
        fragmentShopProductDetailsBinding.setProductDetailsViewModel(shopProductDetailsViewModel);
    }



    private static final String TAG = "ShopProductDetailsFragm";

    Target target = new Target() {

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            AppHelper.shareCustom(requireActivity(),shopProductDetailsViewModel.getProductRepository().getProductResponse().data.productName,
                    shopProductDetailsViewModel.getProductRepository().getProductResponse().data.descName,
                    bitmap );
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };

    private void setEventShop() {
        shopProductDetailsViewModel.shopMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, shopProductDetailsViewModel.getProductRepository().getMessage());
                assert action != null;
                Timber.e("action:" + action);
                if (action.equals(Constants.PRODUCT_DETAILS)) {
                    shopProductDetailsViewModel.addCartRequest.qty = shopProductDetailsViewModel.getProductRepository().getProductResponse().data.min_qty;
                    shopProductDetailsViewModel.setData();
                    shopProductDetailsViewModel.showPage(true);
                    if (shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors != null && shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors.size() > 0) {
                        colorAdapter = new ColorAdapter(shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors);
                        fragmentShopProductDetailsBinding.rvProductColors.setAdapter(colorAdapter);
                        AppUtils.initHorizontalRV(fragmentShopProductDetailsBinding.rvProductSizes, fragmentShopProductDetailsBinding.rvProductSizes.getContext(), 1);
                        if (shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors.size() > 0) {
                            sizeAdapter = new TextAdapter(shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors.get(0).sizes);
                        } else {
                            sizeAdapter = new TextAdapter(new ArrayList<>());
                        }
                        fragmentShopProductDetailsBinding.rvProductSizes.setAdapter(sizeAdapter);
                        setEventColorAdapter();
                    }
                    if (shopProductDetailsViewModel.getProductRepository().getProductResponse().data.sizes != null && shopProductDetailsViewModel.getProductRepository().getProductResponse().data.sizes.size() > 0) {
                        Timber.e("action:radioAdapter");
                        radioAdapter = new RadioAdapter(shopProductDetailsViewModel.getProductRepository().getProductResponse().data.sizes);
                        AppUtils.initVerticalRV(fragmentShopProductDetailsBinding.rvProductSizes, fragmentShopProductDetailsBinding.rvProductSizes.getContext(), 1);
                        fragmentShopProductDetailsBinding.rvProductSizes.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        fragmentShopProductDetailsBinding.rvProductSizes.setAdapter(radioAdapter);
                    }
                    if (shopProductDetailsViewModel.getProductRepository().getProductResponse().data.additions != null && shopProductDetailsViewModel.getProductRepository().getProductResponse().data.additions.size() > 0) {
                        Timber.e("action:additions");
                        additionAdapter = new AdditionAdapter(shopProductDetailsViewModel.getProductRepository().getProductResponse().data.additions, new ArrayList<>());
                        AppUtils.initVerticalRV(fragmentShopProductDetailsBinding.rvProductAddition, fragmentShopProductDetailsBinding.rvProductAddition.getContext(), 1);
                        fragmentShopProductDetailsBinding.rvProductAddition.setAdapter(additionAdapter);
                    }
                } else if (action.equals(Constants.FAVOURITE)) {
                    toastMessage(shopProductDetailsViewModel.getProductRepository().getMessage());
                    shopProductDetailsViewModel.getProductRepository().getProductResponse().data.isFavourite = !shopProductDetailsViewModel.getProductRepository().getProductResponse().data.isFavourite;
                    shopProductDetailsViewModel.notifyChange();
                } else if (action.equals(Constants.SHARE)) {

                    AppHelper.shareCustom(requireActivity(),shopProductDetailsViewModel.getProductRepository().getProductResponse().data.productName,
                            shopProductDetailsViewModel.getProductRepository().getProductResponse().data.descName+"\n"+
                                    shopProductDetailsViewModel.getProductRepository().getProductResponse().data.share,
                            fragmentShopProductDetailsBinding.sliderHomeCategory.getChildAt(0).findViewById(R.id.daimajia_slider_image) );

//                    String url = "https://moon4online.com/product-details?product_id=" +
//                            shopProductDetailsViewModel.getProductRepository().getProductResponse().data.productId +
//                            "&flag=" + service.mFlag +
//                            "&name=" + shopProductDetailsViewModel.getProductRepository().getProductResponse().data.productName;
//                    FirebaseDynamicLinks.getInstance().createDynamicLink()
//                            .setLink(Uri.parse(url))
//
//                            .setDomainUriPrefix(URLS.BASE_URL_WEB)
//                            .setAndroidParameters(
//                                    new DynamicLink.AndroidParameters.Builder("grand.app.moon")
//                                            .setMinimumVersion(1)
//                                            .build())
//                            .setIosParameters(
//                                    new DynamicLink.IosParameters.Builder("com.Organization.Moon-User")
//                                            .setMinimumVersion("1.0")
//                                            .build())
//                            .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT).addOnCompleteListener(new OnCompleteListener<ShortDynamicLink>() {
//                        @Override
//                        public void onComplete(@NonNull Task<ShortDynamicLink> task) {
//
//                            if (task.isSuccessful()) {
//                                Uri shortURL = task.getResult().getShortLink();
//                                AppHelper.shareDynamicLink(requireActivity(), getString(R.string.app_name),
//                                        product.name + "\n" +
//                                                shopProductDetailsViewModel.getProductRepository().getProductResponse().data.descName + "\n\n" +
//                                                shortURL.toString(), null);
//                            } else {
//                                Toast.makeText(requireContext(), "error:" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//                                Log.d(TAG, "" + task.getException().getMessage());
//                            }
//                        }
//                    });
                }
            }
        });
    }

    private void setEventCart() {
        shopProductDetailsViewModel.cartMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, shopProductDetailsViewModel.getCartRepository().getMessage());
                assert action != null;
                Timber.e("cartSubmit:" + action);
                if (action.equals(Constants.SUBMIT)) {
                    if (colorAdapter != null) {
                        shopProductDetailsViewModel.addCartRequest.color_id = String.valueOf(shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors.get(colorAdapter.selected).id);
                    }
                    if (colorAdapter != null && sizeAdapter != null) {
                        shopProductDetailsViewModel.addCartRequest.size_id = shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors.get(colorAdapter.selected).sizes.get(sizeAdapter.selected).id;
                    }
                    if (radioAdapter != null) {
                        shopProductDetailsViewModel.addCartRequest.size_id = shopProductDetailsViewModel.getProductRepository().getProductResponse().data.sizes.get(radioAdapter.selected).id;
                    }
                    if (additionAdapter != null && additionAdapter.selected.size() > 0) {
                        String addition = additionAdapter.selected.toString();
                        shopProductDetailsViewModel.addCartRequest.additions = addition.substring(1, addition.length() - 1);
                    }
                    shopProductDetailsViewModel.callService();
                } else if (action.equals(Constants.SUCCESS)) {//added to cart

                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.cart))
                            .setMessage(shopProductDetailsViewModel.getCartRepository().getMessage())
                            .setActionText(ResourceManager.getString(R.string.continue_))
                            .setActionCancel(ResourceManager.getString(R.string.move_to_cart))
                            .show(new DialogHelperActionInterface() {
                                @Override
                                public void onSubmit(Dialog dialog, View view) {
                                    ((ParentActivity) context).finish();
                                }

                                @Override
                                public void onCancel(Dialog dialog, View view) {
                                    Timber.e("cancel : Done");
                                    ((ParentActivity) context).finishAffinity();
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.putExtra(Constants.PAGE, Constants.CART);
                                    startActivity(intent);
                                }
                            });

                } else if (action.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.delete_cart))
                            .setMessage(shopProductDetailsViewModel.getCartRepository().getMessage())
                            .setActionText(ResourceManager.getString(R.string.continue_))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    shopProductDetailsViewModel.deleteCart();
                                }
                            });
                } else if (action.equals(Constants.DELETED)) {
                    toastMessage(shopProductDetailsViewModel.getCartRepository().getMessage());
                } else if (action.equals(Constants.LOGOUT)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.dialog))
                            .setMessage(ResourceManager.getString(R.string.please_login_to_complete_this_action))
                            .setActionText(ResourceManager.getString(R.string.label_login))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    getActivityBase().finishAffinity();
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                                    startActivity(intent);
                                }
                            });

                }
            }
        });
    }

    private void setEventColorAdapter() {
        colorAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                List<IdName> idNames = shopProductDetailsViewModel.getProductRepository().getProductResponse().data.colors.get(colorAdapter.selected).sizes;
                sizeAdapter.update(idNames);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shopProductDetailsViewModel.reset();
    }

}
