package grand.app.moon.views.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.base.ParentActivity;
import grand.app.moon.databinding.FragmentOpenLocationBinding;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.maputils.location.GPSAllowListener;
import grand.app.moon.utils.maputils.location.GPSLocation;
import grand.app.moon.utils.maputils.location.LocationChangeListener;
import grand.app.moon.utils.maputils.location.LocationLatLng;
import grand.app.moon.utils.maputils.permission.MapPermissionFragment;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.viewmodels.location.OpenLocationViewModel;
import grand.app.moon.views.activities.MainActivity;
import grand.app.moon.views.activities.MapAddressActivity;
import timber.log.Timber;


public class OpenLocationFragment extends BaseFragment {
    private FragmentOpenLocationBinding fragmentOpenLocationBinding;
    private OpenLocationViewModel openLocationViewModel;
    LocationLatLng locationLatLng;
    MapPermissionFragment mapPermissionFragment;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentOpenLocationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_open_location, container, false);
        mapPermissionFragment = new MapPermissionFragment(this);
        bind();

        return fragmentOpenLocationBinding.getRoot();
    }


    private void bind() {
        openLocationViewModel = new OpenLocationViewModel();
        setEvents();
        fragmentOpenLocationBinding.setOpenLocationViewModel(openLocationViewModel);
    }

    private void setEvents() {
        openLocationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.LOCATION_ENABLE)) {
                    enableLocation();
//                    ((ParentActivity) context).finishAffinity();
//                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });

    }

    public void enableLocation() {
        GPSLocation.EnableGPSAutoMatically(getActivityBase(), new GPSAllowListener() {
            @Override
            public void GPSStatus(boolean isOpen) {
//                ((ParentActivity) context).finishAffinity();
//                startActivity(new Intent(context, MainActivity.class));

                if (isOpen) {
                    Timber.e("start here");
                    if (mapPermissionFragment.validLocationPermission()) {
                        Timber.e("start validLocationPermission");
                        fetchLocation();
                    }else{
                        Timber.e("start  not validLocationPermission");
                        mapPermissionFragment.makeRequestPermission(Constants.LOCATION_REQUEST);
                    }
                } else {
                    Timber.e("location GPS disable");
                }
            }
        });
    }

    private void fetchLocation() {
        locationLatLng = new LocationLatLng(getActivityBase(), OpenLocationFragment.this);

//        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }



        locationLatLng.getLocation(new LocationChangeListener() {
            @Override
            public void select(String address, LatLng latLng) {
                if (latLng != null) {
                    Timber.e("start here fetch done");
                    UserHelper.saveKey(Constants.LAT, "" + latLng.latitude);
                    UserHelper.saveKey(Constants.LNG, "" + latLng.longitude);
                    Timber.e("lat:" + latLng.latitude);
                    Timber.e("lng:" + latLng.longitude);
                    UserHelper.saveKey(Constants.USER_ADDRESS, address);
                    if(getActivityBase() != null) {
                        getActivityBase().finishAffinity();
                    }
                    startActivity(new Intent(context, MainActivity.class));
                }
            }

            @Override
            public void error() {
                Intent intent = new Intent(context, MapAddressActivity.class);
                intent.putExtra(Constants.CURRENT_LOCATION,true);
                startActivityForResult(intent, Constants.ADDRESS_RESULT);
            }
        });
    }

    private static final String TAG = "OpenLocationFragment";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Log.d(TAG, String.valueOf(requestCode));
        Log.d(TAG, String.valueOf(resultCode));
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            UserHelper.saveKey(Constants.LAT, "" + data.getDoubleExtra("lat", 0));
            UserHelper.saveKey(Constants.LNG, "" + data.getDoubleExtra("lng", 0));
            String address = locationLatLng.getAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
            UserHelper.saveKey(Constants.USER_ADDRESS, address);
            ((ParentActivity) context).finishAffinity();
            startActivity(new Intent(context, MainActivity.class));
        }else if(resultCode == Activity.RESULT_FIRST_USER && requestCode == Constants.LOCATION_REQUEST){
            if(mapPermissionFragment.validLocationPermission())
                fetchLocation();
            else
                mapPermissionFragment.makeRequestPermission(Constants.LOCATION_REQUEST);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragmentOpenLocationBinding = null;
        locationLatLng = null;
        openLocationViewModel.reset();
    }
}
