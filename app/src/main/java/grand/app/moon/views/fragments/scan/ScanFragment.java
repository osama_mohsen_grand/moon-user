package grand.app.moon.views.fragments.scan;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moon.R;
import grand.app.moon.databinding.FragmentScanBinding;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.moon.MoonHelper;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.app.ZoomViewModel;
import grand.app.moon.views.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScanFragment extends Fragment {

    private CodeScanner mCodeScanner;

    FragmentScanBinding binding;
    public String image = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding =   DataBindingUtil.inflate(inflater,R.layout.fragment_scan, container, false);
        mCodeScanner = new CodeScanner(requireActivity(), binding.scannerView);

        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                requireActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String res = result.getText();
                            String[] ar = res.split(",");
                            Service service = new Service();
                            int id = Integer.parseInt(ar[0]);
                            service.mType = Integer.parseInt(ar[1]);
                            service.mFlag = Integer.parseInt(ar[2]);
                            MoonHelper.shopDetails(requireContext(),service.mType,service.mFlag,id,null);
                        }catch (Exception exception){
                            exception.printStackTrace();
                            Toast.makeText(requireContext(), ""+ ResourceManager.getString(R.string.error_null_cursor), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        binding.scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    public void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }
}
