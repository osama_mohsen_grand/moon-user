package grand.app.moon.views.fragments.rate;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.base.BaseFragment;
import grand.app.moon.customviews.dialog.rate.DialogRateModel;
import grand.app.moon.databinding.FragmentRateBinding;
import grand.app.moon.databinding.FragmentRateNotificationBinding;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.viewmodels.rate.RateNotificationViewModel;
import grand.app.moon.viewmodels.rate.RateViewModel;
import grand.app.moon.views.activities.MainActivity;

public class RateNotificationFragment extends BaseFragment {
    View rootView;
    private FragmentRateNotificationBinding fragmentRateNotificationBinding;
    private RateNotificationViewModel rateNotificationViewModel;
    DialogRateModel dialogRateModel;
    String name="",image="",tripId="";
    public String total = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentRateNotificationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_rate_notification, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.RATE_NOTIFICATION))
            dialogRateModel = (DialogRateModel) getArguments().getSerializable(Constants.RATE_NOTIFICATION);
    }

    private void bind() {
        rateNotificationViewModel = new RateNotificationViewModel(dialogRateModel);
        fragmentRateNotificationBinding.setRateNotificationViewModel(rateNotificationViewModel);
        rootView = fragmentRateNotificationBinding.getRoot();
    }

    private void setEvent() {
        rateNotificationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, rateNotificationViewModel.getCommonRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    ((AppCompatActivity)context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }else if(action.equals(Constants.CANCELED)){
                    getActivityBase().finish();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(rateNotificationViewModel != null) rateNotificationViewModel.reset();
    }

}
