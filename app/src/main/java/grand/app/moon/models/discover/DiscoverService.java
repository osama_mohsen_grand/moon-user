package grand.app.moon.models.discover;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.base.IdName;

public class DiscoverService {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("flag")
    @Expose
    public Integer flag;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("categories")
    @Expose
    public List<IdName> categories = new ArrayList<>();
}
