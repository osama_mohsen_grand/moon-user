package grand.app.moon.models.taxi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.IdNamePrice;

public class TruckModel {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("start_counter")
    @Expose
    public int start_counter;
    @SerializedName("drivers")
    @Expose
    public List<IdNamePrice> drivers = null;
}
