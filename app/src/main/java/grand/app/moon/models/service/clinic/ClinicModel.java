package grand.app.moon.models.service.clinic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClinicModel {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("isFollow")
    @Expose
    public Boolean isFollow;
    @SerializedName("rate")
    @Expose
    public float rate;

}
