package grand.app.moon.models.user.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;

public class FacebookLoginRequest {
    @SerializedName("facebook_id")
    @Expose
    private String facebook_id = "";
    @SerializedName("type")
    @Expose
    private String type = Constants.DEFAULT_USER;

    public FacebookLoginRequest(String facebook_id) {
        this.facebook_id = facebook_id;
    }
}
