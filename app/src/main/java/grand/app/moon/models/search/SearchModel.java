package grand.app.moon.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchModel{
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("avg")
    @Expose
    public String avg;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("rate")
    @Expose
    public float rate = -1;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("work_days")
    @Expose
    public Integer workDays;

    @SerializedName("type")
    @Expose
    public Integer type;

    @SerializedName("flag")
    @Expose
    public Integer flag;

    @SerializedName("seller")
    @Expose
    public Seller seller;

    public class Seller{
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("shop_image")
        @Expose
        public String shopImage;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("description")
        @Expose
        public String description;
    }
}