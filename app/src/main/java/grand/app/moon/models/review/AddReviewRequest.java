package grand.app.moon.models.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.storage.user.UserHelper;


public class AddReviewRequest {
    @SerializedName("shop_id")
    @Expose
    private int shop_id;

    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("to_type")
    @Expose
    private String to_type;

    @SerializedName("account_type")
    @Expose
    private String account_type = UserHelper.getUserDetails().type;

    @SerializedName("rate")
    @Expose
    private float rate = 0;

    @SerializedName("flag")
    @Expose
    private String flag;

    @SerializedName("comment")
    @Expose
    private String comment = "";


    public ObservableField commentError = new ObservableField();

    public AddReviewRequest(int shop_id, int to_type, float rate) {
        this.shop_id = shop_id;
        this.to_type = String.valueOf(to_type);
        this.rate = rate;
    }


    public AddReviewRequest(){ }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public String getFlag() {
        return flag;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
