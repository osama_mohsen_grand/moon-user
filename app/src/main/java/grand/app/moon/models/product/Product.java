package grand.app.moon.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.base.IdNamePrice;

public class Product implements Serializable {
    @SerializedName("product_id")
    @Expose
    public Integer productId;

    @SerializedName("share")
    @Expose
    public String share;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("desc_name")
    @Expose
    public String descName;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("price_after")
    @Expose
    public String priceAfter;
    @SerializedName("product_image")
    @Expose
    public String productImage;

    @SerializedName("images")
    @Expose
    public ArrayList<IdNameImage> images;

    @SerializedName("case")
    @Expose
    public Integer _case;

    @SerializedName("min_qty")
    @Expose
    public String min_qty = "1";

    @SerializedName("isFavourite")
    @Expose
    public boolean isFavourite = false;

    @SerializedName("colors")
    @Expose
    public List<Color> colors = null;
    @SerializedName("sizes")
    @Expose
    public List<IdNamePrice> sizes = null;


    @SerializedName("additions")
    @Expose
    public List<Addition> additions = null;
}
