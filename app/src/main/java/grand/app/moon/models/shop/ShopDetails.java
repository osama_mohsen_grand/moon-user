package grand.app.moon.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.models.home.response.Story;

public class ShopDetails implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("nick_name")
    @Expose
    public String nickname;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("background_image")
    @Expose
    public String backgroundImage;

    @SerializedName("is_story")
    @Expose
    public int isStory;
    @SerializedName("stories")
    @Expose
    public Story story;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("avg")
    @Expose
    public String avg;
    @SerializedName("share")
    @Expose
    public String share;
    @SerializedName("mini_charge")
    @Expose
    public String miniCharge;
    @SerializedName("is_famous")
    @Expose
    public String isFamous = "0";
    @SerializedName("delivery")
    @Expose
    public String delivery;
    @SerializedName("followers_count")
    @Expose
    public String followersCount;
    @SerializedName("comment_count")
    @Expose
    public Integer commentCount;
    @SerializedName("rate")
    @Expose
    public float rate;
    @SerializedName("tags")
    @Expose
    public String tags;

    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("isFollow")
    @Expose
    public boolean isFollow;

    @SerializedName("lat")
    @Expose
    public Double lat;

    @SerializedName("lng")
    @Expose
    public Double lng;

    @SerializedName("work_days")
    @Expose
    public int work_days;

    @SerializedName("offer")
    @Expose
    public int offer;

    @SerializedName("story_view")
    @Expose
    public int storyView;


    @SerializedName("voucher")
    @Expose
    public int voucher;

}