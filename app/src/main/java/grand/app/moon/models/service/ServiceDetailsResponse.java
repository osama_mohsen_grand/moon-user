
package grand.app.moon.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.models.base.StatusMsg;

public class ServiceDetailsResponse extends StatusMsg implements Serializable {

    @SerializedName("data")
    @Expose
    public DataDetails mData;
}
