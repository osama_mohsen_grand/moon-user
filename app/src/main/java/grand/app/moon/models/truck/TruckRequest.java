package grand.app.moon.models.truck;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import androidx.databinding.ObservableField;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;

public class TruckRequest implements Serializable {
    @SerializedName("type")
    @Expose
    public String type = Constants.URGENT; // type urgent or scheduled

    @SerializedName("trip_type")
    @Expose
    public String trip_type; // if cycle tax send type 7 else if trucks send 12

    @SerializedName("lat")
    @Expose
    public ArrayList<Double> lat = new ArrayList<>();

    @SerializedName("lng")
    @Expose
    public ArrayList<Double> lng = new ArrayList<>();

    @SerializedName("address")
    @Expose
    public ArrayList<String> address = new ArrayList<>();

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("time")
    @Expose
    public String time;

    @SerializedName("distance")
    @Expose
    public String distance = "";

    @SerializedName("trip_time")
    @Expose
    public String trip_time = "";

    @SerializedName("name")
    @Expose
    public String name = "";

    @SerializedName("phone")
    @Expose
    public String phone = "";

    @SerializedName("goods_type")
    @Expose
    public String goods_type = "";

    @SerializedName("goods_weight")
    @Expose
    public String goods_weight = "";

    @SerializedName("helper")
    @Expose
    public int helper = 0; // if not need helper send 0 else 1

    @SerializedName("driver_id")
    @Expose
    public String driver_id; // send if truck

    @SerializedName("total_price")
    @Expose
    public String total_price = ""; // total price


    public ObservableField<String> nameError;
    public ObservableField<String> phoneError;
    public ObservableField<String> weightError;


    public TruckRequest() {
        nameError = new ObservableField<>();
        phoneError = new ObservableField<>();
        weightError = new ObservableField<>();
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name)) {
            if (nameError == null) nameError = new ObservableField<>();
            nameError.set(Validate.error);
            valid = false;
        } else
            nameError.set(null);
        if (!Validate.isValid(phone, Constants.PHONE)) {
            if (phoneError == null) phoneError = new ObservableField<>();
            phoneError.set(Validate.error);
            valid = false;
        } else
            phoneError.set(null);
        if (!Validate.isValid(goods_weight)) {
            if (weightError == null) weightError = new ObservableField<>();
            weightError.set(Validate.error);
            valid = false;
        } else
            weightError.set(null);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getGoods_weight() {
        return goods_weight;
    }

    public void setGoods_weight(String goods_weight) {
        this.goods_weight = goods_weight;
        weightError.set(null);
    }
}
