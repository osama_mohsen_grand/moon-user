package grand.app.moon.models.clinic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.models.base.StatusMsg;

public class ClinicDetailsResponse  extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public Data data;
}
