package grand.app.moon.models.user.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

public class RegisterUserRequest {
    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";

    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("password")
    @Expose
    private String password = "";

    private String country = "";

    @SerializedName("address")
    @Expose
    private String address = "";


    @SerializedName("country_id")
    @Expose
    private String country_id = "";

    private String city = "";

    @SerializedName("city_id")
    @Expose
    private String city_id = "";


    private String region = "";

    @SerializedName("region_id")
    @Expose
    private String region_id = "";

    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("lng")
    @Expose
    private double lng = 0;

    public String facebook_id = "";
    public String facebook_image = "";

    @SerializedName("type")
    @Expose
    private String type = Constants.DEFAULT_USER;


    public ObservableField nameError;
    public ObservableField phoneError;
    public ObservableField emailError;
    public ObservableField passwordError;
    public ObservableField countryError;
    public ObservableField cityError;
    public ObservableField regionError;
    public ObservableField addressError;

    public RegisterUserRequest() {
        country_id = UserHelper.retrieveKey(Constants.COUNTRY_ID);
        nameError = new ObservableField();
        phoneError = new ObservableField();
        emailError = new ObservableField();
        passwordError = new ObservableField();
        addressError = new ObservableField();
        countryError = new ObservableField();
        cityError = new ObservableField();
        regionError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;
        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(phone)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("PHONE:error");
        }
        if(!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
            Timber.e("EMAIL:error");
        }
        if(!Validate.isValid(password)) {
            passwordError.set(Validate.error);
            valid = false;
            Timber.e("password:error");
        }
        if(!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        if(!Validate.isValid(city)) {
            cityError.set(Validate.error);
            valid = false;
            Timber.e("city:error");
        }
        if(!Validate.isValid(region) ) {
            regionError.set(Validate.error);
            valid = false;
            Timber.e("region:error");
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        passwordError.set(null);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
        countryError.set(null);
    }

    public String getCountry_id() {
        return country_id;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        cityError.set(null);
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
        cityError.set(null);
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
        regionError.set(null);
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
        regionError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }



    private RegisterUserRequest(String name, String phone, String email, String password) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.password = password;
    }
}
