package grand.app.moon.models.famous.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;

public class FollowFamousRequest {
    @SerializedName("to")
    @Expose
    public int to;
    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("account_type")
    @Expose
    public String account_type;

    public FollowFamousRequest(int to) {
        this.to = to;
        this.type = 3;
        this.account_type = Constants.DEFAULT_USER;
    }
}
