package grand.app.moon.models.famous.advertise;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("service_name")
    @Expose
    public String serviceName  = "";
    @SerializedName("service_image")
    @Expose
    public String serviceImage  = "";
    @SerializedName("price")
    @Expose
    public String price  = "";
    @SerializedName("description")
    @Expose
    public String description = "";
    @SerializedName("shop")
    @Expose
    public String shop  = "";
}
