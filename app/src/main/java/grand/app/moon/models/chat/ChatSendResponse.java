package grand.app.moon.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

public class ChatSendResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public ChatDetailsModel data;
}
