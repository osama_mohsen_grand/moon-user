package grand.app.moon.models.famous.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.StatusMsg;

public class FamousAlbumImagesResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ArrayList<ImageVideo> data;
}
