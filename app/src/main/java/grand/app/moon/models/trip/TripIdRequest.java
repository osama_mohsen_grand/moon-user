package grand.app.moon.models.trip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripIdRequest {
    @SerializedName("trip_id")
    @Expose
    public String trip_id;

    public TripIdRequest(int trip_id) {
        this.trip_id = String.valueOf(trip_id);
    }
}
