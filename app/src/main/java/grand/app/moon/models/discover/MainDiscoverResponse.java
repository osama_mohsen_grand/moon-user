package grand.app.moon.models.discover;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.country.Datum;

public class MainDiscoverResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<DiscoverService> data = new ArrayList<>();

    @SerializedName("discover")
    @Expose
    public List<ImageVideo> discover = new ArrayList<>();

}
