package grand.app.moon.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

public class AddAdsRequest {

    @SerializedName("id")
    @Expose
    private int id = -1;

    @SerializedName("type")
    @Expose
    private String type = Constants.TYPE_ADVERTISING;

    @SerializedName("phone")
    @Expose
    private String phone = "";

    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("name")
    @Expose
    private String name = "";

    private transient String country = "";

    private transient String city = "";


    @SerializedName("address")
    @Expose
    private String address = "";


    @SerializedName("country_id")
    @Expose
    private String country_id = "";

    @SerializedName("city_id")
    @Expose
    private String city_id = "";

    @SerializedName("price")
    @Expose
    private String price = "";

    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("lng")
    @Expose
    private double lng = 0;


    @SerializedName("description")
    @Expose
    private String description = "";

    @SerializedName("service_id")
    @Expose
    private int service_id;

    @SerializedName("category_id")
    @Expose
    private int category_id;

    @SerializedName("subCategory_id")
    @Expose
    private int subCategory_id;



    public transient ObservableField nameError;
    public transient ObservableField phoneError;
    public transient ObservableField emailError;
    public transient ObservableField priceError;
    public transient ObservableField cityError;
    public transient ObservableField addressError;
    public transient ObservableField descriptionError;


    public void init(){
        country_id = UserHelper.retrieveKey(Constants.COUNTRY_ID);
        nameError = new ObservableField();
        phoneError = new ObservableField();
        emailError = new ObservableField();
        priceError = new ObservableField();
        addressError = new ObservableField();
        cityError = new ObservableField();
        descriptionError = new ObservableField();
    }

    public AddAdsRequest(int service_id,int category_id,int subCategory_id) {
        this.service_id = service_id;
        this.category_id = category_id;
        this.subCategory_id = subCategory_id;
        init();
    }

    public AddAdsRequest(int id) {
        this.id = id;
        init();
    }


    public boolean isValid() {
        boolean valid = true;
        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(phone, Constants.PHONE)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("PHONE:error");
        }
        if(!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
            Timber.e("EMAIL:error");
        }
        if(!Validate.isValid(price,Constants.NUMBER)) {
            priceError.set(Validate.error);
            valid = false;
            Timber.e("price:error");
        }
        if(!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        if(!Validate.isValid(city)) {
            cityError.set(Validate.error);
            valid = false;
            Timber.e("city:error");
        }
        if(!Validate.isValid(description) ) {
            descriptionError.set(Validate.error);
            valid = false;
            Timber.e("region:error");
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

    public String getCountry_id() {
        return country_id;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        cityError.set(null);
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
        cityError.set(null);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        descriptionError.set(null);
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
        priceError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }



}
