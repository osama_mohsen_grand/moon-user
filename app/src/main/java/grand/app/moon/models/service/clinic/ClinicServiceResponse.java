
package grand.app.moon.models.service.clinic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.service.DataDetails;

public class ClinicServiceResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public ClinicDataDetails mData;
}
