package grand.app.moon.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class PrivacyTextResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("translation")
        @Expose
        public Translation translation;

    }

    public class Translation {

        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("privacy_policy_id")
        @Expose
        public Integer privacyPolicyId;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("locale")
        @Expose
        public String locale;

    }
}
