package grand.app.moon.models.user.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

public class ForgetPasswordRequest {

    @SerializedName("type")
    @Expose
    public String type = Constants.DEFAULT_USER;


    @SerializedName("phone")
    @Expose
    public String phone = "";

    @SerializedName("cpp")
    @Expose
    public String cpp = "";

    @SerializedName("check_phone")
    @Expose
    public String checkPhone = "2"; //1 => forget password , 2 => checkphone

    public transient String kind = Constants.REGISTRATION;
    public ForgetPasswordRequest(String cpp, String phone) {
        this.cpp = cpp;
        this.phone = phone;//country code + phone
    }

    public void setRequestKind(String kind) {
        this.kind = kind;
        if(!this.kind.equals(Constants.REGISTRATION)){
            checkPhone = "1";
        }
    }
}
