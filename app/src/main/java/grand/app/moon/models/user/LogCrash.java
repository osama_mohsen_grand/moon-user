package grand.app.moon.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogCrash {
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("message")
    @Expose
    public String message;

    public LogCrash(String message) {
        this.date = "28/03/2021";
        this.message = message;
    }
}
