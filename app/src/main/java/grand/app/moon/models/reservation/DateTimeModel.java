package grand.app.moon.models.reservation;

public class DateTimeModel {
    public String time;
    public int day;
    public int period;

    public DateTimeModel(String time, int day, int period) {
        this.time = time;
        this.day = day;
        this.period = period;
    }
}
