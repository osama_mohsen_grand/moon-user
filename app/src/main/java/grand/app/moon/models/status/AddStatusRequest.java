package grand.app.moon.models.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.storage.user.UserHelper;

public class AddStatusRequest {
    @SerializedName("type")
    @Expose
    public String type = "";

    public AddStatusRequest() {
        this.type = UserHelper.getUserDetails().type;
    }
}
