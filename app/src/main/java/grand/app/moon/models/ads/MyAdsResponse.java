package grand.app.moon.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class MyAdsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<MyAdsModel> data;
}
