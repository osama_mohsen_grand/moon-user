package grand.app.moon.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class SettingsResponse  extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ArrayList<Settings> data;
}
