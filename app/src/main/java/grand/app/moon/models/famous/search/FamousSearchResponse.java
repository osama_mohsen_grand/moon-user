package grand.app.moon.models.famous.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.famous.list.Famous;

public class FamousSearchResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<Famous> data;
}
