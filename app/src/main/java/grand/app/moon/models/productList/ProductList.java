package grand.app.moon.models.productList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductList implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("price_after")
    @Expose
    public String priceAfter;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
}
