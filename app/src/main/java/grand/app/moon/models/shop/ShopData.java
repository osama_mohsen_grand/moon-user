package grand.app.moon.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdNameImage;

public class ShopData implements Serializable {
    @SerializedName("shop_details")
    @Expose
    public ShopDetails shop_details;

    @SerializedName("categories")
    @Expose
    public List<IdNameImage> categories;

    @SerializedName("gallery")
    @Expose
    public List<ImageVideo> gallery;

    @SerializedName("photographer")
    @Expose
    public List<ImageVideo> photographer;

    @SerializedName("famous")
    @Expose
    public List<ImageVideo> famous;

    @SerializedName("branches")
    @Expose
    public List<Branch> branches;

}


