package grand.app.moon.models.trip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

public class EndTripResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public String data;
}
