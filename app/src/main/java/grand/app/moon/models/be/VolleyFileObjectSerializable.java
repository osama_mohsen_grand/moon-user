package grand.app.moon.models.be;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import grand.app.moon.vollyutils.VolleyFileObject;

public class VolleyFileObjectSerializable implements Serializable {
    public Map<String, VolleyFileObject> volleyFileObjectMap = new HashMap<>();
}
