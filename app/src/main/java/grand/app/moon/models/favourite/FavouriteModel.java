package grand.app.moon.models.favourite;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavouriteModel {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("seller_name")
    @Expose
    public String sellerName;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("flag")
    @Expose
    public Integer flag;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
}
