package grand.app.moon.models.trip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;

public class TripActionRequest {
    @SerializedName("trip_id")
    @Expose
    public int trip_id;
    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("comment")
    @Expose
    public String comment = "";



    public TripActionRequest(int trip_id) {
        this.trip_id = trip_id;
    }
}