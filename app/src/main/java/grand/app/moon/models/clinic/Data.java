package grand.app.moon.models.clinic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNameDescription;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.shipping.Shipping;

public class Data  implements Serializable {

    @SerializedName("details")
    @Expose
    public ClinicDetails details;
    @SerializedName("specialist")
    @Expose
    public List<IdNameDescription> specialist = null;
    @SerializedName("gallery")
    @Expose
    public List<ImageVideo> gallery = null;
    @SerializedName("Adds")
    @Expose
    public List<ImageVideo> adds = null;
    @SerializedName("doctors_list")
    @Expose
    public List<DoctorsList> doctorsList = null;
    @SerializedName("services")
    @Expose
    public List<IdNamePrice> services = null;

    @SerializedName("categories")
    @Expose
    public List<IdNameImage> categories;

    @SerializedName("payment_list")
    @Expose
    public List<IdNameImage> paymentList;

}
