package grand.app.moon.models.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class ReservationResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<ReservationOrder> reservationOrders;

}
