package grand.app.moon.models.trip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.base.StatusMsg;


public class TripDetailsResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public TripDetailsModel data;

}
