package grand.app.moon.models.user.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.storage.user.UserHelper;

public class ForgetPasswordResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public String data;

}
