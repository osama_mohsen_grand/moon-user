package grand.app.moon.models.adsDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.base.IdNameImage;

public class AdsDetailsModel {


    public AdsDetailsModel() {
    }

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("lat")
    @Expose
    public double lat;
    @SerializedName("lng")
    @Expose
    public double lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("price_after")
    @Expose
    public String priceAfter;
    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("city_id")
    @Expose
    public String city_id;
    @SerializedName("product_images")
    @Expose
    public ArrayList<IdNameImage> productImages = new ArrayList<>();

    @SerializedName("isFavourite")
    @Expose
    public boolean isFavourite = false;
}
