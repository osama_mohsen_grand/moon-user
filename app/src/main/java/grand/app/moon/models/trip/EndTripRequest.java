package grand.app.moon.models.trip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.SharedPreferenceHelper;

public class EndTripRequest {
    @SerializedName("trip_id")
    @Expose
    public int trip_id;
    @SerializedName("country_id")
    @Expose
    public int country_id;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("lat")
    @Expose
    public double lat;
    @SerializedName("lng")
    @Expose
    public double lng;

    public EndTripRequest(int trip_id, String address) {
        this.country_id = Constants.DEFAULT_COUNTRY_ID;
        this.trip_id = trip_id;
        this.address = address;
        String latter =(SharedPreferenceHelper.getKey(Constants.LAT).equals("") ? "0" : SharedPreferenceHelper.getKey(Constants.LAT));
        String lngger =(SharedPreferenceHelper.getKey(Constants.LNG).equals("") ? "0" : SharedPreferenceHelper.getKey(Constants.LNG));
        this.lat = Double.parseDouble(latter);
        this.lng = Double.parseDouble(lngger);
    }
}
