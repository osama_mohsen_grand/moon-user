package grand.app.moon.models.clinic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DoctorsList implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("yearsExperience")
    @Expose
    public Integer yearsExperience;
    @SerializedName("doctor_specialties")
    @Expose
    public String doctorSpecialties;
    @SerializedName("rate")
    @Expose
    public float rate;

}
