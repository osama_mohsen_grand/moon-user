package grand.app.moon.models.app;

import android.view.View;

public class Mutable {
    public String type;
    public int position;
    public Object object;
    public View v = null;

    public Mutable(String type) {
        this.type = type;
    }

    public Mutable(String type , Object object) {
        this.type = type;
        this.object = object;
    }


    public Mutable(String type, int position) {
        this.type = type;
        this.position = position;
    }


    public Mutable(String type, int position, View v) {
        this.type = type;
        this.position = position;
        this.v = v;
    }
}
