package grand.app.moon.models.famous.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AlbumModel implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("shops_count")
    @Expose
    public Integer shopsCount;
    @SerializedName("categories")
    @Expose
    public List<Category> categories = null;
}
