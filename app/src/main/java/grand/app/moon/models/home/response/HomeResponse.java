
package grand.app.moon.models.home.response;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

@SuppressWarnings("unused")
public class HomeResponse extends StatusMsg implements Serializable {

    @SerializedName("data")
    @Expose
    public List<Service> mData;

    @SerializedName("sliders")
    @Expose
    public List<Slider> mSliders;

    @SerializedName("stories")
    @Expose
    public List<Story> stories;

}
