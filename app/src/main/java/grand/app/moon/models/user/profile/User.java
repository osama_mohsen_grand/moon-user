package grand.app.moon.models.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.Constants;

public class User {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("account_type")
    @Expose
    public Integer accountType;
    @SerializedName("package_id")
    @Expose
    public Integer packageId;
    @SerializedName("followers")
    @Expose
    public Integer followers;
    @SerializedName("is_shop")
    @Expose
    public Boolean isShop;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("jwt_token")
    @Expose
    public String jwtToken;

    @SerializedName("city_id")
    @Expose
    public int city_id;
    @SerializedName("region_id")
    @Expose
    public int region_id;
    @SerializedName("start_counter")
    @Expose
    public int start_counter;


    @SerializedName("country_id")
    @Expose
    public int country_id;

    @SerializedName("chats_count")
    @Expose
    public int chats_count;


    @SerializedName("city_name")
    @Expose
    public String city_name = "";

    @SerializedName("region_name")
    @Expose
    public String region_name = "";

    @SerializedName("country_name")
    @Expose
    public String country_name;

    @SerializedName("taxi_cost")
    @Expose
    public int taxi_price_per_kilo;

    @SerializedName("social_id")
    @Expose
    public String socialId = "";

    public String type = Constants.DEFAULT_USER;
}
