package grand.app.moon.models.favourite;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FavouriteRequest {
    @SerializedName("product_id")
    @Expose
    public int product_id;

    @SerializedName("type")
    @Expose
    public int type;

    public FavouriteRequest(int product_id, int type) {
        this.product_id = product_id;
        this.type = type;
    }
}
