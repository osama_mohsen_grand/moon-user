package grand.app.moon.models.service.tags;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.base.StatusMsg;

public class TagsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<IdNameImage> data;
}
