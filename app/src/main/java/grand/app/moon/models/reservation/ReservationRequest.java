package grand.app.moon.models.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReservationRequest {
    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("doctor_id")
    @Expose
    public int doctor_id;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("service")
    @Expose
    public String service;

    @SerializedName("day_id")
    @Expose
    public int day_id;

    @SerializedName("period")
    @Expose
    public int period;


    @SerializedName("payment_method")
    @Expose
    public int payment_method = 2;

    public ReservationRequest(String type, int doctor_id) {
        this.type = type;
        this.doctor_id = doctor_id;
    }
}
