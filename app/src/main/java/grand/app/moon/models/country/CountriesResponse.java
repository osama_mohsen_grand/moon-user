package grand.app.moon.models.country;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class CountriesResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
}
