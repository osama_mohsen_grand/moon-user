
package grand.app.moon.models.home.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Service implements Serializable {

    @SerializedName("flag")
    @Expose
    public int mFlag;
    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("image")
    @Expose
    public String mImage;
    @SerializedName("name")
    @Expose
    public String mName;
    @SerializedName("type")
    @Expose
    public int mType;


}
