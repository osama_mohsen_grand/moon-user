
package grand.app.moon.models.home.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.base.IdNameImage;

public class ViewStoryRequest implements Serializable {

    @SerializedName("story_ids")
    @Expose
    public ArrayList<String> storiesId;

    public ViewStoryRequest(ArrayList<String> storiesId) {
        this.storiesId = storiesId;
    }
}
