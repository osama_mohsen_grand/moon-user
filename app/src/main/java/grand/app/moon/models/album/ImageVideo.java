
package grand.app.moon.models.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageVideo implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("capture")
    @Expose
    public String capture = "";
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("shop_name")
    @Expose
    public String shopName;

    @SerializedName("video_count")
    @Expose
    public int videoCount;



    @SerializedName("famous_id")
    @Expose
    public int famous_id;
    @SerializedName("famous_name")
    @Expose
    public String famous_name;
    @SerializedName("shop_id")
    @Expose
    public int shopId;
    @SerializedName("account_type")
    @Expose
    public int accountType;

}
