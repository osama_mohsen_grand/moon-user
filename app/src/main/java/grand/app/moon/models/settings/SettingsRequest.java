package grand.app.moon.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.SharedPreferenceHelper;
import grand.app.moon.utils.storage.user.UserHelper;

public class SettingsRequest {
    @SerializedName("type")
    @Expose
    public String type = Constants.DEFAULT_USER;

    @SerializedName("country_id")
    @Expose
    public String countryId = UserHelper.retrieveKey(Constants.COUNTRY_ID);


    // 1: privacy, 2: terms , 3: faq
    @SerializedName("setting_type")
    @Expose
    public int settingType;

    public SettingsRequest(int setting_type) {
        this.settingType = setting_type;
    }
}
