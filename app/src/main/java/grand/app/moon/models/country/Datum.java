package grand.app.moon.models.country;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.IdName;

public class Datum {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("country_name")
    @Expose
    public String countryName;

    @SerializedName("country_image")
    @Expose
    public String countryImage;
    @SerializedName("currency")
    @Expose
    public String currency;
    @SerializedName("code")
    @Expose
    public String code = "";
    @SerializedName("cities")
    @Expose
    public List<City> cities = null;

    @SerializedName("trucks")
    @Expose
    public List<IdName> trucks = null;
}
