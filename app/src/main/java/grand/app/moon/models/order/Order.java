package grand.app.moon.models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.R;
import grand.app.moon.utils.resources.ResourceManager;

public class Order {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("order_number")
    @Expose
    public String orderNumber;
    @SerializedName("order_date")
    @Expose
    public String orderDate;
    @SerializedName("order_status")
    @Expose
    public String order_status;

    @SerializedName("status_id")
    @Expose
    public int status_id;
}
