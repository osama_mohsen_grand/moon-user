package grand.app.moon.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNamePrice;

public class Color implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("sizes")
    @Expose
    public List<IdName> sizes;
}
