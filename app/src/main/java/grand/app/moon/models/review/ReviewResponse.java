
package grand.app.moon.models.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class ReviewResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<Review> mData;

}
