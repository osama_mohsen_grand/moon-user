
package grand.app.moon.models.status;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class StatusResponse extends StatusMsg {

    @SerializedName("data")
    private List<Status> mData;


    public List<Status> getData() {
        return mData;
    }

    public void setData(List<Status> data) {
        mData = data;
    }

}
