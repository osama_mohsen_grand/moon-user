package grand.app.moon.models.adsDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.models.base.StatusMsg;

public class AdsDetailsResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public AdsDetailsModel data = new AdsDetailsModel();
}
