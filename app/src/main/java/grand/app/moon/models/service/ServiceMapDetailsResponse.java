
package grand.app.moon.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

public class ServiceMapDetailsResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public DataMapDetails mData;
}
