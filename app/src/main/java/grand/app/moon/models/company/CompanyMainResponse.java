package grand.app.moon.models.company;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.models.ads.AdsDetails;
import grand.app.moon.models.base.StatusMsg;

public class CompanyMainResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public CompanyDetails mData;
}
