package grand.app.moon.models.famous.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FamousAddImageInsideAlbumRequest {
    @SerializedName("album_id")
    @Expose
    public int album_id;

    @SerializedName("type")
    @Expose
    public String type = "";

    @SerializedName("tab")
    @Expose
    public String tab = "";

    public FamousAddImageInsideAlbumRequest(int album_id, String type, String tab) {
        this.album_id = album_id;
        this.type = type;
        this.tab = tab;
    }
}
