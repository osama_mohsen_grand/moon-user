
package grand.app.moon.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.home.response.Story;

@SuppressWarnings("unused")
public class ShopDetails implements Serializable {

    @SerializedName("avg")
    @Expose
    public String mAvg;
    @SerializedName("description")
    @Expose
    public String mDescription;
    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("image")
    @Expose
    public String mImage;
    @SerializedName("nick_name")
    @Expose
    public String nickname;
    @SerializedName("name")
    @Expose
    public String mName;

    @SerializedName("rate")
    @Expose
    public float mRate;
    @SerializedName("tags")
    @Expose
    public String mTags;
    @SerializedName("voucher")
    @Expose
    public int mVoucher;
    @SerializedName("work_days")
    @Expose
    public int mWorkDays = 1;
    @SerializedName("offer")
    @Expose
    public int mOffer;


    @SerializedName("lat")
    @Expose
    public double lat;

    @SerializedName("lng")
    @Expose
    public double lng;

    @SerializedName("stories")
    @Expose
    public Story stories;

}
