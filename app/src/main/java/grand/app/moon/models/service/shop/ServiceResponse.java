package grand.app.moon.models.service.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class ServiceResponse extends StatusMsg {
    @SerializedName("services")
    @Expose
    public List<Service> services = null;
}
