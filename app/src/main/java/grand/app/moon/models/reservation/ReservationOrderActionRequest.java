package grand.app.moon.models.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.storage.user.UserHelper;


public class ReservationOrderActionRequest {
    @SerializedName("order_id")
    @Expose
    public int order_id;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;
}
