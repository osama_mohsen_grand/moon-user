package grand.app.moon.models.chat.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatList {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("chats_count")
    @Expose
    public String chatsCount;


}
