package grand.app.moon.models.user.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;

public class LoginRequest {
    @SerializedName("country_id")
    @Expose
    public String countryId = null;
    @SerializedName("email")
    @Expose
    private String email = "";
    @SerializedName("password")
    @Expose
    private String password = "";
    @SerializedName("type")
    @Expose
    private String type = Constants.DEFAULT_USER;

    @SerializedName("social_image")
    @Expose
    public  String socialImage = "";
    @SerializedName("social_id")
    @Expose
    public  String socialId = "";
    @SerializedName("name")
    @Expose
    public  String socialName = "";
    @SerializedName("social_type")
    @Expose
    public  String socialType = "";



    public ObservableField typeError;
    public ObservableField emailError;
    public ObservableField passwordError;

    public LoginRequest() {
        typeError = new ObservableField();
        emailError = new ObservableField();
        passwordError = new ObservableField();
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        passwordError.set(null);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        typeError.set(null);
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(type)) {
            typeError.set(Validate.error);
            valid = false;
        } else
            typeError.set(null);
        if (!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
        } else
            emailError.set(null);
        if (!Validate.isValid(password)) {
            passwordError.set(Validate.error);
            valid = false;
        } else
            passwordError.set(null);
        return valid;
    }
}
