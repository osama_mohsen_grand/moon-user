package grand.app.moon.models.explore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.home.response.Story;

public class Explore {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("shop_id")
    @Expose
    public Integer shopId;
    @SerializedName("shop_name")
    @Expose
    public String shopName;
    @SerializedName("shop_image")
    @Expose
    public String shopImage;
    @SerializedName("is_story")
    @Expose
    public int isStory;
    @SerializedName("stories")
    @Expose
    public Story story;

    @SerializedName("is_follow")
    @Expose
    public String isFollow;
    @SerializedName("discover")
    @Expose
    public List<ImageVideo> discover = null;

    @SerializedName("shop_flag")
    @Expose
    public int flag;

    @SerializedName("work_days")
    @Expose
    public Integer workDays;

    @SerializedName("shop_type")
    @Expose
    public int type;

}
