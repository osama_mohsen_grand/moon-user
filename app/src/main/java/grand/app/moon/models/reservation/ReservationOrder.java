package grand.app.moon.models.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReservationOrder {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("cost")
    @Expose
    public String cost;
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("order_date")
    @Expose
    public String orderDate;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("order_status")
    @Expose
    public int order_status;
}
