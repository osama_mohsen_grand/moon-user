package grand.app.moon.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.StatusMsg;

public class AddShopResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<IdName> suggestions;
}
