package grand.app.moon.models.favourite;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moon.models.base.StatusMsg;

public class FavouriteResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ArrayList<FavouriteModel> data;
}
