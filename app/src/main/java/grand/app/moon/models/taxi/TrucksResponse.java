package grand.app.moon.models.taxi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class TrucksResponse extends StatusMsg{
    @SerializedName("data")
    @Expose
    public List<TruckModel> truckModels;
}
