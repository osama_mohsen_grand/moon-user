
package grand.app.moon.models.famous.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Famous {

    @SerializedName("id")
    @Expose
    private int mId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;

    public int getmId() {
        return mId;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
