package grand.app.moon.models.personalnfo;

import android.accounts.Account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.shop.Branch;
import grand.app.moon.viewmodels.profile.Transfer;

public class AccountInfoResponse extends StatusMsg implements Serializable {
    @SerializedName("name")
    @Expose
    public String name = "";
    @SerializedName("email")
    @Expose
    public String email = "";
    @SerializedName("nick_name")
    @Expose
    public String nickname = "";
    @SerializedName("image")
    @Expose
    public String image = "";
    @SerializedName("number")
    @Expose
    public String number = "";
    @SerializedName("followers")
    @Expose
    public String followers = "";
    @SerializedName("website")
    @Expose
    public String website = "";


    @SerializedName("description")
    @Expose
    public String description = "";

    @SerializedName("lat")
    @Expose
    public double lat = 0;
    @SerializedName("lng")
    @Expose
    public double lng = 0;

    @SerializedName("info_services")
    @Expose
    public List<IdNameImage> infoServices = null;
    @SerializedName("profile")
    @Expose
    public List<Profile> profile = new ArrayList<>();
    @SerializedName("transfer")
    @Expose
    public Transfer transfer = null;

    @SerializedName("photographer")
    @Expose
    public List<ImageVideo> photographer;

    @SerializedName("famous")
    @Expose
    public List<ImageVideo> famous;

    @SerializedName("branches")
    @Expose
    public List<Branch> branches;


    @SerializedName("gallery")
    @Expose
    public List<ImageVideo> gallery;


    public AccountInfoResponse getAccountInfo(){
        return this;
    }

}
