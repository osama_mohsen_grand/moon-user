
package grand.app.moon.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;

public class AdsDetails implements Serializable {

    @SerializedName("services")
    @Expose
    public List<Service> mServices;
    @SerializedName("advertisements")
    @Expose
    public List<AdsCompanyModel> ads;

}
