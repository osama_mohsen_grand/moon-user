package grand.app.moon.models.shop;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class ShopDepartment {
    public String name;
    public Drawable drawable;
    public String constant;

    public ShopDepartment(String name, Drawable drawable, String constant) {
        this.name = name;
        this.drawable = drawable;
        this.constant = constant;
    }
}
