
package grand.app.moon.models.famous.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccountDetails {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image = "";
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("nick_name")
    @Expose
    public String nickname = "";

}
