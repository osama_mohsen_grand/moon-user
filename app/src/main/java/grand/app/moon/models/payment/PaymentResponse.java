package grand.app.moon.models.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

public class PaymentResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {

        @SerializedName("driver_credit")
        @Expose
        public String driverCredit = "";
        @SerializedName("admin_credit")
        @Expose
        public String adminCredit = "";
        @SerializedName("type")
        @Expose
        public String type;

    }
}
