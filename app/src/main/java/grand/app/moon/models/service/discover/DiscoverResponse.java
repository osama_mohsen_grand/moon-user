package grand.app.moon.models.service.discover;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.StatusMsg;

public class DiscoverResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<ImageVideo> data;
}
