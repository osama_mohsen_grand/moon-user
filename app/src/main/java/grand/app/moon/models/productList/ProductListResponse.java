package grand.app.moon.models.productList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class ProductListResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<ProductList> products;
}
