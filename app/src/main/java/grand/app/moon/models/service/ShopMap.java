package grand.app.moon.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShopMap {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("lat")
    @Expose
    public double lat;
    @SerializedName("lng")
    @Expose
    public double lng;
    @SerializedName("address")
    @Expose
    public String address;
}
