package grand.app.moon.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNameImage;

public class CartData implements Serializable {

    @SerializedName("items")
    @Expose
    public List<Item> items = null;
    @SerializedName("sub_total")
    @Expose
    public double subTotal;
    @SerializedName("delivery")
    @Expose
    public double delivery;
    @SerializedName("total_price")
    @Expose
    public double totalPrice;
    @SerializedName("has_delegate")
    @Expose
    public boolean hasDelegate;

    @SerializedName("payment_type")
    @Expose
    public int payment_type;

    @SerializedName("payment_list")
    @Expose
    public List<IdNameImage> paymentList;


}
