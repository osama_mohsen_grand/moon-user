
package grand.app.moon.models.home.response;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.IdNameImage;

public class Story implements Serializable {

    @SerializedName("id")
    @Expose
    public int mId;

    @SerializedName("story_view")
    @Expose
    public int storyView;

    @SerializedName("story_view_count")
    @Expose
    public int storyViewCount = 0;


    @SerializedName("shop_type")
    @Expose
    public int type; // type == 17 (ADMIN) , >= 17 (no details)
    @SerializedName("shop_flag")
    @Expose
    public int flag;

    @SerializedName("work_days")
    @Expose
    public Integer workDays;

    @SerializedName("image")
    @Expose
    public String mImage;
    @SerializedName("lat")
    @Expose
    public Double mLat;
    @SerializedName("lng")
    @Expose
    public Double mLng;
    @SerializedName("name")
    @Expose
    public String mName;
    @SerializedName("nick_name")
    @Expose
    public String nickname;
    @SerializedName("stories")
    @Expose
    public List<IdNameImage> mStories;

}
