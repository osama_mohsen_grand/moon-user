package grand.app.moon.models.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.storage.user.UserHelper;

public class PaymentGetRequest {
    @SerializedName("driver_id")
    @Expose
    public int driver_id;

    public PaymentGetRequest() {
        this.driver_id = UserHelper.getUserId();
    }
}
