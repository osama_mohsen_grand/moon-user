package grand.app.moon.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.IdNamePrice;

public class Addition {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("adds")
    @Expose
    public List<IdNamePrice> adds;
}
