package grand.app.moon.models.famous.advertise;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

public class AdvertiseDetailsResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public Data data;
}
