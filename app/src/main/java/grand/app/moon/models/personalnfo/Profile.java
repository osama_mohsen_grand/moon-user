package grand.app.moon.models.personalnfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Profile  implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("media")
    @Expose
    public String media;
    @SerializedName("image")
    @Expose
    public String image;

}
