package grand.app.moon.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCartRequest {
    @SerializedName("product_id")
    @Expose
    public String product_id;

    @SerializedName("cart_item_id")
    @Expose
    public String cart_item_id;

    @SerializedName("qty")
    @Expose
    public String qty = "1";

    @SerializedName("size_id")
    @Expose
    public String size_id;

    @SerializedName("color_id")
    @Expose
    public String color_id;

    @SerializedName("description")
    @Expose
    public String description = "";

    @SerializedName("additions")
    @Expose
    public String additions = "";

    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("flag")
    @Expose
    public int flag;


}
