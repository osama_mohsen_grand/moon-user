package grand.app.moon.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.AppHelper;

public class UpdateTokenRequest {
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("firebase_token")
    @Expose
    public String token;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("device_id")
    @Expose
    public String deviceId;


    public UpdateTokenRequest(String token) {
        this.type = UserHelper.getUserDetails().type;
        this.token = token;
        this.lat = UserHelper.retrieveKey(Constants.LAT);
        this.lng = UserHelper.retrieveKey(Constants.LNG);
        this.address = UserHelper.retrieveKey(Constants.USER_ADDRESS);
        this.deviceId = AppHelper.getDeviceIdWithoutPermission();
    }
}
