package grand.app.moon.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("shop_id")
    @Expose
    public String shopId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("additions")
    @Expose
    public String additions;

    @SerializedName("price_addition")
    @Expose
    public String price_addition;
    @SerializedName("color")
    @Expose
    public String color;
    @SerializedName("size")
    @Expose
    public String size;
    @SerializedName("total_price")
    @Expose
    public String totalPrice;
    @SerializedName("size_name")
    @Expose
    public String sizeName;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("desc")
    @Expose
    public String desc;
    @SerializedName("qty")
    @Expose
    public String qty = "0";
    public String qty_tmp = "0";
    @SerializedName("special_request")
    @Expose
    public String specialRequest;
    @SerializedName("product_id")
    @Expose
    public String productId;

    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("flag")
    @Expose
    public int flag;

}
