
package grand.app.moon.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.home.response.Service;

public class DataMapDetails {

    @SerializedName("services")
    @Expose
    public List<Service> mServices;
    @SerializedName("shops")
    @Expose
    public List<ShopMap> mShops;

    @SerializedName("clinics")
    @Expose
    public List<ShopMap> clinics;

}
