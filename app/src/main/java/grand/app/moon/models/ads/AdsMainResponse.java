package grand.app.moon.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.service.DataDetails;

public class AdsMainResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public AdsDetails mData;
}
