package grand.app.moon.models.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import androidx.databinding.ObservableField;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class ProfileRequest {
    public transient VolleyFileObject volleyFileObject = null;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("name")
    @Expose
    private String name = "";


    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";


    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("lng")
    @Expose
    private double lng = 0;

    @SerializedName("address")
    @Expose
    private String address = "";


    @SerializedName("country_name")
    @Expose
    public String country_name = "";

    @SerializedName("city_name")
    @Expose
    public String city_name = "";


    @SerializedName("region_name")
    @Expose
    public String region_name = "";


    @SerializedName("country_id")
    @Expose
    public String country_id = "";

    @SerializedName("city_id")
    @Expose
    public String city_id = "";

    @SerializedName("region_id")
    @Expose
    public String region_id = "";


    public ObservableField nameError;
    public ObservableField emailError;
    public ObservableField phoneError;
    public ObservableField addressError;
    public ObservableField cityError;
    public ObservableField regionError;

    public ProfileRequest() {
        User user = UserHelper.getUserDetails();
        type = user.type;
        name = user.name;
        email = user.email;
        phone = user.phone;
        lat = Double.parseDouble(user.lat);
        lng = Double.parseDouble(user.lng);
        address = user.address;
        country_name = user.country_name;
        country_id = user.country_id+"";
        city_name = user.city_name;
        city_id = user.city_id+"";
        region_name = user.region_name;
        region_id = user.region_id+"";

        nameError = new ObservableField();
        phoneError = new ObservableField();
        emailError = new ObservableField();
        addressError = new ObservableField();
        cityError = new ObservableField();
        regionError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;

        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(phone)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("phone:error");
        }
        if(!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
            Timber.e("email:error");
        }

        if(!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }

        if(!Validate.isValid(city_name)) {
            cityError.set(Validate.error);
            valid = false;
            Timber.e("city:error");
        }

        if(!Validate.isValid(region_name)) {
            regionError.set(Validate.error);
            valid = false;
            Timber.e("region:error");
        }
        Timber.e("valid_input:"+valid);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
        cityError.set(null);
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
        regionError.set(null);
    }
}

