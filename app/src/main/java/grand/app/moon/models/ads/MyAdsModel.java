package grand.app.moon.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyAdsModel {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("type")
    @Expose
    public int type;
}
