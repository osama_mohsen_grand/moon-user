package grand.app.moon.models.ads.companyAdsCategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ad {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("shop_id")
    @Expose
    public Integer shopId;
    @SerializedName("shop_type")
    @Expose
    public Integer shopType;
    @SerializedName("flag")
    @Expose
    public Integer flag;
}
