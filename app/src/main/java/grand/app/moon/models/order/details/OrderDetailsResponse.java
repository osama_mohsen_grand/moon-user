package grand.app.moon.models.order.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class OrderDetailsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public OrderDetails data;
}
