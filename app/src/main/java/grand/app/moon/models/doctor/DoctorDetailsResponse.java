package grand.app.moon.models.doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

public class DoctorDetailsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public DoctorDetails data;
}
