package grand.app.moon.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateCartRequest {
    @SerializedName("cart_item_id")
    @Expose
    public int cart_item_id;

    @SerializedName("product_id")
    @Expose
    public String product_id;

    @SerializedName("qty")
    @Expose
    public String qty = "1";

    public UpdateCartRequest(int cart_item_id, String product_id, String qty) {
        this.cart_item_id = cart_item_id;
        this.product_id = product_id;
        this.qty = qty;
    }
}
