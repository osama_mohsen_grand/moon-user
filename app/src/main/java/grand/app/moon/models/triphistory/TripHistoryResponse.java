package grand.app.moon.models.triphistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.base.StatusMsg;


public class TripHistoryResponse extends StatusMsg implements Serializable {

    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum implements Serializable{

        @SerializedName("trip_type")
        @Expose
        public Integer tripType;
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("start_lat")
        @Expose
        public String startLat;
        @SerializedName("start_lng")
        @Expose
        public String startLng;
        @SerializedName("start_address")
        @Expose
        public String startAddress;
        @SerializedName("end_lat")
        @Expose
        public String endLat;
        @SerializedName("end_lng")
        @Expose
        public String endLng;
        @SerializedName("end_address")
        @Expose
        public String endAddress;
        @SerializedName("price")
        @Expose
        public Integer price;
        @SerializedName("trip_date")
        @Expose
        public String tripDate;
        @SerializedName("rate")
        @Expose
        public float rate;
        @SerializedName("receiver_name")
        @Expose
        public String receiverName;
        @SerializedName("receiver_phone")
        @Expose
        public String receiverPhone;
        @SerializedName("goods_type")
        @Expose
        public Integer goodsType;
        @SerializedName("goods_weight")
        @Expose
        public String goodsWeight;
        @SerializedName("helper")
        @Expose
        public Integer helper;
        @SerializedName("dateA")
        @Expose
        public String dateSchedule;
        @SerializedName("time")
        @Expose
        public String timeSchedule;

        @SerializedName("status")
        @Expose
        public int status;

        @SerializedName("car_name")
        @Expose
        public String car_name;
        @SerializedName("car_brand")
        @Expose
        public String car_brand;
        @SerializedName("car_number")
        @Expose
        public String car_number;
        @SerializedName("car_color")
        @Expose
        public String carColor;

        @SerializedName("driver_image")
        @Expose
        public String driver_image;

        @SerializedName("driver_name")
        @Expose
        public String driver_name;
    }
}
