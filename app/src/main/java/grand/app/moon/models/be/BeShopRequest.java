package grand.app.moon.models.be;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import androidx.databinding.ObservableField;
import grand.app.moon.R;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.Validate;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import timber.log.Timber;

public class BeShopRequest implements Serializable {

    private String typeText = "";
    @SerializedName("type")
    @Expose
    private String type = "";


    private String serviceText = "";
    @SerializedName("service_id")
    @Expose
    private String service_id = "";

    private String shopService = "";
    @SerializedName("shops_services")
    @Expose
    private String shops_services = "";

    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("nick_name")
    @Expose
    private String nickName = "";


    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("password")
    @Expose
    private String password = "";

    private String password_confirm = "";


    @SerializedName("phone")
    @Expose
    private String phone = "";


    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("gender")
    @Expose
    private String gender = "1";


    @SerializedName("lng")
    @Expose
    private double lng = 0;

    @SerializedName("address")
    @Expose
    private String address = "";

    private String country = "";


    @SerializedName("country_id")
    @Expose
    private String country_id = "";

    private String city = "";

    @SerializedName("city_id")
    @Expose
    private String city_id = "";


    private String region = "";

    @SerializedName("region_id")
    @Expose
    private String region_id = "";

    private String commercial = "";

    @SerializedName("subCategory_id")
    private ArrayList<Integer> subCategory_id = new ArrayList<>();

    @SerializedName("service_id_list")
    private ArrayList<Integer> serviceIdList = new ArrayList<>();


//    @SerializedName("commercial_image")
//    @Expose
//    private String commercial_image = "";

    private String license = "";

//    @SerializedName("license_image")
//    @Expose
//    private String license_image = "";

    @SerializedName("description")
    @Expose
    private String description = "";

    public transient String cpp="";
    @SerializedName("category_id")
    @Expose
    private int category_id = -1;

    private transient String category = "";
    private transient String service_list = "";
    public transient String phone2 = "",phone3="";

    public transient ObservableField typeError;
    public transient ObservableField serviceIdError;
    public transient ObservableField shopsServicesError;
    public transient ObservableField nameError;
    public transient ObservableField nickNameError;
    public transient ObservableField emailError;
    public transient ObservableField phoneError;
    public transient ObservableField passwordError;
    public transient ObservableField passwordConfirmError;
    public transient ObservableField addressError;
    public transient ObservableField countryError;
    public transient ObservableField cityError;
    public transient ObservableField regionError;
    public transient ObservableField commercialError;
    public transient ObservableField licenceError;
    public transient ObservableField descriptionError;
    public transient ObservableField categoryError;


    public BeShopRequest() {
        country_id =  UserHelper.retrieveKey(Constants.COUNTRY_ID);
        typeError = new ObservableField();
        serviceIdError = new ObservableField();
        shopsServicesError = new ObservableField();
        nameError = new ObservableField();
        nickNameError = new ObservableField();
        phoneError = new ObservableField();
        emailError = new ObservableField();
        passwordError = new ObservableField();
        passwordConfirmError = new ObservableField();
        addressError = new ObservableField();
        countryError = new ObservableField();
        cityError = new ObservableField();
        regionError = new ObservableField();
        commercialError = new ObservableField();
        licenceError = new ObservableField();
        descriptionError = new ObservableField();
        categoryError = new ObservableField();

    }


    public boolean isValid() {
        boolean valid = true;
        if(!Validate.isValid(type)) {
            typeError.set(Validate.error);
            valid = false;
            Timber.e("type:error");
        }
        if(!Validate.isValid(service_id) && !type.equals(Constants.TYPE_PHOTOGRAPHER) &&
                !type.equals(Constants.TYPE_FAMOUS_PEOPLE)) {
            if(!type.equals("") && AppMoon.getServiceType(Integer.parseInt(type)).size() > 0) {
                serviceIdError.set(Validate.error);
                valid = false;
                Timber.e("service_id:error");
            }
        }
        if(!Validate.isValid(shops_services) && !type.equals(Constants.TYPE_PHOTOGRAPHER) &&
                !type.equals(Constants.TYPE_FAMOUS_PEOPLE)) {
            shopsServicesError.set(Validate.error);
            valid = false;
            Timber.e("shops_services:error");
        }
        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(nickName)) {
            nickNameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(phone, Constants.PHONE)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("phone:error");
        }
        if(!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
            Timber.e("email:error");
        }
        if(!Validate.isValid(password)) {
            passwordError.set(Validate.error);
            valid = false;
            Timber.e("password:error");
        }
        if(!Validate.isValid(password_confirm)) {
            passwordConfirmError.set(Validate.error);
            valid = false;
            Timber.e("password_confirm:error");
        }
        if(!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        if(!Validate.isValid(city)) {
            cityError.set(Validate.error);
            valid = false;
            Timber.e("city:error");
        }
        if(!Validate.isValid(region) ) {
            regionError.set(Validate.error);
            valid = false;
            Timber.e("region:error");
        }
        if(!Validate.isValid(commercial) && !type.equals(Constants.TYPE_PHOTOGRAPHER) &&
                !type.equals(Constants.TYPE_FAMOUS_PEOPLE)) {
            commercialError.set(Validate.error);
            valid = false;
            Timber.e("commercial:error");
        }
        if(!Validate.isValid(license) && !type.equals(Constants.TYPE_PHOTOGRAPHER) &&
                !type.equals(Constants.TYPE_FAMOUS_PEOPLE)) {
            licenceError.set(Validate.error);
            valid = false;
            Timber.e("license:error");
        }
        if(!Validate.isValid(description)) {
            descriptionError.set(Validate.error);
            valid = false;
            Timber.e("description:error");
        }
        if(valid && !Validate.isMatchPassword(password,password_confirm)){
            passwordConfirmError.set(Validate.error);
            passwordError.set(Validate.error);
            valid = false;
            Timber.e("password_password_confirm:error");
        }
        Timber.e("valid_input:"+valid);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        passwordError.set(null);
    }

    public String getPassword_confirm() {
        return password_confirm;
    }

    public void setPassword_confirm(String password_confirm) {
        this.password_confirm = password_confirm;
        passwordConfirmError.set(null);
    }

    public String getTypeText() {
        return typeText;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
        typeError.set(null);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        typeError.set(null);
    }

    public String getServiceText() {
        return serviceText;
    }

    public void setServiceText(String serviceText) {
        this.serviceText = serviceText;
        serviceIdError.set(null);
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
        serviceIdError.set(null);
    }

    public String getShopService() {
        return shopService;
    }

    public void setShopService(String shopService) {
        this.shopService = shopService;
        shopsServicesError.set(null);
    }

    public String getShops_services() {
        return shops_services;
    }

    public void setShops_services(String shops_services) {
        this.shops_services = shops_services;
        shopsServicesError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
        countryError.set(null);
    }




    public String getCountry_id() {
        return country_id;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        cityError.set(null);
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
        cityError.set(null);
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
        regionError.set(null);
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
        regionError.set(null);
    }

    public String getCommercial() {
        return commercial;
    }

    public void setCommercial(String commercial) {
        this.commercial = commercial;
        commercialError.set(null);
    }

//    public String getCommercial_image() {
//        return commercial_image;
//    }
//
//    public void setCommercial_image(String commercial_image) {
//        this.commercial_image = commercial_image;
//        commercialError.set(null);
//    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
        licenceError.set(null);
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
        this.nickNameError.set(null);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        descriptionError.set(null);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
//        subCategoryError.set(null);
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
        this.setCategory(ResourceManager.getString(R.string.done_selected_service));
    }

    public void setSubCategory_id(ArrayList<Integer> subCategory_id) {
        this.subCategory_id = subCategory_id;
//        subCategoryError.set(null);
    }
    public void setServiceIdList(ArrayList<Integer> serviceIdList) {
        this.serviceIdList = serviceIdList;
        service_list = ResourceManager.getString(R.string.done_selected_service);
        shopsServicesError.set(null);
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
