package grand.app.moon.models.trip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripDetailsModel {

    @SerializedName("driver_id")
    @Expose
    public Integer driverId;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("receiver_name")
    @Expose
    public String receiverName;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("goods_type")
    @Expose
    public Integer goodsType;
    @SerializedName("goods_weight")
    @Expose
    public String goodsWeight;
    @SerializedName("helper")
    @Expose
    public Integer helper;
    @SerializedName("status")
    @Expose
    public Integer status;
    @SerializedName("start_lat")
    @Expose
    public double startLat;
    @SerializedName("start_lng")
    @Expose
    public double startLng;
    @SerializedName("start_address")
    @Expose
    public String startAddress;
    @SerializedName("end_lat")
    @Expose
    public double endLat;
    @SerializedName("end_lng")
    @Expose
    public double endLng;
    @SerializedName("end_address")
    @Expose
    public String endAddress;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("rate")
    @Expose
    public float rate;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("distance")
    @Expose
    public String distance;

    @SerializedName("trip_time")
    @Expose
    public String trip_time;

    @SerializedName("car_brand")
    @Expose
    public String carBrand;

    @SerializedName("car_number")
    @Expose
    public String carNumber;

    @SerializedName("car_color")
    @Expose
    public String carColor;
}
