package grand.app.moon.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.models.base.StatusMsg;

public class ShopDetailsResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public ShopData shopData;
}
