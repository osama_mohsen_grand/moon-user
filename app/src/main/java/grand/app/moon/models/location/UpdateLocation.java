package grand.app.moon.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UpdateLocation {
    @SerializedName("trip_id")
    @Expose
    public String trip_id;

    @SerializedName("locations")
    @Expose
    public ArrayList<String> locations;

    public UpdateLocation(String trip_id, ArrayList<String> locations) {
        this.trip_id = trip_id;
        this.locations = locations;
    }
}
