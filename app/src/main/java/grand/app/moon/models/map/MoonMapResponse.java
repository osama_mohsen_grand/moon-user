package grand.app.moon.models.map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.followers.Follower;
import grand.app.moon.models.home.response.Story;

public class MoonMapResponse extends StatusMsg {
    @SerializedName("stories")
    @Expose
    public List<Story> stories;
    @SerializedName("followers")
    @Expose
    public List<Follower> followers = null;
}
