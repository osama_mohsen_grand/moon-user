
package grand.app.moon.models.service.clinic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.IdNamePrice;
import grand.app.moon.models.home.response.Service;
import grand.app.moon.models.service.ShopDetails;

public class ClinicDataDetails {

    @SerializedName("services")
    @Expose
    public List<Service> mServices;
    @SerializedName("clinics")
    @Expose
    public List<ClinicModel> clinics;

}
