package grand.app.moon.models.user.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.user.profile.User;


public class LoginResponse extends StatusMsg {


    @SerializedName("data")
    @Expose
    public User data;


}
