
package grand.app.moon.models.status;

import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("created_at")
    public String mCreatedAt;
    @SerializedName("id")
    public int mId;
    @SerializedName("image")
    public String mImage;
}
