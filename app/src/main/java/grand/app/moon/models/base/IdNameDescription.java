package grand.app.moon.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class IdNameDescription implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image = "";

    @SerializedName("capture")
    @Expose
    public String capture = "";

    public IdNameDescription(int id, String name, String image, String capture) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.capture = capture;
    }
}
