package grand.app.moon.models.app;

import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.models.country.Datum;
import grand.app.moon.notification.NotificationGCMModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.fragments.beauty.BeautyServiceListFragment;
import grand.app.moon.views.fragments.company.CompaniesListFragment;
import grand.app.moon.views.fragments.company.CompanyMapFragment;
import grand.app.moon.views.fragments.institution.InstitutionShopFragment;
import grand.app.moon.views.fragments.reservation.ClinicListFragment;
import grand.app.moon.views.fragments.service.ServiceMapFragment;
import grand.app.moon.views.fragments.service.ServiceShopFragment;

import static grand.app.moon.utils.resources.ResourceManager.getString;

public class AppMoonServices {

    public static ArrayList<TabModel>  getTabShopModels(Bundle getArguments){
        ArrayList<TabModel> tabModels = new ArrayList<>();

        ServiceShopFragment serviceNewestFragment = new ServiceShopFragment();
        Bundle bundleNews = new Bundle();
        bundleNews.putString(Constants.TYPE, Constants.NEWEST);
        bundleNews.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceNewestFragment.setArguments(bundleNews);

        ServiceShopFragment serviceOfferFragment = new ServiceShopFragment();
        Bundle bundleOffer = new Bundle();
        bundleOffer.putString(Constants.TYPE, Constants.OFFER);
        bundleOffer.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceOfferFragment.setArguments(bundleOffer);

        ServiceShopFragment serviceVoucherFragment = new ServiceShopFragment();
        Bundle bundleVoucher = new Bundle();
        bundleVoucher.putString(Constants.TYPE, Constants.VOUCHER);
        bundleVoucher.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceVoucherFragment.setArguments(bundleVoucher);

        ServiceMapFragment serviceMapFragment = new ServiceMapFragment();
        serviceMapFragment.setArguments(getArguments);

        tabModels.add(new TabModel(getString(R.string.newest), serviceNewestFragment));
        tabModels.add(new TabModel(getString(R.string.offer), serviceOfferFragment));
        tabModels.add(new TabModel(getString(R.string.vouchers), serviceVoucherFragment));
        tabModels.add(new TabModel(getString(R.string.map), serviceMapFragment));
        return tabModels;
    }



    public static ArrayList<TabModel>  getTabShopModelsWithoutVoucher(Bundle getArguments){
        ArrayList<TabModel> tabModels = new ArrayList<>();

        ServiceShopFragment serviceNewestFragment = new ServiceShopFragment();
        Bundle bundleNews = new Bundle();
        bundleNews.putString(Constants.TYPE, Constants.NEWEST);
        bundleNews.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceNewestFragment.setArguments(bundleNews);

        ServiceShopFragment serviceOfferFragment = new ServiceShopFragment();
        Bundle bundleOffer = new Bundle();
        bundleOffer.putString(Constants.TYPE, Constants.OFFER);
        bundleOffer.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceOfferFragment.setArguments(bundleOffer);

        ServiceMapFragment serviceMapFragment = new ServiceMapFragment();
        serviceMapFragment.setArguments(getArguments);

        tabModels.add(new TabModel(getString(R.string.newest), serviceNewestFragment));
        tabModels.add(new TabModel(getString(R.string.offer), serviceOfferFragment));
        tabModels.add(new TabModel(getString(R.string.map), serviceMapFragment));
        return tabModels;
    }

    public static ArrayList<TabModel>  getTabInstitutionModels(Bundle getArguments){
        ArrayList<TabModel> tabModels = new ArrayList<>();

        InstitutionShopFragment serviceNewestFragment = new InstitutionShopFragment();
        Bundle bundleNews = new Bundle();
        bundleNews.putString(Constants.TYPE,Constants.NEWEST);
        bundleNews.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceNewestFragment.setArguments(bundleNews);

        InstitutionShopFragment serviceOfferFragment = new InstitutionShopFragment();
        Bundle bundleOffer = new Bundle();
        bundleOffer.putString(Constants.TYPE, Constants.OFFER);
        bundleOffer.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceOfferFragment.setArguments(bundleOffer);

        ServiceMapFragment serviceMapFragment = new ServiceMapFragment();
        serviceMapFragment.setArguments(getArguments);

        tabModels.add(new TabModel(getString(R.string.newest), serviceNewestFragment));
        tabModels.add(new TabModel(getString(R.string.offer), serviceOfferFragment));
        tabModels.add(new TabModel(getString(R.string.map), serviceMapFragment));

        return tabModels;
    }

    public static ArrayList<TabModel>  getTabReservationClinic(Bundle getArguments){
        ArrayList<TabModel> tabModels = new ArrayList<>();

        ClinicListFragment clinicListFragment = new ClinicListFragment();
        Bundle bundleNews = new Bundle();
        bundleNews.putString(Constants.TYPE,Constants.NEWEST);
        bundleNews.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        clinicListFragment.setArguments(bundleNews);

        ClinicListFragment serviceOfferFragment = new ClinicListFragment();
        Bundle bundleOffer = new Bundle();
        bundleOffer.putString(Constants.TYPE, Constants.OFFER);
        bundleOffer.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceOfferFragment.setArguments(bundleOffer);

        ServiceMapFragment serviceMapFragment = new ServiceMapFragment();
        serviceMapFragment.setArguments(getArguments);

        tabModels.add(new TabModel(getString(R.string.newest), clinicListFragment));
        tabModels.add(new TabModel(getString(R.string.offer), serviceOfferFragment));
        tabModels.add(new TabModel(getString(R.string.map), serviceMapFragment));



        return tabModels;
    }


    public static ArrayList<TabModel> getTabReservationBeauty(Bundle getArguments) {
        ArrayList<TabModel> tabModels = new ArrayList<>();

        ClinicListFragment beautyServiceListFragment = new ClinicListFragment();
        Bundle bundleNews = new Bundle();
        bundleNews.putString(Constants.TYPE,Constants.NEWEST);
        bundleNews.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        beautyServiceListFragment.setArguments(bundleNews);

        ClinicListFragment serviceOfferFragment = new ClinicListFragment();
        Bundle bundleOffer = new Bundle();
        bundleOffer.putString(Constants.TYPE, Constants.OFFER);
        bundleOffer.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        serviceOfferFragment.setArguments(bundleOffer);

        ServiceMapFragment serviceMapFragment = new ServiceMapFragment();
        serviceMapFragment.setArguments(getArguments);

        tabModels.add(new TabModel(getString(R.string.services), beautyServiceListFragment));
        tabModels.add(new TabModel(getString(R.string.offer), serviceOfferFragment));
        tabModels.add(new TabModel(getString(R.string.map), serviceMapFragment));


        return tabModels;
    }

    public static ArrayList<TabModel>  getTabCompanies(Bundle getArguments){
        ArrayList<TabModel> tabModels = new ArrayList<>();

        CompaniesListFragment companiesNewestFragment = new CompaniesListFragment();
        Bundle bundleNews = new Bundle();
        bundleNews.putString(Constants.TYPE,Constants.NEWEST);
        bundleNews.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        companiesNewestFragment.setArguments(bundleNews);

        CompaniesListFragment companiesNearestFragment = new CompaniesListFragment();
        Bundle bundleOffer = new Bundle();
        bundleOffer.putString(Constants.TYPE, Constants.NEAREST);
        bundleOffer.putSerializable(Constants.SERVICE, getArguments.getSerializable(Constants.SERVICE));
        companiesNearestFragment.setArguments(bundleOffer);

        ServiceMapFragment serviceMapFragment = new ServiceMapFragment();
        serviceMapFragment.setArguments(getArguments);

        tabModels.add(new TabModel(getString(R.string.newest), companiesNewestFragment));
        tabModels.add(new TabModel(getString(R.string.nearest), companiesNearestFragment));
        tabModels.add(new TabModel(getString(R.string.map), serviceMapFragment));


        return tabModels;
    }

}
