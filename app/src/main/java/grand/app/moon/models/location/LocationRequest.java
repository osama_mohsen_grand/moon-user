package grand.app.moon.models.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.storage.user.UserHelper;

public class LocationRequest {
    @SerializedName("driver_id")
    @Expose
    public int driver_id;
    @SerializedName("available")
    @Expose
    public int available;
    @SerializedName("lat")
    @Expose
    public double lat;
    @SerializedName("lng")
    @Expose
    public double lng;

    public LocationRequest(double lat, double lng) {
        driver_id = UserHelper.getUserId();
        this.lat = lat;
        this.lng = lng;
    }
}
