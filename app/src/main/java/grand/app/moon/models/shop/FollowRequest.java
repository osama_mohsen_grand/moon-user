package grand.app.moon.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.utils.Constants;

public class FollowRequest {
    @SerializedName("to")
    @Expose
    public int to;

    @SerializedName("to_type")
    @Expose
    public int toType;

    @SerializedName("account_type")
    @Expose
    public String accountType;

    public FollowRequest(int to, int toType) {
        this.to = to;
        this.toType = toType;
        this.accountType = Constants.DEFAULT_USER;
    }
}
