
package grand.app.moon.models.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Review {

    @SerializedName("comment")
    @Expose
    public String mComment;
    @SerializedName("date")
    @Expose
    public String mDate;
    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("name")
    @Expose
    public String mName;
    @SerializedName("rate")
    @Expose
    public float mRate;
}
