package grand.app.moon.models.reservation.detatils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.IdName;

public class ReservationDetailsModel {
    @SerializedName("order_id")
    @Expose
    public Integer orderId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("order_date")
    @Expose
    public String orderDate;

    @SerializedName("phone")
    @Expose
    public String phone;


    @SerializedName("order_status")
    @Expose
    public int orderStatus;

    @SerializedName("order_flag")
    @Expose
    public int orderFlag;

    @SerializedName("fees")
    @Expose
    public Integer fees;

    @SerializedName("doctor_name")
    @Expose
    public String doctorName;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("doctor_image")
    @Expose
    public String doctorImage;
    @SerializedName("yearsExperience")
    @Expose
    public Integer yearsExperience;
    @SerializedName("doctor_specialties")
    @Expose
    public String doctorSpecialties;
    @SerializedName("rate")
    @Expose
    public float rate;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("services")
    @Expose
    public List<IdName> services;
}
