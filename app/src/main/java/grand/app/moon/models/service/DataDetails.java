
package grand.app.moon.models.service;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.home.response.Service;

public class DataDetails implements Serializable {

    @SerializedName("services")
    @Expose
    public List<Service> mServices;
    @SerializedName("shops")
    @Expose
    public List<ShopDetails> mShops;

}
