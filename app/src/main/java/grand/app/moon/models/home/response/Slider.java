
package grand.app.moon.models.home.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Slider {

    @SerializedName("image")
    @Expose
    public String mImage;

}
