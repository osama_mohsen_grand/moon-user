package grand.app.moon.models.app;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import grand.app.moon.R;
import grand.app.moon.customviews.dialog.rate.DialogRateModel;
import grand.app.moon.models.country.City;
import grand.app.moon.models.country.CountriesResponse;
import grand.app.moon.models.country.Datum;
import grand.app.moon.models.shop.ShopDepartment;
import grand.app.moon.notification.NotificationGCMModel;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.fragments.chat.ChatFragment;
import grand.app.moon.views.fragments.shop.ShopDetailsFragment;
import timber.log.Timber;

public class AppMoon {
    public static ArrayList<AccountTypeModel> accountType = new ArrayList<>();

    public static String getDefaultLocationCountry() {
        return "country_id=" + UserHelper.retrieveKey(Constants.COUNTRY_ID) + "&lat=" + UserHelper.retrieveKey(Constants.LAT) +
                "&lng=" + UserHelper.retrieveKey(Constants.LNG);
    }


    public static Datum getCities(String countryId, List<Datum> countries) {
        for (Datum country : countries) {
            if (String.valueOf(country.id).equals(countryId)) {
                return country;
            }
        }

        return new Datum();
    }

    public static Datum getCountry(String countryId) {
        CountriesResponse countriesResponse = UserHelper.getCountries();
        for (Datum country : countriesResponse.data) {
            if (String.valueOf(country.id).equals(countryId)) {
                return country;
            }
        }
        return new Datum();
    }

    public static int getCountryPosition(String countryId) {
        CountriesResponse countriesResponse = UserHelper.getCountries();
        for (int i = 0; i < countriesResponse.data.size(); i++) {
            if (String.valueOf(countriesResponse.data.get(i).id).equals(countryId)) {
                return i;
            }
        }
        return -1;
    }


    public static ArrayList<ShopDepartment> shopDetailsDepartment(int follow) {
        ArrayList<ShopDepartment> shopDepartments = new ArrayList<>();
        shopDepartments.add(new ShopDepartment(ResourceManager.getString(R.string.chat), ResourceManager.getDrawable(R.drawable.ic_chat_history), Constants.CHAT));
        shopDepartments.add(new ShopDepartment(ResourceManager.getString(R.string.details), ResourceManager.getDrawable(R.drawable.ic_info_white), Constants.ACCOUNT_INFO));
        shopDepartments.add(new ShopDepartment(ResourceManager.getString(R.string.follow), ResourceManager.getDrawable(follow), Constants.FOLLOW));
        shopDepartments.add(new ShopDepartment(ResourceManager.getString(R.string.share), ResourceManager.getDrawable(R.drawable.ic_share_white), Constants.SHARE));

        return shopDepartments;
    }

    public static boolean haveDelivery(int type) {
        if (type == Integer.parseInt(Constants.TYPE_COMPANIES) || type == Integer.parseInt(Constants.TYPE_ADVERTISING) ||
                type == Integer.parseInt(Constants.TYPE_INSTITUTIONS)
                || type == Integer.parseInt(Constants.TYPE_INDUSTRIES))
            return false;
        return true;
    }

    public static String getServiceId(String typeSelect) {
        if (typeSelect.equals(ResourceManager.getString(R.string.sector)))
            return "1";
        else if (typeSelect.equals(ResourceManager.getString(R.string.wholesale)))
            return "2";
        else if (typeSelect.equals(ResourceManager.getString(R.string.wholesale_and_sector)))
            return "1,2";
        return "";
    }

    public static boolean searchVisibility(String id) {
        if (id.equals(Constants.DISCOVER) || id.equals(Constants.SERVICE_DETAILS) || id.equals(Constants.HOME)) {
            return true;
        }
        return false;
    }

    public static ArrayList<String> genderList() {
        ArrayList<String> popUpModels = new ArrayList<>();
        popUpModels.add(ResourceManager.getString(R.string.all)); // 3
        popUpModels.add(ResourceManager.getString(R.string.men)); // 1
        popUpModels.add(ResourceManager.getString(R.string.women)); // 2
        return popUpModels;
    }

    public static ArrayList<String> photographerList() {
        ArrayList<String> popUpModels = new ArrayList<>();
        popUpModels.add(ResourceManager.getString(R.string.all)); // 3
        popUpModels.add(ResourceManager.getString(R.string.designers)); // 1
        popUpModels.add(ResourceManager.getString(R.string.photographers)); // 2
        return popUpModels;
    }

    public static int getGenderId(String text) {
        if (text.equals(ResourceManager.getString(R.string.all))) return Constants.GENDER_ALL;
        if (text.equals(ResourceManager.getString(R.string.men))) return Constants.GENDER_MALE;
        else return Constants.GENDER_WOMAN;
    }

    public static int getPhotograhperTypeId(String text) {
        if (text.equals(ResourceManager.getString(R.string.all))) return Constants.GENDER_ALL;
        if (text.equals(ResourceManager.getString(R.string.designers))) return Constants.DESIGNERS;
        else return Constants.PHOTOGRAPHERS;
    }

    public static NotificationGCMModel getNotification(String mJsonString) {
        JsonParser parser = new JsonParser();
        JsonElement mJson = parser.parse(mJsonString);
        Gson gson = new Gson();
        return gson.fromJson(mJson, NotificationGCMModel.class);
    }

    public static String getUserType() {
        return UserHelper.getUserDetails().type;
    }


    public static ArrayList<AccountTypeModel> getShopAccountType() {
        accountType.clear();
//        if(accountType.size() == 0) {
        accountType.add(new AccountTypeModel(Constants.TYPE_CONSUMER_MARKET, ResourceManager.getString(R.string.consumer_market)));
        accountType.add(new AccountTypeModel(Constants.TYPE_MARKET_SERVICE, ResourceManager.getString(R.string.marker_service)));
        accountType.add(new AccountTypeModel(Constants.TYPE_PHOTOGRAPHER, ResourceManager.getString(R.string.photographer)));
        accountType.add(new AccountTypeModel(Constants.TYPE_INSTITUTIONS, ResourceManager.getString(R.string.institutions)));
        accountType.add(new AccountTypeModel(Constants.TYPE_COMPANIES, ResourceManager.getString(R.string.commercials)));
        accountType.add(new AccountTypeModel(Constants.TYPE_RESERVATION, ResourceManager.getString(R.string.reservation)));
        accountType.add(new AccountTypeModel(Constants.TYPE_INDUSTRIES, ResourceManager.getString(R.string.industries)));
        accountType.add(new AccountTypeModel(Constants.TYPE_ADVERTISING, ResourceManager.getString(R.string.advertising)));
        accountType.add(new AccountTypeModel(Constants.TYPE_FAMOUS_PEOPLE, ResourceManager.getString(R.string.famous_people)));
        accountType.add(new AccountTypeModel(Constants.TYPE_COMPANIES, ResourceManager.getString(R.string.companies)));
//        }
        return accountType;
    }

    public static ArrayList<AccountTypeModel> getAccountType() {
        accountType.clear();
//        if (accountType.size() == 0) {
        accountType.add(new AccountTypeModel(Constants.DEFAULT_DELEGATE, ResourceManager.getString(R.string.delegate)));
        accountType.add(new AccountTypeModel(Constants.DEFAULT_TAXI, ResourceManager.getString(R.string.taxi)));
        accountType.add(new AccountTypeModel(Constants.DEFAULT_TRANSPORTATION, ResourceManager.getString(R.string.transporation)));
//        }
        return accountType;
    }

    public static ArrayList<String> getServiceType(int id) {
        ArrayList<String> serviceType = new ArrayList<>();
        if (id == 1) {
            serviceType.add(ResourceManager.getString(R.string.wholesale));
            serviceType.add(ResourceManager.getString(R.string.sector));
            serviceType.add(ResourceManager.getString(R.string.wholesale_and_sector));
        } else if (id == 2) {
            serviceType.add(ResourceManager.getString(R.string.sector));
            serviceType.add(ResourceManager.getString(R.string.wholesale_and_sector));
        } else if (id == 3) {
            serviceType.add(ResourceManager.getString(R.string.wholesale));
            serviceType.add(ResourceManager.getString(R.string.sector));
        }
        // 4 - 5 not have service type
        return serviceType;
    }

    public static int getDay(int dayOfWeek) {
        if (dayOfWeek < 7) return dayOfWeek + 1;
        else return 1;
    }

    public static boolean haveVoucher(int type) {
        if (type == Integer.parseInt(Constants.TYPE_COMPANIES) || type == Integer.parseInt(Constants.TYPE_ADVERTISING) || type == Integer.parseInt(Constants.TYPE_INSTITUTIONS)
                || type == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) || type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY))
            return false;
        return true;
    }

    public static DialogRateModel getDialogRateModel(NotificationGCMModel notificationGCMModel) {
        Timber.e("notification_app_moon:" + notificationGCMModel.id);
        Timber.e("notification_app_moon:" + notificationGCMModel.order_id);
        Timber.e("notification_app_moon:" + notificationGCMModel.name);
        Timber.e("notification_app_moon:" + notificationGCMModel.image);
        DialogRateModel dialogRateModel = new DialogRateModel();
        dialogRateModel.id = notificationGCMModel.id;
        dialogRateModel.order_id = notificationGCMModel.order_id;
        dialogRateModel.name = notificationGCMModel.name;
        dialogRateModel.image = notificationGCMModel.image;
        dialogRateModel.type = String.valueOf(notificationGCMModel.notification_type);
        return dialogRateModel;
    }

    public static ArrayList<String> getGoodTypes() {
        ArrayList<String> goods = new ArrayList<>();
        goods.add(ResourceManager.getString(R.string.breakable));
        goods.add(ResourceManager.getString(R.string.normal));
        goods.add(ResourceManager.getString(R.string.foodstuffs));
        goods.add(ResourceManager.getString(R.string.other));
        return goods;
    }


}
