package grand.app.moon.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moon.vollyutils.VolleyFileObject;

public class IdNameImage implements Serializable {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image = "";

    @SerializedName("type")
    @Expose
    public int type = 0;

    @SerializedName("duration")
    @Expose
    public int duration = 0;

    public transient VolleyFileObject volleyFileObject = null;

    public IdNameImage() {
    }

    public IdNameImage(String id, String image) {
        this.id = id;
        this.image = image;
    }
}
