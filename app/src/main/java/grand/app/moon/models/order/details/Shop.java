package grand.app.moon.models.order.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public int type;
    @SerializedName("description")
    @Expose
    public String description;
}
