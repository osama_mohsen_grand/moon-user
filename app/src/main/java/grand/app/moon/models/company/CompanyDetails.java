
package grand.app.moon.models.company;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.ads.AdsCompanyModel;
import grand.app.moon.models.home.response.Service;

public class CompanyDetails implements Serializable {

    @SerializedName("services")
    @Expose
    public List<Service> mServices;
    @SerializedName("companies")
    @Expose
    public List<AdsCompanyModel> companies;

}
