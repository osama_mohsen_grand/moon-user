package grand.app.moon.models.reservation.detatils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.reservation.ReservationOrder;

public class ReservationDetailsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ReservationDetailsModel reservationDetailsModel;

}
