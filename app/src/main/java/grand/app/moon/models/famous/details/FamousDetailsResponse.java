
package grand.app.moon.models.famous.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.models.personalnfo.AccountInfoResponse;

public class FamousDetailsResponse extends AccountInfoResponse {

    @SerializedName("data")
    @Expose
    public List<ImageVideo> data;

    @SerializedName("ads")
    @Expose
    public List<AlbumModel> ads;

    @SerializedName("account_details")
    @Expose
    public AccountDetails accountDetails = new AccountDetails();

    @SerializedName("rate")
    @Expose
    public float rate;

    @SerializedName("is_follow")
    @Expose
    public int isFollow;

    @SerializedName("share")
    @Expose
    public String share;

    @SerializedName("has_shop")
    @Expose
    public boolean hasShop;

    @SerializedName("is_story")
    @Expose
    public int isStory;

    @SerializedName("stories")
    @Expose
    public Story story;

    @SerializedName("services")
    @Expose
    public List<Service> services = null;
}
