package grand.app.moon.models.clinic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ClinicDetails  implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("nick_name")
    @Expose
    public String nickname;


    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("phones")
    @Expose
    public String phones;
    @SerializedName("is_follow")
    @Expose
    public boolean isFollow;
    @SerializedName("share")
    @Expose
    public String share;

    @SerializedName("followers_count")
    @Expose
    public String followersCount;

    @SerializedName("comment_count")
    @Expose
    private String commentCount;
    @SerializedName("rate")
    @Expose
    public float rate;
    @SerializedName("payment_type")
    @Expose
    public int payment_type;

    @SerializedName("order_flag")
    @Expose
    public int orderFlag;

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = "("+commentCount+")";
    }
}
