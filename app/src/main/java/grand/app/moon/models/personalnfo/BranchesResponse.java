package grand.app.moon.models.personalnfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdNameImage;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.shop.Branch;
import grand.app.moon.viewmodels.profile.Transfer;

public class BranchesResponse extends StatusMsg implements Serializable {
    @SerializedName("branches")
    @Expose
    public List<Branch> branches;



}
