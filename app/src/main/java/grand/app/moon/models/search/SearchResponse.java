package grand.app.moon.models.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class SearchResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<SearchModel> data;

}
