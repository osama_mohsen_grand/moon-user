package grand.app.moon.models.followers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Follower {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("rate")
    @Expose
    public float rate;
    @SerializedName("type")
    @Expose
    public int type;
    @SerializedName("flag")
    @Expose
    public int flag;
    public boolean isFollow = true;
}
