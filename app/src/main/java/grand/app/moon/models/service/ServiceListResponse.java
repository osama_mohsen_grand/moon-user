package grand.app.moon.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moon.models.home.response.Service;

public class ServiceListResponse implements Serializable {
    @SerializedName("services")
    @Expose
    public List<Service> mServices;

    public ServiceListResponse(List<Service> mServices) {
        this.mServices = mServices;
    }
}
