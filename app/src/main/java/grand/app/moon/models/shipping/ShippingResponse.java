package grand.app.moon.models.shipping;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class ShippingResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<Shipping> shippings;
}
