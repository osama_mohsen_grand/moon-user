package grand.app.moon.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteCartRequest {
    @SerializedName("cart_item_id")
    @Expose
    public int cart_item_id;

    @SerializedName("status")
    @Expose
    public int status = -1;

    public DeleteCartRequest(int cart_item_id, int status) {
        this.cart_item_id = cart_item_id;
        this.status = status;
    }
}
