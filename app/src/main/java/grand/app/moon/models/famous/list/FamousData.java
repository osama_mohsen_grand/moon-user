
package grand.app.moon.models.famous.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FamousData {

    @SerializedName("all_famous")
    @Expose
    public List<Famous> allFamous = null;
    @SerializedName("sliders")
    @Expose
    public List<Slider> sliders = null;


}
