package grand.app.moon.models.explore;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moon.models.base.StatusMsg;

public class ExploreResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<Explore> data;
}
