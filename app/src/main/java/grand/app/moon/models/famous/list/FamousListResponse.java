
package grand.app.moon.models.famous.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moon.models.base.StatusMsg;

public class FamousListResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public FamousData data;
}
