package grand.app.moon.models.rate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RateRequest {
    @SerializedName("shop_id")
    @Expose
    public String shop_id;
    @SerializedName("order_id")
    @Expose
    public String order_id;
    @SerializedName("type")
    @Expose
    public String type = "driver";

    @SerializedName("rate")
    @Expose
    public int rate;

    @SerializedName("feedback")
    @Expose
    public String feedback;

    public RateRequest(String shop_id, int rate,String feedback) {
        this.shop_id = shop_id;
        this.rate = rate;
        this.feedback = feedback;
    }

    public RateRequest(String shop_id,String order_id, int rate,String feedback) {
        this.shop_id = shop_id;
        this.order_id = order_id;
        this.rate = rate;
        this.feedback = feedback;
    }


}
