package grand.app.moon.models.famous.advertise;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moon.models.base.IdNameDescription;
import grand.app.moon.models.base.StatusMsg;

public class FilterShopAdsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ArrayList<IdNameDescription> data;
}
