package grand.app.moon.retrofitutils;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import grand.app.moon.models.base.StatusMsg;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
public interface Api<T> {

    @Multipart
    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint" , encoded = true) String url, @QueryMap Map<String, String> map, @Part MultipartBody.Part file);

    @Headers({
            "Accept: application/vnd.yourapi.v1.full+json",
            "User-Agent: Your-App-Name"
    })
    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint" , encoded = true)  String url, @QueryMap Map<String, String> map, @Body RequestBody file);

    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint" , encoded = true) String url, @QueryMap Map<String, String> map);


    @Multipart
    @POST("order/user/orderChat")
    Observable<StatusMsg> chat(@QueryMap Map<String, String> addProductRequest, @Part MultipartBody.Part file);

    @Multipart
    @POST
    Call<JsonObject> post(@Url String url, @QueryMap Map<String, String> map, @Part List<MultipartBody.Part> image);

    @POST
    Call<JsonObject> post(@Url String url, @QueryMap Map<String, String> map);
}


