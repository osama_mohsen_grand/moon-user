package grand.app.moon.repository.service;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.album.AlbumResponse;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.discover.MainDiscoverResponse;
import grand.app.moon.models.explore.ExploreResponse;
import grand.app.moon.repository.AdsRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class DiscoverRepository extends AdsRepository {
    private AlbumResponse albumResponse = null;
    public DiscoverRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getDiscover(String service_id,int type , int flag) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    albumResponse = (AlbumResponse) response;
                    if (albumResponse != null) {
                        setMessage(albumResponse.status,albumResponse.msg);
                        if (albumResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DISCOVER);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(albumResponse.status,albumResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.DISCOVER+"?"+AppMoon.getDefaultLocationCountry()+"&type="+type+"&flag="+flag+"&service_id="+service_id, null, AlbumResponse.class);
    }

    public MainDiscoverResponse mainDiscoverResponse = new MainDiscoverResponse();
    public void getAllDiscoverWithServices() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    mainDiscoverResponse = (MainDiscoverResponse) response;
                    if (mainDiscoverResponse != null) {
                        setMessage(mainDiscoverResponse.status,mainDiscoverResponse.msg);
                        if (mainDiscoverResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DISCOVER);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(mainDiscoverResponse.status,mainDiscoverResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.DISCOVER_ALL_WITH_SERVICES +"?"+AppMoon.getDefaultLocationCountry(), null, MainDiscoverResponse.class);
    }

    public void getAllDiscover(int category_id , int type , int flag) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    albumResponse = (AlbumResponse) response;
                    if (albumResponse != null) {
                        setMessage(albumResponse.status,albumResponse.msg);
                        if (albumResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ALBUM);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(albumResponse.status,albumResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.DISCOVER_ALL +"?"+AppMoon.getDefaultLocationCountry()+"&category_id="+category_id+"&type="+type+"&flag="+flag, null, AlbumResponse.class);
    }

    public ExploreResponse exploreResponse;

    public void getShopsDiscover(int shop_id , int category_id , int type , int flag) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    exploreResponse = (ExploreResponse) response;
                    if (exploreResponse != null) {
                        setMessage(exploreResponse.status,exploreResponse.msg);
                        if (exploreResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.EXPLORE);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(exploreResponse.status,exploreResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOPS_DISCOVER +"?"+AppMoon.getDefaultLocationCountry()+"&shop_id="+shop_id+"&category_id="+category_id+"&type="+type+"&flag="+flag, null, ExploreResponse.class);
    }


    public AlbumResponse getAlbumResponse() {
        return albumResponse;
    }
}



