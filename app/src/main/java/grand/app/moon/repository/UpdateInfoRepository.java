package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.base.UserId;
import grand.app.moon.models.user.profile.ProfileRequest;
import grand.app.moon.models.user.profile.ProfileResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class UpdateInfoRepository extends BaseRepository {
    ProfileResponse profileResponse = null;
    ProfileRequest profileRequest;
    StatusMsg statusMsg;
    UserId userId = null;
    public UpdateInfoRepository(MutableLiveData<Object> mutableLiveData, ProfileRequest profileRequest) {
        super(mutableLiveData);
        this.profileRequest = profileRequest;
    }
    public void submit() {
//        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
//        new ConnectionHelper(new ConnectionListener() {
//            @Override
//            public void onRequestSuccess(Object response) {
//                if(!catchErrorResponse(response)) {
//                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
//                    profileResponse = (ProfileResponse) response;
//                    if (profileResponse != null) {
//                        setMessage(profileResponse.status,profileResponse.msg);
//                        if (profileResponse.status == Constants.RESPONSE_SUCCESS) {
//                            getmMutableLiveData().setValue(Constants.UPDATE_PROFILE);
//                        }else {
//                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
//                            setMessage(profileResponse.status,profileResponse.msg);
//                        }
//                    }
//                }
//
//            }
//        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PROFILE, profileRequest.getMap(), ProfileResponse.class);
    }



    public ProfileResponse getData() {
        return profileResponse;
    }
}



