package grand.app.moon.repository;

import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.MutableLiveData;
import grand.app.moon.R;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;
import timber.log.Timber;


public class VerificationFirebaseSMSRepository extends BaseRepository {
    private static final String TAG = "VerificationFirebaseSMS";
    private String verificationId = "";
    public PhoneAuthCredential mAuthCredentials;
    private final FirebaseAuth mAuth= FirebaseAuth.getInstance();;

    public VerificationFirebaseSMSRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);

    }

    public void sendVerificationCode(String phone,FragmentActivity fragmentActivity) {
        Log.e(TAG, "sendVerificationCode: "+phone );
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(phone)       // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(fragmentActivity)                 // Activity (for callback binding)
                        .setCallbacks(verificationStateChangedCallbacks)          // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
//        PhoneAuthProvider.getInstance().verifyPhoneNumber(phone, 60, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD, verificationStateChangedCallbacks);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
    }



    private PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationStateChangedCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            mAuthCredentials = phoneAuthCredential;
        }

        /* This one is never called: so i assume there's no problem on my part */
        @Override
        public void onVerificationFailed(FirebaseException e) {
            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            e.printStackTrace();
            setMessage(401,e.getMessage());
            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);

        }

        /* This one is called */
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
            Log.e(TAG, "onCodeSent: "+verificationId );
            setMessage(401, ResourceManager.getString(R.string.code_had_been_sent));
            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            getmMutableLiveData().setValue(Constants.WRITE_CODE);
        }

        /* This one is also called */
        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            super.onCodeAutoRetrievalTimeOut(s);

        }
    };

    public String getVerificationId() {
        return verificationId;
    }

    public void verifyCode(String verify_id, String checkCode) {
        Log.e(TAG, "checkCode: "+checkCode );
        Log.e(TAG, "verificationId: "+verificationId );
        this.verificationId = verify_id;
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId,checkCode);
        signInWithCredentials(credential);
    }

    private void signInWithCredentials(PhoneAuthCredential credential) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        if(task.isSuccessful()){
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else{
                            setMessage(Constants.RESPONSE_ERROR, ResourceManager.getString(R.string.verification_code_not_valid));
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                });
    }
}



