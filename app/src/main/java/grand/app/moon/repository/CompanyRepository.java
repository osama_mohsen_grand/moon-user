package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.company.CompanyMainResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class CompanyRepository extends BaseRepository {

    StatusMsg statusMsg;
    CompanyMainResponse companyMainResponse;


    public CompanyRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void getCompanies(int type, String service_id, int filter,int category_id,int sub_category,int city_id,int tab) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        String service = "";
        String category = "",city="";
        if(category_id != 0 && sub_category != 0)
            category = "&category_id="+category_id+"&sub_category_id="+sub_category+"&filter="+filter;
        if(!service_id.equals("")) service = "&service_id="+service_id;
        if(city_id != -1) city = "&city_id="+city_id;

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    companyMainResponse = (CompanyMainResponse) response;
                    if (companyMainResponse != null) {
                        setMessage(companyMainResponse.status,companyMainResponse.msg);
                        if (companyMainResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.COMPANIES_LIST+"?country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID)+"&lat="+UserHelper.retrieveKey(Constants.LAT)+
                "&lng="+UserHelper.retrieveKey(Constants.LNG)+"&type="+type+
                service+category+city+"&tab="+tab, null, CompanyMainResponse.class);
    }

    public CompanyMainResponse getCompanyMainResponse() {
        return companyMainResponse;
    }
}



