package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.base.UserId;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.models.shop.ShopDetailsResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class ShopRepository extends BaseRepository {
    ShopDetailsResponse shopDetailsResponse = null;
    StatusMsg statusMsg = null;
    UserId userId = null;
    public ShopRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getShopDetails(int shop_id,int type , int flag) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    shopDetailsResponse = (ShopDetailsResponse) response;
                    if (shopDetailsResponse != null) {
                        setMessage(shopDetailsResponse.status,shopDetailsResponse.msg);
                        if (shopDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SHOP_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(shopDetailsResponse.status,shopDetailsResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOP_DETAILS+"?id="+shop_id+"&type="+type+"&flag="+flag , null, ShopDetailsResponse.class);
    }

    public void follow(FollowRequest followRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FOLLOW);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.FOLLOW, followRequest, StatusMsg.class);
    }



    public ShopDetailsResponse getData() {
        return shopDetailsResponse;
    }
}



