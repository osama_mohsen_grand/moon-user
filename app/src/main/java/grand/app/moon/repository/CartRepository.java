package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.cart.AddCartRequest;
import grand.app.moon.models.cart.CartResponse;
import grand.app.moon.models.cart.DeleteCartRequest;
import grand.app.moon.models.shipping.ShippingResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class CartRepository extends BaseRepository {

    CartResponse cartResponse = null;
    StatusMsg statusMsg = null;
    ShippingResponse shippingResponse;

    public CartRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void addOrUpdate(AddCartRequest addCartRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            if(statusMsg.status == Constants.RESPONSE_405)
                                getmMutableLiveData().setValue(Constants.DELETE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.CART_ADD, addCartRequest, StatusMsg.class);
    }

    public void deleteCart(DeleteCartRequest deleteCartRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.CART_DELETE, deleteCartRequest, StatusMsg.class);
    }

    public void getCart(){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    cartResponse = (CartResponse) response;
                    if (cartResponse != null) {
                        setMessage(cartResponse.status,cartResponse.msg);
                        if (cartResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CART);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.CART_USER, null, CartResponse.class);
    }



    public void getShipping(){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    shippingResponse = (ShippingResponse) response;
                    if (shippingResponse != null) {
                        setMessage(shippingResponse.status,shippingResponse.msg);
                        if (shippingResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SHIPPING);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.CART_SHIPPING, null, ShippingResponse.class);
    }


    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public CartResponse getCartResponse() {
        return cartResponse;
    }
}



