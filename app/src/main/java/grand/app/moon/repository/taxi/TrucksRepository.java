package grand.app.moon.repository.taxi;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.taxi.TrucksResponse;
import grand.app.moon.models.truck.TruckRequest;
import grand.app.moon.repository.BaseRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class TrucksRepository extends BaseRepository {
    private TrucksResponse trucksResponse = null;
    private StatusMsg statusMsg;
    public TrucksRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getTrucks() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    trucksResponse = (TrucksResponse) response;
                    if (trucksResponse != null) {
                        setMessage(trucksResponse.status,trucksResponse.msg);
                        if (trucksResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.TRUCKS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(trucksResponse.status,trucksResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.TRUCK_LIST+"?country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID), null, TrucksResponse.class);
    }

    public void makeOrder(TruckRequest truckRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DONE);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.TRUCK_CREATE_ORDER, truckRequest, StatusMsg.class);
    }

    public TrucksResponse getTrucksResponse() {
        return trucksResponse;
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }
}



