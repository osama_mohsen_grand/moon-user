package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.UserId;
import grand.app.moon.models.product.ProductResponse;
import grand.app.moon.models.productList.ProductListResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class ProductRepository extends BaseRepository {
    ProductListResponse productListResponse = null;
    ProductResponse productResponse = null;
    UserId userId = null;
    public ProductRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getShopProducts(String category_id , String shop_id,int type , int flag) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    productListResponse = (ProductListResponse) response;
                    if (productListResponse != null) {
                        setMessage(productListResponse.status,productListResponse.msg);
                        if (productListResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.PRODUCTS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(productListResponse.status,productListResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOP_PRODUCTS+"?category_id="+category_id+"&shop_id="+shop_id+"&type="+type+"&flag="+flag , null, ProductListResponse.class);
    }

    public void getProductDetails(String product_id ,  int flag) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    productResponse = (ProductResponse) response;
                    if (productResponse != null) {
                        setMessage(productResponse.status,productResponse.msg);
                        if (productResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.PRODUCT_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(productResponse.status,productResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOP_PRODUCT_DETAILS+"?product_id="+product_id+"&flag="+flag , null, ProductResponse.class);
    }




    public ProductListResponse getData() {
        return productListResponse;
    }

    public ProductResponse getProductResponse() {
        return productResponse;
    }
}



