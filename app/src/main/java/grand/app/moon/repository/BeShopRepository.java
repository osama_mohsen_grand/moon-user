package grand.app.moon.repository;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.be.BeShopRequest;
import grand.app.moon.models.shop.AddShopResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.upload.FileOperations;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class BeShopRepository extends BaseRepository {
    StatusMsg statusMsg = null;
    AddShopResponse addShopResponse = null;

    public BeShopRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void registerUser(BeShopRequest registerShopRequest, ArrayList<String> imagesKeys, ArrayList<String> imagesPath) {
        String endPoint = URLS.REGISTER_SHOP;
        if(registerShopRequest.getType().equals(Constants.TYPE_FAMOUS_PEOPLE) || registerShopRequest.getType().equals(Constants.TYPE_PHOTOGRAPHER))
            endPoint = URLS.REGISTER;
            getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

            ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();


        for(int i=0; i<imagesKeys.size(); i++) {
                Timber.e("key:"+imagesKeys.get(i)+",value:"+imagesPath.get(i));


                volleyFileObjects.add(FileOperations.getVolleyFileObject(imagesKeys.get(i), imagesPath.get(i), Constants.FILE_TYPE_IMAGE));
            }


        new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    if(!catchErrorResponse(response)) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        addShopResponse = (AddShopResponse) response;
                        if (addShopResponse != null) {
                            setMessage(addShopResponse.status,addShopResponse.msg);
                            if (addShopResponse.status == Constants.RESPONSE_SUCCESS) {
                                getmMutableLiveData().setValue(Constants.REGISTRATION);
                            }else {
                                if(addShopResponse.status == Constants.RESPONSE_401)
                                    getmMutableLiveData().setValue(Constants.SUGGESTIONS);
                                else
                                    getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            }
                        }
                    }

                }
                @Override
                public void onRequestError(Object error) {
                    super.onRequestError(error);
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                }
        }).multiPartConnect(endPoint,registerShopRequest,volleyFileObjects, AddShopResponse.class);
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public AddShopResponse getAddShopResponse() {
        return addShopResponse;
    }
}



