package grand.app.moon.repository;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.ads.AddAdsRequest;
import grand.app.moon.models.ads.AdsMainResponse;
import grand.app.moon.models.ads.AdsResponse;
import grand.app.moon.models.ads.MyAdsResponse;
import grand.app.moon.models.adsCompanyFilter.AdsCompanyFilterResponse;
import grand.app.moon.models.adsDetails.AdsDetailsResponse;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.famous.advertise.FilterShopAdsResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;
import grand.app.moon.vollyutils.VolleyFileObject;
import timber.log.Timber;


public class AdsRepository extends BaseRepository {
    AdsResponse adsResponse;
    FilterShopAdsResponse filterShopAdsResponse;
    StatusMsg statusMsg;
    AdsMainResponse adsMainResponse;
    AdsCompanyFilterResponse adsCompanyFilterResponse;
    AdsDetailsResponse adsDetailsResponse;
    MyAdsResponse myAdsResponse;


    public AdsRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void getAds(String tab) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsResponse = (AdsResponse) response;
                    if (adsResponse != null) {
                        setMessage(adsResponse.status, adsResponse.msg);
                        if (adsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_ALBUMS + "?tab=" + tab+"&account_type="+ AppMoon.getUserType(), null, AdsResponse.class);
    }

    public void getShopAds(String url) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    filterShopAdsResponse = (FilterShopAdsResponse) response;
                    Timber.e("filter:"+filterShopAdsResponse.data.size());
                    if (filterShopAdsResponse != null) {
                        setMessage(filterShopAdsResponse.status, filterShopAdsResponse.msg);
                        if (filterShopAdsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FILTER);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_FILTER_SHOP_ADS+ url, null, FilterShopAdsResponse.class);
    }

    public void getShopFilterAds(int shop_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    filterShopAdsResponse = (FilterShopAdsResponse) response;
                    Timber.e("filter:"+filterShopAdsResponse.data.size());
                    if (filterShopAdsResponse != null) {
                        setMessage(filterShopAdsResponse.status, filterShopAdsResponse.msg);
                        if (filterShopAdsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FILTER);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FILTER_SHOP_ADS+ "?shop_id="+shop_id, null, FilterShopAdsResponse.class);
    }


    public static String convertArrayToStringMethod(ArrayList<Integer> data) {

        StringBuilder stringBuilder = new StringBuilder();
        if(data.size() > 0) {
            stringBuilder.append(data.get(0));
            for (int i = 1; i < data.size(); i++) {
                stringBuilder.append(","+data.get(i));

            }
        }
        Timber.e("shops_id:"+stringBuilder.toString());
        return stringBuilder.toString();

    }

    public void getFamousShopAds(ArrayList<Integer> ids, int famous_id, int type) {
        String result = convertArrayToStringMethod(ids);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsResponse = (AdsResponse) response;
                    if (adsResponse != null) {
                        setMessage(adsResponse.status, adsResponse.msg);
                        if (adsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FAMOUS_SHOP_ADS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_FILTER + "?shop_id=" + result+"&famous_id="+famous_id+"&type=1", null, AdsResponse.class);
    }


    public void getAds(int type, int service_id, int filter,int category_id,int sub_category,int city_id,String endPointUrl) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        String service = "";
        String category = "",city="";
        if(category_id != 0 && sub_category != 0)
            category = "&category_id="+category_id+"&sub_category_id="+sub_category+"&filter="+filter;
        if(service_id != -1) service = "&service_id="+service_id;
        if(city_id != -1) city = "&city_id="+city_id;

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsMainResponse = (AdsMainResponse) response;
                    if (adsMainResponse != null) {
                        setMessage(adsMainResponse.status,adsMainResponse.msg);
                        if (adsMainResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, endPointUrl+"?country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID)+"&lat="+UserHelper.retrieveKey(Constants.LAT)+
                "&lng="+UserHelper.retrieveKey(Constants.LNG)+"&type="+type+
                service+category+city+"&tab=1", null, AdsMainResponse.class);
    }


    public void getAdsCompanyFilter(int service_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsCompanyFilterResponse = (AdsCompanyFilterResponse) response;
                    if (adsCompanyFilterResponse != null) {
                        setMessage(adsCompanyFilterResponse.status,adsCompanyFilterResponse.msg);
                        if (adsCompanyFilterResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADS_FILTER+"?service_id="+service_id, null, AdsCompanyFilterResponse.class);
    }

    public void getAdsDetails(int id,int type){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsDetailsResponse = (AdsDetailsResponse) response;
                    if (adsDetailsResponse != null) {
                        setMessage(adsDetailsResponse.status,adsDetailsResponse.msg);
                        if (adsDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ADS_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADS_DETAILS+"?id="+id+"&type="+type, null, AdsDetailsResponse.class);
    }



    public void myAds() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    myAdsResponse = (MyAdsResponse) response;
                    if (myAdsResponse != null) {
                        setMessage(myAdsResponse.status, myAdsResponse.msg);
                        if (myAdsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.MY_ADS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.MY_ADS , null, MyAdsResponse.class);
    }


    public void addAds(AddAdsRequest addAdsRequest, List<VolleyFileObject> volleyFileObjects) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.ADD_ADV,addAdsRequest,volleyFileObjects, StatusMsg.class);
    }

    public void updateAds(AddAdsRequest addAdsRequest, List<VolleyFileObject> volleyFileObjects) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        if(volleyFileObjects != null && volleyFileObjects.size() > 0){
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    if(!catchErrorResponse(response)) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        statusMsg = (StatusMsg) response;
                        if (statusMsg != null) {
                            setMessage(statusMsg.status,statusMsg.msg);
                            if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                                getmMutableLiveData().setValue(Constants.SUCCESS);
                            }else {
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            }
                        }
                    }

                }
                @Override
                public void onRequestError(Object error) {
                    super.onRequestError(error);
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                }
            }).multiPartConnect(URLS.UPDATE_ADV,addAdsRequest,volleyFileObjects, StatusMsg.class);
        }else{
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    if(!catchErrorResponse(response)) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        statusMsg = (StatusMsg) response;
                        if (statusMsg != null) {
                            setMessage(statusMsg.status,statusMsg.msg);
                            if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                                getmMutableLiveData().setValue(Constants.SUCCESS);
                            }else {
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            }
                        }
                    }

                }
                @Override
                public void onRequestError(Object error) {
                    super.onRequestError(error);
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                }
            }).requestJsonObject(Request.Method.POST,URLS.UPDATE_ADV,addAdsRequest, StatusMsg.class);
        }
    }
    public void deleteAdvertisement(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_ADV+"/"+id+"?type=11", null, StatusMsg.class);
    }



    public AdsResponse getAdsResponse() {
        return adsResponse;
    }

    public MyAdsResponse getMyAdsResponse() {
        return myAdsResponse;
    }

    public AdsMainResponse getAdsMainResponse() {
        return adsMainResponse;
    }

    public AdsCompanyFilterResponse getAdsCompanyFilterResponse() {
        return adsCompanyFilterResponse;
    }

    public AdsDetailsResponse getAdsDetailsResponse() {
        return adsDetailsResponse;
    }

    public FilterShopAdsResponse getFilterShopAdsResponse() {
        return filterShopAdsResponse;
    }


    public void deleteImage(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_PRODUCT+"/"+id+"?type="+ UserHelper.getUserDetails().type+"&delete_image=1", new Object(), StatusMsg.class);
    }
}



