package grand.app.moon.repository;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;

public class AdvertisementCompanyRepository extends BaseRepository {
    StatusMsg statusMsg = null;


    public AdvertisementCompanyRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }



}



