package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.models.triphistory.TripHistoryResponse;
import grand.app.moon.vollyutils.URLS;

public class TripHistoryRepository extends BaseRepository {
    TripHistoryResponse tripHistoryResponse = null;
    public TripHistoryRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getTrips(String type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tripHistoryResponse = (TripHistoryResponse) response;
                    if (tripHistoryResponse != null) {
                        setMessage(tripHistoryResponse.status, tripHistoryResponse.msg);
                        if (tripHistoryResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.HISTORY);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.TRIP_HISTORY+"?type="+UserHelper.getUserDetails().type+"&reserved_type="+type, null, TripHistoryResponse.class);
    }

    public TripHistoryResponse getTripHistory() {
        return tripHistoryResponse;
    }
}



