package grand.app.moon.repository.search;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.search.SearchResponse;
import grand.app.moon.repository.BaseRepository;
import grand.app.moon.models.famous.search.AlbumSearchResponse;
import grand.app.moon.models.famous.search.FamousSearchResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class SearchRepository extends BaseRepository {

    AlbumSearchResponse albumSearchResponse = null;
    FamousSearchResponse famousSearchResponse = null;
    SearchResponse searchResponse = null;

    public SearchRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void album(String text) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    albumSearchResponse = (AlbumSearchResponse) response;
                    if (albumSearchResponse != null) {
                        setMessage(albumSearchResponse.status,albumSearchResponse.msg);
                        if (albumSearchResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_ALBUM_SEARCH +"?name="+ text, null, AlbumSearchResponse.class);
    }

    public void famous(int type,String text) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    searchResponse = (SearchResponse) response;
                    if (searchResponse != null) {
                        setMessage(searchResponse.status,searchResponse.msg);
                        if (searchResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_SEARCH +"?type="+type+"&name="+ text+"&country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID), null, SearchResponse.class);
    }

    public AlbumSearchResponse getAlbumSearchResponse() {
        return albumSearchResponse;
    }

    public FamousSearchResponse getFamousSearchResponse() {
        return famousSearchResponse;
    }

    public SearchResponse getSearchResponse() {
        return searchResponse;
    }

    public void submitSearch(String search, int type, int flag, int service_id) {
        String searchText = "?name="+search+ "&" + AppMoon.getDefaultLocationCountry();
        String url = URLS.SEARCH_HOME;
        if(type != -1 || flag != -1 || service_id != -1) {
            searchText += "&type=" + type + "&flag=" + flag + "&service_id=" + service_id;
            url = URLS.SEARCH_SHOP;
        }
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    searchResponse = (SearchResponse) response;
                    if (searchResponse != null) {
                        setMessage(searchResponse.status,searchResponse.msg);
                        if (searchResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SEARCH);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, url +searchText, null, SearchResponse.class);
    }
}



