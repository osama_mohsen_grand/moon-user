package grand.app.moon.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.home.response.HomeResponse;
import grand.app.moon.models.service.ServiceDetailsResponse;
import grand.app.moon.models.service.ServiceMapDetailsResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class HomeRepository extends BaseRepository {
    HomeResponse HomeResponse = null;
    ServiceDetailsResponse serviceDetailsResponse = null;
    ServiceMapDetailsResponse serviceMapDetailsResponse = null;
    public HomeRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void home() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    HomeResponse = (HomeResponse) response;
                    if (HomeResponse != null) {
                        setMessage(HomeResponse.status,HomeResponse.msg);
                        if (HomeResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(HomeResponse.status,HomeResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.HOME+"?country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID), null, HomeResponse.class);
    }

    public void serviceDetails(int type, int flag, int tab, String service_id, int filter, ArrayList<Integer> tags,int city_id,int region_id,String endPointUrl) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        String tags_selected = "",service_id_selected ="";
        for(Integer tag : tags)
            tags_selected += "&tags[]="+tag;
        if(!service_id.equals("") && !service_id.equals("0")){
            service_id_selected +="&service_id="+service_id;
        }
        if(city_id != -1)
            tags_selected += "&city_id="+city_id;
        if(region_id != -1)
            tags_selected += "&region_id="+region_id;
        if(filter != 0)
            tags_selected +="&filter="+filter;

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    serviceDetailsResponse = (ServiceDetailsResponse) response;
                    if (serviceDetailsResponse != null) {
                        setMessage(serviceDetailsResponse.status,serviceDetailsResponse.msg);
                        if (serviceDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(serviceDetailsResponse.status,serviceDetailsResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, endPointUrl+"?country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID)+"&lat="+UserHelper.retrieveKey(Constants.LAT)+
                "&lng="+UserHelper.retrieveKey(Constants.LNG)+"&type="+type+"&flag="+flag+
                "&tab="+tab+service_id_selected+tags_selected, null, ServiceDetailsResponse.class);
    }
    //URLS.SERVICE_DETAILS

    public void serviceMapDetails(int type, int flag,int tab, String service_id, int filter, ArrayList<Integer> tags,String endPointUrl) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        String tags_selected = "";
        for(Integer tag : tags)
            tags_selected += "&tags[]="+tag;

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    serviceMapDetailsResponse = (ServiceMapDetailsResponse) response;
                    if (serviceMapDetailsResponse != null) {
                        setMessage(serviceMapDetailsResponse.status,serviceMapDetailsResponse.msg);
                        if (serviceMapDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(serviceMapDetailsResponse.status,serviceMapDetailsResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, endPointUrl+"?country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID)+"&lat="+UserHelper.retrieveKey(Constants.LAT)+
                "&lng="+UserHelper.retrieveKey(Constants.LNG)+"&type="+type+"&flag="+flag+
                "&tab="+tab+"&service_id="+service_id+"&filter="+filter+tags_selected, null, ServiceMapDetailsResponse.class);
    }


    public ServiceMapDetailsResponse getServiceMapDetailsResponse() {
        return serviceMapDetailsResponse;
    }

    public ServiceDetailsResponse getServiceDetailsResponse() {
        return serviceDetailsResponse;
    }

    public grand.app.moon.models.home.response.HomeResponse getHomeResponse() {
        return HomeResponse;
    }
}



