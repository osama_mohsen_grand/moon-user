package grand.app.moon.repository;

import android.util.Log;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.models.location.LocationRequest;
import grand.app.moon.models.user.profile.User;
import grand.app.moon.vollyutils.URLS;

public class LocationRepository extends BaseRepository {
    StatusMsg statusMsg = null;
    private static final String TAG = "LocationRepository";
    public LocationRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void updateLocation(LocationRequest locationRequest,boolean isForeground) {
        if(isForeground) getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    if(isForeground) getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            User driver = UserHelper.getUserDetails();
//                            driver.available = locationRequest.available;
                            UserHelper.saveUserDetails(driver);
                            Log.e(TAG, "update_location Done"+UserHelper.getUserDetails());
                        }else {
                            if(isForeground){
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            }
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.REGISTER_USER, locationRequest, StatusMsg.class);
    }
}



