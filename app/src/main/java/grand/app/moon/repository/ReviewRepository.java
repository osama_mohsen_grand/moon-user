package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.review.ReviewResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class ReviewRepository extends CommonRepository {
    ReviewResponse reviewResponse = null;
    StatusMsg statusMsg;
    public ReviewRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getReviews(int shop_id,int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        String to_type=  "";
        if(type != 0){
            to_type ="&to_type="+type;
        }
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    reviewResponse = (ReviewResponse) response;
                    if (reviewResponse != null) {
                        setMessage(reviewResponse.status,reviewResponse.msg);
                        if (reviewResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.REVIEW);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOP_REVIEW+"?shop_id="+shop_id+to_type, null, ReviewResponse.class);
    }

    public ReviewResponse getReviewResponse() {
        return reviewResponse;
    }
}



