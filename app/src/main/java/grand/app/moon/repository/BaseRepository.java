package grand.app.moon.repository;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import androidx.lifecycle.MutableLiveData;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.country.CountriesResponse;
import grand.app.moon.models.famous.advertise.FilterShopAdsResponse;
import grand.app.moon.models.favourite.FavouriteRequest;
import grand.app.moon.models.favourite.FavouriteResponse;
import grand.app.moon.retrofitutils.NoConnectivityException;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;
import timber.log.Timber;

public class BaseRepository {
    private int status = 0;
    private String message = "";
    private MutableLiveData<Object> mMutableLiveData;
    private static final String TAG = "BaseRepository";

    public BaseRepository(MutableLiveData<Object> mMutableLiveData) {
        this.mMutableLiveData = mMutableLiveData;
    }

    public boolean catchErrorResponse(Object response) {
        if (response instanceof VolleyError) {
            VolleyError volleyError = (VolleyError) response;
            message = volleyError.getMessage();
            if (mMutableLiveData != null) {
                mMutableLiveData.setValue(Constants.HIDE_PROGRESS);
                if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    mMutableLiveData.setValue(Constants.SERVER_ERROR);
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError || volleyError instanceof NetworkError || volleyError instanceof AuthFailureError || volleyError instanceof TimeoutError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    mMutableLiveData.setValue(Constants.FAILURE_CONNECTION);
                }
            }
            return true;
        }else if(response instanceof NoConnectivityException) {
            Timber.e("NoConnectivityException");
            mMutableLiveData.setValue(Constants.HIDE_PROGRESS);
            NoConnectivityException throwable = (NoConnectivityException) response;
            message = throwable.getMessage();
            Timber.e("NoConnectivityException2:"+message);
            mMutableLiveData.setValue(Constants.FAILURE_CONNECTION);
            return true;
        }else if(response instanceof Throwable){
            Timber.e("Throwable");
            mMutableLiveData.setValue(Constants.HIDE_PROGRESS);
            Throwable throwable = (Throwable) response;
            message = throwable.getMessage();
            mMutableLiveData.setValue(Constants.SERVER_ERROR);
            return true;
        }
        return false;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }



    public void setMessage(int status, String message) {
        this.status = status;
        this.message = message;
        if (status == Constants.RESPONSE_JWT_EXPIRE) {
            Log.d(TAG,"EXPIRE");
            if (mMutableLiveData != null) mMutableLiveData.setValue(Constants.LOGOUT);
        }
    }
    FavouriteResponse favouriteResponse;
    public void getFavourite(int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    favouriteResponse = (FavouriteResponse) response;
                    if (favouriteResponse != null) {
                        setMessage(favouriteResponse.status,favouriteResponse.msg);
                        if (favouriteResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(favouriteResponse.status,favouriteResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.FAVOURITE+"?type="+type , null, FavouriteResponse.class);
    }

    public FavouriteResponse getFavouriteResponse() {
        return favouriteResponse;
    }

    StatusMsg statusMsg;
    public void addFavourite(FavouriteRequest favouriteRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FAVOURITE);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.FAVOURITE_ADD , favouriteRequest, StatusMsg.class);
    }

    private CountriesResponse countriesResponse = null;
    public void getCountries(boolean showProgress) {
        if(showProgress) getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    if(showProgress) getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    countriesResponse = (CountriesResponse) response;
                    if (countriesResponse != null) {
                        setMessage(countriesResponse.status,countriesResponse.msg);
                        if (countriesResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveCountries(countriesResponse);
                            getmMutableLiveData().setValue(Constants.COUNTRIES);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(countriesResponse.status,countriesResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.GET_ALL_COUNTRIES, null, CountriesResponse.class);
    }

    public CountriesResponse getCountriesResponse() {
        return countriesResponse;
    }


    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public MutableLiveData<Object> getmMutableLiveData() {
        return mMutableLiveData;
    }
}
