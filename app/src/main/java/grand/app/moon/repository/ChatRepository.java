package grand.app.moon.repository;

import android.util.Log;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.app.AppMoon;
import grand.app.moon.models.chat.ChatDetailsResponse;
import grand.app.moon.models.chat.ChatRequest;
import grand.app.moon.models.chat.ChatSendResponse;
import grand.app.moon.models.chat.list.ChatListResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;
import grand.app.moon.vollyutils.VolleyFileObject;


public class ChatRepository extends BaseRepository {
    ChatDetailsResponse chatDetailsResponse;
    ChatListResponse chatListResponse = null;
    ChatSendResponse chatSendResponse;

    public ChatRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void getChatDetails(int id, int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatDetailsResponse = (ChatDetailsResponse) response;
                    if (chatDetailsResponse != null) {
                        setMessage(chatDetailsResponse.status, chatDetailsResponse.msg);
                        if (chatDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(chatDetailsResponse.status, chatDetailsResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.CHAT_DETAILS + "?order_id=" + id + "&type=" + AppMoon.getUserType() + "&receiver_type=" + type, null, ChatDetailsResponse.class);
    }

    public void getChatDetailsFromChatList(int id, int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatDetailsResponse = (ChatDetailsResponse) response;
                    if (chatDetailsResponse != null) {
                        setMessage(chatDetailsResponse.status, chatDetailsResponse.msg);
                        if (chatDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(chatDetailsResponse.status, chatDetailsResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.CHAT + "?id=" + id + "&type=" + AppMoon.getUserType() + "&receiver_type=" + type, null, ChatDetailsResponse.class);
    }


    public void send(ChatRequest chatRequest, VolleyFileObject volleyFileObject, int id_chat) {
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
        if (volleyFileObject != null) {
            volleyFileObjects.add(volleyFileObject);
        }
        String url = "";
        if (id_chat == -1)
            url = URLS.CHAT_DETAILS_SUBMIT;
        else
            url = URLS.CHAT_SEND;

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatSendResponse = (ChatSendResponse) response;
                    if (chatSendResponse != null) {
                        setMessage(chatSendResponse.status, chatSendResponse.msg);
                        if (chatSendResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT_SEND);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(url, chatRequest, volleyFileObjects, ChatSendResponse.class);
    }

    private static final String TAG = "ChatRepository";

    public void getChatList() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatListResponse = (ChatListResponse) response;
                    if (chatListResponse != null) {
                        setMessage(chatListResponse.status, chatListResponse.msg);
                        if (chatListResponse.status == Constants.RESPONSE_SUCCESS) {

                            getmMutableLiveData().setValue(Constants.CHAT_LIST);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(chatListResponse.status, chatListResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.CHAT_GET + "?account_type=" + AppMoon.getUserType(), null, ChatListResponse.class);
    }


    public ChatDetailsResponse getChatDetailsResponse() {
        return chatDetailsResponse;
    }

    public ChatSendResponse getChatSendResponse() {
        return chatSendResponse;
    }

    public ChatListResponse getChatListResponse() {
        return chatListResponse;
    }
}



