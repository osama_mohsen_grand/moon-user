package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.settings.PrivacyTextResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class PrivacyRepository extends BaseRepository {
    PrivacyTextResponse privacyTextResponse = null;
    public PrivacyRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
        privacy();
    }
    public void privacy() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    privacyTextResponse = (PrivacyTextResponse) response;
                    if (privacyTextResponse != null) {
                        setMessage(privacyTextResponse.status, privacyTextResponse.msg);
                        if (privacyTextResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.PRIVACY_POLICY);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(privacyTextResponse.status, privacyTextResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.REGISTER_USER, null, PrivacyTextResponse.class);
    }

    public PrivacyTextResponse getPrivacyTextResponse() {
        return privacyTextResponse;
    }
}



