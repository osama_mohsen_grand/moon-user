package grand.app.moon.repository.service;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.service.tags.TagsResponse;
import grand.app.moon.repository.BaseRepository;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class TagsRepository extends BaseRepository {
    private TagsResponse tagsResponse = null;
    public TagsRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getTags(String service_id,int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tagsResponse = (TagsResponse) response;
                    if (tagsResponse != null) {
                        setMessage(tagsResponse.status,tagsResponse.msg);
                        if (tagsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.TAGS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(tagsResponse.status,tagsResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.TAGS+"?service_id="+service_id+"&type="+type, null, TagsResponse.class);
    }

    public TagsResponse getTagsResponse() {
        return tagsResponse;
    }
}



