package grand.app.moon.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.clinic.ClinicDetailsResponse;
import grand.app.moon.models.doctor.DoctorDetailsResponse;
import grand.app.moon.models.reservation.ReservationOrderActionRequest;
import grand.app.moon.models.reservation.ReservationRequest;
import grand.app.moon.models.reservation.ReservationResponse;
import grand.app.moon.models.reservation.detatils.ReservationDetailsResponse;
import grand.app.moon.models.service.clinic.ClinicServiceResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class ClinicRepository extends CommonRepository {

    StatusMsg statusMsg;
    ClinicServiceResponse clinicServiceResponse;
    ClinicDetailsResponse clinicDetailsResponse;
    DoctorDetailsResponse doctorDetailsResponse;
    ReservationResponse reservationResponse;
    ReservationDetailsResponse reservationDetailsResponse;
    public ClinicRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void serviceDetails(int type, int flag, int tab, String service_id, int filter, ArrayList<Integer> tags) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        String tags_selected = "";
        for (Integer tag : tags)
            tags_selected += "&tags[]=" + tag;

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    clinicServiceResponse = (ClinicServiceResponse) response;
                    if (clinicServiceResponse != null) {
                        setMessage(clinicServiceResponse.status, clinicServiceResponse.msg);
                        if (clinicServiceResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(clinicServiceResponse.status, clinicServiceResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.CLINIC_LIST + "?country_id=" + UserHelper.retrieveKey(Constants.COUNTRY_ID) + "&lat=" + UserHelper.retrieveKey(Constants.LAT) +
                "&lng=" + UserHelper.retrieveKey(Constants.LNG) + "&type=" + type + "&flag=" + flag +
                "&tab=" + tab + "&service_id=" + service_id + "&filter=" + filter + tags_selected, null, ClinicServiceResponse.class);
    }

    public ClinicServiceResponse getClinicServiceResponse() {
        return clinicServiceResponse;
    }

    public void clinicDetails(int id, int type,int special) {
        String url = "";
        if(special != -1){
            url = "&filter=1&specialist_id="+special;
        }
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    clinicDetailsResponse = (ClinicDetailsResponse) response;
                    if (clinicDetailsResponse != null) {
                        setMessage(clinicDetailsResponse.status, clinicDetailsResponse.msg);
                        if (clinicDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CLINIC_DETAILS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(clinicDetailsResponse.status, clinicDetailsResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.CLINIC_DETAILS + "?id=" + id + "&type=" + type+url, null, ClinicDetailsResponse.class);
    }

    public void doctorDetails(int id, String date) {
        String url = "?id=" + id;
        if (date != null && !date.equals(""))
            url += "&date=" + date;
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    doctorDetailsResponse = (DoctorDetailsResponse) response;
                    if (doctorDetailsResponse != null) {
                        setMessage(doctorDetailsResponse.status, doctorDetailsResponse.msg);
                        if (doctorDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DOCTOR_DETAILS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(doctorDetailsResponse.status, doctorDetailsResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.CLINIC_DOCTOR_DETAILS + url, null, DoctorDetailsResponse.class);
    }

    //CLINIC_RESERVATION_ORDER

    public void makeReservation(ReservationRequest reservationRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.POST, URLS.CLINIC_RESERVATION_ORDER , reservationRequest, StatusMsg.class);
    }


    public void getReservations(String status,int reserved_type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    reservationResponse = (ReservationResponse) response;
                    if (reservationResponse != null) {
                        setMessage(reservationResponse.status,reservationResponse.msg);
                        if (reservationResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET,URLS.CLINIC_RESERVATION_LIST+"?type="+UserHelper.getUserDetails().type+"&status="+status+"&reserved_type="+reserved_type, null, ReservationResponse.class);
    }

    public ReservationResponse getReservationResponse() {
        return reservationResponse;
    }

    public ClinicDetailsResponse getClinicDetailsResponse() {
        return clinicDetailsResponse;
    }

    public DoctorDetailsResponse getDoctorDetailsResponse() {
        return doctorDetailsResponse;
    }



    public void getReservationDetails(int order_id,int reserved_type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    reservationDetailsResponse = (ReservationDetailsResponse) response;
                    if (reservationDetailsResponse != null) {
                        setMessage(reservationDetailsResponse.status,reservationDetailsResponse.msg);
                        if (reservationDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.RESERVATION_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            if(reservationDetailsResponse.status == Constants.RESPONSE_405)
                                getmMutableLiveData().setValue(Constants.DELETE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.CLINIC_RESERVATION_DETAILS+"?order_id="+ order_id+"&type="+ UserHelper.getUserDetails().type+"&reserved_type="+ reserved_type, null, ReservationDetailsResponse.class);
    }

    public ReservationDetailsResponse getReservationDetailsResponse() {
        return reservationDetailsResponse;
    }


    public void reservationOrderAction(ReservationOrderActionRequest reservationOrderActionRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CONFIRM);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CLINIC_DELETE, reservationOrderActionRequest, StatusMsg.class);
    }


    @Override
    public StatusMsg getStatusMsg() {
        return statusMsg;
    }
}



