package grand.app.moon.repository;

import com.android.volley.Request;

import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.user.LogCrash;
import grand.app.moon.models.user.UpdateTokenRequest;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.fragments.base.MediaViewRequest;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class FirebaseRepository {
    public void updateToken(String token) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FIREBASE, new UpdateTokenRequest(token), StatusMsg.class);
    }
    public void logCrash(String text) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.POST, URLS.LOG_CRASH, new LogCrash(text), StatusMsg.class);
    }

    public void mediaView(MediaViewRequest request) {
        if (UserHelper.isLogin()) {
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {

                }

                @Override
                public void onRequestError(Object error) {
                    super.onRequestError(error);
                }
            }).requestJsonObject(Request.Method.POST, URLS.MEDIA_VIEW, request, StatusMsg.class);
        }
    }
}



