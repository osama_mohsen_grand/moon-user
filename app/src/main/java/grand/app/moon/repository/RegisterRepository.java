package grand.app.moon.repository;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.user.activation.VerificationResponse;
import grand.app.moon.models.user.login.LoginResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.models.user.register.RegisterUserRequest;
import grand.app.moon.vollyutils.URLS;
import grand.app.moon.vollyutils.VolleyFileObject;

public class RegisterRepository extends BaseRepository {
    LoginResponse loginResponse = null;
    public RegisterRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void registerUser(RegisterUserRequest firstRegister, VolleyFileObject volleyFileObject) {
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
        if(volleyFileObject != null)
        volleyFileObjects.add(volleyFileObject);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status,loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
                            getmMutableLiveData().setValue(Constants.REGISTRATION);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status,loginResponse.msg);
                        }
                    }
                }
            }
        }).multiPartConnect(URLS.REGISTER_USER, firstRegister,volleyFileObjects, LoginResponse.class);
    }

    public LoginResponse getLoginResponse() {
        return loginResponse;
    }

    VerificationResponse verificationResponse = null;

//    public void registerCar(SecondRegisterRequest secondRegisterRequest) {
//        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
//        new ConnectionHelper(new ConnectionListener() {
//            @Override
//            public void onRequestSuccess(Object response) {
//                if(!catchErrorResponse(response)) {
//                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
//                    verificationResponse = (VerificationResponse) response;
//                    if (verificationResponse != null) {
//                        setMessage(verificationResponse.status,verificationResponse.msg);
//                        if (verificationResponse.status == Constants.RESPONSE_SUCCESS) {
//                            getmMutableLiveData().setValue(Constants.REGISTRATION_CAR);
//                        }else {
//                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
//                            setMessage(verificationResponse.status,verificationResponse.msg);
//                        }
//                    }
//                }
//
//            }
//        }).requestJsonObject(Request.Method.POST, URLS.SECOND_REGISTER, secondRegisterRequest, VerificationResponse.class);
//    }


//    public void updateCar(SecondRegisterRequest secondRegisterRequest) {
//        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
//        new ConnectionHelper(new ConnectionListener() {
//            @Override
//            public void onRequestSuccess(Object response) {
//                if(!catchErrorResponse(response)) {
//                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
//                    statusMsg = (StatusMsg) response;
//                    if (statusMsg != null) {
//                        setMessage(statusMsg.status,statusMsg.msg);
//                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
//                            getmMutableLiveData().setValue(Constants.UPDATE_PROFILE);
//                        }else {
//                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
//                            setMessage(statusMsg.status,statusMsg.msg);
//                        }
//                    }
//                }
//
//            }
//        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PROFILE, secondRegisterRequest, StatusMsg.class);
//    }

}



