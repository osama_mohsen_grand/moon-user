package grand.app.moon.repository;

import android.util.Log;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.notifications.NotificationResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class NotificationRepository extends BaseRepository {
    NotificationResponse notificationResponse = null;
    public NotificationRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    private static final String TAG = "NotificationRepository";
    public void getNotification() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    notificationResponse = (NotificationResponse) response;
                    Log.d(TAG,"start here");
                    if (notificationResponse != null) {
                        Log.d(TAG,notificationResponse.status+"");
                        setMessage(notificationResponse.status,notificationResponse.msg);
                        if (notificationResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.NOTIFICATION);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(notificationResponse.status,notificationResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.NOTIFICATION+"?type="+UserHelper.getUserDetails().type, null, NotificationResponse.class);
    }

    public void deleteNotification(int id) {
//        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
//                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                }

            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_NOTIFICATION+id, null, NotificationResponse.class);
    }

    public NotificationResponse getData() {
        return notificationResponse;
    }
}



