package grand.app.moon.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.ads.companyAdsCategory.CompanyAdsCategoriesResponse;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.famous.advertise.AdvertiseDetailsResponse;
import grand.app.moon.models.famous.album.FamousAddImageInsideAlbumRequest;
import grand.app.moon.models.famous.album.FamousAlbumImagesResponse;
import grand.app.moon.models.famous.details.FamousDetailsResponse;
import grand.app.moon.models.famous.list.FamousListResponse;
import grand.app.moon.models.famous.search.FamousSearchShopResponse;
import grand.app.moon.models.shop.FollowRequest;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.views.fragments.ads.FamousCompanyAdsCategoriesFragment;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;
import grand.app.moon.vollyutils.VolleyFileObject;

public class FamousRepository extends BaseRepository {
    FamousListResponse famousListResponse = null;
    FamousDetailsResponse famousDetailsResponse = null;
    AdvertiseDetailsResponse advertiseDetailsResponse;
    FamousSearchShopResponse famousSearchShopResponse;
    FamousAlbumImagesResponse famousAlbumImagesResponse;
    StatusMsg statusMsg;
    public FamousRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }



    public void getAlbumImages(int id,int famous_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousAlbumImagesResponse = (FamousAlbumImagesResponse) response;
                    if (famousAlbumImagesResponse != null) {
                        setMessage(famousAlbumImagesResponse.status,famousAlbumImagesResponse.msg);
                        if (famousAlbumImagesResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ALBUM_IMAGES +"?album_id="+id+"&shop_id="+famous_id,
                null, FamousAlbumImagesResponse.class);
    }

    public void getShopAds(int id,int famous_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousAlbumImagesResponse = (FamousAlbumImagesResponse) response;
                    if (famousAlbumImagesResponse != null) {
                        setMessage(famousAlbumImagesResponse.status,famousAlbumImagesResponse.msg);
                        if (famousAlbumImagesResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ALBUM_IMAGES +"?id="+id+"&shop_id="+famous_id,
                null, FamousAlbumImagesResponse.class);
    }


    public void getFamousList(int type, int gender, int photographerTypeId) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousListResponse = (FamousListResponse) response;
                    if (famousListResponse != null) {
                        setMessage(famousListResponse.status,famousListResponse.msg);
                        if (famousListResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS+"?type="+type+"&gender="+gender+"&country_id="+UserHelper.retrieveKey(Constants.COUNTRY_ID)+"&flag="+photographerTypeId, null, FamousListResponse.class);
    }

    public void getShopByName(String name) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousSearchShopResponse = (FamousSearchShopResponse) response;
                    if (famousSearchShopResponse != null) {
                        setMessage(famousSearchShopResponse.status,famousSearchShopResponse.msg);
                        if (famousSearchShopResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SEARCH);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_SEARCH_SHOP+"?name="+name, null, FamousSearchShopResponse.class);
    }



    public void getFamousDetails(int id , int tab, int type) {
//        id = 4 ; type = 2; //TODO
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousDetailsResponse = (FamousDetailsResponse) response;
                    if (famousDetailsResponse != null) {
                        setMessage(famousDetailsResponse.status,famousDetailsResponse.msg);
                        if (famousDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_DETAILS+"?id="+id+"&tab="+tab+"&account_type="+ UserHelper.getUserDetails().type+"&type="+type, null, FamousDetailsResponse.class);
    }




    public void follow(int id,int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FAMOUS_FOLLOW);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FOLLOW , new FollowRequest(id,type), StatusMsg.class);
    }

    public void getAdvertiseDetails(int id , int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    advertiseDetailsResponse = (AdvertiseDetailsResponse) response;
                    if (advertiseDetailsResponse != null) {
                        setMessage(advertiseDetailsResponse.status,advertiseDetailsResponse.msg);
                        if (advertiseDetailsResponse.status == Constants.RESPONSE_SUCCESS) {

                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADVERTISE_DETAILS +"?id="+id+"&type="+type,
                null, AdvertiseDetailsResponse.class);
    }

    public AdvertiseDetailsResponse getAdvertiseDetailsResponse() {
        return advertiseDetailsResponse;
    }

    public FamousDetailsResponse getFamousDetailsResponse() {
        return famousDetailsResponse;
    }


    public FamousListResponse getFamousListResponse() {
        return famousListResponse;
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public FamousSearchShopResponse getFamousSearchShopResponse() {
        return famousSearchShopResponse;
    }

    public FamousAlbumImagesResponse getFamousAlbumImagesResponse() {
        return famousAlbumImagesResponse;
    }

    public void removeAlbumImage(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETE);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.FAMOUS_DELETE_IMAGE +"/"+id,
                null, StatusMsg.class);
    }


    //Add Image To Album
    public void addImageToAlbum(FamousAddImageInsideAlbumRequest famousAddImageInsideAlbumRequest, ArrayList<VolleyFileObject> volleyFileObject) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DONE);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).multiPartConnect(URLS.FAMOUS_EDIT_ALBUM ,
                famousAddImageInsideAlbumRequest,volleyFileObject, StatusMsg.class);
    }


    public void getFamousDiscover(int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousAlbumImagesResponse = (FamousAlbumImagesResponse) response;
                    if (famousAlbumImagesResponse != null) {
                        setMessage(famousAlbumImagesResponse.status,famousAlbumImagesResponse.msg);
                        if (famousAlbumImagesResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_DISCOVER+"?type="+type+"&country_id="+UserHelper.retrieveKey(Constants.COUNTRY_ID) ,
                null, FamousAlbumImagesResponse.class);
    }

    public CompanyAdsCategoriesResponse companyAdsCategoriesResponse;

    public void getCompanyAdsCategories(int famous_id, int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    companyAdsCategoriesResponse = (CompanyAdsCategoriesResponse) response;
                    if (companyAdsCategoriesResponse != null) {
                        setMessage(companyAdsCategoriesResponse.status,companyAdsCategoriesResponse.msg);
                        if (companyAdsCategoriesResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_COMPANY_ADS_CATEGORIES+"?id="+famous_id+"&category_id="+id ,
                null, CompanyAdsCategoriesResponse.class);
    }
}



