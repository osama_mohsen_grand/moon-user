package grand.app.moon.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.user.changepassword.ChangePasswordRequest;
import grand.app.moon.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moon.models.user.forgetpassword.ForgetPasswordResponse;
import grand.app.moon.models.user.login.FacebookLoginRequest;
import grand.app.moon.models.user.login.LoginRequest;
import grand.app.moon.models.user.login.LoginResponse;
import grand.app.moon.models.user.profile.ProfileRequest;
import grand.app.moon.models.user.register.RegisterUserRequest;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;
import grand.app.moon.vollyutils.VolleyFileObject;

public class LoginRepository extends BaseRepository {
    LoginResponse loginResponse = null;
    ForgetPasswordResponse forgetPasswordResponse = null;

    private static final String TAG = "LoginRepository";

    public LoginRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void loginUser(LoginRequest loginRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status, loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
                            getmMutableLiveData().setValue(Constants.HOME);
//                            if (loginResponse.data.trip == null) {
//                                UserHelper.clearTripDetails();
//                                getmMutableLiveData().setValue(Constants.HOME);
//                            } else {
//                                UserHelper.saveTripDetails(loginResponse.data.trip);
//                                getmMutableLiveData().setValue(Constants.TRIP);
//                            }
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status, loginResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.LOGIN, loginRequest, LoginResponse.class);
    }

    public void loginSocial(LoginRequest request) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        UserHelper.saveUserDetails(loginResponse.data);
                        getmMutableLiveData().setValue(Constants.HOME);
                    }
                }
            }
        }).requestJsonObject(Request.Method.POST, URLS.REGISTER_USER, request, LoginResponse.class);
    }


    public LoginResponse getUser() {
        return loginResponse;
    }


    public StatusMsg statusMsg;

    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.CHANGE_PASSWORD, changePasswordRequest, StatusMsg.class);
    }

    public void checkPhone(ForgetPasswordRequest forgetPasswordRequest) {

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    forgetPasswordResponse = (ForgetPasswordResponse) response;
                    if (forgetPasswordResponse != null) {
                        setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        if ((forgetPasswordResponse.status == Constants.RESPONSE_SUCCESS && forgetPasswordRequest.kind.equals(Constants.REGISTRATION))
                                || (forgetPasswordResponse.status == Constants.RESPONSE_401 && forgetPasswordRequest.kind.equals(Constants.FORGET_PASSWORD))) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.CHECK_PHONE, forgetPasswordRequest, ForgetPasswordResponse.class);
    }

    public void updateProfile(ProfileRequest profileRequest, VolleyFileObject volleyFileObject) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();
        volleyFileObjects.add(volleyFileObject);


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status, loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status, loginResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.UPDATE_PROFILE, profileRequest, volleyFileObjects, LoginResponse.class);
    }

    public void updateProfile(ProfileRequest profileRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status, loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status, loginResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PROFILE, profileRequest, LoginResponse.class);
    }


    public ForgetPasswordResponse getForgetPasswordResponse() {
        return forgetPasswordResponse;
    }

    public void checkSocialIdExist(FacebookLoginRequest facebookLoginRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                loginResponse = (LoginResponse) response;
                if (loginResponse != null) {
                    setMessage(loginResponse.status, loginResponse.msg);
                    if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                        UserHelper.saveUserDetails(loginResponse.data);
                        getmMutableLiveData().setValue(Constants.SUCCESS);
                    } else {
                        getmMutableLiveData().setValue(Constants.UNAUTHORIZED);
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FACEBOOK_LOGIN, facebookLoginRequest, LoginResponse.class);
    }
}



