package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.trip.TripActionRequest;
import grand.app.moon.models.trip.TripDetailsResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;

public class TripRepository extends BaseRepository {
    TripDetailsResponse tripDetailsResponse = null;
    StatusMsg statusMsg;

    public TripRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void getDetails(int trip_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tripDetailsResponse = (TripDetailsResponse) response;
                    if (tripDetailsResponse != null) {
                        setMessage(tripDetailsResponse.status, tripDetailsResponse.msg);
                        if (tripDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.TRIP_DETAILS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(tripDetailsResponse.status, tripDetailsResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.TRIP_DETAILS+"?type="+ UserHelper.getUserDetails().type+"&id="+trip_id, null, TripDetailsResponse.class);
    }


    public void tripAction(TripActionRequest tripActionRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CANCELED);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.TRIP_ACTION, tripActionRequest, StatusMsg.class);
    }


    public TripDetailsResponse getTripDetailsResponse() {
        return tripDetailsResponse;
    }
}