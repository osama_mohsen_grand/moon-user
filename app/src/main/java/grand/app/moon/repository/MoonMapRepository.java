package grand.app.moon.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.home.response.ViewStoryRequest;
import grand.app.moon.models.map.MoonMapResponse;
import grand.app.moon.models.service.ServiceDetailsResponse;
import grand.app.moon.models.service.ServiceMapDetailsResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class MoonMapRepository extends CommonRepository {
    MoonMapResponse moonMapResponse = null;
    ServiceDetailsResponse serviceDetailsResponse = null;
    ServiceMapDetailsResponse serviceMapDetailsResponse = null;
    public MoonMapRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getStories() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    moonMapResponse = (MoonMapResponse) response;
                    if (moonMapResponse != null) {
                        setMessage(moonMapResponse.status,moonMapResponse.msg);
                        if (moonMapResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(moonMapResponse.status,moonMapResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.STORIES+"?country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID), null, MoonMapResponse.class);
    }

    public void viewStory(ArrayList<String> storiesId) {
        if(UserHelper.getUserId() != -1) {
            getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    if (!catchErrorResponse(response)) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        moonMapResponse = (MoonMapResponse) response;
                        if (moonMapResponse != null) {
                            setMessage(moonMapResponse.status, moonMapResponse.msg);
                            if (moonMapResponse.status == Constants.RESPONSE_SUCCESS) {
                                getmMutableLiveData().setValue(Constants.SUCCESS);
                            } else {
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                                setMessage(moonMapResponse.status, moonMapResponse.msg);
                            }
                        }
                    }

                }
            }).requestJsonObject(Request.Method.POST, URLS.STORY_VIEW , new ViewStoryRequest(storiesId), MoonMapResponse.class);
        }
    }

    public MoonMapResponse getMoonMapResponse() {
        return moonMapResponse;
    }
}



