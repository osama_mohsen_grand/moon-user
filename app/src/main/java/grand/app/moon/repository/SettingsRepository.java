package grand.app.moon.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moon.models.base.StatusMsg;
import grand.app.moon.models.contact.ContactUsRequest;
import grand.app.moon.models.personalnfo.AccountInfoResponse;
import grand.app.moon.models.settings.SettingsRequest;
import grand.app.moon.models.settings.SettingsResponse;
import grand.app.moon.utils.Constants;
import grand.app.moon.vollyutils.ConnectionHelper;
import grand.app.moon.vollyutils.ConnectionListener;
import grand.app.moon.vollyutils.URLS;


public class SettingsRepository extends BaseRepository {
    public SettingsResponse settingsResponse = null;
    StatusMsg statusMsg;

    public SettingsRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getSettings(int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    settingsResponse = (SettingsResponse) response;
                    if (settingsResponse != null) {
                        setMessage(settingsResponse.status, settingsResponse.msg);
                        if (settingsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(settingsResponse.status, settingsResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.SETTINGS, new SettingsRequest(type), SettingsResponse.class);
    }

    public void contactUs(ContactUsRequest request) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CONTACT_US, request, StatusMsg.class);
    }

    public AccountInfoResponse accountInfoResponse;

    private static final String TAG = "SettingsRepository";

    public void accountInfo(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    accountInfoResponse = (AccountInfoResponse) response;
                    if (accountInfoResponse != null) {
                        setMessage(accountInfoResponse.status, accountInfoResponse.msg);
                        if (accountInfoResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ACCOUNT_INFO);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(accountInfoResponse.status, accountInfoResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ACCOUNT_INFO + "id=" + id, null, AccountInfoResponse.class);
    }
}



