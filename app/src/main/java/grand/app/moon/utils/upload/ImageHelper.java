package grand.app.moon.utils.upload;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import grand.app.moon.vollyutils.VolleyFileObject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

public class ImageHelper {
    //  400 is good
    public static Bitmap getResizedBitmap(Context context, Uri u) {
        int maxSize = 400;
        InputStream imageStream = null;
        try {
            imageStream = context.getContentResolver().openInputStream(u);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap image = BitmapFactory.decodeStream(imageStream);

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

//    public static Bitmap getResizedBitmap(File file) {
//        int maxSize = 400;
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),bmOptions);
//
//        int width = bitmap.getWidth();
//        int height = bitmap.getHeight();
//
//        float bitmapRatio = (float)width / (float) height;
//        if (bitmapRatio > 1) {
//            width = maxSize;
//            height = (int) (width / bitmapRatio);
//        } else {
//            height = maxSize;
//            width = (int) (height * bitmapRatio);
//        }
//        return Bitmap.createScaledBitmap(bitmap, width, height, true);
//    }

//    public static VolleyFileObject compressImage(VolleyFileObject volleyFileObject) {
//        File imageFile = new File(volleyFileObject.getFilePath());
//        Bitmap bitmap = getResizedBitmap(imageFile);
//        volleyFileObject.setBitmap(bitmap);
//        OutputStream os;
//        try {
//            os = new FileOutputStream(imageFile);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, os);
//            os.flush();
//            os.close();
//        } catch (Exception e) {
//            Log.e("err_compress_image",e.getMessage());
//        }
//        volleyFileObject.setFilePath(imageFile.getAbsolutePath());
//        volleyFileObject.setFile(imageFile);
//        volleyFileObject.setBitmap(bitmap);
//        return volleyFileObject;
//    }


    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static MultipartBody.Part createMultiPartFile(Context context, Fragment fragment, Uri u){
        Bitmap bitmap = ImageHelper.getResizedBitmap(context,u);
        File file = FilesUtilsFragment.getFile(fragment, ImageHelper.getImageUri(context,bitmap));
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileMultiPart = MultipartBody.Part.createFormData("file", file.getName(), mFile);
        return fileMultiPart;
    }

}
