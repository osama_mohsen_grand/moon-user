package grand.app.moon.utils.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.FilterAdapter;
import grand.app.moon.databinding.CustomFilterBinding;
import grand.app.moon.databinding.CustomFilterFlexBinding;
import grand.app.moon.databinding.CustomScheduleRideBinding;
import grand.app.moon.models.base.IdName;
import grand.app.moon.models.base.IdNameDescription;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.schedule.ScheduleViewModel;

public class BottomSheetFlexDialogHelper {

    Activity activity;
    public String date_server = "";
    public String date_select = "";
    public String time_server = "";
    public BottomSheetDialog calenderDialog = null;
    private boolean allowMultipleSelected;
    private static final String TAG = "BottomSheetDialogHelper";

    public BottomSheetFlexDialogHelper(Activity activity) {
        this.activity = activity;
    }

    public BottomSheetDialog filterDialog = null;
    CustomFilterFlexBinding binding = null;
    List<IdNameDescription> AllResult,dataSearch;
    public ArrayList<Integer> selected = new ArrayList<>();

    public void addTextView(IdNameDescription idNameDescription){
        FlexboxLayout flexboxLayout = binding.filterBoxContainer;

        TextView textView = new TextView(flexboxLayout.getContext());
        FlexboxLayout.LayoutParams params = new FlexboxLayout.LayoutParams(
                FlexboxLayout.LayoutParams.WRAP_CONTENT,
                FlexboxLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(12, 12, 12, 12);
        Log.d(TAG,"DONE 1");
        textView.setBackgroundResource(R.drawable.border_background_primary_strock);
        textView.setTextSize(15f);
        textView.setPadding(30, 20, 30, 20);
        textView.setLayoutParams(params);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            textView.setTextColor(ContextCompat.getColor(flexboxLayout.getContext(), R.color.colorPrimaryDark));
        } else{
            textView.setTextColor(flexboxLayout.getContext().getResources().getColor(R.color.colorPrimaryDark));
        }
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!allowMultipleSelected) clearAllSelected();
                if(!selected.contains(idNameDescription.id)) {
                    selected.add(idNameDescription.id);
                    textView.setBackgroundResource(R.drawable.border_background_primary);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
                        textView.setTextColor(ContextCompat.getColor(flexboxLayout.getContext(), R.color.colorWhite));
                    } else{
                        textView.setTextColor(flexboxLayout.getContext().getResources().getColor(R.color.colorWhite));
                    }
                }else {
                    selected.remove(selected.indexOf(idNameDescription.id));
                    textView.setBackgroundResource(R.drawable.border_background_primary_strock);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
                        textView.setTextColor(ContextCompat.getColor(flexboxLayout.getContext(), R.color.colorPrimaryDark));
                    } else{
                        textView.setTextColor(flexboxLayout.getContext().getResources().getColor(R.color.colorPrimaryDark));
                    }
                }
            }
        });


//        Log.d(TAG,"DONE 2");
//        textView.setTextColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        textView.setText(idNameDescription.name);
        flexboxLayout.addView(textView);
    }

    private void clearAllSelected() {
        selected.clear();
        FlexboxLayout flexboxLayout = binding.filterBoxContainer;
        for (int i = 0; i < flexboxLayout.getChildCount(); i++) {
            TextView textView = (TextView) flexboxLayout.getChildAt(i);
            textView.setBackgroundResource(R.drawable.border_background_primary_strock);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
                textView.setTextColor(ContextCompat.getColor(flexboxLayout.getContext(), R.color.colorPrimaryDark));
            } else{
                textView.setTextColor(flexboxLayout.getContext().getResources().getColor(R.color.colorPrimaryDark));
            }
        }
    }

    public void addTexts(List< IdNameDescription > tmp){
        FlexboxLayout flexboxLayout = binding.filterBoxContainer;
        if(flexboxLayout.getChildCount() > 0) flexboxLayout.removeAllViews();
        Log.d("FILTER",tmp.size()+"");
        for(int i=0;i<tmp.size();i++)
            addTextView(tmp.get(i));
    }

    public void filterFamousShopAds(List<IdNameDescription> data,boolean allowMultipleSelected, DialogHelperSelectedInterface dialogHelperInterface) {
        this.allowMultipleSelected = allowMultipleSelected;
        if (filterDialog == null) {
            AllResult = new ArrayList<>(data);
            LayoutInflater layoutInflater = LayoutInflater.from(activity);
            binding = DataBindingUtil.inflate(layoutInflater, R.layout.custom_filter_flex, null, true);
            addTexts(AllResult);
            binding.search.setActivated(true);
            binding.search.setQueryHint(ResourceManager.getString(R.string.search_hint));
            binding.search.onActionViewExpanded();
            binding.search.setIconified(false);
            binding.search.clearFocus();

            binding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    if (AllResult != null) {
                        if (!s.equals("")) {
                            dataSearch = new ArrayList<>();
                            for (IdNameDescription item : AllResult) {
                                if (item.name.contains(s))
                                    dataSearch.add(item);
                            }
                        } else {
                            dataSearch = new ArrayList<>(AllResult);
                        }
                        addTexts(dataSearch);
                    }
                    return false;
                }
            });
            View customView = binding.getRoot();
            filterDialog = new BottomSheetDialog(activity);
            binding.tvReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearAllSelected();
                }
            });
            binding.imgDialogClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterDialog.dismiss();
                }
            });

            binding.tvFilterSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterDialog.dismiss();
                    dialogHelperInterface.onClickListener(filterDialog,Constants.ARRAY,selected);
                }
            });
            filterDialog.setContentView(customView);
        }
        filterDialog.show();
    }
}
