package grand.app.moon.utils.maputils.tracking;

public class FireToken {
    private String token;

    public FireToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
