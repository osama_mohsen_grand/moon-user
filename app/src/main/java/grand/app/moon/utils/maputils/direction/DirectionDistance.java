package grand.app.moon.utils.maputils.direction;

public interface DirectionDistance {
    public void receive(int distanceM,int timePerSec);
}
