package grand.app.moon.utils.maputils.base;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import de.hdodenhof.circleimageview.CircleImageView;
import grand.app.moon.R;
import grand.app.moon.databinding.CustomMarkerLayoutBinding;
import grand.app.moon.models.helper.MarkerHelper;
import grand.app.moon.models.home.response.Story;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.images.ImageLoaderHelper;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.utils.storage.SharedPreferenceHelper;
import grand.app.moon.viewmodels.story.ItemStoryViewModel;
import grand.app.moon.vollyutils.MyApplication;

public class MapConfig {
    public List<LatLng> markers_service = new ArrayList<>();
    public ArrayList<Marker> markers = new ArrayList<>();
    private static final String TAG = "MapConfig";
    Context context;
    GoogleMap mMap;
    Marker driverMarker;
    public Polyline direction;
    public double distanceKm;
    public HashMap<Marker, Object> markersId = new HashMap<>();

    public MapConfig(Context context, GoogleMap map) {
        this.context = context;
        this.mMap = map;
    }

    public MapConfig() {
    }

    public void setMapStyle() {
        try {
            boolean isSuccess = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map));
            if (!isSuccess)
                Log.e("errorMapStyle", "errorMap");
        } catch (Resources.NotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public void setSettings() {
        if (isVisible()) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        mMap.setTrafficEnabled(true);
//        mMap.setBuildingsEnabled(true);
            mMap.setIndoorEnabled(true);
            mMap.getUiSettings().setRotateGesturesEnabled(false);
        }
//        mMap.getUiSettings().setZoomControlsEnabled(true);
//        mMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    public void setPadding(int left, int top, int right, int bottom) {
        if (mMap != null) mMap.setPadding(left, top, right, bottom);
    }

    public void setPaddingRunTime(int left, int top, int right, int bottom) {
        if (mMap != null) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    getGoogleMap().setPadding(left, top, right, bottom);
                }
            });

        }
    }

    public void setDriverMarker(LatLng driverLatLng) {
        SharedPreferenceHelper.saveKey(Constants.DRIVER_LAT, "" + driverLatLng.latitude);
        SharedPreferenceHelper.saveKey(Constants.DRIVER_LNG, "" + driverLatLng.longitude);
//        if(driverMarker == null || !driverMarker.isVisible()) {
        if (driverMarker != null) driverMarker.remove();
        driverMarker = addMarker(driverLatLng, R.drawable.ic_pin_car3, "");
//        }
    }

    public void enableLocationButton() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.setOnMyLocationButtonClickListener(() -> true);
    }

    public void saveLastLocation(LatLng location) {
        SharedPreferenceHelper.saveKey(Constants.LAT, String.valueOf(location.latitude));
        SharedPreferenceHelper.saveKey(Constants.LNG, String.valueOf(location.longitude));
    }

    public void checkLastLocation() {
        if (!SharedPreferenceHelper.getKey(Constants.LAT).equals("") && !SharedPreferenceHelper.getKey(Constants.LAT).equals(""))
            moveCamera(new LatLng(Double.parseDouble(SharedPreferenceHelper.getKey(Constants.LAT)), Double.parseDouble(SharedPreferenceHelper.getKey(Constants.LNG))));
    }

    public void moveCamera(LatLng latLng) {
        if (isVisible())
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
    }

    public void moveCamera(ArrayList<LatLng> latLngs) {
        if (latLngs.size() > 0) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LatLng latLng : latLngs) {
                builder.include(latLng);
            }
//            int padding = 400; // offset from edges of the map in pixels
            int padding = 100; // offset from edges of the map in pixels
            LatLngBounds bounds = builder.build();
            final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            /**call the map call back to know map is loaded or not*/
            getGoogleMap().setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    /**set animated zoom camera into map*/
                    getGoogleMap().animateCamera(cu);
                }
            });
        }
    }

    public void setRoute(Polyline direction) {
        this.direction = direction;
    }

    public void removeRoute() {
        if (direction != null)
            direction.remove();
    }

    public Marker getDriverMarker() {
        return driverMarker;
    }


    public void moveCamera(List<MarkerHelper> points) {
        markers.clear();
        int counter = 0;
        for (MarkerHelper point : points) {
            if (point != null && point.latLng != null && point.latLng.latitude != 0 && point.latLng.longitude != 0) {
                int resource = (counter == 0 ? R.drawable.ic_map_pin : R.drawable.ic_marker_dest);
                Marker marker = mMap.addMarker(new MarkerOptions().position(point.latLng)
                        .icon(BitmapDescriptorFactory.fromResource(resource))
                        .title(point.text_select));
                ;
                markers.add(marker);
            }
            counter++;
        }
        int padding = 50; // offset from edges of the map in pixels
        if (markers.size() > 0) {
            zoomCamera(50);
        }
    }


    public Marker addMarker(LatLng latLng, int icon, String title) {
        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(icon))
                .title(title));
        markers.add(marker);
        return marker;
    }

    public Marker addMarker(LatLng latLng, int icon, String title,String snipped) {
        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(icon))
                .title(title)
        .snippet(snipped));
        markers.add(marker);
        return marker;
    }


    public ArrayList<Marker> getMarkers() {
        return markers;
    }

    public void clearMarkers() {
        markers.clear();
    }

    public void changeMyLocationButtonLocation(MapView mapView) {
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);

            layoutParams.setMargins(0, 0, 0, 0);
        }
    }

    public void clear() {
        markersId.clear();
        markers.clear();
        getGoogleMap().clear();
    }

    public LatLngBounds zoomCamera(int padding) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);
        return bounds;
    }

    public void navigation(double lat, double lng) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lng));
        context.startActivity(intent);
    }

    public void navigation(LatLng latLngPicker, LatLng latLngDestination) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + latLngPicker.latitude + "," + latLngPicker.longitude + "&daddr=" + latLngDestination.latitude + "," + latLngDestination.longitude + ""));
        context.startActivity(intent);
    }

    public void setLocationButtonListeners() {
        mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyLocationClickListener);
    }

    private LocationRequest mLocationRequest;
    private static int UPDATE_INTERVAL = 5000 * 60;
    private static int FATEST_INTERVAL = 3000;
    private static int DISPLACEMENT = 10;


    public void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    public LocationRequest getmLocationRequest() {
        if (mLocationRequest == null) {
            createLocationRequest();
        }
        return mLocationRequest;
    }

    public void setmLocationRequest(LocationRequest mLocationRequest) {
        this.mLocationRequest = mLocationRequest;
    }

    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {
                    Log.e(TAG, "onMyLocationClick: " + location.getLatitude() + "," + location.getLongitude());
                }
            };

    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            () -> {
//                    mMap.setMinZoomPreference(15);
                return false;
            };


    public GoogleMap getGoogleMap() {
        return mMap;
    }

    private ArrayList<LatLng> latLngPointsDirections = new ArrayList();

    public void setLatLngPointsDirections(ArrayList<LatLng> pointsDirections) {
        latLngPointsDirections = new ArrayList(pointsDirections);
    }

    public ArrayList<LatLng> getLatLngPointsDirections() {
        return latLngPointsDirections;
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    public void addMarker(LatLng latLng, Object id, String url, String title, ImageLoaderHelper.ImageMarkerListeners imageMarkerListeners) {
        View markerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        CustomMarkerLayoutBinding binding = DataBindingUtil.inflate(((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)), R.layout.custom_marker_layout,
                null, false);


        if (isVisible()) {
            Log.d(TAG, url);
            ImageLoaderHelper.ImageLoaderLoad(context, url, new ImageLoaderHelper.ImageListeners() {
                @Override
                public void isLoaded(Bitmap bitmap) {
                    if (isVisible()) {
                        Story story = new Story();
                        ItemStoryViewModel itemStoryViewModel = new ItemStoryViewModel(story, -1);
                        binding.setItemStoryViewModel(itemStoryViewModel);
                        Log.d(TAG, "lat " + latLng.latitude);
                        Log.d(TAG, "lng " + latLng.longitude);

                        CircleImageView circleImageView = markerView.findViewById(R.id.image);
                        circleImageView.setImageBitmap(bitmap);
//                markerImage.setImageBitmap(bitmap);
                        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(context, markerView)))
                                .title(title));

                        markers.add(marker);
                        markersId.put(marker, id);
                        imageMarkerListeners.isLoaded(marker);
                    }
                }
            });
        }
    }


    public void addMarker(Story story, Object i, ImageLoaderHelper.ImageMarkerListeners imageMarkerListeners) {
        View markerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);


        if (isVisible()) {
            Log.d(TAG, story.mImage);
            ImageLoaderHelper.ImageLoaderLoad(context, story.mImage, new ImageLoaderHelper.ImageListeners() {
                @Override
                public void isLoaded(Bitmap bitmap) {
                    if (isVisible()) {
                        ImageView imageView = markerView.findViewById(R.id.image);
                        Bitmap resized = Bitmap.createScaledBitmap(bitmap, 90, 90, true);
                        BitmapDescriptor icon = BitmapDescriptorFactory
                                .fromBitmap(getCircleBitmap(resized));


                        ImageLoader.getInstance().displayImage(
                                story.mImage, imageView,
                                new SimpleImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {
                                    }

                                    @Override
                                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                        imageView.setImageDrawable(ResourceManager.getDrawable(R.mipmap.ic_launcher));
                                    }

                                    @Override
                                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                        if (story.storyView == 0)
                                            imageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_gradient));
                                        else
                                            imageView.setBackground(ResourceManager.getDrawable(R.drawable.border_story_silver));
                                        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(story.mLat, story.mLng))
                                                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(context, markerView)))
                                                .title(story.mName));
                                        marker.setTag(i);
                                        markers.add(marker);
                                        markersId.put(marker, i);
                                        imageMarkerListeners.isLoaded(marker);
                                    }
                                }
                        );


                    }
                }
            });
        }
    }

    public Boolean isVisible() {
        if (context == null || mMap == null) return false;
        return true;
    }

    public void destroy() {
        context = null;
        mMap = null;
    }

    int counter = 0;
    public ArrayList<MarkerOptions> markersData = new ArrayList<>();

    public void addMarkerMarkerOptions(ArrayList<CustomMarker> customMarkers, ImageLoaderHelper.MarkersLoaded markersLoaded) {
        markersData.clear();
        counter = 0;
        if (isVisible()) {
            for (CustomMarker customMarker : customMarkers) {
                View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
                CircleImageView markerImage = marker.findViewById(R.id.image);
                if (isVisible()) {
                    ImageLoaderHelper.ImageLoaderLoad(context, customMarker.url, new ImageLoaderHelper.ImageBitmapListeners() {
                        @Override
                        public void isLoaded(Bitmap bitmap) {
                            if (isVisible()) {
                                Bitmap resized = Bitmap.createScaledBitmap(bitmap, 90, 90, true);
                                markerImage.setImageBitmap(resized);
                                MarkerOptions markerOptions = new MarkerOptions().position(customMarker.latLng)
                                        .icon(BitmapDescriptorFactory.fromBitmap(resized))
                                        .title(customMarker.name);
                                markersData.add(markerOptions);
                                counter++;
                                if (counter == customMarkers.size())
                                    markersLoaded.isLoadedAllMarkers(markersData);
                            }
                        }

                        @Override
                        public void failed() {
                            if (isVisible()) {
                                counter++;
                                if (counter == customMarkers.size())
                                    markersLoaded.isLoadedAllMarkers(markersData);
                            }
                        }
                    });

                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
                    marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
                    marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
                    marker.buildDrawingCache();
                    Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    marker.draw(canvas);
                }
            }
        }
    }

    public Object getId(Marker marker) {
        return markersId.get(marker);
    }

    public String getAddress(double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses = null;
        String address = "";
        geocoder = new Geocoder(MyApplication.getInstance(), Locale.ENGLISH);
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.size() > 0 && addresses.get(0).getAddressLine(0) != null) {
                address = addresses.get(0).getAddressLine(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (address.equals("")) address = ResourceManager.getString(R.string.your_location);
        return address;
    }

    public Double setDistanceWithKilo(int distance_meters) {
        distanceKm = (distance_meters / 1000); //by kilo
        return distanceKm;
    }


//    public class DownloadImagesTask extends AsyncTask<ImageView, Void, Bitmap> {
//
//        ImageView imageView = null;
//
//        @Override
//        protected Bitmap doInBackground(ImageView... imageViews) {
//            this.imageView = imageViews[0];
//            return download_Image((String)imageView.getTag());
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap result) {
//            imageView.setImageBitmap(result);
//        }
//
//
//        private Bitmap download_Image(String url) {
//
//        }
//    }
}
