package grand.app.moon.utils.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableInt;
import grand.app.moon.R;


/**
 * Created by osama on 12/16/2017.
 */

public class ImageLoaderHelper {

    // compile 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
    public ImageLoaderHelper(Context context) {
        DisplayImageOptions imageOptions = new DisplayImageOptions.Builder()
//                .cacheInMemory(true)
//                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(imageOptions).build();


        ImageLoader.getInstance().init(config);
    }

    public static void ImageLoaderLoad(final Context context, String url, final ImageView image, final ObservableInt avi){
        ImageLoader.getInstance().displayImage(
                url,image,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        if(avi != null) avi.set(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        if(avi != null) avi.set(View.GONE);
                        image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if(avi != null) avi.set(View.GONE);
                    }
                }
        );
    }

    public static void ImageLoaderLoad(final Context context, String url, final ImageView image, final ProgressBar progressBar){
        ImageLoader.getInstance().displayImage(
                url,image,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        if(context != null && progressBar != null)
                           progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        if(context != null && progressBar != null)
                            progressBar.setVisibility(View.GONE);
                        image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if(context != null && progressBar != null)
                            progressBar.setVisibility(View.GONE);
                    }
                }
        );
    }


    public static void ImageLoaderLoad(final Context context, String url, final ImageView image){
        ImageLoader.getInstance().displayImage(
                url,image,
                new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        image.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_launcher));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    }
                }
        );
    }

    private static final String TAG = "ImageLoaderHelper";
    //here marker inage
    public static void ImageLoaderLoad(final Context context, String url,ImageListeners imageListeners ){
        Log.d(TAG,"url:"+url);
        ImageLoader.getInstance().loadImage(url,new ImageLoadingListener(){
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                Log.d(TAG,"onLoadingStarted");
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                Log.d(TAG,"onLoadingFailed");
            }
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Log.d(TAG,"onLoadingComplete");
                imageListeners.isLoaded(loadedImage);
            }
            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
    }

    public static void ImageLoaderLoad(final Context context, String url,ImageBitmapListeners imageListeners ){
        ImageLoader.getInstance().loadImage(url,new ImageLoadingListener(){
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                imageListeners.failed();
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                imageListeners.failed();
            }
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                imageListeners.isLoaded(loadedImage);
            }
            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                imageListeners.failed();
            }
        });
    }


    public interface ImageListeners{
        public void isLoaded(Bitmap bitmap);
    }

    public interface ImageBitmapListeners{
        public void isLoaded(Bitmap bitmap);
        public void failed();
    }

    public interface ImageMarkerListeners{
        public void isLoaded(Marker marker);
    }
    public interface MarkersLoaded{
        public void isLoadedAllMarkers(ArrayList<MarkerOptions> markers);
    }


}
