package grand.app.moon.utils.maputils.tracking;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.user.UserHelper;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.SharedPreferenceHelper;


public class FireRealTime {

    public FireRealTime(){
        updateFireToken();
    }

    public void updateFireToken() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(Constants.TOKENS_TBL);

        FireToken token = new FireToken(FirebaseInstanceId.getInstance().getToken());
        tokens.child(UserHelper.retrieveKey(Constants.TOKEN))
                .setValue(token);

    }

}
