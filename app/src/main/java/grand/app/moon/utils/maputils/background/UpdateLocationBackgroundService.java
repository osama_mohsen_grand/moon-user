package grand.app.moon.utils.maputils.background;

import java.util.ArrayList;

import grand.app.moon.models.location.UpdateLocation;
import grand.app.moon.utils.storage.location.LocationPosition;

public class UpdateLocationBackgroundService {

//    TripActionRepository tripActionRepository = new TripActionRepository(null);
    ArrayList<LocationPosition> arrayList = null;
    ArrayList<String> locations = null;
    public void updateLocations(String trip_id,ArrayList<LocationPosition> locations) {
        arrayList =  new ArrayList<>(locations);
        this.locations = new ArrayList<>();
        for(LocationPosition locationPosition : arrayList) {
            this.locations.add(locationPosition.getLat()+"");
            this.locations.add(locationPosition.getLng()+"");
        }
        UpdateLocation updateLocation = new UpdateLocation(trip_id, this.locations);
//        tripActionRepository.updateLocations(updateLocation);
    }
}
