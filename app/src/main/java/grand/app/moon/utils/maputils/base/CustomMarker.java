package grand.app.moon.utils.maputils.base;

import com.google.android.gms.maps.model.LatLng;

public class CustomMarker {
    public int id;
    public String name;
    public LatLng latLng;
    public String url;

    public CustomMarker(int id, String name, LatLng latLng, String url) {
        this.id = id;
        this.name = name;
        this.latLng = latLng;
        this.url = url;
    }
}
