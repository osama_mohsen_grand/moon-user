package grand.app.moon.utils.storage.user;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import grand.app.moon.models.country.CountriesResponse;
import grand.app.moon.models.user.login.Trip;
import grand.app.moon.models.user.profile.User;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.storage.SharedPreferenceHelper;


/**
 * Created by mohamedatef on 1/12/19.
 */

public class UserHelper {

    public static int getUserId() {
        return SharedPreferenceHelper.getSharedPreferenceInstance().getInt("id", -1);
    }

    public static void clearUserId() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putInt("id", -1);
        prefsEditor.commit();
    }

    public static void saveUserId() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putInt("id", 1);
        prefsEditor.commit();
    }


    public static void saveUserDetails(User userModel) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        prefsEditor.putString("userDetails", json);
        prefsEditor.putInt("id", userModel.id);
        Log.e("userDetails", json.toString());
        prefsEditor.commit();
    }

    public static void saveTripDetails(Trip trip) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(trip);
        prefsEditor.putString("tripDetails", json);
        Log.e("tripDetails", json.toString());
        prefsEditor.commit();
    }


    public static void saveCountries(CountriesResponse countriesResponse) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(countriesResponse);
        prefsEditor.putString("countriesResponse", json);
        Log.e("countriesResponse", json.toString());
        prefsEditor.commit();
    }


    public static CountriesResponse getCountries() {
        Gson gson = new Gson();
        String json = SharedPreferenceHelper.getSharedPreferenceInstance().getString("countriesResponse", "");
        CountriesResponse countriesResponse = gson.fromJson(json, CountriesResponse.class);
        if (countriesResponse == null) {
            return new CountriesResponse();
        }
        return countriesResponse;
    }


    public static Trip getTripDetails() {
        Gson gson = new Gson();
        String json = SharedPreferenceHelper.getSharedPreferenceInstance().getString("tripDetails", "");
        Trip trip = gson.fromJson(json, Trip.class);
        if (trip == null) {
            return new Trip();
        }
        return trip;
    }

    public static void clearUserDetails() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString("userDetails", null);
        prefsEditor.putInt("id", -1);
        prefsEditor.commit();
    }

    public static void clearTripDetails() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString("tripDetails", null);
        prefsEditor.commit();
    }


    public static void saveKey(String key, String value) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public static void saveCurrency(String value) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString(Constants.CURRENCY, value);
        prefsEditor.commit();
    }

    public static String retrieveCurrency() {
        SharedPreferences preferences = SharedPreferenceHelper.getSharedPreferenceInstance();
        if (preferences.getString(Constants.CURRENCY, "").length() > 0) {
            return preferences.getString(Constants.CURRENCY, "");
        } else {
            return "";
        }
    }


    public static String retrieveKey(String key) {
        SharedPreferences preferences = SharedPreferenceHelper.getSharedPreferenceInstance();
        if (preferences.getString(key, "").length() > 0) {
            return preferences.getString(key, "");
        } else {
            return "";
        }
    }

    public static double getLat() {
        String lat = retrieveKey(Constants.LAT);
        double lat_res = 0;
        if (!lat.equals("")) lat_res = Double.parseDouble(lat);
        return lat_res;
    }

    public static double getLng() {
        String lng = retrieveKey(Constants.LNG);
        double lng_res = 0;
        if (!lng.equals("")) lng_res = Double.parseDouble(lng);
        return lng_res;
    }


    public static User getUserDetails() {
        Gson gson = new Gson();
        String json = SharedPreferenceHelper.getSharedPreferenceInstance().getString("userDetails", "");
        User userItem = gson.fromJson(json, User.class);
        if (userItem == null) {
            return new User();
        }
        return userItem;
    }

    public static boolean isLogin() {
        SharedPreferences preferences = SharedPreferenceHelper.getSharedPreferenceInstance();
        return preferences.getInt("id", -1) != -1;
    }
}
