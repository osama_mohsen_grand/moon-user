package grand.app.moon.utils.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moon.R;
import grand.app.moon.adapter.FilterAdapter;
import grand.app.moon.databinding.CustomFilterBinding;
import grand.app.moon.databinding.CustomScheduleRideBinding;
import grand.app.moon.models.album.ImageVideo;
import grand.app.moon.models.base.IdNameDescription;
import grand.app.moon.utils.AppUtils;
import grand.app.moon.utils.Constants;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.viewmodels.schedule.ScheduleViewModel;

public class BottomSheetDialogHelper {

    Activity activity;
    public String date_server = "";
    public String date_select = "";
    public String time_server = "";
    public BottomSheetDialog calenderDialog = null;

    private static final String TAG = "BottomSheetDialogHelper";

    public BottomSheetDialogHelper(Activity activity) {
        this.activity = activity;
    }

    public BottomSheetDialog filterDialog = null;
    CustomFilterBinding customFilterBinding = null;
    FilterAdapter filterAdapter;
    List<IdNameDescription> AllResult,dataSearch;

    public void initRecyclerViewStaggered(){
//        customFilterBinding.rvFilterAds.setHasFixedSize(true);
//        customFilterBinding.rvFilterAds.setItemViewCacheSize(30);
//        customFilterBinding.rvFilterAds.setDrawingCacheEnabled(true);
//        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
//        customFilterBinding.rvFilterAds.setLayoutManager(staggeredGridLayoutManager);
//        customFilterBinding.rvFilterAds.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        AppUtils.initHorizontalRV(customFilterBinding.rvFilterAds, customFilterBinding.rvFilterAds.getContext(), 1);
    }
    public void filterFamousShopAds(List<IdNameDescription> data,boolean allowMultipleSelected, DialogHelperSelectedInterface dialogHelperInterface) {

        if (filterDialog == null) {
            AllResult = new ArrayList<>(data);
            LayoutInflater layoutInflater = LayoutInflater.from(activity);
            customFilterBinding = DataBindingUtil.inflate(layoutInflater, R.layout.custom_filter, null, true);
            customFilterBinding.search.setActivated(true);
            customFilterBinding.search.setQueryHint(ResourceManager.getString(R.string.search_hint));
            customFilterBinding.search.onActionViewExpanded();
            customFilterBinding.search.setIconified(false);
            customFilterBinding.search.clearFocus();

            customFilterBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    if (AllResult != null) {
                        if (!s.equals("")) {
                            dataSearch = new ArrayList<>();
                            for (IdNameDescription item : AllResult) {
                                if (item.name.contains(s))
                                    dataSearch.add(item);
                            }
                        } else {
                            dataSearch = new ArrayList<>(AllResult);
                        }
                        filterAdapter.update(dataSearch);
                    }
                    return false;
                }
            });

            initRecyclerViewStaggered();
            View customView = customFilterBinding.getRoot();
            filterDialog = new BottomSheetDialog(activity);
            filterAdapter = new FilterAdapter(data,allowMultipleSelected);
//            filterAdapter.mMutableLiveData.observeForever(new Observer<Object>() {
//                @Override
//                public void onChanged(@Nullable Object o) {
//                    dialogHelperInterface.onClickListener(filterDialog, Constants.,o);
//                }
//            });

            customFilterBinding.tvReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterAdapter.reset();
                }
            });
            customFilterBinding.imgDialogClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterDialog.dismiss();
                }
            });

            customFilterBinding.tvFilterSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(filterAdapter.selected.size() == 0)
                        Toast.makeText(activity, ""+activity.getString(R.string.please_select_at_least_one_filter), Toast.LENGTH_SHORT).show();
                    else{
                        filterDialog.dismiss();
                        dialogHelperInterface.onClickListener(filterDialog,Constants.ARRAY,filterAdapter.selected);
                    }
                }
            });
            filterDialog.setContentView(customView);


            customFilterBinding.rvFilterAds.setAdapter(filterAdapter);
        }
        filterDialog.show();
    }


    public void showScheduleCalendar(DialogHelperInterface dialogHelperInterface) {
        if (calenderDialog == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(activity);
            final CustomScheduleRideBinding customScheduleRideBinding = DataBindingUtil.inflate(layoutInflater, R.layout.custom_schedule_ride, null, true);
            ScheduleViewModel scheduleViewModel = new ScheduleViewModel();
            scheduleViewModel.mMutableScheduleLiveData.observe((LifecycleOwner) activity, new Observer<Object>() {
                @Override
                public void onChanged(@Nullable Object o) {
                    String action = (String) o;
                    if (action.equals(Constants.SCHEDULE)) {
                        date_select = ScheduleViewModel.date_select;
                        date_server = ScheduleViewModel.date_server;
                        time_server = ScheduleViewModel.time_server;
                        dialogHelperInterface.OnClickListenerContinue(calenderDialog, null);
                        scheduleViewModel.reset();
                        calenderDialog.dismiss();
                    }
                }
            });
            customScheduleRideBinding.setScheduleViewModel(scheduleViewModel);
            View customView = customScheduleRideBinding.getRoot();
            calenderDialog = new BottomSheetDialog(activity);
            calenderDialog.setContentView(customView);
            calenderDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    scheduleViewModel.reset();
                }
            });
        }
        calenderDialog.show();
    }

}
