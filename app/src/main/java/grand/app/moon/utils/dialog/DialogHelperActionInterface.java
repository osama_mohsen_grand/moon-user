package grand.app.moon.utils.dialog;

import android.app.Dialog;
import android.view.View;

/**
 * Created by osama on 1/7/2018.
 */

public interface DialogHelperActionInterface {
    void onSubmit(Dialog dialog, View view);
    void onCancel(Dialog dialog, View view);
}
