package grand.app.moon.utils;
import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.Calendar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moon.R;
import grand.app.moon.customviews.dialog.DialogConfirm;
import grand.app.moon.utils.dialog.DialogHelperInterface;
import grand.app.moon.utils.resources.ResourceManager;
import grand.app.moon.vollyutils.MyApplication;


public class AppUtils {

    public static final String appColor = "#020224";

    //here you can find function to must used tools in the application like start activity , make call , etc.....



    @SuppressLint("HardwareIds")
    public static String getDeviceIdWithoutPermission() {
        return Settings.Secure.getString(MyApplication.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    public static DatePickerDialog initCalender(Context context, DatePickerDialog.OnDateSetListener datePickerDialog){
        Calendar calendar = Calendar.getInstance();

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
//        DatePickerDialog dPickerDialog =  new DatePickerDialog(context, R.style.datepicker,datePickerDialog, year, month, day);
        DatePickerDialog dPickerDialog =  new DatePickerDialog(context, R.style.datepicker,datePickerDialog, year, month, day);
        dPickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        return dPickerDialog;
    }


    public static boolean isServiceRunning(Activity activity , Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static String dateConvert(int year , int month , int day){
        int month_select = month+1;
        String month_final = String.valueOf(month_select),day_final = String.valueOf(day);
        if(month_select < 10){

            month_final = "0" + month_final;
        }
        if(day < 10){
            day_final  = "0" + day_final ;
        }
        return String.valueOf(year) + "-" + String.valueOf(month_final) + "-" + String.valueOf(day_final);
    }

    public static String numberToDecimal(int number){
        String number2Decimal = String.valueOf(number);
        if(number < 10){
            number2Decimal  = "0" + number2Decimal ;
        }
        return number2Decimal;
    }


    public static void hideKeyboard(Context context,View view){
        InputMethodManager imm =(InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @SuppressLint("NewApi")
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    @SuppressLint("WrongConstant")
    public static void initVerticalRV(RecyclerView recyclerView, Context context, int spanCount) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new GridLayoutManager(context, spanCount, LinearLayoutManager.VERTICAL, false));
    }

    @SuppressLint("WrongConstant")
    public static void initHorizontalRV(RecyclerView recyclerView, Context context, int spanCount) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(30);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new GridLayoutManager(context, spanCount, LinearLayoutManager.HORIZONTAL, false));
    }

    public static void openPhone(Context context , String phone){
        Uri call = Uri.parse("tel:" + phone);
        Intent surf = new Intent(Intent.ACTION_DIAL, call);
        context.startActivity(surf);
    }



    public static void startWebPage(Context context, String page) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(page)));
        }
        catch (Exception e){
            e.getStackTrace();
        }
    }

    public static String getPlayStoreLink(Context context){
        String appPackageName = MyApplication.getInstance().getPackageName();
        return "https://play.google.com/store/apps/details?id="+appPackageName;
    }


    public static void openEmail(Context context , String textEmail,String subject , String message){
        try {
            if (Validate.isMail(textEmail)) {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{textEmail});
                email.putExtra(Intent.EXTRA_SUBJECT, subject);
                email.putExtra(Intent.EXTRA_TEXT, message);

                //need this to prompts email client only
                email.setType("message/rfc822");

                context.startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String currentTime() {
        Calendar currentTime    = Calendar.getInstance()                ;
        int hour                = currentTime.get(Calendar.HOUR_OF_DAY) ;
        int minute              = currentTime.get(Calendar.MINUTE)      ;
        return ""+hour+":"+minute;
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) MyApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static void pleaseLoginFirst(Context context , DialogHelperInterface dialogHelperInterface ) {
        new DialogConfirm(context)
                .setTitle(ResourceManager.getString(R.string.app_name))
                .setTitleTextSize(ResourceManager.getDimens(R.dimen.sp9))
                .setMessage(ResourceManager.getString(R.string.please_login_to_complete_this_action))
                .setMessageTextSize(ResourceManager.getDimens(R.dimen.sp7))
                .setActionText(ResourceManager.getString(R.string.label_login))
                .setActionTextSize(ResourceManager.getDimens(R.dimen.sp5))
                .setActionCancel(ResourceManager.getString(R.string.cancel))
                .setImage(R.mipmap.ic_launcher, ResourceManager.getDimens(R.dimen.dp70w), ResourceManager.getDimens(R.dimen.dp70h))
                .show(new DialogHelperInterface() {
                    @Override
                    public void OnClickListenerContinue(Dialog dialog, View view) {
                        dialogHelperInterface.OnClickListenerContinue(dialog,view);
                    }
                });
    }

    public static boolean isVisibleShipping(int type,int flag){
        if(type == 1 && flag == 2) return false;
        else if(type == 2 && flag == 2) return false;
        else if(type == 3 && flag <= 2) return false;
        return true;
    }

    public static String getNumberForDate(int number) {
        String date = ""+number;
        if(number < 10){
            date = "0"+number;
        }
        return date;
    }

    public static void openBrowser(Context context, String website) {
        try {
            if (Validate.isUrl(website)) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
                context.startActivity(browserIntent);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void openWhatsApp(Context context, String phone) {
        try {
            String url = "https://api.whatsapp.com/send?phone="+phone;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        } catch (Exception e) {
            Log.e("exc",e.getMessage().toString());
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.whatsapp&hl=en"));
            context.startActivity(browserIntent);
        }
    }
}
